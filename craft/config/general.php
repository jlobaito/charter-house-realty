<?php

/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/general.php
 */

return array(
    '*' => array(
        'cpTrigger' => 'backstage',
        'omitScriptNameInUrls' => true,
        'enableCsrfProtection' => true,
        'csrfTokenName' => 'CSRF',
    ),
    'local.charterhouse.com' => array(
        'devMode' => true,
        'siteUrl' => 'http://local.charterhouse.com/',
        'environmentVariables' => array(
            'basePath' => $_SERVER['DOCUMENT_ROOT'] . '/',
            'baseUrl' => 'http://local.charterhouse.com/',
        )
    ),
    'charterhouseiowa.com' => array(
        'siteUrl' => 'http://www.charterhouseiowa.com/',
        'environmentVariables' => array(
            'basePath' => $_SERVER['DOCUMENT_ROOT'] . '/',
            'baseUrl' => 'http://www.charterhouseiowa.com/'
        )
    ),
    'charterhouse.beardedgingerdesigns.com' => array(
        'siteUrl' => 'http://charterhouse.beardedgingerdesigns.com',
        'environmentVariables' => array(
            'basePath' => $_SERVER['DOCUMENT_ROOT'] . '/',
            'baseUrl' => 'http://charterhouse.beardedgingerdesigns.com/'
        )
    )
);
