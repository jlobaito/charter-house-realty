<?php
namespace Craft;

class ReservationPlugin extends BasePlugin
{
    public function getName()
    {
        return Craft::t('Reservation');
    }

    public function getVersion()
    {
        return '1.0';
    }

    public function getDescription()
    {
        return 'Easily manage moving truck reservations.';
    }

    public function getDeveloper()
    {
        return 'Damon Gentry';
    }

    public function getDeveloperUrl()
    {
        return 'http://www.damonjentree.me';
    }

    /**
     * Plugin settings that can be updated via the CP.
     *
     * @return array
     *
     */
    protected function defineSettings()
    {
        return array(
            'toEmail' => array(AttributeType::String, 'required' => true)
        );
    }

    /**
     * Settings html template.
     *
     * @return string
     */
    public function getSettingsHtml()
    {
        return craft()->templates->render('reservation/_settings', array(
            'settings' => $this->getSettings()
        ));
    }

    /**
     * Tells the system we need a page in the CP for our plugin.
     *
     * @return bool
     *
     */
    public function hasCpSection()
    {
        return false;
    }

    public function init()
    {
        /**
         * Event listener for saving a reservation entry.
         * If the reservation is confirmed, send an email to the customer.
         *
         */
        craft()->on('entries.onSaveEntry', function (Event $event) {
            if (craft()->request->isSiteRequest()) {
                // No worries; entry is getting saved from the CP.
            } else {
                $entry = $event->params['entry'];

                if ($entry->section == 'Truck Reservations') {
                    if ($entry->electronicSignature == '') {
                        if ($entry->reservationStatus) {

                            $confirmUrl = UrlHelper::getSiteUrl('reservation/confirmation/' . $entry->slug);
                            $email = new EmailModel();

                            $email->fromEmail = 'noreply@charterhouseiowa.com';
                            $email->replyTo = 'noreply@charterhouseiowa.com';
                            $email->sender = 'noreply@charterhouseiowa.com';
                            $email->fromName = 'Charter House Real Estate';

                            $email->toEmail = $entry->email;
                            $email->subject = 'Truck Reservation | Charter House Real Estate';
                            $email->body = '<p>Hi ' . $entry->firstName . ' &mdash; good news! Your reservation has been confirmed.<br><a href="' . $confirmUrl . '">Use this link to complete your reservation.</a></p><p>Regards,<br>The friendly folks at Charter House</p>';

                            craft()->email->sendEmail($email);
                        }
                    } else {
                        if ($entry->reservationStatus) {
                            $settings = craft()->plugins->getPlugin('reservation')->getSettings();

                            $toEmails = ArrayHelper::stringToArray($settings->toEmail);

                            foreach ($toEmails as $toEmail) {
                                
                                $email = new EmailModel();

                                $email->fromEmail = 'noreply@charterhouseiowa.com';
                                $email->replyTo = 'noreply@charterhouseiowa.com';
                                $email->sender = 'noreply@charterhouseiowa.com';
                                $email->fromName = 'Charter House Real Estate';

                                $email->toEmail = $email->toEmail = $toEmail;
                                $email->subject = 'Truck Reservation | Charter House Real Estate';
                                $email->body = '<p><strong>' . $entry->firstName . '</strong> - Has completed the online signature process for a truck reservation</p>';

                                craft()->email->sendEmail($email);
                            }
                        }
                    }
                }
            }
        });
    }
}
