<?php
namespace Craft;

class ReservationController extends BaseController
{
    protected $allowAnonymous = true;

    /**
     * Checks if the reservation is available RIGHT. NOW.
     *
     * @throws HttpException
     *
     */
    public function actionAvailable()
    {
        $this->requireAjaxRequest();
        $this->returnJson($this->_returnCurrentStatus());
    }


    /**
     * Save a new reservation.
     *
     * @throws HttpException
     *
     */
    public function actionVerifyReservation()
    {
        if (craft()->request->isAjaxRequest()) {
            if (!craft()->request->isSiteRequest()) {
                throw new HttpException(404);
            }

            // check if the expire date is in the past -> return false

            $isBooked = [];

            $criteria = craft()->elements->getCriteria(ElementType::Entry);
            $criteria->section = 'truckReservations';
            $criteria->limit = null;
            $criteria->status = ['live', 'pending'];

            $entries = $criteria->find();

            if ($entries) {
                $requestedPostDate = date('Y-m-d', strtotime(craft()->request->getPost('reservationStartDate')));
                $requestedExpiryDate = date('Y-m-d', strtotime(craft()->request->getPost('reservationEndDate')));

//                error_log('requested start: ' . $requestedPostDate);
//                error_log('requested end: ' . $requestedExpiryDate);

                foreach ($criteria as $entry) {
//                    error_log('--------------------------');
//                    error_log(':: EXISTING RESERVATION ::');
//                    error_log('Name: ' . $entry->title);
//                    error_log('Start: ' . $entry->reservationStartDate);
//                    error_log('End: ' . $entry->reservationEndDate);
//                    error_log('--------------------------');

                    $existingPostDate = date('Y-m-d', strtotime($entry->reservationStartDate));
                    $existingExpiryDate = date('Y-m-d', strtotime($entry->reservationEndDate));

                    if (($existingPostDate <= $requestedExpiryDate) && ($existingExpiryDate >= $requestedPostDate)) {
                        array_push($isBooked, $entry);
                    }
                }

                if (count($isBooked) == 0) {
//                    error_log('NO CONFLICTS');
//                    error_log('BOOKING RESERVATION');

                    if ($this->_saveReservation()) {
//                        error_log('RESERVATION SUCCESS');
                        $result = [
                            'status' => 'success'
                        ];
                    } else {
//                        error_log('RESERVATION FAILED');
                        $result = [
                            'status' => 'fail'
                        ];
                    }
                } else {
//                    error_log('RESERVATION TIME UNAVAILABLE');
                    $result = [
                        'status' => 'unavailable'
                    ];
                }
            } else {
                if ($this->_saveReservation()) {
//                    error_log('RESERVATION SUCCESS');
                    $result = [
                        'status' => 'success'
                    ];
                } else {
//                    error_log('RESERVATION FAILED');
                    $result = [
                        'status' => 'fail'
                    ];
                }
            }
        }

        return $this->returnJson($result);
    }


    /**
     *
     */
    public function actionCompleteReservation()
    {
        $criteria = craft()->elements->getCriteria(ElementType::Entry);
        $criteria->section = 'truckReservations';
        $criteria->limit = null;
        $criteria->status = null;
        $entries = $criteria->find();

        if ($entries) {
            foreach ($criteria as $entry) {
                if ($entry->slug == craft()->request->segments[2]) {

                    // Get the current time zone from the system.
                    date_default_timezone_set(craft()->timezone);

                    // Get the current time
                    $time = date('H:i:s');

                    // Create the post dateTime
                    $postDate = date('Y-m-d', strtotime($time));
                    $postDateTime = $postDate . ' ' . $time;

                    $entry->setContentFromPost(array(
                        'agreeToRentalTerms' => array(
                            craft()->request->getPost('fields.agreeToRentalTerms'),
                        ),
                        'electronicSignature' => craft()->request->getPost('fields.electronicSignature'),
                        'signatureDate' => $postDateTime
                    ));

                    if (craft()->entries->saveEntry($entry)) {
                        $this->redirectToPostedUrl();
                    } else {
                        // Could not update entry.
                    }
                } else {
                    // No match.
                }
            }
        }
    }

    /**
     *
     * Create a new reservation (entry).
     *
     * @return bool
     *
     */
    private function _saveReservation()
    {
        // Get the current time zone from the system.
        date_default_timezone_set(craft()->timezone);

        // Get the current time
        $time = date('H:i:s');

        // Create the post dateTime
        $postDate = date('Y-m-d', strtotime(craft()->request->getPost('reservationStartDate')));
        $postDateTime = $postDate . ' ' . $time;

        // Create the expiry dateTime.
        $expireDate = date('Y-m-d', strtotime(craft()->request->getPost('reservationEndDate')));
        $expireDateTime = $expireDate . ' ' . $time;

        // Create the DateTime object to send to Craft.
        $postDate = new \DateTime($postDateTime);
        $expiryDate = new \DateTime($expireDateTime);

        // Put it all together.
        $entry = new EntryModel();

        $entry->sectionId = 3;
        $entry->typeId = 1;
        $entry->enabled = true;
        $entry->expiryDate = $expiryDate;
        $entry->slug = StringHelper::randomString(12);

        $entry->getContent()->title = craft()->request->getPost('firstName') . ' ' . craft()->request->getPost('lastName');

        $entry->setContentFromPost(array(
            'firstName' => craft()->request->getPost('firstName'),
            'lastName' => craft()->request->getPost('lastName'),
            'email' => craft()->request->getPost('email'),
            'phone' => craft()->request->getPost('phone'),
            'reservationStartDate' => date('Y-m-d', strtotime(craft()->request->getPost('reservationStartDate'))),
            'reservationEndDate' => date('Y-m-d', strtotime(craft()->request->getPost('reservationEndDate'))),
            'body' => craft()->request->getPost('comment')
        ));

        if (craft()->entries->saveEntry($entry)) {
            $settings = craft()->plugins->getPlugin('reservation')->getSettings();

            $toEmails = ArrayHelper::stringToArray($settings->toEmail);

            /**
             * Send an email to each address listed
             * in the plugin settings.
             */
            foreach ($toEmails as $toEmail) {
                $emailSettings = craft()->email->getSettings();

                $email = new EmailModel();

                $email->fromEmail = craft()->request->getPost('email');
                $email->replyTo = 'noreply@charterhouseiowa.com';
                $email->sender = $emailSettings['emailAddress'];
                $email->fromName = craft()->request->getPost('firstName') . ' ' . craft()->request->getPost('lastName');
                $email->toEmail = $toEmail;
                $email->subject = 'New truck reservation from Charter House Real Estate';

                $emailMessage = '<p>Hi there.<br>';
                $emailMessage .= 'There has been a new truck reservation!</p>';
                $emailMessage .= '<p><strong>Name:</strong> ' . craft()->request->getPost('firstName') . ' ' . craft()->request->getPost('lastName') . '<br>';
                $emailMessage .= '<strong>Email:</strong> ' . craft()->request->getPost('email') . '<br>';
                $emailMessage .= '</p>';

                if (craft()->request->getPost('comment') != '') {
                    $emailMessage .= '<p><strong>Comments:</strong><br>';
                    $emailMessage .= craft()->request->getPost('comment');
                    $emailMessage .= '</p>';
                } else {
                    $emailMessage .= '<p>Bummer, it looks like they didn\'t want to leave a message.</p>';
                }

                $email->body = $emailMessage;

                craft()->email->sendEmail($email);
            }

            /**
             * Send an email to the user.
             *
             */
            $email = new EmailModel();

            $email->fromEmail = 'noreply@charterhouseiowa.com';
            $email->replyTo = 'noreply@charterhouseiowa.com';
            $email->sender = 'noreply@charterhouseiowa.com';
            $email->fromName = 'Charter House Real Estate';
            $email->toEmail = craft()->request->getPost('email');
            $email->subject = 'Charter House Real Estate Truck Reservation';
            $email->body = '<p>Hi ' . craft()->request->getPost('firstName') . ' &ndash; thank you for reaching out to us!<br>We wanted to drop you a quick note that we\'ve received your information and we\'ll be in touch very soon!<p>Regards,<br>Charter House Real Estate</p>';

            if (craft()->email->sendEmail($email)) {
                return true;
            }
            return true;
        } else {
            Craft::log('Couldn’t save the entry "' . $entry->title . '"', LogLevel::Error);
            return false;
        }
    }

    /**
     *
     * Checks if the current date falls in-between
     * the postDate and an expiryDate of any existing reservation.
     *
     * @return bool
     *
     */
    private function _returnCurrentStatus()
    {
        $isBooked = [];

        $criteria = craft()->elements->getCriteria(ElementType::Entry);
        $criteria->section = 'truckReservations';
        $criteria->limit = null;
        $criteria->status = null;
        $entries = $criteria->find();

        if ($entries) {
            foreach ($criteria as $entry) {
                $today = date('Y - m - d', strtotime(date('Y - m - d')));
                $postDate = date('Y - m - d', strtotime($entry->reservationStartDate));
                $expiryDate = date('Y - m - d', strtotime($entry->reservationEndDate));

                if ($today >= $postDate) {
//                    error_log('today is after or equal to the post date');
//                    error_log('truck is unavailable');
                    array_push($isBooked, $entry);

                    if ($today <= $expiryDate) {
//                        error_log('today is before or equal to the expire date');
//                        error_log('truck is unavailable');
                        array_push($isBooked, $entry);
                    }
                }
            }
        }

        if (count($isBooked) == 0) {
//            error_log('current status: truck is available right now');
            $result = [
                'status' => 'available'
            ];
        } else {
//            error_log('current status: truck is unavailable right now');
            $result = [
                'status' => 'unavailable'
            ];
        }

        return $result;
    }
}