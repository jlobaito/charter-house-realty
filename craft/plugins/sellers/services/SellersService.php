<?php
namespace Craft;

/**
 * Sellers service.
 *
 * Provides an API for the plugin to access the database.
 *
 * @package Craft
 *
 */
class SellersService extends BaseApplicationComponent
{
    /**
     * Save a new seller.
     *
     * @param SellersModel $seller
     * @throws Exception
     *
     */
    public function saveSeller(SellersModel $seller)
    {
        $settings = craft()->plugins->getPlugin('sellers')->getSettings();

        if (!$settings->toEmail) {
            throw new Exception('The "To Email" address is not set on the plugin\'s settings page.');
        }

        $record = new SellersRecord();
        $record->setAttributes($seller->getAttributes());

        if ($record->save()) {
            $toEmails = ArrayHelper::stringToArray($settings->toEmail);

            /**
             * Send an email to each address listed
             * in the plugin settings.
             */
            foreach ($toEmails as $toEmail) {
                $emailSettings = craft()->email->getSettings();

                $email = new EmailModel();

                $email->fromEmail = $emailSettings['emailAddress'];
                $email->replyTo = $seller->email;
                $email->sender = $emailSettings['emailAddress'];
                $email->fromName = $seller->firstName;
                $email->toEmail = $toEmail;
                $email->subject = $settings->adminSubject;

                $emailMessage = '<p>Hi there.<br>';
                $emailMessage .= 'There has been a new sellers form submission!</p>';
                $emailMessage .= '<p><strong>Name:</strong> ' . $seller->firstName . '' . $seller->lastName . '<br>';
                $emailMessage .= '<strong>Email:</strong> ' . $seller->email . '<br>';
                $emailMessage .= '<strong>Plan:</strong> ' . $seller->plan;
                $emailMessage .= '</p>';

                $emailMessage .= '<p><small>P.S. You can find all of this information in the control panel as well.</small></p>';

                $email->body = $emailMessage;

                craft()->email->sendEmail($email);
            }

            /**
             * Send an email to the user.
             *
             */
            $email = new EmailModel();

            $email->fromEmail = $emailSettings['emailAddress'];
            $email->replyTo = 'noreply@charterhouseiowa.com';
            $email->sender = $emailSettings['emailAddress'];
            $email->fromName = $emailSettings['senderName'];
            $email->toEmail = $seller->email;
            $email->subject = $settings->guestSubject;
            $email->body = craft()->templates->renderString($settings->welcomeEmailMessage, array('firstName' => $seller->firstName));

            if (craft()->email->sendEmail($email)) {
                return true;
            }

        } else {
            $seller->addErrors($record->getErrors());

            return false;
        }
    }

    /**
     * Return a list of all sellers.
     *
     * @return array|\CDbDataReader
     *
     */
    public function getSellers()
    {
        return SellersRecord::model()->findAll(array('order' => 'dateCreated desc'));
    }

    /**
     * Delete the given seller.
     *
     * @param $sellerId
     * @return bool
     *
     */
    public function deleteSeller($sellerId)
    {
        return SellersRecord::model()->deleteByPk($sellerId);
    }

    /**
     * Get any given seller by Id.
     * @param {string} $sellerId | Seller ID.
     * @return {object}
     *
     */
    public function getSeller($sellerId)
    {
        return SellersRecord::model()->findByPk($sellerId);
    }
}