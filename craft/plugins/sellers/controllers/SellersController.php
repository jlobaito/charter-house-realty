<?php
namespace Craft;

/**
 * Sellers controller.
 *
 * Defines actions which can be posted to by forms within the templates.
 *
 * @package Craft
 *
 */
class SellersController extends BaseController
{
    protected $allowAnonymous = true;

    public function actionSaveSeller()
    {
        $this->requireAjaxRequest();

        $seller = new SellersModel();

        $seller->firstName = craft()->request->getPost('firstName');
        $seller->lastName = craft()->request->getPost('lastName');
        $seller->email = craft()->request->getPost('email');
        $seller->phone = craft()->request->getPost('phone');
        $seller->plan = craft()->request->getPost('plan');

        if ($seller->validate()) {
            if (craft()->sellers->saveSeller($seller)) {

                $this->returnJson(array('success' => true));
            } else {
                $this->returnJson(array('success' => false));
            }
        } else {
            if (craft()->request->isAjaxRequest()) {
                return $this->returnErrorJson($seller->getErrors());
            } else {
                craft()->urlManager->setRouteVariables(array(
                    'seller' => $seller
                ));

                return $seller->getErrors();
            }
        }
    }


    /**
     * List all contacts within the CP.
     *
     * @return void
     *
     */
    public function actionListSellers()
    {
        $vars['sellers'] = craft()->seller->listSellers();

        $this->renderTemplate('sellers/index', $vars);
    }

    /**
     * Delete the given seller.
     *
     * @throws HttpException
     *
     */
    public function actionDeleteSeller()
    {
        $this->requireAjaxRequest();
        $sellerId = craft()->request->getPost('id');

        if (craft()->sellers->deleteSeller($sellerId)) {
            $response = array('status' => 'success', 'comment' => null);
        } else {
            $response = array('status' => 'fail', 'comment' => 'Could not delete.');
        }

        header('Content-Type: application/json');

        echo json_encode($response);
    }
}