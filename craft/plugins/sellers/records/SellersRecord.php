<?php
namespace Craft;

/**
 * Class Lead record.
 *
 * Provides a definition of the database tables for this plugin.
 * This class should only be called by the service layer.
 *
 * @package Craft
 *
 */
class SellersRecord extends BaseRecord
{
    /**
     * Return the name of the database table for this plugin.
     *
     * @return string
     *
     */
    public function getTableName()
    {
        return 'sellers';
    }

    /**
     * Define the table attributes for this plugin.
     *
     * @return array
     *
     */
    public function defineAttributes()
    {
        return array(
            'firstName' => array(AttributeType::Name, 'default' => null),
            'lastName' => array(AttributeType::Name, 'default' => null),
            'email' => array(AttributeType::Email, 'default' => null),
            'phone' => array(AttributeType::Name, 'default' => null),
            'plan' => array(AttributeType::Name, 'default' => null)
        );
    }
}