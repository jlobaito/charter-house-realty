<?php
namespace Craft;

/**
 * Class SellerVariable
 * @package Craft
 */
class SellersVariable
{
    /**
     * Return a list of all sellers.
     *
     * @return array|\CDbDataReader
     *
     */
    public function getSellers()
    {
        return craft()->sellers->getSellers();
    }

    /**
     * Get any given lead.
     *
     * @param {string} $leadId | The lead ID.
     * @returns {object}
     *
     */
    public function getSeller($leadId)
    {
        return craft()->sellers->getSeller($leadId);
    }
}