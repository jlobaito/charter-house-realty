<?php
namespace Craft;

/**
 * ListingInquiries model.
 *
 * Provides a read-only data object representing a inquiry.
 * Data is returned by the service.
 *
 * @package Craft
 *
 */
class ListingInquiriesModel extends BaseModel
{
    /**
     * Define the data attributes for each record (row in the database).
     *
     * @return array
     *
     */
    public function defineAttributes()
    {
        return array(
            'firstName' => array(
                'type' => AttributeType::Name,
                'required' => true,
                'maxLength' => 100,
            ),
            'lastName' => array(
                'type' => AttributeType::Name,
                'required' => true,
                'maxLength' => 100,
            ),
            'email' => array(
                'type' => AttributeType::Email,
                'required' => true,
                'maxLength' => 100,
            ),
            'phone' => array(
                'type' => AttributeType::Name,
                'required' => true,
                'maxLength' => 20,
            ),
            'mlsNumber' => array(
                'type' => AttributeType::Name,
                'required' => true,
                'maxLength' => 100,
            ),
            'comment' => array(
                'type' => AttributeType::Template
            )
        );
    }
}