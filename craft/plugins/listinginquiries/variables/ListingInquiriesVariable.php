<?php
namespace Craft;

/**
 * Class LeadVariable
 * @package Craft
 */
class ListingInquiriesVariable
{
    /**
     * Return a list of all leads.
     *
     * @return array|\CDbDataReader
     *
     */
    public function getInquiries()
    {
        return craft()->listingInquiries->getInquiries();
    }

    /**
     * Get any given lead.
     *
     * @param {string} $leadId | The lead ID.
     * @returns {object}
     *
     */
    public function getInquiry($leadId)
    {
        return craft()->listingInquiries->getInquiry($leadId);
    }
}