<?php
namespace Craft;

/**
 * ListingInquiries service.
 *
 * Provides an API for the plugin to access the database.
 *
 * @package Craft
 *
 */
class ListingInquiriesService extends BaseApplicationComponent
{
    /**
     * Save a new lead.
     *
     * @param ListingInquiries $inquiry
     * @throws Exception
     *
     */
    public function saveInquiry(ListingInquiriesModel $inquiry)
    {
        $settings = craft()->plugins->getPlugin('listinginquiries')->getSettings();

        if (!$settings->toEmail) {
            throw new Exception('The "To Email" address is not set on the plugin\'s settings page.');
        }

        $record = new ListingInquiriesRecord();
        $record->setAttributes($inquiry->getAttributes());

        if ($record->save()) {
            $toEmails = ArrayHelper::stringToArray($settings->toEmail);

            /**
             * Send an email to each address listed
             * in the plugin settings.
             */
            foreach ($toEmails as $toEmail) {
                $emailSettings = craft()->email->getSettings();

                $email = new EmailModel();

                $email->fromEmail = $emailSettings['emailAddress'];
                $email->replyTo = $inquiry->email;
                $email->sender = $emailSettings['emailAddress'];
                $email->fromName = $inquiry->firstName.' '.$inquiry->lastName;
                $email->toEmail = $toEmail;
                $email->subject = $settings->adminSubject;

                $emailMessage = '<p>Hi there.<br>';
                $emailMessage .= 'There has been a new contact form submission!</p>';
                $emailMessage .= '<p><strong>Name:</strong> ' . $inquiry->firstName . ' ' . $inquiry->lastName . '<br>';
                $emailMessage .= '<strong>Email:</strong> ' . $inquiry->email . '<br>';
                $emailMessage .= '<strong>Phone:</strong> ' . $inquiry->phone . '<br>';
                $emailMessage .= '<strong>MLS#:</strong> ' . $inquiry->mlsNumber;
                $emailMessage .= '</p>';

                if ($inquiry->comment != '') {
                    $emailMessage .= '<p><strong>Comments:</strong><br>';
                    $emailMessage .= $inquiry->comment;
                    $emailMessage .= '</p>';
                } else {
                    $emailMessage .= '<p>Bummer, it looks like they didn\'t want to leave a message.</p>';
                }

                $emailMessage .= '<p><small>P.S. You can find all of this information in the control panel as well.</small></p>';

                $email->body = $emailMessage;

                craft()->email->sendEmail($email);
            }

            /**
             * Send an email to the user.
             *
             */
            $email = new EmailModel();

            $email->fromEmail = $emailSettings['emailAddress'];
            $email->replyTo = 'noreply@charterhouseiowa.com';
            $email->sender = $emailSettings['emailAddress'];
            $email->fromName = $emailSettings['senderName'];
            $email->toEmail = $inquiry->email;
            $email->subject = $settings->guestSubject;
            $email->body = craft()->templates->renderString($settings->welcomeEmailMessage, array('firstName' => $inquiry->firstName));

            if (craft()->email->sendEmail($email)) {
                return true;
            }
        } else {
            $inquiry->addErrors($record->getErrors());

            return false;
        }
    }

    /**
     * Return a list of all inquiries.
     *
     * @return array|\CDbDataReader
     *
     */
    public function getInquiries()
    {
        return ListingInquiriesRecord::model()->findAll(array('order' => 'dateCreated desc'));
    }

    /**
     * Delete the given inquiry.
     *
     * @param $inquiryId
     * @return bool
     *
     */
    public function deleteInquiry($inquiryId)
    {
        return ListingInquiriesRecord::model()->deleteByPk($inquiryId);
    }

    /**
     * Get any given inquiry by Id.
     * @param {string} $inquiryId | inquiry ID.
     * @return {object}
     *
     */
    public function getInquiry($inquiryId)
    {
        return ListingInquiriesRecord::model()->findByPk($inquiryId);
    }
}