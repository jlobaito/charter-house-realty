<?php
namespace Craft;

/**
 * Class ListingInquiriesPlugin
 *
 * @package Craft
 */
class ListingInquiriesPlugin extends BasePlugin
{
    /**
     * Returns the name of the plugin.
     *
     * @return string
     *
     */
    function getName()
    {
        return Craft::t('Listing Inquiries');
    }

    /**
     * Returns the version of the plugin.
     *
     * @return string
     *
     */
    function getVersion()
    {
        return '1.0';
    }

    /**
     * Returns a description of this plugin.
     *
     * @return null|string
     */
    public function getDescription()
    {
        return Craft::t('A convenient way to manage people who have filled out an inquiry about a listing.');
    }

    /**
     * Returns the developer(s) for this plugin.
     *
     * @return string
     *
     */
    function getDeveloper()
    {
        return 'Damon Gentry';
    }

    /**
     * Returns the developers url.
     *
     * @return string
     *
     */
    function getDeveloperUrl()
    {
        return 'http://www.damonjentree.me';
    }

    /**
     * Plugin settings that can be updated via the CP.
     *
     * @return array
     *
     */
    protected function defineSettings()
    {
        return array(
            'toEmail' => array(AttributeType::String, 'required' => true),
            'adminSubject' => array(AttributeType::String, 'required' => true),
            'guestSubject' => array(AttributeType::String, 'required' => true),
            'welcomeEmailMessage' => array(AttributeType::String, 'required' => true)
        );
    }

    /**
     * Settings html template.
     *
     * @return string
     */
    public function getSettingsHtml()
    {
        return craft()->templates->render('listinginquiries/_settings', array(
            'settings' => $this->getSettings()
        ));
    }

    /**
     * Tells the system we need a page in the CP for our plugin.
     *
     * @return bool
     *
     */
    public function hasCpSection()
    {
        return true;
    }

    /**
     * Register some CP routes.
     *
     * @return array
     *
     */
    public function registerCpRoutes()
    {
        return array(
            'listinginquiries/delete' => array('action' => 'listingInquiries/deleteInquiry'),
            'listinginquiries/inquiry/(?P<contactId>\d+)' => 'listingInquiries/_details'
        );
    }

    /**
     * Tell the CP we need to load some js/css assets.
     *
     */
    public function init()
    {
        if (craft()->request->isCpRequest() && craft()->userSession->isLoggedIn()) {
            craft()->templates->includeJsResource('listinginquiries/js/inquiries.js');
        }
    }
}