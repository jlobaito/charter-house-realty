<?php
namespace Craft;

/**
 * Class ListingInquiries record.
 *
 * Provides a definition of the database tables for this plugin.
 * This class should only be called by the service layer.
 *
 * @package Craft
 *
 */
class ListingInquiriesRecord extends BaseRecord
{
    /**
     * Return the name of the database table for this plugin.
     *
     * @return string
     *
     */
    public function getTableName()
    {
        return 'inquiries';
    }

    /**
     * Define the table attributes for this plugin.
     *
     * @return array
     *
     */
    public function defineAttributes()
    {
        return array(
            'firstName' => array(AttributeType::Name, 'default' => null),
            'lastName' => array(AttributeType::Name, 'default' => null),
            'email' => array(AttributeType::Email, 'default' => null),
            'phone' => array(AttributeType::Name, 'default' => null),
            'mlsNumber' => array(AttributeType::Name, 'default' => null),
            'comment' => array(AttributeType::Name, 'default' => null)
        );
    }
}