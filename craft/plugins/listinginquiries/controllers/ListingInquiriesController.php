<?php
namespace Craft;

/**
 * ListingInquiries controller.
 *
 * Defines actions which can be posted to by forms within the templates.
 *
 * @package Craft
 *
 */
class ListingInquiriesController extends BaseController
{
    protected $allowAnonymous = true;

    public function actionSaveInquiry()
    {
        $this->requireAjaxRequest();

        $inquiry = new ListingInquiriesModel();

        $inquiry->firstName = craft()->request->getPost('firstName');
        $inquiry->lastName = craft()->request->getPost('lastName');
        $inquiry->email = craft()->request->getPost('email');
        $inquiry->phone = craft()->request->getPost('phone');
        $inquiry->mlsNumber = craft()->request->getPost('mlsNumber');
        $inquiry->comment = craft()->request->getPost('comment');

        if ($inquiry->validate()) {
            if (craft()->listingInquiries->saveInquiry($inquiry)) {
                $this->returnJson(array('success' => true));
            } else {
                $this->returnJson(array('success' => false));
            }
        } else {
            if (craft()->request->isAjaxRequest()) {
                return $this->returnErrorJson($inquiry->getErrors());
            } else {
                craft()->urlManager->setRouteVariables(array(
                    'inquiry' => $inquiry
                ));

                return $inquiry->getErrors();
            }
        }
    }


    /**
     * List all inquiries within the CP.
     *
     * @return void
     *
     */
    public function actionListInquiries()
    {
        $vars['listinginquiries'] = craft()->inquiry->listInquiries();

        $this->renderTemplate('listinginquiries/index', $vars);
    }

    /**
     * Delete the given inquiry.
     *
     * @throws HttpException
     *
     */
    public function actionDeleteInquiry()
    {
        $this->requireAjaxRequest();
        $inquiryId = craft()->request->getPost('id');

        if (craft()->listingInquiries->deleteInquiry($inquiryId)) {
            $response = array('status' => 'success', 'comment' => null);
        } else {
            $response = array('status' => 'fail', 'comment' => 'Could not delete.');
        }

        header('Content-Type: application/json');

        echo json_encode($response);
    }
}