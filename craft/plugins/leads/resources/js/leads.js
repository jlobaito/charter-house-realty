$(function () {
    /**
     * Event listener for delete button click(s).
     *
     * @param {object} event | The click event.
     * @returns {object}
     *
     */
    $('.delete-lead').click(function (event) {
        var $anchor = $(this);

        if (confirm('Are you sure you want to delete "' + event.currentTarget.dataset.name + '"?')) {
            var data = {
                'id': event.currentTarget.dataset.id
            };

            data[window.csrfTokenName] = window.csrfTokenValue;

            $.ajax({
                'type': 'post',
                'contentType': 'application/x-www-form-urlencoded; charset=UTF-8',
                'cache': false,
                'url': 'leads/delete',
                'data': data,
                'dataType': 'json',
                'timeout': 50000
            }).done(function (response) {
                switch (response.status) {
                    case 'success':
                        $anchor.closest('tr').remove();

                        if ($('#leadsTable tr').length === 1) {
                            $('#leadsTable').parent().parent().html('<p>Sadly, no one has filled out the lead form.</p>');
                        }

                        Craft.cp.displayNotice(Craft.t("Lead deleted."));
                        break;
                    case 'fail':
                        Craft.cp.displayError(Craft.t("Couldn't delete contact."));
                        break;
                    default:
                        Craft.cp.displayError(Craft.t("Couldn't delete contact."));
                        break;
                }
            }).fail(function (error) {
                Craft.cp.displayError(Craft.t("Couldn't delete contact."));
            });

        }
        else {
            // Do nothing.
        }

        event.preventDefault();
    });
});