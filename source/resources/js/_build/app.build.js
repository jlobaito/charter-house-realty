({
    'appDir': '../',
    'baseUrl': './',
    'dir': '../../../../web/resources/js',
    'mainConfigFile': '../app.config.js',
    'modules': [
        {
            'name': 'application'
        }
    ],
    'fileExclusionRegExp': /^(?:_build|(?:r|app.build)\.js)$/,
    'removeCombined': false,
    //'optimize': 'none',
    'paths': {
        'jquery': 'vendor/jquery.min',
        'jquery.validate': 'plugins/jquery.validate',
        'jquery.flexslider': 'plugins/jquery.flexslider',
        'jquery.fancybox': 'plugins/jquery.fancybox',
        'mixitup' : 'plugins/mixitup',
        'pricingtable' : 'plugins/pricingtable',
        'velocity': 'plugins/velocity.min',
        'domReady': 'plugins/domReady',
        'dragdealer' : 'plugins/dragdealer',
        'moment': 'plugins/moment',
        'bootstrap.transition': 'vendor/bootstrap/transition',
        'bootstrap.tab': 'vendor/bootstrap/tab',
        'bootstrap.collapse': 'vendor/bootstrap/collapse',
        'bootstrap.datetimepicker': 'plugins/bootstrap.datetimepicker'
    },
    'shim': {
        'jquery.validate': {
            'deps': ['jquery']
        },
        'jquery.flexslider': {
            'deps': ['jquery']
        },
        'jquery.fancybox': {
            'deps': ['jquery']
        },
        'mixitup': {
            'deps': ['jquery']
        },
        'pricingtable': {
            'deps': ['jquery']
        },
        'moment': {
            'deps': ['jquery']
        },
        'dragdealer': {
            'deps': ['jquery']
        },
        'bootstrap.transition': {
            'deps': ['jquery']
        },
        'bootstrap.collapse': {
            'deps': ['jquery']
        },
        'bootstrap.tab': {
            'deps': ['jquery', 'bootstrap.transition']
        },
        'bootstrap.datetimepicker': {
            'deps': ['jquery', 'moment', 'bootstrap.transition', 'bootstrap.collapse']
        }
    }
})