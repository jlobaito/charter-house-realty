/**
 * RequireJS configuration.
 *
 */
require.config({
    'baseUrl': '/resources/js/',
    'paths': {
        'jquery': 'vendor/jquery.min',
        'jquery.validate': 'plugins/jquery.validate',
        'jquery.flexslider': 'plugins/jquery.flexslider',
        'jquery.fancybox': 'plugins/jquery.fancybox',
        'moment': 'plugins/moment',
        'velocity': 'plugins/velocity.min',
        'domReady': 'plugins/domReady',
        'mixitup' : 'plugins/mixitup',
        'pricingtable' : 'plugins/pricingtable',
        'bootstrap.transition': 'vendor/bootstrap/transition',
        'bootstrap.tab': 'vendor/bootstrap/tab',
        'bootstrap.collapse': 'vendor/bootstrap/collapse',
        'bootstrap.datetimepicker': 'plugins/bootstrap.datetimepicker',
        'bootstrap.modal': 'vendor/bootstrap/modal',
        'cookie' : 'plugins/cookie',
        'dragdealer' : 'plugins/dragdealer'
    },
    'shim': {
        'jquery.validate': {
            'deps': ['jquery']
        },
        'jquery.flexslider': {
            'deps': ['jquery']
        },
        'jquery.fancybox': {
            'deps': ['jquery']
        },
        'mixitup': {
            'deps': ['jquery']
        },
        'pricingtable': {
            'deps': ['jquery']
        },
        'moment': {
            'deps': ['jquery']
        },
        'bootstrap.transition': {
            'deps': ['jquery']
        },
        'bootstrap.modal': {
            'deps': ['jquery']
        },
        'bootstrap.collapse': {
            'deps': ['jquery']
        },
        'bootstrap.button': {
            'deps': ['button']
        },
        'cookie': {
            'deps': ['jquery']
        },
        'dragdealer': {
            'deps': ['jquery']
        },
        'bootstrap.tab': {
            'deps': ['jquery', 'bootstrap.transition']
        },
        'bootstrap.datetimepicker': {
            'deps': ['jquery', 'moment', 'bootstrap.transition', 'bootstrap.collapse']
        }
    }
});

define([], function () {
    /**
     * Application initialization.
     *
     * @param {object} application | Main application module.
     * @returns {object}
     *
     */
    require(['application'], function (application) {
        application.init();
    });
});