'use strict';

/**
 * Reservation module.
 *
 * @param {function} $ | jQuery library
 * @param {object} notification | Notification module.
 * @param {object} prefs | System preferences module.
 * @returns {object}
 *
 */

define([
    'jquery',
    'modules/system/notification',
    'modules/system/prefs',
    'moment',
    'bootstrap.transition',
    'bootstrap.collapse',
    'bootstrap.datetimepicker',
    'velocity',
    'jquery.validate'
], function ($, notification, prefs) {
    /**
     * Initialization method.
     *
     * @returns {void}
     *
     */
    var init = function () {

        /**
         * Reservation start datetime picker.
         *
         */
        $('#reservationStartDate').datetimepicker({
            'format': 'L'
        });

        /**
         * Reservation end datetime picker.
         *
         */
        $('#reservationEndDate').datetimepicker({
            'format': 'L',
            'useCurrent': false
        });

        /**
         * Event listener for start date change.
         *
         * @param {object} event | The change event.
         *
         */
        $('#reservationStartDate').on('dp.change', function (event) {
            $('#reservationEndDate').data('DateTimePicker').minDate(event.date);
        });

        /**
         * Event listener for end date change.
         * Also makes sure the active date cant be past the end date.
         *
         * @param {object} event | The change event.
         *
         */
        $('#reservationEndDate').on('dp.change', function (event) {
            $('#reservationStartDate').data('DateTimePicker').maxDate(event.date);
        });

        if ($('#reservationForm').length) {
            var data = {
                'CSRF': $('input[name="CSRF"]').val()
            };

            $.ajax({
                'type': 'post',
                'contentType': 'application/x-www-form-urlencoded; charset=UTF-8',
                'cache': false,
                'data': data,
                'url': '/actions/reservation/available',
                'dataType': 'json',
                'timeout': 50000
            }).done(function (response) {
                switch (response.status) {
                    case 'available':
                        $('#truckStatus').html('<p>Awesome, the truck is waiting for you right now!</p>');
                        break;
                    case 'unavailable':
                        $('#truckStatus').html('<p>Sorry, the turck currently unavailable right now, try choosing a different date below.</p>');
                        break;
                    default:
                        break;
                }
            }).fail(function (error) {
                // Total fail.
            });
        }


        /**
         * Event listener for "fix error" clicks.
         * Link needs to be manually tied since button is created dynamically.
         *
         * @param {object} event | The click event.
         * @returns {void}
         *
         */
        $('body').on('click', '.fixReservationErrors', function (event) {
            $('#reservationStatusMessage').velocity('slideUp', {
                'duration': 300,
                'complete': function () {
                    $('#reservationForm').velocity('slideDown', {
                        'duration': 300
                    });

                    $('#reservationForm').velocity({
                        'opacity': 1
                    }, {
                        'duration': 200,
                        'complete': function () {
                            // Done.
                        }
                    });

                }
            });

            event.preventDefault();
        });

        /**
         * Reservation form validation rules and submit handler.
         *
         */
        $('#reservationForm').validate({
            'errorPlacement': function (error, element) {
                if (element[0].type === 'radio') {
                    error.insertAfter(element.parent().parent());
                }
                else {
                    error.insertAfter(element);
                }
            },
            'highlight': function (element, errorClass) {
                $(element).addClass(errorClass).velocity({'opacity': .3}).velocity({'opacity': 1});
            },
            'rules': {
                'reservationStartDate': 'required',
                'reservationEndDate': 'required',
                'firstName': 'required',
                'lastName': 'required',
                'email': {
                    'required': true,
                    'email': true
                }
            },
            'messages': {
                'reservationStartDate': notification.reservationStartDateRequired,
                'reservationEndDate': notification.reservationEndDateRequired,
                'firstName': notification.firstNameRequired,
                'lastName': notification.lastNameRequired,
                'email': {
                    'required': notification.emailRequired,
                    'email': notification.emailInvalid
                }
            },
            'focusInvalid': true,
            'errorClass': 'has-error',
            'validClass': 'has-success',
            'submitHandler': function (form) {
                $('#reservationStatusMessage').html('<div class="spinner"> <img src="/resources/img/default-ajax.svg" width="60" height="60" alt=""> </div><p class="text-center">Checking reservation...</p>');

                $('#reservationForm')
                    .velocity({
                        'opacity': 0
                    }, {
                        'duration': prefs.duration
                    })
                    .velocity('slideUp', {
                        'duration': prefs.duration,
                        'complete': function () {
                            $('#reservationStatus').velocity('slideDown', {
                                'duration': prefs.duration
                            }).velocity({'opacity': 1});

                            /**
                             * Get the base form module.
                             *
                             * @param {object} form | Base form module.
                             * @returns {object}
                             *
                             */
                            require(['modules/system/form'], function (form) {
                                onTruckReservationFormSubmit(function (response) {
                                    $('#reservationStatusMessage').velocity('slideUp', {
                                        'duration': prefs.duration,
                                        'complete': function () {
                                            switch (response.status) {
                                                case 'success':
                                                    $('#reservationStatusMessage').html('<h2 class="has-success"><span class="fa-stack fa-lg"><i class="fa fa-circle-o fa-stack-2x"></i> <i class="fa fa-check fa-stack-1x"></i> </span><br> Thanks!</h2><p>We\'ve received your request&mdash;we\'ll be in touch soon!</p>')
                                                        .velocity('slideDown', {
                                                            'duration': prefs.duration
                                                        });
                                                    break;
                                                case 'fail':
                                                    $('#reservationStatusMessage').html('<h2 class="has-error"><span class="fa-stack fa-lg"><i class="fa fa-circle-o fa-stack-2x"></i> <i class="fa fa-exclamation fa-stack-1x"></i> </span><br>Woah!</h2><p>It looks like you missed something.<br><a href="javascript;" class="fixReservationErrors">Go back and check it out.</a></h2>')
                                                        .velocity({'opacity': 1}, {
                                                            'duration': prefs.duration
                                                        });

                                                    $.each(response.error, function (key, val) {
                                                        form.renderFeedback('error', $('#' + key), val);
                                                    });
                                                    break;
                                                case 'unavailable':
                                                    $('#reservationStatusMessage').html('<h2 class="has-error"><span class="fa-stack fa-lg"><i class="fa fa-circle-o fa-stack-2x"></i> <i class="fa fa-exclamation fa-stack-1x"></i> </span><br>Woah!</h2><p>It looks someone beat you to it.<br><a href="javascript;" class="fixReservationErrors">Go back and pick another date.</a></h2>')
                                                        .velocity('slideDown', {
                                                            'duration': prefs.duration
                                                        });
                                                    break;
                                                default :
                                                    $('#reservationStatusMessage').html('<h2 class="has-error"><span class="fa-stack fa-lg"><i class="fa fa-circle-o fa-stack-2x"></i> <i class="fa fa-exclamation fa-stack-1x"></i> </span><br>System Error</h2><p>Please try again later.</p>')
                                                        .velocity('slideDown', {
                                                            'duration': prefs.duration
                                                        });
                                                    break;
                                            }
                                        }
                                    });
                                });
                            });
                        }
                    });
            }
        });
    };

    /**
     * Handle reservation form data.
     *
     * @param {callback}
     * @retuns {void}
     *
     */
    function onTruckReservationFormSubmit(callback) {
        $.ajax({
            'type': 'post',
            'contentType': 'application/x-www-form-urlencoded; charset=UTF-8',
            'cache': false,
            'data': $('#reservationForm').serialize(),
            'url': '/actions/reservation/verifyReservation',
            'dataType': 'json',
            'timeout': 50000
        }).done(function (response) {
            callback(response);
        }).fail(function (error) {
            // Total fail.
        });
    }

    return {
        'init': init
    };
});