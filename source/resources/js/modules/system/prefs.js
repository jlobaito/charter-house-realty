'use strict';

/**
 * Application prefrences.
 * A single place for application prefrences/constants.
 *
 * @returns {object}
 *
 */
define([], function () {
    return {
        'duration': 200 // Default transition duration (miliseconds) for the velocity plugin.
    }
});