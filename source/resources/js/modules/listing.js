'use strict';

/**
 * Primary application module.
 *
 * @param {function} $ | jQuery library
 * @param {object} notification | Notification module.
 * @param {object} prefs | System preferences module.
 * @returns {object}
 *
 */

define([
    'jquery',
    'modules/system/notification',
    'modules/system/prefs',
    'velocity',
    'jquery.validate'
], function ($, notification, prefs) {
    /**
     * Initialization method.
     *
     * @returns {void}
     *
     */
    var init = function () {

        /**
         * Event listener for "fix error" clicks.
         * Link needs to be manually tied since button is created dynamically.
         *
         * @param {object} event | The click event.
         * @returns {void}
         *
         */
        $('body').on('click', '#fixListingErrors', function (event) {
            $('#listingStatus:visible').velocity({'opacity': 0}, {
                'duration': prefs.duration
            }).velocity('slideUp', {
                'duration': prefs.duration,
                'complete': function () {
                    $('#listingInfoRequestForm').velocity('slideDown', {
                        'duration': prefs.duration
                    }).velocity({'opacity': 1}, {
                        'duration': prefs.duration
                    });
                }
            });

            event.preventDefault();
        });

        /**
         * Contact form validation rules and submit handler.
         *
         */
        $('#listingInfoRequestForm').validate({
            'errorPlacement': function (error, element) {
                if (element[0].type === 'radio') {
                    error.insertAfter(element.parent().parent());
                }
                else {
                    error.insertAfter(element);
                }
            },
            'highlight': function (element, errorClass) {
                $(element).addClass(errorClass).velocity({'opacity': .3}).velocity({'opacity': 1});
            },
            'rules': {
                'firstName': 'required',
                'lastName': 'required',
                'email': {
                    'required': true,
                    'email': true
                }
            },
            'messages': {
                'firstName': notification.firstNameRequired,
                'lastName': notification.lastNameRequired,
                'email': {
                    'required': notification.emailRequired,
                    'email': notification.emailInvalid
                }
            },
            'focusInvalid': true,
            'errorClass': 'has-error',
            'validClass': 'has-success',
            'submitHandler': function (form) {
                $('#listingStatusMessage').html('<div class="spinner"> <img src="/resources/img/default-ajax.svg" width="60" height="60" alt=""> </div><p class="text-center">Submitting...</p>');

                $('#listingInfoRequestForm:visible')
                    .velocity({'opacity': 0}, {
                        'duration': prefs.duration
                    })
                    .velocity('slideUp', {
                        'duration': prefs.duration,
                        'complete': function () {
                            $('#listingStatus:hidden').velocity('slideDown', {
                                'duration': prefs.duration
                            }).velocity({'opacity': 1});

                            /**
                             * Get the base form module.
                             *
                             * @param {object} form | Base form module.
                             * @returns {object}
                             *
                             */
                            require(['modules/system/form'], function (form) {
                                onListingInfoRequestForm(function (response) {
                                    $('#listingStatusMessage:visible').velocity({'opacity': 0}, {
                                        'duration': prefs.duration,
                                        'complete': function () {
                                            if (response.success) {
                                                $('#listingStatusMessage').html('<h2 class="has-success"><span class="fa-stack fa-lg"><i class="fa fa-circle-o fa-stack-2x"></i> <i class="fa fa-check fa-stack-1x"></i> </span><br> Thanks!</h2><p>We\'ve received your request&mdash;we\'ll be in touch soon!</p>')
                                                    .velocity({'opacity': 1}, {
                                                        'duration': prefs.duration
                                                    });
                                            } else {
                                                if (response.error) {
                                                    $('#listingStatusMessage').html('<h2 class="has-error"><span class="fa-stack fa-lg"><i class="fa fa-circle-o fa-stack-2x"></i> <i class="fa fa-exclamation fa-stack-1x"></i> </span><br>Woah!</h2><p>It looks like you missed something.<br><a href="javascript;" id="fixContactErrors">Go back and check it out.</a></h2>')
                                                        .velocity({'opacity': 1}, {
                                                            'duration': prefs.duration
                                                        });

                                                    $.each(response.error, function (key, val) {
                                                        form.renderFeedback('error', $('#' + key), val);
                                                    });
                                                } else {
                                                    $('#listingStatusMessage').html('<h2 class="has-error"><span class="fa-stack fa-lg"><i class="fa fa-circle-o fa-stack-2x"></i> <i class="fa fa-exclamation fa-stack-1x"></i> </span><br>System Error</h2><p>Please try again later.</p>')
                                                        .velocity({'opacity': 1}, {
                                                            'duration': prefs.duration
                                                        });
                                                }
                                            }
                                        }
                                    });
                                });
                            });
                        }
                    });
            }
        });

    };

    /**
     * Handle contact form data.
     *
     * @param {callback}
     * @retuns {void}
     *
     */
    function onListingInfoRequestForm(callback) {
        $.ajax({
            'type': 'post',
            'contentType': 'application/x-www-form-urlencoded; charset=UTF-8',
            'cache': false,
            'data': $('#listingInfoRequestForm').serialize(),
            'url': '/',
            'dataType': 'json',
            'timeout': 50000
        }).done(function (response) {
            callback(response);
        }).fail(function (error) {
            // Total fail.
        });
    }

    return {
        'init': init
    };
});