module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        'concat': {
            'css': {
                'src': [
                    'resources/css/*.css'
                ],
                'dest': '../web/resources/css/production.css'
            }
        },
        'compass': {
            'dist': {
                'options': {
                    'sassDir': 'resources/sass',
                    'cssDir': 'resources/css'
                }
            }
        },
        'watch': {
            'options': {
                'livereload': true
            },
            'css': {
                'files': ['resources/sass/**/*.scss'],
                'tasks': ['compass', 'concat'],
                'options': {
                    'spawn': false
                }
            },
            'html': {
                'files': ['../web/*.html', '../web/resources/css/*.css'],
                'options': {
                    'livereload': true
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-compass');

    grunt.registerTask('default', ['concat', 'compass', 'watch']);
};