

/**
 * Primary application module.
 *
 * @param {function} $ | jQuery library
 * @param {object} notification | Notification module.
 * @param {object} prefs | System preferences module.
 * @returns {object}
 *
 */

 function ($, notification, prefs, listing, reservation) {
    /**
     * Initialization method.
     *
     * @returns {void}
     *
     */
    var init = function () {
        listing.init();
        reservation.init();

        var formHeight = $('footer .left').outerHeight();
        var lowerFooter = $('.footer-lower').outerHeight();
        var footerHeight = $('footer').outerHeight();
        var offsetHeight = lowerFooter + footerHeight + 10;
        var contactHeight = $('.contact').outerHeight();
        $('footer textarea').css('height', formHeight);


        $(window).bind('scroll', function () {
            if ($(window).scrollTop() >= $('.marked').offset().top + $('.marked').outerHeight() - window.innerHeight) {
                $('.contact').addClass('hasScrolled');
                $('.hasScrolled').css('bottom', offsetHeight);
            } else {
                $('.contact').removeClass('hasScrolled').css('bottom', '0');
            }
        });

        $("#gallery .slides a").fancybox({
            openEffect: 'elastic',
            closeEffect: 'elastic'
        });


        $('.marked,.contact.hasScrolled').css('bottom', offsetHeight);
        
        /**
         * Insert the <hr> after that last available option
         * in the sellers list options. But only if
         * the list has unavailable items.
         *
         */
        $('.cd-pricing-features').each(function () {
            $(this).find('li.available:last').not(':last-child').after('<hr>');
        });

        /*
        Set Session Cookie
        */
            
        
        if ($.cookie('visited') != 'true'){
            $('#popup').modal('show');
            $.cookie('visited', 'true', { expires: 15 });
        }
        
        $('.playVideo').click(function(){
            $('.content-container').velocity("fadeOut", { duration: 1000 })
            $('.video-wrapper').velocity("fadeIn", {  delay: 999, duration: 1000 })
        })
        
        

        /**
         * Event listener for the buyer/seller buttons
         * on the home page.
         *
         * @param {object} event | The click event.
         *
         */
        $('.controls .button').click(function (event) {
            $('.controls .button').toggleClass('active');

            event.preventDefault();
        });

        /* Testimonials Page mixItUp Plugin */
        $('.testimonials').mixItUp();

        $('.listingsList').mixItUp({
            layout: {
                display: 'block'
            }
        });

        /* Default Flexslider */
        $('#toggle').click(function () {
            var windowSize = $(window).height();

            $('#overlay').css('height', windowSize);
            $(this).toggleClass('active');
            $('#overlay').toggleClass('open');
            $('html').toggleClass('open');
        });


        $('#gallery').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            directionNav: false,
            slideshow: false,
            sync: "#carousel",
            prevText: "",
            nextText: ""
        });

        $('#carousel').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            itemWidth: 100,
            itemMargin: 5,
            asNavFor: '#gallery',
            prevText: "",
            nextText: ""
        });

        new Dragdealer('savingsSlider', {
            x: 0.21,
            animationCallback: function (x, y) {
                var lowVal = 500;
                var highVal = 10000;
                var diff = highVal - lowVal;
                var thisVal = (Math.round(x * diff) + lowVal);
                var output = (Math.ceil(thisVal / 100.0) * 10000);
                var traditionalFee = (Math.round(output * 0.07));
                var savings = traditionalFee - 2500;

                $('#savingsSlider .value').text(numberWithCommas(output));
                $('#traditionalSellFee span').html(numberWithCommas(traditionalFee));
                $('#yourSavings span').html(numberWithCommas(savings));

            }
        });

        /**
         * Listings flexslider
         * https://github.com/woothemes/FlexSlider/wiki/FlexSlider-Properties
         *
         */
        $('.listing .hero .flexslider').flexslider({
            'directionNav': false
        });

        /**
         * Testimonials flexslider
         *
         */
        $('.testimonials .flexslider').flexslider({
            'animation': 'slide',
            'controlNav': false,
            'prevText': "",
            'nextText': ""
        });

        if ($(window).width() < 767) {
            $('.house-slider').flexslider({
                animation: "slide",
                directionNav: false
            });
        } else {
            flexdestroy('.house-slider');
        }

        $(window).resize(function () {

            if ($(window).width() < 600) {
                $('.house-slider').flexslider({
                    animation: "slide",
                    directionNav: false
                });
            } else {
                flexdestroy('.house-slider');
            }
        });


        /**
         * Event listener for "fix error" clicks.
         * Link needs to be manually tied since button is created dynamically.
         *
         * @param {object} event | The click event.
         * @returns {void}
         *
         */
        $('body').on('click', '#fixContactErrors', function (event) {
            $('#contactStatus:visible').velocity({'opacity': 0}, {
                'duration': prefs.duration
            }).velocity('slideUp', {
                'duration': prefs.duration,
                'complete': function () {
                    $('#contactForm').velocity('slideDown', {
                        'duration': prefs.duration
                    }).velocity({'opacity': 1}, {
                        'duration': prefs.duration
                    });
                }
            });

            event.preventDefault();
        });


        /**
         * Sellers form validation rules and submit handler.
         *
         */
        $('#sellersForm').validate({
            'errorPlacement': function (error, element) {
                if (element[0].type === 'radio') {
                    error.insertAfter(element.parent().parent());
                }
                else {
                    error.insertAfter(element);
                }
            },
            'highlight': function (element, errorClass) {
                $(element).addClass(errorClass).velocity({'opacity': .3}).velocity({'opacity': 1});
            },
            'rules': {
                'firstName': 'required',
                'lastName': 'required',
                'email': {
                    'required': true,
                    'email': true
                },
                'phone': 'required'
            },
            'messages': {
                'firstName': notification.firstNameRequired,
                'lastName': notification.lastNameRequired,
                'email': {
                    'required': notification.emailRequired,
                    'email': notification.emailInvalid
                },
                'phone': notification.phoneRequired
            },
            'focusInvalid': true,
            'errorClass': 'has-error',
            'validClass': 'has-success',
            'submitHandler': function (form) {
                $('#sellerStatusMessage').html('<div class="spinner"> <img src="/resources/img/contact-ajax.svg" width="60" height="60" alt=""> </div><p class="text-center">Submitting...</p>');

                $('#sellersForm:visible')
                    .velocity({'opacity': 0}, {
                        'duration': prefs.duration
                    })
                    .velocity('slideUp', {
                        'duration': prefs.duration,
                        'complete': function () {
                            $('#sellerStatus:hidden').velocity('slideDown', {
                                'duration': prefs.duration
                            }).velocity({'opacity': 1});

                            /**
                             * Get the base form module.
                             *
                             * @param {object} form | Base form module.
                             * @returns {object}
                             *
                             */
                            require(['modules/system/form'], function (form) {
                                onSellerFormSubmit(function (response) {
                                    $('#sellerStatusMessage:visible').velocity({'opacity': 0}, {
                                        'duration': prefs.duration,
                                        'complete': function () {
                                            if (response.success) {
                                                $('#sellerStatusMessage').html('<h2 class="has-success"><span class="fa-stack fa-lg"><i class="fa fa-circle-o fa-stack-2x"></i> <i class="fa fa-check fa-stack-1x"></i> </span><br> Thanks!</h2><p>We\'ve received your request&mdash;we\'ll be in touch soon!</p>')
                                                    .velocity({'opacity': 1}, {
                                                        'duration': prefs.duration
                                                    });
                                            }
                                            else {
                                                if (response.error) {
                                                    $('#sellerStatusMessage').html('<h2 class="has-error"><span class="fa-stack fa-lg"><i class="fa fa-circle-o fa-stack-2x"></i> <i class="fa fa-exclamation fa-stack-1x"></i> </span><br>Woah!</h2><p>It looks like you missed something.<br><a href="javascript;" id="fixContactErrors">Go back and check it out.</a></h2>')
                                                        .velocity({'opacity': 1}, {
                                                            'duration': prefs.duration
                                                        });

                                                    $.each(response.error, function (key, val) {
                                                        form.renderFeedback('error', $('#' + key), val);
                                                    });
                                                } else {
                                                    $('#sellerStatusMessage').html('<h2 class="has-error"><span class="fa-stack fa-lg"><i class="fa fa-circle-o fa-stack-2x"></i> <i class="fa fa-exclamation fa-stack-1x"></i> </span><br>System Error</h2><p>Please try again later.</p>')
                                                        .velocity({'opacity': 1}, {
                                                            'duration': prefs.duration
                                                        });
                                                }
                                            }
                                        }
                                    });
                                });
                            });
                        }
                    });
            }
        });


        /**
         * Contact form validation rules and submit handler.
         *
         */
        $('#contactForm').validate({
            'errorPlacement': function (error, element) {
                if (element[0].type === 'radio') {
                    error.insertAfter(element.parent().parent());
                }
                else {
                    error.insertAfter(element);
                }
            },
            'highlight': function (element, errorClass) {
                $(element).addClass(errorClass).velocity({'opacity': .3}).velocity({'opacity': 1});
            },
            'rules': {
                'name': 'required',
                'email': {
                    'required': true,
                    'email': true
                }
            },
            'messages': {
                'name': notification.nameRequired,
                'email': {
                    'required': notification.emailRequired,
                    'email': notification.emailInvalid
                }
            },
            'focusInvalid': true,
            'errorClass': 'has-error',
            'validClass': 'has-success',
            'submitHandler': function (form) {
                $('#contactStatusMessage').html('<div class="spinner"> <img src="/resources/img/contact-ajax.svg" width="60" height="60" alt=""> </div><p class="text-center">Submitting...</p>');

                $('#contactForm:visible')
                    .velocity({'opacity': 0}, {
                        'duration': prefs.duration
                    })
                    .velocity('slideUp', {
                        'duration': prefs.duration,
                        'complete': function () {
                            $('#contactStatus:hidden').velocity('slideDown', {
                                'duration': prefs.duration
                            }).velocity({'opacity': 1});

                            /**
                             * Get the base form module.
                             *
                             * @param {object} form | Base form module.
                             * @returns {object}
                             *
                             */
                            require(['modules/system/form'], function (form) {
                                onContactFormSubmit(function (response) {
                                    $('#contactStatusMessage:visible').velocity({'opacity': 0}, {
                                        'duration': prefs.duration,
                                        'complete': function () {
                                            if (response.success) {
                                                $('#contactStatusMessage').html('<h2 class="has-success"><span class="fa-stack fa-lg"><i class="fa fa-circle-o fa-stack-2x"></i> <i class="fa fa-check fa-stack-1x"></i> </span><br> Thanks!</h2><p>We\'ve received your request&mdash;we\'ll be in touch soon!</p>')
                                                    .velocity({'opacity': 1}, {
                                                        'duration': prefs.duration
                                                    });
                                            }
                                            else {
                                                if (response.error) {
                                                    $('#contactStatusMessage').html('<h2 class="has-error"><span class="fa-stack fa-lg"><i class="fa fa-circle-o fa-stack-2x"></i> <i class="fa fa-exclamation fa-stack-1x"></i> </span><br>Woah!</h2><p>It looks like you missed something.<br><a href="javascript;" id="fixContactErrors">Go back and check it out.</a></h2>')
                                                        .velocity({'opacity': 1}, {
                                                            'duration': prefs.duration
                                                        });

                                                    $.each(response.error, function (key, val) {
                                                        form.renderFeedback('error', $('#' + key), val);
                                                    });
                                                } else {
                                                    $('#contactStatusMessage').html('<h2 class="has-error"><span class="fa-stack fa-lg"><i class="fa fa-circle-o fa-stack-2x"></i> <i class="fa fa-exclamation fa-stack-1x"></i> </span><br>System Error</h2><p>Please try again later.</p>')
                                                        .velocity({'opacity': 1}, {
                                                            'duration': prefs.duration
                                                        });
                                                }
                                            }
                                        }
                                    });
                                });
                            });
                        }
                    });
            }
        });

    };

    /* Flex Destroy */
    function flexdestroy(selector) {
        var el = $(selector);
        var elClean = el.clone();

        elClean.find('.flex-viewport').children().unwrap();
        elClean
            .removeClass('flexslider')
            .find('.clone, .flex-direction-nav, .flex-control-nav')
            .remove()
            .end()
            .find('*').removeAttr('style').removeClass(function (index, css) {
                return (css.match(/\bflex\S+/g) || []).join(' ');
            });

        elClean.insertBefore(el);
        el.remove();
    }

    /**
     * Event listener for contact us tab click.
     *
     */
    $('.contact').click(function () {
        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 500);
        return false;
    });


    /**
     * Ajax handler for seller form submit.
     *
     * @param callback
     *
     */
    function onSellerFormSubmit(callback) {
        $.ajax({
            'type': 'post',
            'contentType': 'application/x-www-form-urlencoded; charset=UTF-8',
            'cache': false,
            'data': $('#sellersForm').serialize(),
            'url': '/',
            'dataType': 'json',
            'timeout': 50000
        }).done(function (response) {
            callback(response);
        }).fail(function (error) {
            // Total fail.
        });
    }

    /**
     * Ajax handler for contact form submissions.
     *
     * @param callback
     *
     *
     */
    function onContactFormSubmit(callback) {
        $.ajax({
            'type': 'post',
            'contentType': 'application/x-www-form-urlencoded; charset=UTF-8',
            'cache': false,
            'data': $('#contactForm').serialize(),
            'url': '/',
            'dataType': 'json',
            'timeout': 50000
        }).done(function (response) {
            callback(response);
        }).fail(function (error) {
            // Total fail.
        });
    }

    return {
        'init': init
    };
};



