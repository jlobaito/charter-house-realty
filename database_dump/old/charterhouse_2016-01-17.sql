# ************************************************************
# Sequel Pro SQL dump
# Version 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.42)
# Database: charterhouse
# Generation Time: 2016-01-18 00:10:21 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table assetfiles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `assetfiles`;

CREATE TABLE `assetfiles` (
  `id` int(11) NOT NULL,
  `sourceId` int(11) DEFAULT NULL,
  `folderId` int(11) NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kind` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unknown',
  `width` int(11) unsigned DEFAULT NULL,
  `height` int(11) unsigned DEFAULT NULL,
  `size` int(11) unsigned DEFAULT NULL,
  `dateModified` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `assetfiles_filename_folderId_unq_idx` (`filename`,`folderId`),
  KEY `assetfiles_sourceId_fk` (`sourceId`),
  KEY `assetfiles_folderId_fk` (`folderId`),
  CONSTRAINT `assetfiles_folderId_fk` FOREIGN KEY (`folderId`) REFERENCES `assetfolders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assetfiles_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assetfiles_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `assetsources` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `assetfiles` WRITE;
/*!40000 ALTER TABLE `assetfiles` DISABLE KEYS */;

INSERT INTO `assetfiles` (`id`, `sourceId`, `folderId`, `filename`, `kind`, `width`, `height`, `size`, `dateModified`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(67,1,3,'house.jpg','image',620,465,229000,'2016-01-06 23:58:32','2016-01-06 23:58:32','2016-01-06 23:58:32','9692bef0-5f81-4125-980c-b2d204b238cc'),
	(83,2,9,'buyer-hero.jpg','image',1298,780,185385,'2016-01-14 03:56:33','2016-01-14 03:56:33','2016-01-16 00:54:24','15cc26ea-bd37-4f54-8745-71f861666773'),
	(85,2,9,'24.jpg','image',88,71,1486,'2016-01-14 04:44:02','2016-01-14 04:44:02','2016-01-14 04:44:02','17a07bec-5f5c-4a45-aa41-70dff52c85c7'),
	(86,2,9,'access.jpg','image',118,67,1444,'2016-01-14 04:45:57','2016-01-14 04:45:57','2016-01-14 04:45:57','1e2b2658-fa34-4bca-ab8c-58c81700f1de'),
	(88,2,9,'lender.jpg','image',93,66,1348,'2016-01-14 04:47:41','2016-01-14 04:47:41','2016-01-14 04:47:41','c7a07105-d1c1-4f9f-9dfd-43de2787bbed'),
	(90,2,9,'home1.jpg','image',3072,2304,3575929,'2016-01-14 05:01:00','2016-01-14 05:01:01','2016-01-14 05:01:01','70cea260-e2ca-46eb-a420-678101a946a5'),
	(92,2,9,'seller-hero.jpg','image',1298,799,197069,'2016-01-16 00:54:10','2016-01-16 00:54:10','2016-01-16 00:54:10','fc5acff9-c664-470b-a33c-45aa0cfd3e5b'),
	(98,2,9,'tyler-osby.jpg','image',140,67,10226,'2016-01-16 04:14:11','2016-01-16 04:14:11','2016-01-16 04:14:11','3ef00db9-8393-4873-bcb2-b379ba1fc122'),
	(99,3,11,'tyler-osby.jpg','image',140,67,10226,'2016-01-16 04:19:22','2016-01-16 04:19:22','2016-01-16 04:19:22','e81c371f-f767-4781-a70e-1b2e82119a92'),
	(103,1,4,'featured-1.jpg','image',3072,2304,3575929,'2016-01-17 16:00:14','2016-01-17 16:00:15','2016-01-17 16:00:15','9ce60567-81ce-48a4-bf83-f551329bdd6c'),
	(104,1,4,'featured-2.jpg','image',1476,820,1090357,'2016-01-17 16:00:15','2016-01-17 16:00:16','2016-01-17 16:00:16','864233a5-0e23-4074-9d6a-ee7de34771ac'),
	(105,1,4,'featured-3.jpg','image',2032,1524,676819,'2016-01-17 16:00:16','2016-01-17 16:00:16','2016-01-17 16:00:16','d62db45e-0c75-4ad8-bc30-0d8586224dc6'),
	(106,1,3,'featured-1.jpg','image',3072,2304,3575929,'2016-01-17 16:04:44','2016-01-17 16:04:44','2016-01-17 16:04:44','43814553-21f3-4102-89be-5c88aec0619d'),
	(107,1,3,'featured-2.jpg','image',1476,820,1090357,'2016-01-17 16:04:45','2016-01-17 16:04:45','2016-01-17 16:04:45','1db39aa8-db4f-4823-8f5a-7250fbde873a'),
	(108,1,3,'featured-3.jpg','image',2032,1524,676819,'2016-01-17 16:04:45','2016-01-17 16:04:45','2016-01-17 16:04:45','e660b4fb-d016-47f5-8c57-a831bce9956c'),
	(109,1,8,'featured-1.jpg','image',3072,2304,3575929,'2016-01-17 16:07:06','2016-01-17 16:07:07','2016-01-17 16:07:07','04a7318f-3457-40f2-a2a9-1107fe7655a0'),
	(110,1,8,'featured-2.jpg','image',1476,820,1090357,'2016-01-17 16:07:07','2016-01-17 16:07:07','2016-01-17 16:07:07','e0411307-b3dc-47a7-b092-dc7f93c249d3'),
	(111,1,8,'featured-3.jpg','image',2032,1524,676819,'2016-01-17 16:07:08','2016-01-17 16:07:08','2016-01-17 16:07:08','8f35b5f5-3fce-41a7-96fb-d2c5089bbfd6');

/*!40000 ALTER TABLE `assetfiles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table assetfolders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `assetfolders`;

CREATE TABLE `assetfolders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentId` int(11) DEFAULT NULL,
  `sourceId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `assetfolders_name_parentId_sourceId_unq_idx` (`name`,`parentId`,`sourceId`),
  KEY `assetfolders_parentId_fk` (`parentId`),
  KEY `assetfolders_sourceId_fk` (`sourceId`),
  CONSTRAINT `assetfolders_parentId_fk` FOREIGN KEY (`parentId`) REFERENCES `assetfolders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assetfolders_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `assetsources` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `assetfolders` WRITE;
/*!40000 ALTER TABLE `assetfolders` DISABLE KEYS */;

INSERT INTO `assetfolders` (`id`, `parentId`, `sourceId`, `name`, `path`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,NULL,1,'Properties','','2016-01-06 16:52:47','2016-01-06 23:46:29','fa94ae8f-cb71-46f8-a257-0c9510d79be0'),
	(3,1,1,'504907','504907/','2016-01-06 23:57:47','2016-01-06 23:57:47','0c241761-b349-4dd4-8ee1-e9c9a64fd507'),
	(4,1,1,'506142','506142/','2016-01-07 00:03:55','2016-01-07 00:03:55','1acc0597-a282-46d5-97c2-a7f055eefaf8'),
	(5,NULL,NULL,'Temporary source',NULL,'2016-01-13 05:01:15','2016-01-13 05:01:15','3b2e54ff-ba8f-46b0-9a44-c112cacc07df'),
	(6,5,NULL,'user_1',NULL,'2016-01-13 05:01:15','2016-01-13 05:01:15','3982f371-3a15-48b8-bc24-a9869c586b7f'),
	(7,6,NULL,'field_54','field_54/','2016-01-13 05:01:15','2016-01-13 05:01:15','de43aea8-2265-446f-ad76-e42be775bb09'),
	(8,1,1,'507193','507193/','2016-01-13 05:03:21','2016-01-13 05:03:21','7e20344c-f3d1-447e-9c18-5f7560d2d8e5'),
	(9,NULL,2,'Images','','2016-01-14 03:51:44','2016-01-14 03:51:44','f03a4e3d-17df-487f-b489-ad4587c57b0b'),
	(10,1,1,'12345','12345/','2016-01-14 05:11:56','2016-01-14 05:11:56','fb27e440-95be-49d0-90c0-6f685c9ed699'),
	(11,NULL,3,'Partners','','2016-01-16 04:15:51','2016-01-16 04:15:51','283d1bad-6bd0-41f0-bb81-0fde9914f6e3');

/*!40000 ALTER TABLE `assetfolders` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table assetindexdata
# ------------------------------------------------------------

DROP TABLE IF EXISTS `assetindexdata`;

CREATE TABLE `assetindexdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sessionId` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sourceId` int(10) NOT NULL,
  `offset` int(10) NOT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `size` int(10) DEFAULT NULL,
  `recordId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `assetindexdata_sessionId_sourceId_offset_unq_idx` (`sessionId`,`sourceId`,`offset`),
  KEY `assetindexdata_sourceId_fk` (`sourceId`),
  CONSTRAINT `assetindexdata_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `assetsources` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table assetsources
# ------------------------------------------------------------

DROP TABLE IF EXISTS `assetsources`;

CREATE TABLE `assetsources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `sortOrder` tinyint(4) DEFAULT NULL,
  `fieldLayoutId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `assetsources_name_unq_idx` (`name`),
  UNIQUE KEY `assetsources_handle_unq_idx` (`handle`),
  KEY `assetsources_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `assetsources_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `assetsources` WRITE;
/*!40000 ALTER TABLE `assetsources` DISABLE KEYS */;

INSERT INTO `assetsources` (`id`, `name`, `handle`, `type`, `settings`, `sortOrder`, `fieldLayoutId`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Properties','properties','Local','{\"path\":\"{basePath}media\\/images\\/properties\\/\",\"url\":\"\\/media\\/images\\/properties\\/\"}',1,109,'2016-01-06 16:52:47','2016-01-06 23:55:58','0ae0f9d5-3b70-4855-9926-6e88162f8e67'),
	(2,'Images','images','Local','{\"path\":\"{basePath}media\\/images\\/\",\"url\":\"\\/media\\/images\\/\"}',2,128,'2016-01-14 03:51:44','2016-01-14 03:52:05','502b7326-9b40-40a4-8c2e-218cf9631e23'),
	(3,'Partners','partners','Local','{\"path\":\"{basePath}media\\/images\\/partners\\/\",\"url\":\"\\/media\\/images\\/partners\\/\"}',3,176,'2016-01-16 04:15:51','2016-01-16 04:15:51','b05fb227-ca39-40c2-828e-3994869bb011');

/*!40000 ALTER TABLE `assetsources` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table assettransformindex
# ------------------------------------------------------------

DROP TABLE IF EXISTS `assettransformindex`;

CREATE TABLE `assettransformindex` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fileId` int(11) NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `format` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sourceId` int(11) DEFAULT NULL,
  `fileExists` tinyint(1) DEFAULT NULL,
  `inProgress` tinyint(1) DEFAULT NULL,
  `dateIndexed` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `assettransformindex_sourceId_fileId_location_idx` (`sourceId`,`fileId`,`location`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `assettransformindex` WRITE;
/*!40000 ALTER TABLE `assettransformindex` DISABLE KEYS */;

INSERT INTO `assettransformindex` (`id`, `fileId`, `filename`, `format`, `location`, `sourceId`, `fileExists`, `inProgress`, `dateIndexed`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,98,'tyler-osby.jpg',NULL,'_partnerLogo',2,1,0,'2016-01-16 04:17:19','2016-01-16 04:17:19','2016-01-16 04:17:19','042b0c23-b006-4bba-9f24-6ec7dfa11c7b'),
	(2,99,'tyler-osby.jpg',NULL,'_partnerLogo',3,1,0,'2016-01-16 04:19:28','2016-01-16 04:19:28','2016-01-16 04:19:29','90758465-a1b2-45d7-9d91-cad12178b293');

/*!40000 ALTER TABLE `assettransformindex` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table assettransforms
# ------------------------------------------------------------

DROP TABLE IF EXISTS `assettransforms`;

CREATE TABLE `assettransforms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mode` enum('stretch','fit','crop') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'crop',
  `position` enum('top-left','top-center','top-right','center-left','center-center','center-right','bottom-left','bottom-center','bottom-right') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'center-center',
  `height` int(10) DEFAULT NULL,
  `width` int(10) DEFAULT NULL,
  `format` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quality` int(10) DEFAULT NULL,
  `dimensionChangeTime` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `assettransforms_name_unq_idx` (`name`),
  UNIQUE KEY `assettransforms_handle_unq_idx` (`handle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `assettransforms` WRITE;
/*!40000 ALTER TABLE `assettransforms` DISABLE KEYS */;

INSERT INTO `assettransforms` (`id`, `name`, `handle`, `mode`, `position`, `height`, `width`, `format`, `quality`, `dimensionChangeTime`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Partner Logo','partnerLogo','crop','center-center',134,280,NULL,NULL,'2016-01-16 04:15:10','2016-01-16 04:15:10','2016-01-16 04:15:10','d19c2f99-b667-46f8-8377-936085365813');

/*!40000 ALTER TABLE `assettransforms` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `categories_groupId_fk` (`groupId`),
  CONSTRAINT `categories_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `categorygroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `categories_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;

INSERT INTO `categories` (`id`, `groupId`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(39,3,'2015-12-20 20:27:05','2015-12-20 20:29:17','5b8e1d41-ab08-4de6-9994-680d246f0168'),
	(40,3,'2015-12-20 20:30:17','2015-12-20 20:30:17','9fbd0802-3747-45d4-84ae-d982216254a4'),
	(41,3,'2015-12-20 20:31:04','2015-12-20 20:31:04','145fa1ca-a90e-4593-a8ba-bc08f9bc481d'),
	(42,3,'2015-12-20 20:31:22','2015-12-20 20:31:22','65e3afcf-8b43-4e8f-bad6-7464dda32222'),
	(68,4,'2016-01-07 00:14:03','2016-01-07 00:14:03','7befe215-2bc8-4545-a62b-ec75976232f4'),
	(69,4,'2016-01-07 00:24:07','2016-01-07 00:24:07','7e0d8d30-c43e-4d3b-a23e-9e846d0deb8d'),
	(70,4,'2016-01-07 00:24:35','2016-01-07 00:24:35','dfb8915a-94c9-4fb8-a797-235fc1fc29b2'),
	(100,2,'2016-01-17 04:05:40','2016-01-17 04:10:08','e1f6de30-bbb1-4984-8bd7-bc8fee2d43a4'),
	(101,2,'2016-01-17 04:08:41','2016-01-17 04:10:17','acf4ae68-34cf-4bea-9dc6-9781aaa6746a'),
	(102,2,'2016-01-17 04:09:48','2016-01-17 04:09:59','603c2ee2-1982-46b1-8dd9-641346d71b0f');

/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table categorygroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categorygroups`;

CREATE TABLE `categorygroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hasUrls` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `template` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `categorygroups_name_unq_idx` (`name`),
  UNIQUE KEY `categorygroups_handle_unq_idx` (`handle`),
  KEY `categorygroups_structureId_fk` (`structureId`),
  KEY `categorygroups_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `categorygroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `categorygroups_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `categorygroups` WRITE;
/*!40000 ALTER TABLE `categorygroups` DISABLE KEYS */;

INSERT INTO `categorygroups` (`id`, `structureId`, `fieldLayoutId`, `name`, `handle`, `hasUrls`, `template`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(2,5,171,'Ways To Sell','waysToSell',0,NULL,'2015-12-20 04:40:58','2016-01-16 03:39:54','666de006-99ac-4d05-839a-5ff0ab05753d'),
	(3,6,74,'Team Members','teamMembers',0,NULL,'2015-12-20 20:23:29','2015-12-20 20:39:56','ecf25418-ed99-4fa4-998d-50519df7853d'),
	(4,9,111,'Testimonials','testimonials',0,NULL,'2016-01-07 00:11:36','2016-01-07 00:12:12','82a76085-fba0-4084-bf70-9ec6ae146c12');

/*!40000 ALTER TABLE `categorygroups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table categorygroups_i18n
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categorygroups_i18n`;

CREATE TABLE `categorygroups_i18n` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `urlFormat` text COLLATE utf8_unicode_ci,
  `nestedUrlFormat` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `categorygroups_i18n_groupId_locale_unq_idx` (`groupId`,`locale`),
  KEY `categorygroups_i18n_locale_fk` (`locale`),
  CONSTRAINT `categorygroups_i18n_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `categorygroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `categorygroups_i18n_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `categorygroups_i18n` WRITE;
/*!40000 ALTER TABLE `categorygroups_i18n` DISABLE KEYS */;

INSERT INTO `categorygroups_i18n` (`id`, `groupId`, `locale`, `urlFormat`, `nestedUrlFormat`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(2,2,'en_us',NULL,NULL,'2015-12-20 04:40:58','2015-12-20 04:40:58','bb87c0f0-d34b-4c55-8e10-530fde15c481'),
	(3,3,'en_us',NULL,NULL,'2015-12-20 20:23:29','2015-12-20 20:23:29','263fe484-458c-46bf-b8f0-e2c51990e2d2'),
	(4,4,'en_us',NULL,NULL,'2016-01-07 00:11:36','2016-01-07 00:11:36','1e42cb63-7f8c-4349-9776-022109b5101e');

/*!40000 ALTER TABLE `categorygroups_i18n` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table contactform
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contactform`;

CREATE TABLE `contactform` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `contactform` WRITE;
/*!40000 ALTER TABLE `contactform` DISABLE KEYS */;

INSERT INTO `contactform` (`id`, `name`, `email`, `status`, `comment`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(4,'Damon Gentry','gentryd@gmail.com','seller','hello','2016-01-17 17:28:19','2016-01-17 17:28:19','2ab6354c-d309-4d1a-bda2-6b68374a3259');

/*!40000 ALTER TABLE `contactform` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table content
# ------------------------------------------------------------

DROP TABLE IF EXISTS `content`;

CREATE TABLE `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_body` text COLLATE utf8_unicode_ci,
  `field_pageDescription` varchar(155) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_comments` text COLLATE utf8_unicode_ci,
  `field_email` text COLLATE utf8_unicode_ci,
  `field_phone` varchar(14) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_baths` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_beds` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_foreclosure` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `field_mlsNumber` text COLLATE utf8_unicode_ci,
  `field_shortSale` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `field_squareFeet` text COLLATE utf8_unicode_ci,
  `field_attribution` text COLLATE utf8_unicode_ci,
  `field_propertyType` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `field_parking` text COLLATE utf8_unicode_ci,
  `field_price` text COLLATE utf8_unicode_ci,
  `field_videoUrl` text COLLATE utf8_unicode_ci,
  `field_matterportUrl` text COLLATE utf8_unicode_ci,
  `field_yearBuilt` text COLLATE utf8_unicode_ci,
  `field_officeTitle` text COLLATE utf8_unicode_ci,
  `field_officePhone` varchar(14) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_facebook` text COLLATE utf8_unicode_ci,
  `field_twitter` text COLLATE utf8_unicode_ci,
  `field_linkedin` text COLLATE utf8_unicode_ci,
  `field_youtube` text COLLATE utf8_unicode_ci,
  `field_optionDescription` text COLLATE utf8_unicode_ci,
  `field_optionCommission` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `field_optionBenefits` text COLLATE utf8_unicode_ci,
  `field_markCharterDisclaimer` text COLLATE utf8_unicode_ci,
  `field_teamMemberTitle` text COLLATE utf8_unicode_ci,
  `field_teamMemberPhone` varchar(14) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_teamMemberEmail` text COLLATE utf8_unicode_ci,
  `field_teamMemberBio` text COLLATE utf8_unicode_ci,
  `field_partnerWebsite` text COLLATE utf8_unicode_ci,
  `field_partnerPhone` varchar(14) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_saleStatus` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'available',
  `field_reservationStatus` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `field_electronicSignature` text COLLATE utf8_unicode_ci,
  `field_signatureDate` datetime DEFAULT NULL,
  `field_firstName` text COLLATE utf8_unicode_ci,
  `field_lastName` text COLLATE utf8_unicode_ci,
  `field_officeEmail` text COLLATE utf8_unicode_ci,
  `field_licenseNumber` text COLLATE utf8_unicode_ci,
  `field_agreeToRentalTerms` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_priceReduced` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_elementId_locale_unq_idx` (`elementId`,`locale`),
  KEY `content_title_idx` (`title`),
  KEY `content_locale_fk` (`locale`),
  CONSTRAINT `content_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `content_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `content` WRITE;
/*!40000 ALTER TABLE `content` DISABLE KEYS */;

INSERT INTO `content` (`id`, `elementId`, `locale`, `title`, `field_body`, `field_pageDescription`, `field_comments`, `field_email`, `field_phone`, `field_baths`, `field_beds`, `field_foreclosure`, `field_mlsNumber`, `field_shortSale`, `field_squareFeet`, `field_attribution`, `field_propertyType`, `field_parking`, `field_price`, `field_videoUrl`, `field_matterportUrl`, `field_yearBuilt`, `field_officeTitle`, `field_officePhone`, `field_facebook`, `field_twitter`, `field_linkedin`, `field_youtube`, `field_optionDescription`, `field_optionCommission`, `field_optionBenefits`, `field_markCharterDisclaimer`, `field_teamMemberTitle`, `field_teamMemberPhone`, `field_teamMemberEmail`, `field_teamMemberBio`, `field_partnerWebsite`, `field_partnerPhone`, `field_saleStatus`, `field_reservationStatus`, `field_electronicSignature`, `field_signatureDate`, `field_firstName`, `field_lastName`, `field_officeEmail`, `field_licenseNumber`, `field_agreeToRentalTerms`, `field_priceReduced`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'en_us',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2015-12-19 00:45:33','2015-12-28 00:48:24','f7f261c0-8bb8-4c23-a1b0-91d442605c2a'),
	(2,2,'en_us','Mark Charter Realty','<p>s</p>','',NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2015-12-19 00:45:35','2016-01-16 00:54:26','c031151f-4592-46a9-a340-04526c81cd36'),
	(3,3,'en_us','We just installed Craft!','<p>Craft is the CMS that’s powering Local.charterhouseiowa.dev. It’s beautiful, powerful, flexible, and easy-to-use, and it’s made by Pixel &amp; Tonic. We can’t wait to dive in and see what it’s capable of!</p><!--pagebreak--><p>This is even more captivating content, which you couldn’t see on the News index page because it was entered after a Page Break, and the News index template only likes to show the content on the first page.</p><p>Craft: a nice alternative to Word, if you’re making a website.</p>',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2015-12-19 00:45:35','2015-12-19 00:45:35','e95ac777-13b6-4229-99d2-10cdd420fe6f'),
	(4,4,'en_us','Listings','','',NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2015-12-19 00:52:08','2016-01-16 01:59:54','bb56f9e8-7abc-4a29-b70b-05449b405b08'),
	(5,5,'en_us','Partners','','',NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2015-12-19 00:53:06','2016-01-16 04:08:11','1da33283-e59a-44b8-ba30-7e7a6d583227'),
	(6,6,'en_us','Team','<p>It’s true, this site doesn’t have a whole lot of content yet, but don’t worry. Our web developers have just installed the CMS, and they’re setting things up for the content editors this very moment. Soon Local.charterhouseiowa.dev will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.</p>','',NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2015-12-19 00:54:13','2016-01-16 03:49:25','53c58286-371a-489e-b755-f8447d16afa4'),
	(7,7,'en_us','Reviews','<p>It’s true, this site doesn’t have a whole lot of content yet, but don’t worry. Our web developers have just installed the CMS, and they’re setting things up for the content editors this very moment. Soon Local.charterhouseiowa.dev will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.</p>','',NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2015-12-19 00:54:52','2016-01-16 03:55:08','b4ee2574-7077-44e6-a760-dd636041ed69'),
	(8,8,'en_us','Reservation Success','<p>We got ya\' covered - and you got my truck.</p>','',NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2015-12-19 00:55:30','2015-12-19 02:41:05','534e20b0-0afd-4742-830d-93feb55d08dc'),
	(9,9,'en_us','Truck','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mattis sollicitudin felis, consectetur eleifend mi ornare sed. Vivamus cursus tincidunt ultrices. Nam id tincidunt purus. Morbi lectus eros, molestie non dolor ut, tincidunt fermentum odio. Vivamus consectetur molestie lorem ac varius.</p>','',NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2015-12-19 01:05:51','2016-01-17 22:18:51','aa2821fa-0af5-4d15-997b-b65b172d2e7d'),
	(17,17,'en_us','Amazing golf course home with too many amenities to list!','<p>Stunning Ankeny home located on Otter Creek Golf Course. This 4 bed, 4.5 bath former Show Home is loaded with features and amenities from top to bottom. Try building this home today for less than a million dollars! It all starts with location. You will love cul-de-sac living, close to I35 making a commute a breeze. Step through the front door and you will be wowed by the view! There is not one, but TWO ponds directly behind the home. The outside is just as gorgeous as the inside.&nbsp;</p>\r\n\r\n<p>Full landscaping, flagstone entry, exterior lighting, huge patio and a wraparound deck with outdoor kitchen truly put this home among Ankeny\'s best! Inside the quality is top notch. Beautiful knotty alder trim, Acacia hardwood, Viking appliances, and a beautiful stone fireplace are impossible to miss. The master is an oasis, with beautiful materials. Need an amazing basement? Sunken family room, barn wood floors, huge wet bar, hidden room and golf cart garage await. Too much to list, Email for all features!</p>','Golf course house.',NULL,NULL,NULL,'4.5','1',0,'504907',0,'2,426',NULL,'singleFamily','3 Car Garage','849,900','yZRyXkGqD2c','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2015-12-19 02:52:17','2016-01-17 16:05:04','4ccd886b-2f4c-4250-8b3f-f85fc67fcdb9'),
	(18,18,'en_us','Absolutely amazing, modern rural-meets-urban set up just outside of Grimes!! A must see at $719,000!','<p>Call your builder and call off the meeting! This beats new construction and is the house you have been waiting on to hit the market! This stunning Grimes acreage sits on 2.3 acres and is just a mile north of DCG High School. Awesome location. Pull into the long driveway and enjoy the massive front yard with trees. STUNNING curb appeal!&nbsp;</p>\r\n\r\n<p>This 5 bedroom (could be 6) home has a floor plan you have not seen before. The attention to detail and woodwork is stunning. Built-ins and window seats with storage are found everywhere. The master bedroom is on the main level and offers a gorgeous bath with Carrera marble, and a vey large closet. The main floor laundry is also huge. You will find a second full laundry room in the finished basement, along with a second kitchen! One of the beds is separated from the rest of the house and is perfect for an office or teen room.&nbsp;</p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<p>Too many amenities to list them all!&nbsp;</p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<ul><li>5 garage stalls</li><li>Geothermal</li><li>High end kitchen</li><li>Covered deck</li><li>So much more -&nbsp;A RARE FIND!</li></ul>','',NULL,NULL,NULL,'4','5',0,'506142',0,'2,700',NULL,'singleFamily','3 car attached garage - 2 car detached garage','719,000','','1Hw5ay9jY7y','1990',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2015-12-19 15:30:35','2016-01-17 16:00:29','f0509e79-9c3e-4232-8574-032b4e5a8eca'),
	(19,19,'en_us',NULL,NULL,NULL,NULL,'mark@charterhouseiowa.com',NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'Remax Real Estate Concepts','(515) 864-6444',NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,'mark@charterhouseiowa.com','56865000',NULL,0,'2015-12-19 21:06:00','2016-01-08 21:50:23','837a1daa-68e8-4d2e-a5eb-f2418a4c82a7'),
	(20,20,'en_us',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2015-12-20 04:19:31','2015-12-20 04:19:31','3ae86263-7865-4fac-83d9-6016e7b416f2'),
	(21,21,'en_us','Contact Mark','','',NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2015-12-20 04:23:50','2015-12-20 04:26:39','aece22f3-c5fc-4a91-913f-2e1734904d6d'),
	(22,22,'en_us','Sellers','','',NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2015-12-20 04:39:21','2016-01-16 02:20:22','fc1b5871-91c5-4bda-a60b-26e071d183e9'),
	(26,36,'en_us','Buyers & Sellers','','',NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2015-12-20 05:00:51','2015-12-28 01:03:42','ac59df68-8977-4bc0-b656-63b9336ac581'),
	(27,37,'en_us',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'<p>Mark Charter is a licensed agent with RE/MAX Real Estate Concepts in the state of Iowa, #B If you have received this information and are already listed with an agent, please disregard this as it is not meant to intrude on and existing seller/agent relationship.</p><p>*Full moves are available to clients that are staying within&nbsp;central Iowa.</p>',NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2015-12-20 14:56:56','2015-12-20 15:00:50','ffbbe3cf-7cc5-4fe3-9395-dff2f341f2d3'),
	(28,38,'en_us','About Mark Charter','<p>Mark started his real estate career in 2005 and has been doing it full-time since then. Over time Mark realized that the majority of real estate agents are the same. They say the same things, they market the same way, and they treat clients the same. Mark did not like this so he set about creating his own unique way of practicing real estate.</p><p>In 2014, Mark debuted a three tier pricing plan for listing clients, giving them the choice on how they want to list their home. This is another example of how Mark never accepts the status quo in real estate and is always looking to make things better for his clients.<br></p><p>Mark\'s overall goal on every deal is simple. He wants to provide such great service that after the deal is done, that client will happily provide a positive review of the transaction. This goal is what makes Mark one of the most recommended agents in Central Iowa.<br></p><p>When you add it all up, when you take into account what Mark offers clients in terms of services, marketing and care, Mark truly feels he is the best option available in the market today and he hopes you will experience this for yourself very soon!<br></p>','',NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2015-12-20 20:03:09','2016-01-16 03:57:00','3c29078b-0fd0-4d35-97a4-88e951e2cdc4'),
	(29,39,'en_us','Mark Charter',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,'Owner / Realtor','(515) 864-6444','mark@charterhouseiowa.com','<p>Mark Charter is Iowa\'s most recommended agent on sites like Trulia.com and Angieslist.com and is one of the top selling agents in central Iowa. Mark believes in constantly looking for ways to put his clients first and accomplishes this through things like short listing contracts, flexible commission plans and great perks that benefit his clients. Mark is Married to Katie and has a son named Seton and a daughter named Hollis.</p>',NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2015-12-20 20:27:05','2015-12-20 20:29:17','63e92412-b58e-4568-8251-334036c37fdc'),
	(30,40,'en_us','Amy Meyer',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,'Buyer Specialist','(515) 822-0171','amy@charterhouseiowa.com','<p>Amy Meyer is a fantastic addition to the Mark Charter Team. Talented, dedicated and hard-working, Amy\'s role is to help buyers find the perfect home they are looking for.</p>',NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2015-12-20 20:30:17','2015-12-20 20:30:17','19b9afff-1bec-48db-ac90-f7cebbcb2b68'),
	(31,41,'en_us','Nic Meyer',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,'Business Manager & Client Coordinator','(515) 371-1973','nic@charterhouseiowa.com','<p>Nic\'s role is making sure the client is happy! He helps buyers and seller with all the details of a move . . . things like lining up use of the Mark Charter Real Estate moving truck, scheduling cleaners or anything else that you might need! He is a vital part of the team and is here to help you.</p>',NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2015-12-20 20:31:04','2015-12-20 20:31:04','597311b7-9bef-4eea-ade8-12488daa270c'),
	(32,42,'en_us','Jake Boyd Photo',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,'Photographer','','','<p>Jake Boyd is Central Iowa\'s premier real estate photographer and owns Jake Boyd Photo. Jordan and Jason are also valuable members of the team as well. Mark trusts Jake Boyd Photo to shoot all of his listing photos.</p>',NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2015-12-20 20:31:22','2015-12-20 20:31:22','e06169ae-c834-4b65-a77d-cc5a8c6f0331'),
	(45,61,'en_us','Mortgage - The Tyler Osby Team','<p>Tyler Osby is where Mark turns when his clients need a mortgage or a refinance. Tyler is knowledgeable, accountable and trustworthy. He gets the job done every time!</p>','',NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,' http://www.wealthwithmortgage.com','(515) 257-6729','available',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2015-12-28 01:08:39','2016-01-16 04:19:26','2e8f7fc5-998f-4949-98dd-50a11a362d43'),
	(46,67,'en_us','House',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2016-01-06 23:58:32','2016-01-06 23:58:32','e6377421-4e98-4514-a5f1-69961109c495'),
	(47,68,'en_us','One of Mark Charter’s greatest characteristics is his giving nature.','<p>One of Mark Charter\'s greatest characteristics is his giving nature. He always goes above and beyond for his clients to make sure that their experience in buying or selling a home is first rate. While not just an experienced real estate agent, Mark is also extremely knowledgeable about the investment real estate market as well. He is a career agent that absolutely, without a doubt, has your best interests in mind.</p>',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,'Adam Carroll','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2016-01-07 00:14:03','2016-01-07 00:14:03','e73d4e08-1af1-4d78-81bb-bdb6f1c32e18'),
	(48,69,'en_us','I was so lucky to find them!','<p>Before finding Mark and Amy, I had had a lot of bad experiences with realtors. From the minute I contacted them about possibly being my realtor, I was in constant communication with them--I was never left wondering what was going on! I was paired with Amy and she is FANTASTIC. She listened to not only what I was looking for, but what I was going to use spaces for and looked for those things in the places I wanted to view. She helped me to pro and con the spaces I was looking on based on what she had heard me say about other spaces. She was in constant contact with me about setting up showings and if there was a delay in getting an answer from someone, was letting me know what the hold up was.&nbsp;<br>I have never met a more caring team to work with and when they say you\'re part of the family now, you are! As a first time home buyer, I had lots of questions both for Amy and for the seller and I got every single one answered. I never once felt like there was another goal that the team was working for other than to make sure I was happy with what I was getting! While the people are top notch, it\'s also amazing to me that you can use the moving truck for free and they had my new home cleaned before I moved in! It saved me so much time and hassle, not to mention cost. I was so lucky to find them!</p>',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,'Bethany Wilcke','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2016-01-07 00:24:07','2016-01-07 00:24:07','416f430c-685a-41d0-b3ea-3d5384f1eba3'),
	(49,70,'en_us','Mark was phenomenal to work with.','<p>Mark was phenomenal to work with during the sale of our home. His no-nonsense, tell-it-how-it is attitude is the only reason we got our home sold! We had a unique situation with our home based on the size, garage and price - Mark was the only person that told us what we needed to hear (instead of just fluffing us with what we wanted to hear). Mark is a great communicator and we always knew what was expected of us through the entire process. Thank you, Mark!</p>',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,'Jeremy Orr','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2016-01-07 00:24:35','2016-01-07 00:24:35','b36a014a-e1f9-4197-b991-136937c5c2b6'),
	(56,79,'en_us',NULL,'<p>API conformance is highly measurable and suppliers who claim it must be included in any derivative version prepared by Licensee. In the absence of Modifications or portions thereof, in both Source Code notice required by applicable law) under this License. This customary commercial license in ways explicitly requested by the Copyright Holder explicitly and prominently states near the copyright holder saying it may not copy, modify, and distribute verbatim copies of this license.</p>\r\n\r\n<p>This license has been made available. You are responsible for ensuring that the Copyright Holder provides the Work (including, but not limited to broadcasting, communication and various recording media. If any provision of this License published by Licensor. No one other than as may be filtered to exclude very small or irrelevant contributions.) This applies to any actual or alleged intellectual property rights needed, if any. Legal Entity exercising permissions granted on that web page.</p>\r\n\r\n<p><br>By copying, installing or otherwise make it clear that any Modifications thereto. Inability to Comply Due to Statute or Regulation. If it is impossible for You to the `LaTeX-format\' a program or other tangible form, a copy of the license or (at your option) c) You may not impose any terms that apply to the extent that any such warranty, support, indemnity or liability obligation is offered by You to the terms of this License Agreement. In other words, go ahead and share NetHack, but don\'t try to stop anyone else of these rights. For example, a page is available under the new version.</p>',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2016-01-12 02:03:44','2016-01-12 02:06:50','d04be563-fa47-4b76-857b-a2efa1230a1f'),
	(57,80,'en_us','Amazing Grimes home with plenty of room to roam!',NULL,'',NULL,NULL,NULL,'3.5','5',0,'507193',0,'3,209',NULL,'singleFamily','3 car garage','699,900','','UwmwWVAhRe3','1993',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,'[]',0,'2016-01-13 05:03:20','2016-01-17 16:07:12','3a8e5394-5738-4b80-a066-851581210a7b'),
	(58,83,'en_us','Buyer Hero',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,'[]',0,'2016-01-14 03:56:33','2016-01-16 00:54:24','1b5a7cfb-ec65-4f98-9e50-64d9d07bee1b'),
	(59,85,'en_us','24',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,'[]',0,'2016-01-14 04:44:02','2016-01-14 04:44:02','988dfa6f-7203-4c34-a71b-ec395c53e035'),
	(60,86,'en_us','Access',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,'[]',0,'2016-01-14 04:45:57','2016-01-14 04:45:57','bbd75462-e864-4fb7-bbc2-48a36c7a429f'),
	(61,88,'en_us','Lender',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,'[]',0,'2016-01-14 04:47:41','2016-01-14 04:47:41','f1d2c287-a0d1-42ec-a7aa-54453febb9bc'),
	(62,90,'en_us','Home1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,'[]',0,'2016-01-14 05:01:01','2016-01-14 05:01:01','37cc93d6-81b6-4677-b96b-8e01a6d552dd'),
	(63,91,'en_us','Another Awesome House!',NULL,'',NULL,NULL,NULL,'select','',0,'12345',0,'',NULL,'','','100,000','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,'[]',0,'2016-01-14 05:11:31','2016-01-17 15:58:14','be7ee4dc-32a0-43cd-b2e0-771260f99e2f'),
	(64,92,'en_us','Seller Hero',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,'[]',0,'2016-01-16 00:54:10','2016-01-16 00:54:10','85f9216d-6a30-41e7-9f89-edd58346cffe'),
	(68,98,'en_us','Tyler Osby',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,'[]',0,'2016-01-16 04:14:11','2016-01-16 04:14:11','65ad5106-5444-423e-9a3d-e87a0ba23bb9'),
	(69,99,'en_us','Tyler Osby',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,'[]',0,'2016-01-16 04:19:22','2016-01-16 04:19:22','1a6a601b-fa52-4437-9bf0-a6cdcc538288'),
	(70,100,'en_us','Pro & Go Plan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'The perfect plan to keep it simple. This seller does not require much, and likes to do things themselves. Just list with a pro...and go!','5','[{\"col1\":\"3 Month Listing\",\"col2\":\"1\"},{\"col1\":\"Full MLS Exposure\",\"col2\":\"1\"},{\"col1\":\"Professional Photos\",\"col2\":\"1\"},{\"col1\":\"Social Media Marketing\",\"col2\":\"1\"},{\"col1\":\"Sign and Lockbox\",\"col2\":\"1\"},{\"col1\":\"Facebook Targeted Marketing\",\"col2\":\"\"},{\"col1\":\"3d Matterport Virtual Tour\",\"col2\":\"\"},{\"col1\":\"Access to 15ft. Truck\",\"col2\":\"\"},{\"col1\":\"Free Pest Inspection\",\"col2\":\"\"},{\"col1\":\"$1,000 \\\"offer\\\" Guarantee!\",\"col2\":\"\"},{\"col1\":\"Free Storage\",\"col2\":\"\"},{\"col1\":\"Full Move Package\",\"col2\":\"\"}]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,'[]',0,'2016-01-17 04:05:40','2016-01-17 04:10:08','94a55f95-b527-4e0e-90ee-66c05dc62ece'),
	(71,101,'en_us','Pro Elite Plan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'For the seller who wants the very best. Maximum marketing $$$\'s + maximum convince all in one. The Elite is unrivaled in the marketplace!','7','[{\"col1\":\"3 Month Listing\",\"col2\":\"1\"},{\"col1\":\"Full MLS Exposure\",\"col2\":\"1\"},{\"col1\":\"Professional Photos\",\"col2\":\"1\"},{\"col1\":\"Social Media Marketing\",\"col2\":\"1\"},{\"col1\":\"Sign and Lockbox\",\"col2\":\"1\"},{\"col1\":\"Facebook Targeted Marketing\",\"col2\":\"1\"},{\"col1\":\"3d Matterport Virtual Tour\",\"col2\":\"1\"},{\"col1\":\"Access to 15ft. Truck\",\"col2\":\"1\"},{\"col1\":\"Free Pest Inspection\",\"col2\":\"1\"},{\"col1\":\"$1,000 \\\"offer\\\" Guarantee!\",\"col2\":\"1\"},{\"col1\":\"Free Storage\",\"col2\":\"1\"},{\"col1\":\"Full Move Package\",\"col2\":\"1\"}]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,'[]',0,'2016-01-17 04:08:41','2016-01-17 04:10:17','66437f19-de3a-4837-91b3-eb3f9f20a94b'),
	(72,102,'en_us','Pro Plus Plan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'This popular standard plan packs a punch. Great marketing and perks, all at a common commission rate in the market. Great service PLUS more!','6','[{\"col1\":\"3 Month Listing\",\"col2\":\"1\"},{\"col1\":\"Full MLS Exposure\",\"col2\":\"1\"},{\"col1\":\"Professional Photos\",\"col2\":\"1\"},{\"col1\":\"Social Media Marketing\",\"col2\":\"1\"},{\"col1\":\"Sign and Lockbox\",\"col2\":\"1\"},{\"col1\":\"Facebook Targeted Marketing\",\"col2\":\"1\"},{\"col1\":\"3d Matterport Virtual Tour\",\"col2\":\"1\"},{\"col1\":\"Access to 15ft. Truck\",\"col2\":\"1\"},{\"col1\":\"Free Pest Inspection\",\"col2\":\"1\"},{\"col1\":\"$1,000 \\\"offer\\\" Guarantee!\",\"col2\":\"1\"},{\"col1\":\"Free Storage\",\"col2\":\"\"},{\"col1\":\"Full Move Package\",\"col2\":\"\"}]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,'[]',0,'2016-01-17 04:09:48','2016-01-17 04:09:59','073bcfe6-777d-4052-9646-728c27583d7c'),
	(73,103,'en_us','Featured 1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,'[]',0,'2016-01-17 16:00:15','2016-01-17 16:00:15','e6006983-d940-4046-ac7e-a380467e1d11'),
	(74,104,'en_us','Featured 2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,'[]',0,'2016-01-17 16:00:16','2016-01-17 16:00:16','14626c12-7931-4a49-90dc-e7277d276d6e'),
	(75,105,'en_us','Featured 3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,'[]',0,'2016-01-17 16:00:16','2016-01-17 16:00:16','a9bde331-01f1-4748-a7cb-9b71a65077bf'),
	(76,106,'en_us','Featured 1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,'[]',0,'2016-01-17 16:04:44','2016-01-17 16:04:44','30624ac0-a70a-4430-a949-6d0a14e6491b'),
	(77,107,'en_us','Featured 2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,'[]',0,'2016-01-17 16:04:45','2016-01-17 16:04:45','202e8acb-60b5-48d8-92dd-df4fce5680d3'),
	(78,108,'en_us','Featured 3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,'[]',0,'2016-01-17 16:04:45','2016-01-17 16:04:45','79a9d72c-4a66-4eba-a861-1289b1cec1e6'),
	(79,109,'en_us','Featured 1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,'[]',0,'2016-01-17 16:07:07','2016-01-17 16:07:07','80a64f89-51a2-40e8-a38f-42d6cc8be0d2'),
	(80,110,'en_us','Featured 2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,'[]',0,'2016-01-17 16:07:07','2016-01-17 16:07:07','d75e1ff7-a6a6-454a-b34d-7e9e63b90553'),
	(81,111,'en_us','Featured 3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,NULL,NULL,NULL,NULL,'[]',0,'2016-01-17 16:07:08','2016-01-17 16:07:08','20cd8b55-99d1-4c02-9086-656a8e9e4b3d'),
	(100,132,'en_us','Damon Gentry','',NULL,NULL,'damonjentree@gmail.com','5153714708',NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'available',0,NULL,NULL,'Damon','Gentry',NULL,NULL,'[]',0,'2016-01-17 23:45:11','2016-01-17 23:45:11','91edf251-fad7-46fe-a33d-7307187cecf7');

/*!40000 ALTER TABLE `content` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table deprecationerrors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `deprecationerrors`;

CREATE TABLE `deprecationerrors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fingerprint` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastOccurrence` datetime NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `line` smallint(6) unsigned NOT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `templateLine` smallint(6) unsigned DEFAULT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `traces` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `deprecationerrors_key_fingerprint_unq_idx` (`key`,`fingerprint`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table elementindexsettings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `elementindexsettings`;

CREATE TABLE `elementindexsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `elementindexsettings_type_unq_idx` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `elementindexsettings` WRITE;
/*!40000 ALTER TABLE `elementindexsettings` DISABLE KEYS */;

INSERT INTO `elementindexsettings` (`id`, `type`, `settings`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Entry','{\"sources\":{\"section:4\":{\"tableAttributes\":{\"1\":\"postDate\",\"2\":\"expiryDate\",\"3\":\"link\",\"4\":\"field:11\"}}}}','2016-01-14 03:40:50','2016-01-14 03:41:23','584ea130-0ce0-4a46-91a4-2745b89da430');

/*!40000 ALTER TABLE `elementindexsettings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table elements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `elements`;

CREATE TABLE `elements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `archived` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `elements_type_idx` (`type`),
  KEY `elements_enabled_idx` (`enabled`),
  KEY `elements_archived_dateCreated_idx` (`archived`,`dateCreated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `elements` WRITE;
/*!40000 ALTER TABLE `elements` DISABLE KEYS */;

INSERT INTO `elements` (`id`, `type`, `enabled`, `archived`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'User',1,0,'2015-12-19 00:45:33','2015-12-28 00:48:24','ab9bc828-a3d1-4732-a422-baf4c5d17443'),
	(2,'Entry',1,0,'2015-12-19 00:45:35','2016-01-16 00:54:26','ad5fb7cb-8c7b-4f18-a668-c39bbf0f6f13'),
	(3,'Entry',1,0,'2015-12-19 00:45:35','2015-12-19 00:45:35','951d1a34-b1d4-4dfb-b655-98bd02e8cb4d'),
	(4,'Entry',1,0,'2015-12-19 00:52:08','2016-01-16 01:59:54','80521552-da7d-48a1-a62b-822b29d8b067'),
	(5,'Entry',1,0,'2015-12-19 00:53:06','2016-01-16 04:08:11','4282024f-cc60-4cb2-8e4f-e924a71330ad'),
	(6,'Entry',1,0,'2015-12-19 00:54:13','2016-01-16 03:49:25','4117134c-6550-4bf5-beee-68eb34d906b8'),
	(7,'Entry',1,0,'2015-12-19 00:54:52','2016-01-16 03:55:08','d2d4bb54-95a2-4c43-afc9-835649f340cb'),
	(8,'Entry',1,0,'2015-12-19 00:55:30','2015-12-19 02:41:05','83978c18-b674-4844-9131-eb04b2335c4c'),
	(9,'Entry',1,0,'2015-12-19 01:05:51','2016-01-17 22:18:51','b63bfe87-2a0c-432b-a19d-3ec53c515699'),
	(17,'Entry',1,0,'2015-12-19 02:52:17','2016-01-17 16:05:04','09576c8d-53de-4361-b8a3-c3e814554561'),
	(18,'Entry',1,0,'2015-12-19 15:30:35','2016-01-17 16:00:29','e8b9b492-dbbe-414e-8b5c-62419acf479a'),
	(19,'GlobalSet',1,0,'2015-12-19 21:06:00','2016-01-08 21:50:23','9991a733-ccf1-4ccf-9b3b-ec2fd4394352'),
	(20,'GlobalSet',1,0,'2015-12-20 04:19:31','2015-12-20 04:19:31','704536fa-c84f-4714-ba66-0b1e15ca9a27'),
	(21,'Entry',1,0,'2015-12-20 04:23:50','2015-12-20 04:26:39','1cd7a6fa-9994-4ccc-afa5-89fd9ce591f0'),
	(22,'Entry',1,0,'2015-12-20 04:39:21','2016-01-16 02:20:22','98d55f7a-979c-433d-9ff9-026c0c27f569'),
	(36,'Entry',1,0,'2015-12-20 05:00:51','2015-12-28 01:03:42','51b33d3d-7c82-41d0-8edb-b396db1e9e17'),
	(37,'GlobalSet',1,0,'2015-12-20 14:56:56','2015-12-20 15:00:50','ac6d3bf4-a7a0-45f4-b800-d73275476c36'),
	(38,'Entry',1,0,'2015-12-20 20:03:09','2016-01-16 03:57:00','f02dea5d-0678-447b-99c4-ab43e186eaec'),
	(39,'Category',1,0,'2015-12-20 20:27:05','2015-12-20 20:29:17','acd424f0-4359-4212-bc7a-73a245aeffc1'),
	(40,'Category',1,0,'2015-12-20 20:30:17','2015-12-20 20:30:17','5ae5dba9-d406-4935-86f9-9215cfe61a50'),
	(41,'Category',1,0,'2015-12-20 20:31:04','2015-12-20 20:31:04','d34801e1-0ded-4495-a238-3fd98a36a23b'),
	(42,'Category',1,0,'2015-12-20 20:31:22','2015-12-20 20:31:22','545c0ed8-7e65-4137-8924-e2e90288eb4a'),
	(55,'MatrixBlock',1,0,'2015-12-28 00:53:16','2016-01-16 03:57:00','407ae79b-ac84-4d12-ab3f-8baefbaee7d3'),
	(56,'MatrixBlock',1,0,'2015-12-28 00:55:12','2016-01-16 03:57:00','a4ac1092-66bd-46d1-96a5-faa12ee16113'),
	(57,'MatrixBlock',1,0,'2015-12-28 00:55:54','2016-01-16 03:57:00','3f141201-319a-4ae0-bc4f-b5dd674d0acc'),
	(58,'MatrixBlock',1,0,'2015-12-28 00:57:07','2016-01-16 03:57:00','63f912b2-5564-4504-ab56-c85e104f3de6'),
	(59,'MatrixBlock',1,0,'2015-12-28 00:57:53','2015-12-28 01:03:42','28d1904a-7bf8-4f84-85c3-7678094928ba'),
	(61,'Entry',1,0,'2015-12-28 01:08:39','2016-01-16 04:19:26','2eec7150-0da7-4b79-8848-33f7231faed0'),
	(62,'MatrixBlock',1,0,'2015-12-28 01:08:39','2015-12-28 01:15:49','6cd4d4ae-61ec-47b3-85fb-d9f25dcf6a8f'),
	(63,'MatrixBlock',1,0,'2015-12-28 01:27:07','2016-01-16 04:08:11','5f253cca-ffbc-4c4c-88c7-4b0279bc8c89'),
	(65,'MatrixBlock',1,0,'2015-12-28 01:29:55','2016-01-17 16:05:04','79dd0ef7-5c33-463a-a42f-f7b7a33fe3ab'),
	(66,'MatrixBlock',1,0,'2015-12-28 01:30:06','2016-01-17 16:00:29','77d70e0e-2d2c-4087-9b3e-38eadd709101'),
	(67,'Asset',1,0,'2016-01-06 23:58:32','2016-01-06 23:58:32','176208cf-0130-4acc-8175-34214ab80e24'),
	(68,'Category',1,0,'2016-01-07 00:14:03','2016-01-07 00:14:03','44c6a8b3-f10a-4a50-8353-aa3ce926607a'),
	(69,'Category',1,0,'2016-01-07 00:24:07','2016-01-07 00:24:07','2cb813dc-052a-4561-a7b4-f094ba5d3c6a'),
	(70,'Category',1,0,'2016-01-07 00:24:35','2016-01-07 00:24:35','408d84e4-e10e-4d63-b6d4-15b57110dbb8'),
	(79,'GlobalSet',1,0,'2016-01-12 02:03:44','2016-01-12 02:06:50','364bd11e-87d7-4263-8090-71713048883b'),
	(80,'Entry',1,0,'2016-01-13 05:03:20','2016-01-17 16:07:12','16d3b0aa-f6eb-4e6a-95a3-7a410a14cd6f'),
	(81,'MatrixBlock',1,0,'2016-01-13 05:03:20','2016-01-17 16:07:12','6dfe7e88-fcef-47ad-b55f-f5b93a766813'),
	(82,'MatrixBlock',1,0,'2016-01-13 05:05:40','2016-01-17 16:07:12','72042861-fc95-4990-a53a-05d14f4228f5'),
	(83,'Asset',1,0,'2016-01-14 03:56:33','2016-01-16 00:54:24','f659b30c-520d-4380-a8c6-b2d728baa3f2'),
	(84,'MatrixBlock',1,0,'2016-01-14 04:40:36','2016-01-14 04:48:11','2c7a3ee8-fe44-4d8a-9532-adb23a064e7d'),
	(85,'Asset',1,0,'2016-01-14 04:44:02','2016-01-14 04:44:02','b92f17fa-d8e1-4d64-a2dc-e107c3011820'),
	(86,'Asset',1,0,'2016-01-14 04:45:57','2016-01-14 04:45:57','a7a069ec-a135-41e9-9692-11292a289d73'),
	(87,'MatrixBlock',1,0,'2016-01-14 04:46:00','2016-01-14 04:48:11','8a9a8c0b-cc87-43d2-995b-af648766ec40'),
	(88,'Asset',1,0,'2016-01-14 04:47:41','2016-01-14 04:47:41','cdd9ad8b-b80e-473f-9566-a9051624973d'),
	(89,'MatrixBlock',1,0,'2016-01-14 04:47:44','2016-01-14 04:48:11','319cf89f-dc3d-42b0-85f4-c856015afe6f'),
	(90,'Asset',1,0,'2016-01-14 05:01:01','2016-01-14 05:01:01','d936330d-006d-4e6b-a717-ffef1a04563c'),
	(91,'Entry',1,0,'2016-01-14 05:11:31','2016-01-17 15:58:14','2c932ef2-27a1-4a65-8e4b-e4b153c5afec'),
	(92,'Asset',1,0,'2016-01-16 00:54:10','2016-01-16 00:54:10','677250a5-323f-409d-9e46-9140a31860c5'),
	(93,'MatrixBlock',1,0,'2016-01-16 02:15:19','2016-01-16 02:20:22','a2575bb6-3c13-480c-90c9-ff41122c7c25'),
	(94,'MatrixBlock',1,0,'2016-01-16 02:15:19','2016-01-16 02:20:22','b8b2eb87-df0d-469c-92b4-c0be5da85955'),
	(98,'Asset',1,0,'2016-01-16 04:14:11','2016-01-16 04:14:11','f15eebb6-2d3f-4fba-b623-c896db5b5781'),
	(99,'Asset',1,0,'2016-01-16 04:19:22','2016-01-16 04:19:22','535b3bec-eba5-49b3-98b6-9bdfc3b36973'),
	(100,'Category',1,0,'2016-01-17 04:05:40','2016-01-17 04:10:08','01d0187d-5524-48d2-b95b-c2a18b8e9a38'),
	(101,'Category',1,0,'2016-01-17 04:08:41','2016-01-17 04:10:17','587a6d5a-442e-4f4f-8f14-1bafe019ae4a'),
	(102,'Category',1,0,'2016-01-17 04:09:48','2016-01-17 04:09:59','f50ed2c9-339a-4bb3-9d25-9a4614ffcbe5'),
	(103,'Asset',1,0,'2016-01-17 16:00:15','2016-01-17 16:00:15','2f694086-39bf-4e69-87cb-2d3ccb80f764'),
	(104,'Asset',1,0,'2016-01-17 16:00:15','2016-01-17 16:00:15','efe1009d-d9d4-44e9-bdff-7d5c7bb925d1'),
	(105,'Asset',1,0,'2016-01-17 16:00:16','2016-01-17 16:00:16','0ea6a207-4d8a-4043-aa93-b56c43747b96'),
	(106,'Asset',1,0,'2016-01-17 16:04:44','2016-01-17 16:04:44','b8fb18ba-b81c-4196-a0cc-33d89f05a152'),
	(107,'Asset',1,0,'2016-01-17 16:04:45','2016-01-17 16:04:45','f266e1c0-a0de-4608-966e-268317775e1a'),
	(108,'Asset',1,0,'2016-01-17 16:04:45','2016-01-17 16:04:45','ec1ce59e-3cc9-45f8-818c-c47099467769'),
	(109,'Asset',1,0,'2016-01-17 16:07:07','2016-01-17 16:07:07','26d0683b-ea2b-494e-a37f-398983589e76'),
	(110,'Asset',1,0,'2016-01-17 16:07:07','2016-01-17 16:07:07','d424316d-5b5e-4268-a878-13ec7da15f14'),
	(111,'Asset',1,0,'2016-01-17 16:07:08','2016-01-17 16:07:08','c85b4ae1-4ebf-4502-8bc1-48abf65a756f'),
	(112,'MatrixBlock',1,0,'2016-01-17 22:16:03','2016-01-17 22:18:51','6e1445fb-cf80-4ba9-920f-446d6f95edda'),
	(113,'MatrixBlock',1,0,'2016-01-17 22:18:51','2016-01-17 22:18:51','55d449bc-3c6b-4f54-b5f2-5ba9d995e559'),
	(132,'Entry',1,0,'2016-01-17 23:45:11','2016-01-17 23:45:11','9944710d-d246-40b7-a007-39ca1f20af94');

/*!40000 ALTER TABLE `elements` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table elements_i18n
# ------------------------------------------------------------

DROP TABLE IF EXISTS `elements_i18n`;

CREATE TABLE `elements_i18n` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `elements_i18n_elementId_locale_unq_idx` (`elementId`,`locale`),
  UNIQUE KEY `elements_i18n_uri_locale_unq_idx` (`uri`,`locale`),
  KEY `elements_i18n_slug_locale_idx` (`slug`,`locale`),
  KEY `elements_i18n_enabled_idx` (`enabled`),
  KEY `elements_i18n_locale_fk` (`locale`),
  CONSTRAINT `elements_i18n_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `elements_i18n_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `elements_i18n` WRITE;
/*!40000 ALTER TABLE `elements_i18n` DISABLE KEYS */;

INSERT INTO `elements_i18n` (`id`, `elementId`, `locale`, `slug`, `uri`, `enabled`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'en_us','',NULL,1,'2015-12-19 00:45:33','2015-12-28 00:48:24','af26eb21-c99e-4a2b-bf09-26edd196cf21'),
	(2,2,'en_us','homepage','__home__',1,'2015-12-19 00:45:35','2016-01-16 00:54:26','f1e8c5c7-3dba-4e4c-bbca-b28472687691'),
	(3,3,'en_us','we-just-installed-craft','news/2015/we-just-installed-craft',1,'2015-12-19 00:45:35','2015-12-19 00:45:35','9350db08-b003-48c5-8364-30e0ea306b2d'),
	(4,4,'en_us','properties','listings',1,'2015-12-19 00:52:08','2016-01-16 01:59:54','a394b53a-7672-4074-a438-1367490dd540'),
	(5,5,'en_us','partners','partners',1,'2015-12-19 00:53:06','2016-01-16 04:08:11','fe405578-0f43-460b-8ad1-7930c9315f4f'),
	(6,6,'en_us','team','team',1,'2015-12-19 00:54:13','2016-01-16 03:49:25','2c21a83e-5b84-4e8d-beb9-0d13fb461fd8'),
	(7,7,'en_us','reviews','reviews',1,'2015-12-19 00:54:52','2016-01-16 03:55:08','ec85b961-a63a-49b9-ba85-cdbc11c35c7f'),
	(8,8,'en_us','reservation-success','truck/reserved',1,'2015-12-19 00:55:30','2015-12-19 02:41:05','a188cdb0-fc88-4857-8eba-2f4cee56af1a'),
	(9,9,'en_us','truck','truck',1,'2015-12-19 01:05:51','2016-01-17 22:18:51','5aead5c8-c135-4cbc-8aac-5059b16878f7'),
	(17,17,'en_us','504907','listings/504907',1,'2015-12-19 02:52:17','2016-01-17 16:05:04','e4fa6a6b-4f34-4c8d-acda-06efe696fc96'),
	(18,18,'en_us','506142','listings/506142',1,'2015-12-19 15:30:35','2016-01-17 16:00:29','ef41d744-2283-4beb-ba71-30715fb68df8'),
	(19,19,'en_us','',NULL,1,'2015-12-19 21:06:00','2016-01-08 21:50:23','991ad00a-891b-4892-b1ba-e6008a40e49d'),
	(20,20,'en_us','',NULL,1,'2015-12-20 04:19:31','2015-12-20 04:19:31','fd7f7628-d347-407e-be76-fb5b5e3dfda8'),
	(21,21,'en_us','contact','contact',1,'2015-12-20 04:23:50','2015-12-20 04:26:39','951b03c9-14b5-4be2-89cd-f8f06c158ef8'),
	(22,22,'en_us','ways-to-sell','sellers',1,'2015-12-20 04:39:21','2016-01-16 02:20:22','cc121041-bac3-4d5e-8b58-299e21e7ec2e'),
	(36,36,'en_us','buyers-sellers','buyers-and-sellers',1,'2015-12-20 05:00:51','2015-12-28 01:03:42','f42aaf3f-af71-4a75-9dae-7ba604e2d68b'),
	(37,37,'en_us','',NULL,1,'2015-12-20 14:56:56','2015-12-20 15:00:50','4004a54f-5b0a-46e1-b248-3038f5fb5d24'),
	(38,38,'en_us','about','about',1,'2015-12-20 20:03:09','2016-01-16 03:57:00','9365084e-5d82-4dab-9b08-879771588aee'),
	(39,39,'en_us','mark-charter',NULL,1,'2015-12-20 20:27:05','2015-12-20 20:29:17','e6b12353-9da3-4c1f-ac51-797f8dc25d97'),
	(40,40,'en_us','amy-meyer',NULL,1,'2015-12-20 20:30:17','2015-12-20 20:30:18','02448b30-e3b1-4ea1-a3c6-fe88f4c2b3f9'),
	(41,41,'en_us','nic-meyer',NULL,1,'2015-12-20 20:31:04','2015-12-20 20:31:05','8dce9b00-f04d-49ca-9235-62c58f0909d7'),
	(42,42,'en_us','jake-boyd-photo',NULL,1,'2015-12-20 20:31:22','2015-12-20 20:31:23','129def54-ce0d-4113-bfcf-41e2772e0d9b'),
	(55,55,'en_us','',NULL,1,'2015-12-28 00:53:16','2016-01-16 03:57:00','7f491772-f745-4ec6-8bf9-82f91f7b7399'),
	(56,56,'en_us','',NULL,1,'2015-12-28 00:55:12','2016-01-16 03:57:00','882da4bb-46b7-48fe-b87c-7f5e8e2af0a3'),
	(57,57,'en_us','',NULL,1,'2015-12-28 00:55:54','2016-01-16 03:57:00','5c53a104-7ae6-47c7-a732-5095f5b22d15'),
	(58,58,'en_us','',NULL,1,'2015-12-28 00:57:07','2016-01-16 03:57:00','b23e4287-e3a7-440e-90fb-ca28f38e5888'),
	(59,59,'en_us','',NULL,1,'2015-12-28 00:57:53','2015-12-28 01:03:42','dea57ef1-147d-4093-85f7-c8784f95cbb2'),
	(61,61,'en_us','mortgage-the-tyler-osby-team',NULL,1,'2015-12-28 01:08:39','2016-01-16 04:19:26','63d70650-6588-4383-99cf-8829df6253e9'),
	(62,62,'en_us','',NULL,1,'2015-12-28 01:08:39','2015-12-28 01:15:49','b8977253-ba1f-46ff-a06d-70be5e957f68'),
	(63,63,'en_us','',NULL,1,'2015-12-28 01:27:07','2016-01-16 04:08:11','7727fd40-66ec-4c35-8f65-98db0ffb9ac4'),
	(65,65,'en_us','',NULL,1,'2015-12-28 01:29:55','2016-01-17 16:05:04','529fa7c7-0814-45a3-abb5-4866b5fd0329'),
	(66,66,'en_us','',NULL,1,'2015-12-28 01:30:06','2016-01-17 16:00:29','9a5b5f56-c46d-4796-bdce-1b8c1ecbd875'),
	(67,67,'en_us','house',NULL,1,'2016-01-06 23:58:32','2016-01-06 23:58:32','f330c42b-0b7d-4f42-bb1a-295065d5dc78'),
	(68,68,'en_us','one-of-mark-charters-greatest-characteristics-is-his-giving-nature',NULL,1,'2016-01-07 00:14:03','2016-01-07 00:14:04','7260e1bc-3ab1-4a4b-9068-d4db4762f497'),
	(69,69,'en_us','i-was-so-lucky-to-find-them',NULL,1,'2016-01-07 00:24:07','2016-01-07 00:24:08','936cfa47-6f24-411f-9aa8-042a4b5ca161'),
	(70,70,'en_us','mark-was-phenomenal-to-work-with',NULL,1,'2016-01-07 00:24:35','2016-01-07 00:24:35','1521b691-c4d0-44f6-8fdd-872c1064e286'),
	(79,79,'en_us','',NULL,1,'2016-01-12 02:03:44','2016-01-12 02:06:50','ff0c9fc7-0b91-4fe2-bc5c-f47cf6c35806'),
	(80,80,'en_us','507193','listings/507193',1,'2016-01-13 05:03:20','2016-01-17 16:07:12','3b2d8667-a847-4b7b-8122-9bdcfbf5d032'),
	(81,81,'en_us','',NULL,1,'2016-01-13 05:03:20','2016-01-17 16:07:12','a32cf090-b29e-495c-9ab6-83d079266bd2'),
	(82,82,'en_us','',NULL,1,'2016-01-13 05:05:40','2016-01-17 16:07:12','5f4b01da-95d5-496e-9bf7-e81d77a5c0d4'),
	(83,83,'en_us','hero',NULL,1,'2016-01-14 03:56:33','2016-01-16 00:54:24','620cad86-cf3c-4cd7-b012-1f425d48e686'),
	(84,84,'en_us','',NULL,1,'2016-01-14 04:40:36','2016-01-14 04:48:11','06085d21-208d-4e66-b5d2-a6742e10ba40'),
	(85,85,'en_us','24',NULL,1,'2016-01-14 04:44:02','2016-01-14 04:44:02','e74f982a-4e88-40db-8060-fac7e2ff9215'),
	(86,86,'en_us','access',NULL,1,'2016-01-14 04:45:57','2016-01-14 04:45:57','f10d3c84-6afb-4417-a4cf-324c1df03d69'),
	(87,87,'en_us','',NULL,1,'2016-01-14 04:46:00','2016-01-14 04:48:11','f2e3b16c-599c-4bf4-93ac-863089dc6fad'),
	(88,88,'en_us','lender',NULL,1,'2016-01-14 04:47:41','2016-01-14 04:47:41','7a2e4aab-d598-4ab5-b86b-9ce17b0bfe24'),
	(89,89,'en_us','',NULL,1,'2016-01-14 04:47:44','2016-01-14 04:48:11','b896921c-c897-460e-b853-7dd4d0a5e36f'),
	(90,90,'en_us','home1',NULL,1,'2016-01-14 05:01:01','2016-01-14 05:01:01','3eaee4a5-7b2c-4de1-9da1-3f5424806ffd'),
	(91,91,'en_us','another-awesome-house','listings/12345',1,'2016-01-14 05:11:31','2016-01-17 15:58:14','5c4723e4-e3d3-4677-884d-d549ad8ada1d'),
	(92,92,'en_us','seller-hero',NULL,1,'2016-01-16 00:54:10','2016-01-16 00:54:10','46a23464-1521-4512-ac21-dbba28a1ea6f'),
	(93,93,'en_us','',NULL,1,'2016-01-16 02:15:19','2016-01-16 02:20:22','bbcde420-b588-4e8a-8d86-c027d85b146e'),
	(94,94,'en_us','',NULL,1,'2016-01-16 02:15:19','2016-01-16 02:20:22','e4544704-a5db-4580-9fdb-fb70e57a0d88'),
	(98,98,'en_us','tyler-osby',NULL,1,'2016-01-16 04:14:11','2016-01-16 04:14:11','7725f407-24ab-4b8e-b6b6-9c91dbf655db'),
	(99,99,'en_us','tyler-osby',NULL,1,'2016-01-16 04:19:22','2016-01-16 04:19:22','f93b7ac1-27ec-4e8a-9678-d33f4bbb96aa'),
	(100,100,'en_us','pro-go-plan',NULL,1,'2016-01-17 04:05:40','2016-01-17 04:10:08','911c578b-7f65-4547-bcfd-a74a589b41b2'),
	(101,101,'en_us','pro-elite-plan',NULL,1,'2016-01-17 04:08:41','2016-01-17 04:10:17','692bd6b8-d4da-4b77-95b8-71cb12abf274'),
	(102,102,'en_us','pro-plus-plan',NULL,1,'2016-01-17 04:09:48','2016-01-17 04:09:59','837fb29c-8803-4758-97df-3ffc1d0d0f7b'),
	(103,103,'en_us','featured-1',NULL,1,'2016-01-17 16:00:15','2016-01-17 16:00:15','ad13dfe7-3bca-4b9d-8b36-492204f864e3'),
	(104,104,'en_us','featured-2',NULL,1,'2016-01-17 16:00:16','2016-01-17 16:00:16','b119fd89-ba0f-48a7-90df-02819a466c89'),
	(105,105,'en_us','featured-3',NULL,1,'2016-01-17 16:00:16','2016-01-17 16:00:16','96432b7b-86a0-4d72-928f-d17a520d945a'),
	(106,106,'en_us','featured-1',NULL,1,'2016-01-17 16:04:44','2016-01-17 16:04:44','ac003cf2-39cc-4f59-817e-2e2c21404009'),
	(107,107,'en_us','featured-2',NULL,1,'2016-01-17 16:04:45','2016-01-17 16:04:45','a8ed8afb-d38c-4ed9-a12f-0d7763e60bae'),
	(108,108,'en_us','featured-3',NULL,1,'2016-01-17 16:04:45','2016-01-17 16:04:45','d31a6e6b-e846-4f84-b8ba-2f09ca8efd27'),
	(109,109,'en_us','featured-1',NULL,1,'2016-01-17 16:07:07','2016-01-17 16:07:07','353c748f-aa95-48da-a796-10f98892d9d4'),
	(110,110,'en_us','featured-2',NULL,1,'2016-01-17 16:07:07','2016-01-17 16:07:07','affa7a75-ec60-4940-ac32-2c682aabba20'),
	(111,111,'en_us','featured-3',NULL,1,'2016-01-17 16:07:08','2016-01-17 16:07:08','1441ff91-67d2-4f6d-ac21-69069fdeb346'),
	(112,112,'en_us','',NULL,1,'2016-01-17 22:16:03','2016-01-17 22:18:51','7a3bdc64-2913-4d14-8aeb-87de48e43a09'),
	(113,113,'en_us','',NULL,1,'2016-01-17 22:18:51','2016-01-17 22:18:51','4a4a57b1-8ca0-49be-87d6-52af34c6031e'),
	(132,132,'en_us','damon-gentry','reservation/confirmation/damon-gentry',1,'2016-01-17 23:45:11','2016-01-17 23:45:29','11414ea0-97c4-4dcc-b29f-3a9e1478aa90');

/*!40000 ALTER TABLE `elements_i18n` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table emailmessages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `emailmessages`;

CREATE TABLE `emailmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` char(150) COLLATE utf8_unicode_ci NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `emailmessages_key_locale_unq_idx` (`key`,`locale`),
  KEY `emailmessages_locale_fk` (`locale`),
  CONSTRAINT `emailmessages_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table entries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `entries`;

CREATE TABLE `entries` (
  `id` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `typeId` int(11) DEFAULT NULL,
  `authorId` int(11) DEFAULT NULL,
  `postDate` datetime DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `entries_sectionId_idx` (`sectionId`),
  KEY `entries_typeId_idx` (`typeId`),
  KEY `entries_postDate_idx` (`postDate`),
  KEY `entries_expiryDate_idx` (`expiryDate`),
  KEY `entries_authorId_fk` (`authorId`),
  CONSTRAINT `entries_authorId_fk` FOREIGN KEY (`authorId`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entries_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entries_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entries_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `entrytypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `entries` WRITE;
/*!40000 ALTER TABLE `entries` DISABLE KEYS */;

INSERT INTO `entries` (`id`, `sectionId`, `typeId`, `authorId`, `postDate`, `expiryDate`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(2,1,NULL,NULL,'2015-12-19 00:45:35',NULL,'2015-12-19 00:45:35','2016-01-16 00:54:26','1a78c8ca-dc4c-4ce8-a652-13e1558b79df'),
	(3,2,2,1,'2015-12-19 00:45:35',NULL,'2015-12-19 00:45:35','2015-12-19 00:45:35','abe1e365-de2e-46f4-8ac0-3849f263ab37'),
	(4,5,5,NULL,'2016-01-14 04:02:29',NULL,'2015-12-19 00:52:08','2016-01-16 01:59:54','80ad2a46-0790-4108-861a-5b18dd42ee28'),
	(5,6,NULL,NULL,'2015-12-19 00:53:06',NULL,'2015-12-19 00:53:06','2016-01-16 04:08:11','1af7d165-ef79-4b2a-a9b0-b92c71d0f1b5'),
	(6,7,NULL,NULL,'2015-12-19 00:54:13',NULL,'2015-12-19 00:54:13','2016-01-16 03:49:25','d59cec94-a02b-4808-8325-77c1f466621d'),
	(7,8,8,NULL,'2016-01-14 05:17:45',NULL,'2015-12-19 00:54:52','2016-01-16 03:55:08','3a58ec4b-87df-4bf7-9b78-f681af7cb3a5'),
	(8,9,NULL,NULL,'2015-12-19 00:55:30',NULL,'2015-12-19 00:55:30','2015-12-19 02:41:05','bb3c93c6-0089-4c71-9d0d-b8d608a1768e'),
	(9,10,NULL,NULL,'2015-12-19 01:05:51',NULL,'2015-12-19 01:05:51','2016-01-17 22:18:51','8053aa92-b6dd-4d44-9e01-7ae1d1d31af5'),
	(17,4,4,1,'2015-12-19 02:52:00',NULL,'2015-12-19 02:52:17','2016-01-17 16:05:04','affd115c-2d4a-428b-829f-b27ff990422f'),
	(18,4,4,1,'2015-12-19 15:30:00',NULL,'2015-12-19 15:30:35','2016-01-17 16:00:29','70be5c17-3380-4563-a0fb-37241b9a0870'),
	(21,12,NULL,NULL,'2015-12-20 04:23:50',NULL,'2015-12-20 04:23:50','2015-12-20 04:26:39','52f3e974-668e-4110-b5ce-c445e1f5f2f7'),
	(22,13,13,NULL,'2016-01-16 02:10:30',NULL,'2015-12-20 04:39:21','2016-01-16 02:20:22','9115c449-c946-4407-92cb-1f63bd078782'),
	(36,14,NULL,NULL,'2015-12-20 05:00:50',NULL,'2015-12-20 05:00:51','2015-12-28 01:03:42','2dc853ac-8943-495c-82ba-55f9f93f869d'),
	(38,15,NULL,NULL,'2015-12-20 20:03:09',NULL,'2015-12-20 20:03:09','2016-01-16 03:57:00','ccb9f7c9-3981-4b80-9997-9de6d046c086'),
	(61,16,16,1,'2015-12-28 01:08:00',NULL,'2015-12-28 01:08:39','2016-01-16 04:19:26','661d258f-3ee0-4262-963b-ab1179bf0486'),
	(80,4,4,1,'2016-01-13 05:03:00',NULL,'2016-01-13 05:03:20','2016-01-17 16:07:12','5cf03a72-a4ff-434e-944b-99fcf075d134'),
	(91,4,4,1,'2016-01-14 05:11:00',NULL,'2016-01-14 05:11:31','2016-01-16 02:03:09','1fdba93b-6f9e-4c88-96b9-07c8c599531d'),
	(132,3,3,NULL,'2016-01-17 23:45:11','2016-01-18 23:45:11','2016-01-17 23:45:11','2016-01-17 23:45:11','6e2e7fcb-09a8-4733-9f49-9856a7f96bfd');

/*!40000 ALTER TABLE `entries` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table entrydrafts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `entrydrafts`;

CREATE TABLE `entrydrafts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entryId` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `creatorId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notes` tinytext COLLATE utf8_unicode_ci,
  `data` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `entrydrafts_entryId_locale_idx` (`entryId`,`locale`),
  KEY `entrydrafts_sectionId_fk` (`sectionId`),
  KEY `entrydrafts_creatorId_fk` (`creatorId`),
  KEY `entrydrafts_locale_fk` (`locale`),
  CONSTRAINT `entrydrafts_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entrydrafts_entryId_fk` FOREIGN KEY (`entryId`) REFERENCES `entries` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entrydrafts_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `entrydrafts_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table entrytypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `entrytypes`;

CREATE TABLE `entrytypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sectionId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hasTitleField` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `titleLabel` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'Title',
  `titleFormat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sortOrder` tinyint(4) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `entrytypes_name_sectionId_unq_idx` (`name`,`sectionId`),
  UNIQUE KEY `entrytypes_handle_sectionId_unq_idx` (`handle`,`sectionId`),
  KEY `entrytypes_sectionId_fk` (`sectionId`),
  KEY `entrytypes_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `entrytypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `entrytypes_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `entrytypes` WRITE;
/*!40000 ALTER TABLE `entrytypes` DISABLE KEYS */;

INSERT INTO `entrytypes` (`id`, `sectionId`, `fieldLayoutId`, `name`, `handle`, `hasTitleField`, `titleLabel`, `titleFormat`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,148,'Homepage','homepage',1,'Page Title',NULL,NULL,'2015-12-19 00:45:35','2016-01-16 00:50:41','ad406361-f89d-4fb0-8e80-127cfbe42bd9'),
	(2,2,5,'News','news',1,'Title',NULL,NULL,'2015-12-19 00:45:35','2015-12-19 00:45:35','e6ee15f0-82d3-4231-9892-226df210b0e9'),
	(3,3,126,'Reservation','reservation',1,'Full Name',NULL,NULL,'2015-12-19 00:51:17','2016-01-12 04:17:42','a2354d94-bcf8-47d3-a679-33fae058f7d6'),
	(4,4,177,'Listing','listing',1,'Page Title',NULL,NULL,'2015-12-19 00:51:40','2016-01-16 05:22:16','a8d33aaf-73a6-4e19-8602-8054a92c44de'),
	(5,5,150,'Properties','properties',1,'Page Title',NULL,NULL,'2015-12-19 00:52:08','2016-01-16 02:00:35','baf10729-1552-4951-a280-e6265a6b54a8'),
	(6,6,175,'Partners','partners',1,'Page Title',NULL,NULL,'2015-12-19 00:53:06','2016-01-16 04:07:30','f172a8c6-ff7c-4078-889b-8b455e73a406'),
	(7,7,172,'Team','team',1,'Page Title',NULL,NULL,'2015-12-19 00:54:13','2016-01-16 03:49:06','0ce21136-40b3-4969-a3d7-12259bf8c2ef'),
	(8,8,173,'Reviews','reviews',1,'Page Title',NULL,NULL,'2015-12-19 00:54:52','2016-01-16 03:53:47','58fdb35e-2342-46ae-8c31-a49bba0a15ca'),
	(9,9,22,'Reservation Success','reservationSuccess',1,'Page Title',NULL,NULL,'2015-12-19 00:55:30','2015-12-19 01:44:56','5c6aa4e7-6a20-4884-9053-d5fd5e8877d3'),
	(10,10,178,'Truck','truck',1,'Page Title',NULL,NULL,'2015-12-19 01:05:51','2016-01-17 22:15:41','2b7dd49d-fc4f-4d09-8c92-29014ff0f093'),
	(12,12,90,'Contact','contact',1,'Page Title',NULL,NULL,'2015-12-20 04:23:50','2015-12-28 01:04:27','a6319675-048b-44fc-af89-f18e73847ee7'),
	(13,13,166,'Sellers','waysToSell',1,'Page Title',NULL,NULL,'2015-12-20 04:39:21','2016-01-16 03:37:12','1c9ac519-eec9-4290-86dc-63244df89afa'),
	(14,14,88,'Buyers & Sellers','buyersSellers',1,'Page Title',NULL,NULL,'2015-12-20 05:00:50','2015-12-28 00:58:09','a78941ff-1a23-4ae3-acff-9f28a5f0ff2c'),
	(15,15,174,'About','about',1,'Page Title',NULL,NULL,'2015-12-20 20:03:09','2016-01-16 03:56:43','ec23d35f-d212-4806-8ab6-4c8974483556'),
	(16,16,97,'Partner','partner',1,'Page Title',NULL,NULL,'2015-12-28 01:06:31','2015-12-28 01:16:04','5faf26a3-adc9-48b2-b957-202520d326b6');

/*!40000 ALTER TABLE `entrytypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table entryversions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `entryversions`;

CREATE TABLE `entryversions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entryId` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `creatorId` int(11) DEFAULT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `num` smallint(6) unsigned NOT NULL,
  `notes` tinytext COLLATE utf8_unicode_ci,
  `data` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `entryversions_entryId_locale_idx` (`entryId`,`locale`),
  KEY `entryversions_sectionId_fk` (`sectionId`),
  KEY `entryversions_creatorId_fk` (`creatorId`),
  KEY `entryversions_locale_fk` (`locale`),
  CONSTRAINT `entryversions_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `entryversions_entryId_fk` FOREIGN KEY (`entryId`) REFERENCES `entries` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entryversions_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `entryversions_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `entryversions` WRITE;
/*!40000 ALTER TABLE `entryversions` DISABLE KEYS */;

INSERT INTO `entryversions` (`id`, `entryId`, `sectionId`, `creatorId`, `locale`, `num`, `notes`, `data`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,2,1,1,'en_us',1,NULL,'{\"typeId\":\"1\",\"authorId\":null,\"title\":\"Homepage\",\"slug\":\"homepage\",\"postDate\":1450485935,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":[]}','2015-12-19 00:45:35','2015-12-19 00:45:35','40a987ce-14ba-4a6b-955e-3c6a38422e18'),
	(2,2,1,1,'en_us',2,NULL,'{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.charterhouseiowa.dev!\",\"slug\":\"homepage\",\"postDate\":1450485935,\"expiryDate\":null,\"enabled\":\"1\",\"parentId\":null,\"fields\":{\"1\":\"<p>It\\u2019s true, this site doesn\\u2019t have a whole lot of content yet, but don\\u2019t worry. Our web developers have just installed the CMS, and they\\u2019re setting things up for the content editors this very moment. Soon Local.charterhouseiowa.dev will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.<\\/p>\"}}','2015-12-19 00:45:35','2015-12-19 00:45:35','3f461505-42e8-4114-89a4-bde4576a633f'),
	(3,3,2,1,'en_us',1,NULL,'{\"typeId\":\"2\",\"authorId\":\"1\",\"title\":\"We just installed Craft!\",\"slug\":\"we-just-installed-craft\",\"postDate\":1450485935,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":[]}','2015-12-19 00:45:35','2015-12-19 00:45:35','f6debf29-52d7-477f-a777-c8f952d02d93'),
	(4,4,5,1,'en_us',1,NULL,'{\"typeId\":\"5\",\"authorId\":null,\"title\":\"Properties\",\"slug\":\"properties\",\"postDate\":1450486328,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":[]}','2015-12-19 00:52:08','2015-12-19 00:52:08','13cb317d-e5f5-4cfb-8c9a-846b43cfd914'),
	(5,5,6,1,'en_us',1,NULL,'{\"typeId\":\"6\",\"authorId\":null,\"title\":\"Partners\",\"slug\":\"partners\",\"postDate\":1450486386,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":[]}','2015-12-19 00:53:06','2015-12-19 00:53:06','fd35ec51-01a5-44e6-aa7f-1b85b5fbf93b'),
	(6,6,7,1,'en_us',1,NULL,'{\"typeId\":\"7\",\"authorId\":null,\"title\":\"Team\",\"slug\":\"team\",\"postDate\":1450486453,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":[]}','2015-12-19 00:54:13','2015-12-19 00:54:13','0c5cf14d-5ff7-466f-aba7-c142cef411dc'),
	(7,7,8,1,'en_us',1,NULL,'{\"typeId\":\"8\",\"authorId\":null,\"title\":\"Reviews\",\"slug\":\"reviews\",\"postDate\":1450486492,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":[]}','2015-12-19 00:54:52','2015-12-19 00:54:52','0f2e160a-4b2d-48dd-a565-fba9d33757fe'),
	(8,8,9,1,'en_us',1,NULL,'{\"typeId\":\"9\",\"authorId\":null,\"title\":\"Reservation Success\",\"slug\":\"reservation-success\",\"postDate\":1450486530,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":[]}','2015-12-19 00:55:30','2015-12-19 00:55:30','49838030-29df-4c17-9f5d-cb263e1760d5'),
	(9,9,10,1,'en_us',1,NULL,'{\"typeId\":\"10\",\"authorId\":null,\"title\":\"Truck\",\"slug\":\"truck\",\"postDate\":1450487151,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":[]}','2015-12-19 01:05:51','2015-12-19 01:05:51','203b38d8-9cd6-485d-b467-0b78d97c3159'),
	(12,2,1,1,'en_us',3,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Mark Charter Realty\",\"slug\":\"homepage\",\"postDate\":1450485935,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>It\\u2019s true, this site doesn\\u2019t have a whole lot of content yet, but don\\u2019t worry. Our web developers have just installed the CMS, and they\\u2019re setting things up for the content editors this very moment. Soon Local.charterhouseiowa.dev will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.<\\/p>\",\"3\":\"\"}}','2015-12-19 01:46:32','2015-12-19 01:46:32','c68de794-9c7d-4a25-91bb-9e57457b6c5a'),
	(13,4,5,1,'en_us',2,'','{\"typeId\":\"5\",\"authorId\":null,\"title\":\"Properties\",\"slug\":\"properties\",\"postDate\":1450486881,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>It\\u2019s true, this site doesn\\u2019t have a whole lot of content yet, but don\\u2019t worry. Our web developers have just installed the CMS, and they\\u2019re setting things up for the content editors this very moment. Soon Local.charterhouseiowa.dev will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.<\\/p>\",\"3\":\"\"}}','2015-12-19 01:46:42','2015-12-19 01:46:42','ecf7f226-3abb-469f-8d13-66e26ed750a1'),
	(14,6,7,1,'en_us',2,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Team\",\"slug\":\"team\",\"postDate\":1450486453,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>It\\u2019s true, this site doesn\\u2019t have a whole lot of content yet, but don\\u2019t worry. Our web developers have just installed the CMS, and they\\u2019re setting things up for the content editors this very moment. Soon Local.charterhouseiowa.dev will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.<\\/p>\",\"3\":\"\"}}','2015-12-19 01:46:50','2015-12-19 01:46:50','e4710c54-58d6-4fc4-a56a-c87da5dc0dbb'),
	(15,7,8,1,'en_us',2,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Reviews\",\"slug\":\"reviews\",\"postDate\":1450486492,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>It\\u2019s true, this site doesn\\u2019t have a whole lot of content yet, but don\\u2019t worry. Our web developers have just installed the CMS, and they\\u2019re setting things up for the content editors this very moment. Soon Local.charterhouseiowa.dev will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.<\\/p>\",\"3\":\"\"}}','2015-12-19 01:47:00','2015-12-19 01:47:00','3bdbb7be-3230-49e6-aa9f-9ff3198765f4'),
	(16,5,6,1,'en_us',2,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Partners\",\"slug\":\"partners\",\"postDate\":1450486386,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>It\\u2019s true, this site doesn\\u2019t have a whole lot of content yet, but don\\u2019t worry. Our web developers have just installed the CMS, and they\\u2019re setting things up for the content editors this very moment. Soon Local.charterhouseiowa.dev will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.<\\/p>\",\"3\":\"\"}}','2015-12-19 01:47:10','2015-12-19 01:47:10','91f5a661-a43f-489e-8fc0-919c941251ee'),
	(17,7,8,1,'en_us',3,'','{\"typeId\":\"8\",\"authorId\":null,\"title\":\"Testimonials\",\"slug\":\"reviews\",\"postDate\":1450492004,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>It\\u2019s true, this site doesn\\u2019t have a whole lot of content yet, but don\\u2019t worry. Our web developers have just installed the CMS, and they\\u2019re setting things up for the content editors this very moment. Soon Local.charterhouseiowa.dev will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.<\\/p>\",\"3\":\"\"}}','2015-12-19 02:29:20','2015-12-19 02:29:20','21078f3a-4277-4e20-9a92-ecb3bcdf3461'),
	(25,8,9,1,'en_us',2,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Reservation Success\",\"slug\":\"reservation-success\",\"postDate\":1450486530,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>We got ya\' covered - and you got my truck.<\\/p>\",\"3\":\"\"}}','2015-12-19 02:41:05','2015-12-19 02:41:05','72396450-deee-4330-a479-660185321ddf'),
	(26,17,4,1,'en_us',1,'','{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Amazing golf course home with too many amenities to list!\",\"slug\":\"amazing-golf-course-home-with-too-many-amenities-to-list\",\"postDate\":1450493537,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"1210 NE 42nd St\",\"street2\":\"\",\"city\":\"Ankeny\",\"state\":\"IA\",\"zip\":\"50021\",\"country\":\"United States\",\"lat\":\"41.7657585\",\"lng\":\"-93.5865468\"},\"8\":\"4.5\",\"9\":\"5\",\"1\":\"<p>Stunning Ankeny home located on Otter Creek Golf Course. This 4 bed, 4.5 bath former Show Home is loaded with features and amenities from top to bottom. Try building this home today for less than a million dollars! It all starts with location. You will love cul-de-sac living, close to I35 making a commute a breeze. Step through the front door and you will be wowed by the view! There is not one, but TWO ponds directly behind the home. The outside is just as gorgeous as the inside.&nbsp;<\\/p><p>Full landscaping, flagstone entry, exterior lighting, huge patio and a wraparound deck with outdoor kitchen truly put this home among Ankeny\'s best! Inside the quality is top notch. Beautiful knotty alder trim, Acacia hardwood, Viking appliances, and a beautiful stone fireplace are impossible to miss. The master is an oasis, with beautiful materials. Need an amazing basement? Sunken family room, barn wood floors, huge wet bar, hidden room and golf cart garage await. Too much to list, Email for all features!<\\/p>\",\"10\":\"\",\"11\":\"\",\"3\":\"\",\"12\":\"\",\"13\":\"\",\"14\":\"2,426\"}}','2015-12-19 02:52:17','2015-12-19 02:52:17','06da6f8b-92e6-4c16-9194-7eddc4385ffd'),
	(27,17,4,1,'en_us',2,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Amazing golf course home with too many amenities to list!\",\"slug\":\"amazing-golf-course-home-with-too-many-amenities-to-list\",\"postDate\":1450493520,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"1210 NE 42nd St\",\"street2\":\"\",\"city\":\"Ankeny\",\"state\":\"IA\",\"zip\":\"50021\",\"country\":\"United States\",\"lat\":\"41.76575850\",\"lng\":\"-93.58654680\"},\"8\":\"4.5\",\"9\":\"5\",\"1\":\"<p>Stunning Ankeny home located on Otter Creek Golf Course. This 4 bed, 4.5 bath former Show Home is loaded with features and amenities from top to bottom. Try building this home today for less than a million dollars! It all starts with location. You will love cul-de-sac living, close to I35 making a commute a breeze. Step through the front door and you will be wowed by the view! There is not one, but TWO ponds directly behind the home. The outside is just as gorgeous as the inside.&nbsp;<\\/p>\\r\\n\\r\\n<p>Full landscaping, flagstone entry, exterior lighting, huge patio and a wraparound deck with outdoor kitchen truly put this home among Ankeny\'s best! Inside the quality is top notch. Beautiful knotty alder trim, Acacia hardwood, Viking appliances, and a beautiful stone fireplace are impossible to miss. The master is an oasis, with beautiful materials. Need an amazing basement? Sunken family room, barn wood floors, huge wet bar, hidden room and golf cart garage await. Too much to list, Email for all features!<\\/p>\",\"10\":\"\",\"11\":\"\",\"3\":\"\",\"12\":\"\",\"13\":\"\",\"14\":\"2,426\"}}','2015-12-19 03:12:28','2015-12-19 03:12:28','d174e801-531b-4053-80f6-a9caecb75569'),
	(28,17,4,1,'en_us',3,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Amazing golf course home with too many amenities to list!\",\"slug\":\"amazing-golf-course-home-with-too-many-amenities-to-list\",\"postDate\":1450493520,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"1210 NE 42nd St\",\"street2\":\"\",\"city\":\"Ankeny\",\"state\":\"IA\",\"zip\":\"50021\",\"country\":\"United States\",\"lat\":\"41.76575850\",\"lng\":\"-93.58654680\"},\"8\":\"4.5\",\"9\":\"5\",\"1\":\"<p>Stunning Ankeny home located on Otter Creek Golf Course. This 4 bed, 4.5 bath former Show Home is loaded with features and amenities from top to bottom. Try building this home today for less than a million dollars! It all starts with location. You will love cul-de-sac living, close to I35 making a commute a breeze. Step through the front door and you will be wowed by the view! There is not one, but TWO ponds directly behind the home. The outside is just as gorgeous as the inside.&nbsp;<\\/p>\\r\\n\\r\\n<p>Full landscaping, flagstone entry, exterior lighting, huge patio and a wraparound deck with outdoor kitchen truly put this home among Ankeny\'s best! Inside the quality is top notch. Beautiful knotty alder trim, Acacia hardwood, Viking appliances, and a beautiful stone fireplace are impossible to miss. The master is an oasis, with beautiful materials. Need an amazing basement? Sunken family room, barn wood floors, huge wet bar, hidden room and golf cart garage await. Too much to list, Email for all features!<\\/p>\",\"10\":\"\",\"11\":\"\",\"3\":\"\",\"12\":\"singleFamily\",\"13\":\"\",\"14\":\"2,426\"}}','2015-12-19 03:13:24','2015-12-19 03:13:24','cbc470ae-1b57-493b-9153-aac7b70dbb1d'),
	(29,17,4,1,'en_us',4,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Amazing golf course home with too many amenities to list!\",\"slug\":\"amazing-golf-course-home-with-too-many-amenities-to-list\",\"postDate\":1450493520,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"1210 NE 42nd St\",\"street2\":\"\",\"city\":\"Ankeny\",\"state\":\"IA\",\"zip\":\"50021\",\"country\":\"United States\",\"lat\":\"41.76575850\",\"lng\":\"-93.58654680\"},\"8\":\"4.5\",\"9\":\"5\",\"1\":\"<p>Stunning Ankeny home located on Otter Creek Golf Course. This 4 bed, 4.5 bath former Show Home is loaded with features and amenities from top to bottom. Try building this home today for less than a million dollars! It all starts with location. You will love cul-de-sac living, close to I35 making a commute a breeze. Step through the front door and you will be wowed by the view! There is not one, but TWO ponds directly behind the home. The outside is just as gorgeous as the inside.&nbsp;<\\/p>\\r\\n\\r\\n<p>Full landscaping, flagstone entry, exterior lighting, huge patio and a wraparound deck with outdoor kitchen truly put this home among Ankeny\'s best! Inside the quality is top notch. Beautiful knotty alder trim, Acacia hardwood, Viking appliances, and a beautiful stone fireplace are impossible to miss. The master is an oasis, with beautiful materials. Need an amazing basement? Sunken family room, barn wood floors, huge wet bar, hidden room and golf cart garage await. Too much to list, Email for all features!<\\/p>\",\"10\":\"\",\"16\":\"http:\\/\\/www.realtor.com\\/realestateandhomes-search?mlslid=504907\",\"11\":\"504907\",\"3\":\"\",\"12\":\"singleFamily\",\"13\":\"\",\"14\":\"2,426\"}}','2015-12-19 03:15:53','2015-12-19 03:15:53','e2952804-abf0-4394-a81e-e9767ec03978'),
	(30,17,4,1,'en_us',5,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Amazing golf course home with too many amenities to list!\",\"slug\":\"amazing-golf-course-home-with-too-many-amenities-to-list\",\"postDate\":1450493520,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"1210 NE 42nd St\",\"street2\":\"\",\"city\":\"Ankeny\",\"state\":\"IA\",\"zip\":\"50021\",\"country\":\"United States\",\"lat\":\"41.76575850\",\"lng\":\"-93.58654680\"},\"8\":\"4.5\",\"9\":\"5\",\"1\":\"<p>Stunning Ankeny home located on Otter Creek Golf Course. This 4 bed, 4.5 bath former Show Home is loaded with features and amenities from top to bottom. Try building this home today for less than a million dollars! It all starts with location. You will love cul-de-sac living, close to I35 making a commute a breeze. Step through the front door and you will be wowed by the view! There is not one, but TWO ponds directly behind the home. The outside is just as gorgeous as the inside.&nbsp;<\\/p>\\r\\n\\r\\n<p>Full landscaping, flagstone entry, exterior lighting, huge patio and a wraparound deck with outdoor kitchen truly put this home among Ankeny\'s best! Inside the quality is top notch. Beautiful knotty alder trim, Acacia hardwood, Viking appliances, and a beautiful stone fireplace are impossible to miss. The master is an oasis, with beautiful materials. Need an amazing basement? Sunken family room, barn wood floors, huge wet bar, hidden room and golf cart garage await. Too much to list, Email for all features!<\\/p>\",\"10\":\"\",\"16\":\"http:\\/\\/www.realtor.com\\/realestateandhomes-search?mlslid=504907\",\"11\":\"504907\",\"3\":\"\",\"17\":\"3 Car Garage\",\"12\":\"singleFamily\",\"13\":\"\",\"14\":\"2,426\"}}','2015-12-19 03:17:12','2015-12-19 03:17:12','29be9024-cc1a-466e-af26-d1f3a8eb67da'),
	(31,17,4,1,'en_us',6,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Amazing golf course home with too many amenities to list!\",\"slug\":\"amazing-golf-course-home-with-too-many-amenities-to-list\",\"postDate\":1450493520,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"1210 NE 42nd St\",\"street2\":\"\",\"city\":\"Ankeny\",\"state\":\"IA\",\"zip\":\"50021\",\"country\":\"United States\",\"lat\":\"41.76575850\",\"lng\":\"-93.58654680\"},\"8\":\"4.5\",\"9\":\"5\",\"1\":\"<p>Stunning Ankeny home located on Otter Creek Golf Course. This 4 bed, 4.5 bath former Show Home is loaded with features and amenities from top to bottom. Try building this home today for less than a million dollars! It all starts with location. You will love cul-de-sac living, close to I35 making a commute a breeze. Step through the front door and you will be wowed by the view! There is not one, but TWO ponds directly behind the home. The outside is just as gorgeous as the inside.&nbsp;<\\/p>\\r\\n\\r\\n<p>Full landscaping, flagstone entry, exterior lighting, huge patio and a wraparound deck with outdoor kitchen truly put this home among Ankeny\'s best! Inside the quality is top notch. Beautiful knotty alder trim, Acacia hardwood, Viking appliances, and a beautiful stone fireplace are impossible to miss. The master is an oasis, with beautiful materials. Need an amazing basement? Sunken family room, barn wood floors, huge wet bar, hidden room and golf cart garage await. Too much to list, Email for all features!<\\/p>\",\"10\":\"\",\"16\":\"http:\\/\\/www.realtor.com\\/realestateandhomes-search?mlslid=504907\",\"11\":\"504907\",\"3\":\"\",\"17\":\"3 Car Garage\",\"18\":\"$849,900\",\"12\":\"singleFamily\",\"13\":\"\",\"14\":\"2,426\",\"19\":\"yZRyXkGqD2c\"}}','2015-12-19 15:13:12','2015-12-19 15:13:12','1904c559-e2db-4e4d-b070-ad01407651bd'),
	(32,18,4,1,'en_us',1,'','{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Absolutely amazing, modern rural-meets-urban set up just outside of Grimes!! A must see at $719,000!\",\"slug\":\"absolutely-amazing-modern-rural-meets-urban-set-up-just-outside-of-grimes-a-must-see-at-719-000\",\"postDate\":1450539035,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"7965 NW 142nd St\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"United States\",\"lat\":\"41.705765\",\"lng\":\"-93.813691\"},\"8\":\"select\",\"9\":\"\",\"1\":\"<p>Call your builder and call off the meeting! This beats new construction and is the house you have been waiting on to hit the market! This stunning Grimes acreage sits on 2.3 acres and is just a mile north of DCG High School. Awesome location. Pull into the long driveway and enjoy the massive front yard with trees. STUNNING curb appeal!&nbsp;<\\/p><p>This 5 bedroom (could be 6) home has a floor plan you have not seen before. The attention to detail and woodwork is stunning. Built-ins and window seats with storage are found everywhere. The master bedroom is on the main level and offers a gorgeous bath with Carrera marble, and a vey large closet. The main floor laundry is also huge. You will find a second full laundry room in the finished basement, along with a second kitchen! One of the beds is separated from the rest of the house and is perfect for an office or teen room.&nbsp;<\\/p><p>Too many amenities to list them all!&nbsp;<\\/p><ul><li>5 garage stalls<\\/li><li>Geothermal<\\/li><li>High end kitchen<\\/li><li>Covered deck<\\/li><li>So much more -&nbsp;A RARE FIND!<\\/li><\\/ul>\",\"10\":\"\",\"16\":\"\",\"11\":\"\",\"3\":\"\",\"17\":\"\",\"18\":\"$719,000\",\"12\":\"\",\"13\":\"\",\"14\":\"\",\"19\":\"\"}}','2015-12-19 15:30:35','2015-12-19 15:30:35','9a3cd9b3-ff9e-4e78-be79-aa888f1f9cf0'),
	(33,18,4,1,'en_us',2,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Absolutely amazing, modern rural-meets-urban set up just outside of Grimes!! A must see at $719,000!\",\"slug\":\"absolutely-amazing-modern-rural-meets-urban-set-up-just-outside-of-grimes-a-must-see-at-719-000\",\"postDate\":1450539000,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"7965 NW 142nd St\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"United States\",\"lat\":\"41.70576500\",\"lng\":\"-93.81369100\"},\"8\":\"select\",\"9\":\"\",\"1\":\"<p>Call your builder and call off the meeting! This beats new construction and is the house you have been waiting on to hit the market! This stunning Grimes acreage sits on 2.3 acres and is just a mile north of DCG High School. Awesome location. Pull into the long driveway and enjoy the massive front yard with trees. STUNNING curb appeal!&nbsp;<\\/p>\\r\\n\\r\\n<p>This 5 bedroom (could be 6) home has a floor plan you have not seen before. The attention to detail and woodwork is stunning. Built-ins and window seats with storage are found everywhere. The master bedroom is on the main level and offers a gorgeous bath with Carrera marble, and a vey large closet. The main floor laundry is also huge. You will find a second full laundry room in the finished basement, along with a second kitchen! One of the beds is separated from the rest of the house and is perfect for an office or teen room.&nbsp;<\\/p>\\r\\n\\r\\n<p>Too many amenities to list them all!&nbsp;<\\/p>\\r\\n\\r\\n<ul><li>5 garage stalls<\\/li><li>Geothermal<\\/li><li>High end kitchen<\\/li><li>Covered deck<\\/li><li>So much more -&nbsp;A RARE FIND!<\\/li><\\/ul>\",\"10\":\"\",\"16\":\"http:\\/\\/www.realtor.com\\/realestateandhomes-search?mlslid=506142\",\"11\":\"506142\",\"3\":\"\",\"17\":\"\",\"18\":\"$719,000\",\"12\":\"\",\"13\":\"\",\"14\":\"\",\"19\":\"\"}}','2015-12-19 15:35:53','2015-12-19 15:35:53','bd0e88a7-8986-4c12-9b65-fbe952c01f0e'),
	(34,18,4,1,'en_us',3,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Absolutely amazing, modern rural-meets-urban set up just outside of Grimes!! A must see at $719,000!\",\"slug\":\"absolutely-amazing-modern-rural-meets-urban-set-up-just-outside-of-grimes-a-must-see-at-719-000\",\"postDate\":1450539000,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"7965 NW 142nd St\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"United States\",\"lat\":\"41.70576500\",\"lng\":\"-93.81369100\"},\"8\":\"4\",\"9\":\"5\",\"1\":\"<p>Call your builder and call off the meeting! This beats new construction and is the house you have been waiting on to hit the market! This stunning Grimes acreage sits on 2.3 acres and is just a mile north of DCG High School. Awesome location. Pull into the long driveway and enjoy the massive front yard with trees. STUNNING curb appeal!&nbsp;<\\/p>\\r\\n\\r\\n<p>This 5 bedroom (could be 6) home has a floor plan you have not seen before. The attention to detail and woodwork is stunning. Built-ins and window seats with storage are found everywhere. The master bedroom is on the main level and offers a gorgeous bath with Carrera marble, and a vey large closet. The main floor laundry is also huge. You will find a second full laundry room in the finished basement, along with a second kitchen! One of the beds is separated from the rest of the house and is perfect for an office or teen room.&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>Too many amenities to list them all!&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>5 garage stalls<\\/li><li>Geothermal<\\/li><li>High end kitchen<\\/li><li>Covered deck<\\/li><li>So much more -&nbsp;A RARE FIND!<\\/li><\\/ul>\",\"10\":\"\",\"11\":\"506142\",\"3\":\"\",\"17\":\"\",\"18\":\"$719,000\",\"12\":\"singleFamily\",\"13\":\"\",\"14\":\"2,700\",\"19\":\"\"}}','2015-12-19 15:39:28','2015-12-19 15:39:28','9c19377c-0f82-4eb7-b12a-34671ced5426'),
	(35,18,4,1,'en_us',4,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Absolutely amazing, modern rural-meets-urban set up just outside of Grimes!! A must see at $719,000!\",\"slug\":\"absolutely-amazing-modern-rural-meets-urban-set-up-just-outside-of-grimes-a-must-see-at-719-000\",\"postDate\":1450539000,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"7965 NW 142nd St\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"United States\",\"lat\":\"41.70576500\",\"lng\":\"-93.81369100\"},\"8\":\"4\",\"9\":\"5\",\"1\":\"<p>Call your builder and call off the meeting! This beats new construction and is the house you have been waiting on to hit the market! This stunning Grimes acreage sits on 2.3 acres and is just a mile north of DCG High School. Awesome location. Pull into the long driveway and enjoy the massive front yard with trees. STUNNING curb appeal!&nbsp;<\\/p>\\r\\n\\r\\n<p>This 5 bedroom (could be 6) home has a floor plan you have not seen before. The attention to detail and woodwork is stunning. Built-ins and window seats with storage are found everywhere. The master bedroom is on the main level and offers a gorgeous bath with Carrera marble, and a vey large closet. The main floor laundry is also huge. You will find a second full laundry room in the finished basement, along with a second kitchen! One of the beds is separated from the rest of the house and is perfect for an office or teen room.&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Too many amenities to list them all!&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>5 garage stalls<\\/li><li>Geothermal<\\/li><li>High end kitchen<\\/li><li>Covered deck<\\/li><li>So much more -&nbsp;A RARE FIND!<\\/li><\\/ul>\",\"10\":\"\",\"11\":\"506142\",\"3\":\"\",\"17\":\"3 car attached garage - 2 car detached garage\",\"18\":\"$719,000\",\"12\":\"singleFamily\",\"13\":\"\",\"14\":\"2,700\",\"19\":\"\"}}','2015-12-19 15:41:11','2015-12-19 15:41:11','08448998-b7e0-4513-baf9-ac39e6e6f2a3'),
	(36,18,4,1,'en_us',5,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Absolutely amazing, modern rural-meets-urban set up just outside of Grimes!! A must see at $719,000!\",\"slug\":\"absolutely-amazing-modern-rural-meets-urban-set-up-just-outside-of-grimes\",\"postDate\":1450539000,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"7965 NW 142nd St\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"United States\",\"lat\":\"41.70576500\",\"lng\":\"-93.81369100\"},\"8\":\"4\",\"9\":\"5\",\"1\":\"<p>Call your builder and call off the meeting! This beats new construction and is the house you have been waiting on to hit the market! This stunning Grimes acreage sits on 2.3 acres and is just a mile north of DCG High School. Awesome location. Pull into the long driveway and enjoy the massive front yard with trees. STUNNING curb appeal!&nbsp;<\\/p>\\r\\n\\r\\n<p>This 5 bedroom (could be 6) home has a floor plan you have not seen before. The attention to detail and woodwork is stunning. Built-ins and window seats with storage are found everywhere. The master bedroom is on the main level and offers a gorgeous bath with Carrera marble, and a vey large closet. The main floor laundry is also huge. You will find a second full laundry room in the finished basement, along with a second kitchen! One of the beds is separated from the rest of the house and is perfect for an office or teen room.&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Too many amenities to list them all!&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>5 garage stalls<\\/li><li>Geothermal<\\/li><li>High end kitchen<\\/li><li>Covered deck<\\/li><li>So much more -&nbsp;A RARE FIND!<\\/li><\\/ul>\",\"10\":\"\",\"11\":\"506142\",\"3\":\"\",\"17\":\"3 car attached garage - 2 car detached garage\",\"18\":\"$719,000\",\"12\":\"singleFamily\",\"13\":\"\",\"14\":\"2,700\",\"19\":\"\"}}','2015-12-19 15:41:46','2015-12-19 15:41:46','9b4d5c01-4f2b-4c8d-8e65-3f0941019dc4'),
	(37,18,4,1,'en_us',6,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Absolutely amazing, modern rural-meets-urban set up just outside of Grimes!! A must see at $719,000!\",\"slug\":\"absolutely-amazing-modern-rural-meets-urban-set-up-just-outside-of-grimes\",\"postDate\":1450539000,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"7965 NW 142nd St\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"United States\",\"lat\":\"41.70576500\",\"lng\":\"-93.81369100\"},\"8\":\"4\",\"9\":\"5\",\"1\":\"<p>Call your builder and call off the meeting! This beats new construction and is the house you have been waiting on to hit the market! This stunning Grimes acreage sits on 2.3 acres and is just a mile north of DCG High School. Awesome location. Pull into the long driveway and enjoy the massive front yard with trees. STUNNING curb appeal!&nbsp;<\\/p>\\r\\n\\r\\n<p>This 5 bedroom (could be 6) home has a floor plan you have not seen before. The attention to detail and woodwork is stunning. Built-ins and window seats with storage are found everywhere. The master bedroom is on the main level and offers a gorgeous bath with Carrera marble, and a vey large closet. The main floor laundry is also huge. You will find a second full laundry room in the finished basement, along with a second kitchen! One of the beds is separated from the rest of the house and is perfect for an office or teen room.&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Too many amenities to list them all!&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>5 garage stalls<\\/li><li>Geothermal<\\/li><li>High end kitchen<\\/li><li>Covered deck<\\/li><li>So much more -&nbsp;A RARE FIND!<\\/li><\\/ul>\",\"10\":\"\",\"20\":\"1Hw5ay9jY7y\",\"11\":\"506142\",\"3\":\"\",\"17\":\"3 car attached garage - 2 car detached garage\",\"18\":\"$719,000\",\"12\":\"singleFamily\",\"13\":\"\",\"14\":\"2,700\",\"19\":\"\"}}','2015-12-19 15:43:14','2015-12-19 15:43:14','eb919504-763b-49d3-a244-833e69d66001'),
	(38,18,4,1,'en_us',7,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Absolutely amazing, modern rural-meets-urban set up just outside of Grimes!! A must see at $719,000!\",\"slug\":\"absolutely-amazing-modern-rural-meets-urban-set-up-just-outside-of-grimes\",\"postDate\":1450539000,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"7965 NW 142nd St\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"United States\",\"lat\":\"41.70576500\",\"lng\":\"-93.81369100\"},\"8\":\"4\",\"9\":\"5\",\"1\":\"<p>Call your builder and call off the meeting! This beats new construction and is the house you have been waiting on to hit the market! This stunning Grimes acreage sits on 2.3 acres and is just a mile north of DCG High School. Awesome location. Pull into the long driveway and enjoy the massive front yard with trees. STUNNING curb appeal!&nbsp;<\\/p>\\r\\n\\r\\n<p>This 5 bedroom (could be 6) home has a floor plan you have not seen before. The attention to detail and woodwork is stunning. Built-ins and window seats with storage are found everywhere. The master bedroom is on the main level and offers a gorgeous bath with Carrera marble, and a vey large closet. The main floor laundry is also huge. You will find a second full laundry room in the finished basement, along with a second kitchen! One of the beds is separated from the rest of the house and is perfect for an office or teen room.&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Too many amenities to list them all!&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>5 garage stalls<\\/li><li>Geothermal<\\/li><li>High end kitchen<\\/li><li>Covered deck<\\/li><li>So much more -&nbsp;A RARE FIND!<\\/li><\\/ul>\",\"10\":\"\",\"20\":\"1Hw5ay9jY7y\",\"11\":\"506142\",\"3\":\"\",\"17\":\"3 car attached garage - 2 car detached garage\",\"18\":\"$719,000\",\"12\":\"singleFamily\",\"13\":\"\",\"14\":\"2,700\",\"19\":\"\",\"21\":\"1990\"}}','2015-12-19 16:05:36','2015-12-19 16:05:36','fd801323-f4d5-42df-bd55-7fce0b6da4cb'),
	(39,18,4,1,'en_us',8,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Absolutely amazing, modern rural-meets-urban set up just outside of Grimes!! A must see at $719,000!\",\"slug\":\"absolutely-amazing-modern-rural-meets-urban-set-up-just-outside-of-grimes\",\"postDate\":1450539000,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"7965 NW 142nd St\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"United States\",\"lat\":\"41.70576500\",\"lng\":\"-93.81369100\"},\"8\":\"4\",\"9\":\"5\",\"1\":\"<p>Call your builder and call off the meeting! This beats new construction and is the house you have been waiting on to hit the market! This stunning Grimes acreage sits on 2.3 acres and is just a mile north of DCG High School. Awesome location. Pull into the long driveway and enjoy the massive front yard with trees. STUNNING curb appeal!&nbsp;<\\/p>\\r\\n\\r\\n<p>This 5 bedroom (could be 6) home has a floor plan you have not seen before. The attention to detail and woodwork is stunning. Built-ins and window seats with storage are found everywhere. The master bedroom is on the main level and offers a gorgeous bath with Carrera marble, and a vey large closet. The main floor laundry is also huge. You will find a second full laundry room in the finished basement, along with a second kitchen! One of the beds is separated from the rest of the house and is perfect for an office or teen room.&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Too many amenities to list them all!&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>5 garage stalls<\\/li><li>Geothermal<\\/li><li>High end kitchen<\\/li><li>Covered deck<\\/li><li>So much more -&nbsp;A RARE FIND!<\\/li><\\/ul>\",\"10\":\"1\",\"20\":\"1Hw5ay9jY7y\",\"11\":\"506142\",\"3\":\"\",\"17\":\"3 car attached garage - 2 car detached garage\",\"18\":\"$719,000\",\"12\":\"singleFamily\",\"13\":\"\",\"14\":\"2,700\",\"19\":\"\",\"21\":\"1990\"}}','2015-12-19 16:11:35','2015-12-19 16:11:35','5c6a8c6f-6b88-401b-8c3d-b92571ba06f8'),
	(40,18,4,1,'en_us',9,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Absolutely amazing, modern rural-meets-urban set up just outside of Grimes!! A must see at $719,000!\",\"slug\":\"absolutely-amazing-modern-rural-meets-urban-set-up-just-outside-of-grimes\",\"postDate\":1450539000,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"7965 NW 142nd St\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"United States\",\"lat\":\"41.70576500\",\"lng\":\"-93.81369100\"},\"8\":\"4\",\"9\":\"5\",\"1\":\"<p>Call your builder and call off the meeting! This beats new construction and is the house you have been waiting on to hit the market! This stunning Grimes acreage sits on 2.3 acres and is just a mile north of DCG High School. Awesome location. Pull into the long driveway and enjoy the massive front yard with trees. STUNNING curb appeal!&nbsp;<\\/p>\\r\\n\\r\\n<p>This 5 bedroom (could be 6) home has a floor plan you have not seen before. The attention to detail and woodwork is stunning. Built-ins and window seats with storage are found everywhere. The master bedroom is on the main level and offers a gorgeous bath with Carrera marble, and a vey large closet. The main floor laundry is also huge. You will find a second full laundry room in the finished basement, along with a second kitchen! One of the beds is separated from the rest of the house and is perfect for an office or teen room.&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Too many amenities to list them all!&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>5 garage stalls<\\/li><li>Geothermal<\\/li><li>High end kitchen<\\/li><li>Covered deck<\\/li><li>So much more -&nbsp;A RARE FIND!<\\/li><\\/ul>\",\"10\":\"1\",\"20\":\"1Hw5ay9jY7y\",\"11\":\"506142\",\"3\":\"\",\"17\":\"3 car attached garage - 2 car detached garage\",\"18\":\"$719,000\",\"12\":\"singleFamily\",\"13\":\"1\",\"14\":\"2,700\",\"19\":\"\",\"21\":\"1990\"}}','2015-12-19 16:15:37','2015-12-19 16:15:37','043cefec-7344-418c-a075-bdbe41be60d0'),
	(41,18,4,1,'en_us',10,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Absolutely amazing, modern rural-meets-urban set up just outside of Grimes!! A must see at $719,000!\",\"slug\":\"absolutely-amazing-modern-rural-meets-urban-set-up-just-outside-of-grimes\",\"postDate\":1450539000,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"7965 NW 142nd St\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"United States\",\"lat\":\"41.70576500\",\"lng\":\"-93.81369100\"},\"8\":\"4\",\"9\":\"5\",\"1\":\"<p>Call your builder and call off the meeting! This beats new construction and is the house you have been waiting on to hit the market! This stunning Grimes acreage sits on 2.3 acres and is just a mile north of DCG High School. Awesome location. Pull into the long driveway and enjoy the massive front yard with trees. STUNNING curb appeal!&nbsp;<\\/p>\\r\\n\\r\\n<p>This 5 bedroom (could be 6) home has a floor plan you have not seen before. The attention to detail and woodwork is stunning. Built-ins and window seats with storage are found everywhere. The master bedroom is on the main level and offers a gorgeous bath with Carrera marble, and a vey large closet. The main floor laundry is also huge. You will find a second full laundry room in the finished basement, along with a second kitchen! One of the beds is separated from the rest of the house and is perfect for an office or teen room.&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Too many amenities to list them all!&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>5 garage stalls<\\/li><li>Geothermal<\\/li><li>High end kitchen<\\/li><li>Covered deck<\\/li><li>So much more -&nbsp;A RARE FIND!<\\/li><\\/ul>\",\"10\":\"\",\"20\":\"1Hw5ay9jY7y\",\"11\":\"506142\",\"3\":\"\",\"17\":\"3 car attached garage - 2 car detached garage\",\"18\":\"$719,000\",\"12\":\"singleFamily\",\"13\":\"\",\"14\":\"2,700\",\"19\":\"\",\"21\":\"1990\"}}','2015-12-19 16:15:51','2015-12-19 16:15:51','391f2f18-afbb-411a-87cd-ac1ddde43102'),
	(42,18,4,1,'en_us',11,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Absolutely amazing, modern rural-meets-urban set up just outside of Grimes!! A must see at $719,000!\",\"slug\":\"absolutely-amazing-modern-rural-meets-urban-set-up-just-outside-of-grimes\",\"postDate\":1450539000,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"7965 NW 142nd St\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"United States\",\"lat\":\"41.70576500\",\"lng\":\"-93.81369100\"},\"8\":\"4\",\"9\":\"5\",\"1\":\"<p>Call your builder and call off the meeting! This beats new construction and is the house you have been waiting on to hit the market! This stunning Grimes acreage sits on 2.3 acres and is just a mile north of DCG High School. Awesome location. Pull into the long driveway and enjoy the massive front yard with trees. STUNNING curb appeal!&nbsp;<\\/p>\\r\\n\\r\\n<p>This 5 bedroom (could be 6) home has a floor plan you have not seen before. The attention to detail and woodwork is stunning. Built-ins and window seats with storage are found everywhere. The master bedroom is on the main level and offers a gorgeous bath with Carrera marble, and a vey large closet. The main floor laundry is also huge. You will find a second full laundry room in the finished basement, along with a second kitchen! One of the beds is separated from the rest of the house and is perfect for an office or teen room.&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Too many amenities to list them all!&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>5 garage stalls<\\/li><li>Geothermal<\\/li><li>High end kitchen<\\/li><li>Covered deck<\\/li><li>So much more -&nbsp;A RARE FIND!<\\/li><\\/ul>\",\"10\":\"\",\"20\":\"1Hw5ay9jY7y\",\"11\":\"506142\",\"3\":\"\",\"17\":\"3 car attached garage - 2 car detached garage\",\"18\":\"$719,000\",\"12\":\"singleFamily\",\"13\":\"\",\"14\":\"2,700\",\"19\":\"\",\"21\":\"1990\"}}','2015-12-19 16:18:14','2015-12-19 16:18:14','8be44274-fccb-4b5a-aa52-7909f7ef60f4'),
	(43,21,12,1,'en_us',1,NULL,'{\"typeId\":\"12\",\"authorId\":null,\"title\":\"Contact\",\"slug\":\"contact\",\"postDate\":1450585430,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":[]}','2015-12-20 04:23:50','2015-12-20 04:23:50','f12ddb60-0af1-4991-a136-c65683a72bb5'),
	(44,21,12,1,'en_us',2,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Contact\",\"slug\":\"contact\",\"postDate\":1450585430,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"\",\"3\":\"\"}}','2015-12-20 04:26:31','2015-12-20 04:26:31','c37704c6-dd42-4570-a8db-9507d9be15fe'),
	(45,21,12,1,'en_us',3,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Contact Mark\",\"slug\":\"contact\",\"postDate\":1450585430,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"\",\"3\":\"\"}}','2015-12-20 04:26:39','2015-12-20 04:26:39','2046ae16-5266-4dab-b248-9b372b6199d3'),
	(46,22,13,1,'en_us',1,NULL,'{\"typeId\":\"13\",\"authorId\":null,\"title\":\"Ways To Sell\",\"slug\":\"ways-to-sell\",\"postDate\":1450586361,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":[]}','2015-12-20 04:39:21','2015-12-20 04:39:21','814bcd6a-77a4-4395-8a8d-924c4b973d7f'),
	(47,36,14,1,'en_us',1,NULL,'{\"typeId\":\"14\",\"authorId\":null,\"title\":\"Buyers & Sellers\",\"slug\":\"buyers-sellers\",\"postDate\":1450587650,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":[]}','2015-12-20 05:00:51','2015-12-20 05:00:51','11c964ac-6740-432b-94f9-2e43396f4a6d'),
	(48,36,14,1,'en_us',2,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Buyers & Sellers\",\"slug\":\"buyers-sellers\",\"postDate\":1450587650,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>So you are are thinking about buying or selling. That is great, here are a few things you need to know!<\\/p><h2>Buyers<\\/h2><p><strong><\\/strong>Buying a home is a big purchase, one of the biggest you will ever make. Why in the world would anyone want to make that purchase without the help of a professional real estate agent? Beats me, but many try. The most important thing to remember is that when you are buying a home, you DO NOT PAY YOUR AGENT! The seller of the home pays the entire commission and that commission is split between the agent selling the home and the agent representing the buyer. There is no reason to not have your own agent representing you and working hard on your behalf.<br><\\/p><p>I have talked to many buyers who have said something like \\\"When we bought, we just used the agent who had the house listed.\\\" If you are thinking about doing this, <strong>use extreme caution!<\\/strong>&nbsp;This should only be done if you have complete trust in the agent. If you just met them, this is a terrible idea! This situation is called Dual Agency and it happens when one agent represents both parties. That agent is trying to get the most money for the seller and also get you the best price. How can they do this? It is tricky and again, should only be done if you have total faith in that agent.<br><\\/p><p>The way to avoid this is to hire me! Remember, I can show you <em>all<\\/em> homes, not just the ones that I list.<br><\\/p><p>So when you are my buyer what do you get? Here is a summary of what my buyers get when they work with me:<br><\\/p><ul><li>An agent with 24\\/7 availability<\\/li><li>An agent with a A+ BBB rating<\\/li><li>An agent with more recommendations than any other agent in Central Iowa (Trulia.com)<\\/li><li><a href=\\\"http:\\/\\/local.charterhouseiowa.dev\\/truck#entry:9\\\">Free Moving Truck<\\/a> (based on availability, first come, first served)<\\/li><li>Access to my interior designer if you need help or suggestion on how to furnish your new home<\\/li><li>Lender recommendations, I can get you to the right lender who will get you closed on time and with no surprises<\\/li><li>An experienced agent, I have been selling real estate full-time since 2005<\\/li><li>Auto-prospecting so you never miss a new listing to the market<\\/li><li>Weekly check-in updates to make sure you are happy<\\/li><li><strong>And much more!<\\/strong><\\/li><\\/ul><p>When you work with me, you are working with a one-of-a-kind agent who simply offers my clients more than other agents. Please read through the client reviews on this site to get a good feel of how I treat my clients. You will never regret hiring me as your agent!<\\/p><h2>Sellers<\\/h2><p>When the time comes to put your home on the market, hiring a real estate professional is a crucial step in the right direction. I realize that there are many home-selling options, but the tried and true method of hiring an agent is still the best option by far. It is proven to be the safest way to sell as well as being the way to generate the highest selling price.<br><\\/p>\",\"3\":\"\"}}','2015-12-20 05:05:08','2015-12-20 05:05:08','4a8aea66-00f8-4696-a43a-ffa5e6fce613'),
	(49,36,14,1,'en_us',3,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Buyers & Sellers\",\"slug\":\"buyers-sellers\",\"postDate\":1450587650,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>So you are are thinking about buying or selling. That is great, here are a few things you need to know!<\\/p>\\r\\n\\r\\n<h2>Buyers<\\/h2>\\r\\n\\r\\n<p>Buying a home is a big purchase, one of the biggest you will ever make. Why in the world would anyone want to make that purchase without the help of a professional real estate agent? Beats me, but many try. The most important thing to remember is that when you are buying a home, you DO NOT PAY YOUR AGENT! The seller of the home pays the entire commission and that commission is split between the agent selling the home and the agent representing the buyer. There is no reason to not have your own agent representing you and working hard on your behalf.<br><\\/p>\\r\\n\\r\\n<p>I have talked to many buyers who have said something like \\\"When we bought, we just used the agent who had the house listed.\\\" If you are thinking about doing this, <strong>use extreme caution!<\\/strong>&nbsp;This should only be done if you have complete trust in the agent. If you just met them, this is a terrible idea! This situation is called Dual Agency and it happens when one agent represents both parties. That agent is trying to get the most money for the seller and also get you the best price. How can they do this? It is tricky and again, should only be done if you have total faith in that agent.<br><\\/p>\\r\\n\\r\\n<p>The way to avoid this is to hire me! Remember, I can show you <em>all<\\/em> homes, not just the ones that I list.<br><\\/p>\\r\\n\\r\\n<p>So when you are my buyer what do you get? Here is a summary of what my buyers get when they work with me:<br><\\/p>\\r\\n\\r\\n<ul><li>An agent with 24\\/7 availability<\\/li><li>An agent with a A+ BBB rating<\\/li><li>An agent with more recommendations than any other agent in Central Iowa (Trulia.com)<\\/li><li><a href=\\\"http:\\/\\/local.charterhouseiowa.dev\\/truck#entry:9:url\\\">Free Moving Truck<\\/a> (based on availability, first come, first served)<\\/li><li>Access to my interior designer if you need help or suggestion on how to furnish your new home<\\/li><li>Lender recommendations, I can get you to the right lender who will get you closed on time and with no surprises<\\/li><li>An experienced agent, I have been selling real estate full-time since 2005<\\/li><li>Auto-prospecting so you never miss a new listing to the market<\\/li><li>Weekly check-in updates to make sure you are happy<\\/li><li><strong>And much more!<\\/strong><\\/li><\\/ul>\\r\\n\\r\\n<p>When you work with me, you are working with a one-of-a-kind agent who simply offers my clients more than other agents. Please read through the client reviews on this site to get a good feel of how I treat my clients. You will never regret hiring me as your agent!<\\/p>\\r\\n\\r\\n<h2>Sellers<\\/h2>\\r\\n\\r\\n<p>When the time comes to put your home on the market, hiring a real estate professional is a crucial step in the right direction. I realize that there are many home-selling options, but the tried and true method of hiring an agent is still the best option by far. It is proven to be the safest way to sell as well as being the way to generate the highest selling price.<br><\\/p><p>But, just listing with an agent is not enough! You have to list with an agent who knows how to market and embraces today\'s trends like social media to reach the masses. You want your home exposed everywhere to reach every possible buyer. But you also want to work with an agent who will be enjoyable to work with and one who has great services and benefits to offer you. That agent is me!<br><\\/p><p>I would love to talk to you about listing your home but for now, here is a brief summary of what I offer to sellers:<br><\\/p><ul><li>An agent with 24\\/7 availability<\\/li><li>An agent with an A+ BBB rating<\\/li><li>An agent with more recommendations than any other agent in Central Iowa (Trulia.com)<\\/li><li>A professional photographer will take the photos of your home<\\/li><li>A Professional videographer will do a video of your home (Certain price ranges only)<\\/li><li>Professional staging assistance, my interior designer will make sure your home is ready to go<\\/li><li><a href=\\\"http:\\/\\/local.charterhouseiowa.dev\\/truck#entry:9\\\">Free Moving Truck <\\/a>(Based on availability, first come, first served)<\\/li><li>Professional moving services (at 7% plan while staying in central Iowa)<\\/li><li>Weekly check-in updates to make sure you are happy<\\/li><li>Social Media marketing<\\/li><li><strong>And much more!<\\/strong><\\/li><\\/ul><p>No other agent offers their clients more! You will love your selling experience, I promise. I realize one size does not fit all when it comes to selling your home. I want to talk to you about your specific situation and see if I can help.<\\/p>\",\"3\":\"\"}}','2015-12-20 05:08:19','2015-12-20 05:08:19','e7ee117b-0347-422b-b5c6-da19a8ebdd02'),
	(50,22,13,1,'en_us',2,'','{\"typeId\":null,\"authorId\":null,\"title\":\"3 Ways To List Your Home\",\"slug\":\"ways-to-sell\",\"postDate\":1450586361,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"\",\"3\":\"\"}}','2015-12-20 14:54:52','2015-12-20 14:54:52','9f9c0bd5-24b6-4839-8c9e-8c21be29d4aa'),
	(51,22,13,1,'en_us',3,'','{\"typeId\":null,\"authorId\":null,\"title\":\"3 Ways To List Your Home\",\"slug\":\"ways-to-sell\",\"postDate\":1450586361,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"\",\"38\":\"\",\"3\":\"\"}}','2015-12-20 14:56:25','2015-12-20 14:56:25','4ec70fb6-1edb-43e6-bfa8-b73c80de7ab8'),
	(52,38,15,1,'en_us',1,NULL,'{\"typeId\":\"15\",\"authorId\":null,\"title\":\"About\",\"slug\":\"about\",\"postDate\":1450641789,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":[]}','2015-12-20 20:03:09','2015-12-20 20:03:09','1a1dffd8-8b59-46dd-895a-6b8e09977c9e'),
	(53,38,15,1,'en_us',2,'','{\"typeId\":null,\"authorId\":null,\"title\":\"About\",\"slug\":\"about\",\"postDate\":1450641789,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>Mark started his real estate career in 2005 and has been doing it full-time since then. Over time Mark realized that the majority of real estate agents are the same. They say the same things, they market the same way, and they treat clients the same. Mark did not like this so he set about creating his own unique way of practicing real estate.<\\/p><p>In 2014, Mark debuted a three tier pricing plan for listing clients, giving them the choice on how they want to list their home. This is another example of how Mark never accepts the status quo in real estate and is always looking to make things better for his clients.<br><\\/p><p>Mark\'s overall goal on every deal is simple. He wants to provide such great service that after the deal is done, that client will happily provide a positive review of the transaction. This goal is what makes Mark one of the most recommended agents in Central Iowa.<br><\\/p><p>When you add it all up, when you take into account what Mark offers clients in terms of services, marketing and care, Mark truly feels he is the best option available in the market today and he hopes you will experience this for yourself very soon!<br><\\/p>\",\"3\":\"\"}}','2015-12-20 20:04:30','2015-12-20 20:04:30','0805a412-f818-4b7a-970e-3d63a4c0a65f'),
	(61,9,10,1,'en_us',2,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Truck\",\"slug\":\"truck\",\"postDate\":1450487151,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mattis sollicitudin felis, consectetur eleifend mi ornare sed. Vivamus cursus tincidunt ultrices. Nam id tincidunt purus. Morbi lectus eros, molestie non dolor ut, tincidunt fermentum odio. Vivamus consectetur molestie lorem ac varius.<\\/p>\",\"3\":\"\"}}','2015-12-21 20:41:59','2015-12-21 20:41:59','2ab089dd-91ce-4cef-879a-5c2c3f1a1c1c'),
	(62,38,15,1,'en_us',3,'','{\"typeId\":null,\"authorId\":null,\"title\":\"About\",\"slug\":\"about\",\"postDate\":1450641789,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":{\"new1\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Mark started his real estate career in 2005 and has been doing it full-time since then. Over time Mark realized that the majority of real estate agents are the same. They say the same things, they market the same way, and they treat clients the same. Mark did not like this so he set about creating his own unique way of practicing real estate.<\\/p><p>In 2014, Mark debuted a three tier pricing plan for listing clients, giving them the choice on how they want to list their home. This is another example of how Mark never accepts the status quo in real estate and is always looking to make things better for his clients.<\\/p><p><br>Mark\'s overall goal on every deal is simple. He wants to provide such great service that after the deal is done, that client will happily provide a positive review of the transaction. This goal is what makes Mark one of the most recommended agents in Central Iowa.<\\/p><p><br>When you add it all up, when you take into account what Mark offers clients in terms of services, marketing and care, Mark truly feels he is the best option available in the market today and he hopes you will experience this for yourself very soon!<\\/p>\"}}},\"3\":\"\"}}','2015-12-28 00:53:16','2015-12-28 00:53:16','499bc50b-0213-46dd-9d2a-12cf0a925256'),
	(63,38,15,1,'en_us',4,'','{\"typeId\":null,\"authorId\":null,\"title\":\"About Mark Charter\",\"slug\":\"about\",\"postDate\":1450641789,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":{\"new1\":{\"type\":\"headline\",\"enabled\":\"1\",\"fields\":{\"headline\":\"Meet Mark and his family\"}},\"55\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Mark started his real estate career in 2005 and has been doing it full-time since then. Over time Mark realized that the majority of real estate agents are the same. They say the same things, they market the same way, and they treat clients the same. Mark did not like this so he set about creating his own unique way of practicing real estate.<\\/p>\\r\\n\\r\\n<p>In 2014, Mark debuted a three tier pricing plan for listing clients, giving them the choice on how they want to list their home. This is another example of how Mark never accepts the status quo in real estate and is always looking to make things better for his clients.<\\/p>\\r\\n\\r\\n<p><br>Mark\'s overall goal on every deal is simple. He wants to provide such great service that after the deal is done, that client will happily provide a positive review of the transaction. This goal is what makes Mark one of the most recommended agents in Central Iowa.<\\/p>\\r\\n\\r\\n<p><br>When you add it all up, when you take into account what Mark offers clients in terms of services, marketing and care, Mark truly feels he is the best option available in the market today and he hopes you will experience this for yourself very soon!<\\/p>\"}}},\"3\":\"\"}}','2015-12-28 00:55:12','2015-12-28 00:55:12','66da3bcd-f138-43e3-a6e0-7be5372dd924'),
	(64,38,15,1,'en_us',5,'','{\"typeId\":null,\"authorId\":null,\"title\":\"About Mark Charter\",\"slug\":\"about\",\"postDate\":1450641789,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":{\"new1\":{\"type\":\"quote\",\"enabled\":\"1\",\"fields\":{\"quote\":\"<p>One of Mark Charter\\u2019s greatest characteristics is his giving nature.<\\/p>\",\"quoteAttribution\":\"Adam Carroll\"}},\"56\":{\"type\":\"headline\",\"enabled\":\"1\",\"fields\":{\"headline\":\"Meet Mark and his family\"}},\"55\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Mark started his real estate career in 2005 and has been doing it full-time since then. Over time Mark realized that the majority of real estate agents are the same. They say the same things, they market the same way, and they treat clients the same. Mark did not like this so he set about creating his own unique way of practicing real estate.<\\/p>\\r\\n\\r\\n<p>In 2014, Mark debuted a three tier pricing plan for listing clients, giving them the choice on how they want to list their home. This is another example of how Mark never accepts the status quo in real estate and is always looking to make things better for his clients.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p><br>Mark\'s overall goal on every deal is simple. He wants to provide such great service that after the deal is done, that client will happily provide a positive review of the transaction. This goal is what makes Mark one of the most recommended agents in Central Iowa.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p><br>When you add it all up, when you take into account what Mark offers clients in terms of services, marketing and care, Mark truly feels he is the best option available in the market today and he hopes you will experience this for yourself very soon!<\\/p>\"}}},\"3\":\"\"}}','2015-12-28 00:55:54','2015-12-28 00:55:54','58a6acd4-8986-40af-b4af-b3e52581e526'),
	(65,38,15,1,'en_us',6,'','{\"typeId\":null,\"authorId\":null,\"title\":\"About Mark Charter\",\"slug\":\"about\",\"postDate\":1450641789,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":{\"56\":{\"type\":\"headline\",\"enabled\":\"1\",\"fields\":{\"headline\":\"Meet Mark and his family\"}},\"57\":{\"type\":\"quote\",\"enabled\":\"1\",\"fields\":{\"quote\":\"<p>One of Mark Charter\\u2019s greatest characteristics is his giving nature.<\\/p>\",\"quoteAttribution\":\"Adam Carroll\"}},\"55\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Mark started his real estate career in 2005 and has been doing it full-time since then. Over time Mark realized that the majority of real estate agents are the same. They say the same things, they market the same way, and they treat clients the same. Mark did not like this so he set about creating his own unique way of practicing real estate.<\\/p>\\r\\n\\r\\n<p>In 2014, Mark debuted a three tier pricing plan for listing clients, giving them the choice on how they want to list their home. This is another example of how Mark never accepts the status quo in real estate and is always looking to make things better for his clients.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p><br>Mark\'s overall goal on every deal is simple. He wants to provide such great service that after the deal is done, that client will happily provide a positive review of the transaction. This goal is what makes Mark one of the most recommended agents in Central Iowa.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p><br>When you add it all up, when you take into account what Mark offers clients in terms of services, marketing and care, Mark truly feels he is the best option available in the market today and he hopes you will experience this for yourself very soon!<\\/p>\"}}},\"3\":\"\"}}','2015-12-28 00:56:43','2015-12-28 00:56:43','c0c913a0-2742-4785-9ddb-08cc56a271bc'),
	(66,38,15,1,'en_us',7,'','{\"typeId\":null,\"authorId\":null,\"title\":\"About Mark Charter\",\"slug\":\"about\",\"postDate\":1450641789,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":{\"56\":{\"type\":\"headline\",\"enabled\":\"1\",\"fields\":{\"headline\":\"Meet Mark and his family\"}},\"55\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Mark started his real estate career in 2005 and has been doing it full-time since then. Over time Mark realized that the majority of real estate agents are the same. They say the same things, they market the same way, and they treat clients the same. Mark did not like this so he set about creating his own unique way of practicing real estate.<\\/p>\\r\\n\\r\\n<p>In 2014, Mark debuted a three tier pricing plan for listing clients, giving them the choice on how they want to list their home. This is another example of how Mark never accepts the status quo in real estate and is always looking to make things better for his clients.<\\/p>\"}},\"57\":{\"type\":\"quote\",\"enabled\":\"1\",\"fields\":{\"quote\":\"<p>One of Mark Charter\\u2019s greatest characteristics is his giving nature.<\\/p>\",\"quoteAttribution\":\"Adam Carroll\"}},\"new1\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Mark\'s overall goal on every deal is simple. He wants to provide such great service that after the deal is done, that client will happily provide a positive review of the transaction. This goal is what makes Mark one of the most recommended agents in Central Iowa.<\\/p>\\r\\n\\r\\n<p>When you add it all up, when you take into account what Mark offers clients in terms of services, marketing and care, Mark truly feels he is the best option available in the market today and he hopes you will experience this for yourself very soon!<\\/p>\"}}},\"3\":\"\"}}','2015-12-28 00:57:07','2015-12-28 00:57:07','8f259d23-bd4e-4bed-b3a3-c1ca79e9a742'),
	(67,36,14,1,'en_us',4,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Buyers & Sellers\",\"slug\":\"buyers-sellers\",\"postDate\":1450587650,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>So you are are thinking about buying or selling. That is great, here are a few things you need to know!<\\/p>\\r\\n\\r\\n<h2>Buyers<\\/h2>\\r\\n\\r\\n\\r\\n\\r\\n<p>Buying a home is a big purchase, one of the biggest you will ever make. Why in the world would anyone want to make that purchase without the help of a professional real estate agent? Beats me, but many try. The most important thing to remember is that when you are buying a home, you DO NOT PAY YOUR AGENT! The seller of the home pays the entire commission and that commission is split between the agent selling the home and the agent representing the buyer. There is no reason to not have your own agent representing you and working hard on your behalf.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>I have talked to many buyers who have said something like \\\"When we bought, we just used the agent who had the house listed.\\\" If you are thinking about doing this, <strong>use extreme caution!<\\/strong>&nbsp;This should only be done if you have complete trust in the agent. If you just met them, this is a terrible idea! This situation is called Dual Agency and it happens when one agent represents both parties. That agent is trying to get the most money for the seller and also get you the best price. How can they do this? It is tricky and again, should only be done if you have total faith in that agent.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>The way to avoid this is to hire me! Remember, I can show you <em>all<\\/em> homes, not just the ones that I list.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>So when you are my buyer what do you get? Here is a summary of what my buyers get when they work with me:<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>An agent with 24\\/7 availability<\\/li><li>An agent with a A+ BBB rating<\\/li><li>An agent with more recommendations than any other agent in Central Iowa (Trulia.com)<\\/li><li><a href=\\\"http:\\/\\/local.charterhouseiowa.dev\\/truck#entry:9:url\\\">Free Moving Truck<\\/a> (based on availability, first come, first served)<\\/li><li>Access to my interior designer if you need help or suggestion on how to furnish your new home<\\/li><li>Lender recommendations, I can get you to the right lender who will get you closed on time and with no surprises<\\/li><li>An experienced agent, I have been selling real estate full-time since 2005<\\/li><li>Auto-prospecting so you never miss a new listing to the market<\\/li><li>Weekly check-in updates to make sure you are happy<\\/li><li><strong>And much more!<\\/strong><\\/li><\\/ul>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>When you work with me, you are working with a one-of-a-kind agent who simply offers my clients more than other agents. Please read through the client reviews on this site to get a good feel of how I treat my clients. You will never regret hiring me as your agent!<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<h2>Sellers<\\/h2>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>When the time comes to put your home on the market, hiring a real estate professional is a crucial step in the right direction. I realize that there are many home-selling options, but the tried and true method of hiring an agent is still the best option by far. It is proven to be the safest way to sell as well as being the way to generate the highest selling price.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>But, just listing with an agent is not enough! You have to list with an agent who knows how to market and embraces today\'s trends like social media to reach the masses. You want your home exposed everywhere to reach every possible buyer. But you also want to work with an agent who will be enjoyable to work with and one who has great services and benefits to offer you. That agent is me!<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>I would love to talk to you about listing your home but for now, here is a brief summary of what I offer to sellers:<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>An agent with 24\\/7 availability<\\/li><li>An agent with an A+ BBB rating<\\/li><li>An agent with more recommendations than any other agent in Central Iowa (Trulia.com)<\\/li><li>A professional photographer will take the photos of your home<\\/li><li>A Professional videographer will do a video of your home (Certain price ranges only)<\\/li><li>Professional staging assistance, my interior designer will make sure your home is ready to go<\\/li><li><a href=\\\"http:\\/\\/local.charterhouseiowa.dev\\/truck#entry:9:url\\\">Free Moving Truck <\\/a>(Based on availability, first come, first served)<\\/li><li>Professional moving services (at 7% plan while staying in central Iowa)<\\/li><li>Weekly check-in updates to make sure you are happy<\\/li><li>Social Media marketing<\\/li><li><strong>And much more!<\\/strong><\\/li><\\/ul>\\r\\n\\r\\n\\r\\n\\r\\n<p>No other agent offers their clients more! You will love your selling experience, I promise. I realize one size does not fit all when it comes to selling your home. I want to talk to you about your specific situation and see if I can help.<\\/p>\",\"49\":{\"new1\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>So you are are thinking about buying or selling. That is great, here are a few things you need to know!<\\/p>\\r\\n\\r\\n<h2>Buyers<\\/h2>\\r\\n\\r\\n\\r\\n\\r\\n<p>Buying a home is a big purchase, one of the biggest you will ever make. Why in the world would anyone want to make that purchase without the help of a professional real estate agent? Beats me, but many try. The most important thing to remember is that when you are buying a home, you DO NOT PAY YOUR AGENT! The seller of the home pays the entire commission and that commission is split between the agent selling the home and the agent representing the buyer. There is no reason to not have your own agent representing you and working hard on your behalf.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>I have talked to many buyers who have said something like \\\"When we bought, we just used the agent who had the house listed.\\\" If you are thinking about doing this, <strong>use extreme caution!<\\/strong>&nbsp;This should only be done if you have complete trust in the agent. If you just met them, this is a terrible idea! This situation is called Dual Agency and it happens when one agent represents both parties. That agent is trying to get the most money for the seller and also get you the best price. How can they do this? It is tricky and again, should only be done if you have total faith in that agent.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>The way to avoid this is to hire me! Remember, I can show you <em>all<\\/em> homes, not just the ones that I list.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>So when you are my buyer what do you get? Here is a summary of what my buyers get when they work with me:<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>An agent with 24\\/7 availability<\\/li><li>An agent with a A+ BBB rating<\\/li><li>An agent with more recommendations than any other agent in Central Iowa (Trulia.com)<\\/li><li><a href=\\\"http:\\/\\/local.charterhouseiowa.dev\\/truck#entry:9:url\\\">Free Moving Truck<\\/a> (based on availability, first come, first served)<\\/li><li>Access to my interior designer if you need help or suggestion on how to furnish your new home<\\/li><li>Lender recommendations, I can get you to the right lender who will get you closed on time and with no surprises<\\/li><li>An experienced agent, I have been selling real estate full-time since 2005<\\/li><li>Auto-prospecting so you never miss a new listing to the market<\\/li><li>Weekly check-in updates to make sure you are happy<\\/li><li><strong>And much more!<\\/strong><\\/li><\\/ul>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>When you work with me, you are working with a one-of-a-kind agent who simply offers my clients more than other agents. Please read through the client reviews on this site to get a good feel of how I treat my clients. You will never regret hiring me as your agent!<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<h2>Sellers<\\/h2>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>When the time comes to put your home on the market, hiring a real estate professional is a crucial step in the right direction. I realize that there are many home-selling options, but the tried and true method of hiring an agent is still the best option by far. It is proven to be the safest way to sell as well as being the way to generate the highest selling price.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>But, just listing with an agent is not enough! You have to list with an agent who knows how to market and embraces today\'s trends like social media to reach the masses. You want your home exposed everywhere to reach every possible buyer. But you also want to work with an agent who will be enjoyable to work with and one who has great services and benefits to offer you. That agent is me!<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>I would love to talk to you about listing your home but for now, here is a brief summary of what I offer to sellers:<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>An agent with 24\\/7 availability<\\/li><li>An agent with an A+ BBB rating<\\/li><li>An agent with more recommendations than any other agent in Central Iowa (Trulia.com)<\\/li><li>A professional photographer will take the photos of your home<\\/li><li>A Professional videographer will do a video of your home (Certain price ranges only)<\\/li><li>Professional staging assistance, my interior designer will make sure your home is ready to go<\\/li><li><a href=\\\"http:\\/\\/local.charterhouseiowa.dev\\/truck#entry:9:url\\\">Free Moving Truck <\\/a>(Based on availability, first come, first served)<\\/li><li>Professional moving services (at 7% plan while staying in central Iowa)<\\/li><li>Weekly check-in updates to make sure you are happy<\\/li><li>Social Media marketing<\\/li><li><strong>And much more!<\\/strong><\\/li><\\/ul>\\r\\n\\r\\n\\r\\n\\r\\n<p>No other agent offers their clients more! You will love your selling experience, I promise. I realize one size does not fit all when it comes to selling your home. I want to talk to you about your specific situation and see if I can help.<\\/p>\"}}},\"3\":\"\"}}','2015-12-28 00:57:53','2015-12-28 00:57:53','00738543-86a2-4498-a72f-a885bd1dc987'),
	(68,36,14,1,'en_us',5,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Buyers & Sellers\",\"slug\":\"buyers-sellers\",\"postDate\":1450587650,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"\",\"49\":{\"59\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>So you are are thinking about buying or selling. That is great, here are a few things you need to know!<\\/p>\\r\\n\\r\\n<h2>Buyers<\\/h2>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Buying a home is a big purchase, one of the biggest you will ever make. Why in the world would anyone want to make that purchase without the help of a professional real estate agent? Beats me, but many try. The most important thing to remember is that when you are buying a home, you DO NOT PAY YOUR AGENT! The seller of the home pays the entire commission and that commission is split between the agent selling the home and the agent representing the buyer. There is no reason to not have your own agent representing you and working hard on your behalf.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>I have talked to many buyers who have said something like \\\"When we bought, we just used the agent who had the house listed.\\\" If you are thinking about doing this, <strong>use extreme caution!<\\/strong>&nbsp;This should only be done if you have complete trust in the agent. If you just met them, this is a terrible idea! This situation is called Dual Agency and it happens when one agent represents both parties. That agent is trying to get the most money for the seller and also get you the best price. How can they do this? It is tricky and again, should only be done if you have total faith in that agent.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>The way to avoid this is to hire me! Remember, I can show you <em>all<\\/em> homes, not just the ones that I list.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>So when you are my buyer what do you get? Here is a summary of what my buyers get when they work with me:<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>An agent with 24\\/7 availability<\\/li><li>An agent with a A+ BBB rating<\\/li><li>An agent with more recommendations than any other agent in Central Iowa (Trulia.com)<\\/li><li><a href=\\\"http:\\/\\/local.charterhouseiowa.dev\\/truck#entry:9:url\\\">Free Moving Truck<\\/a> (based on availability, first come, first served)<\\/li><li>Access to my interior designer if you need help or suggestion on how to furnish your new home<\\/li><li>Lender recommendations, I can get you to the right lender who will get you closed on time and with no surprises<\\/li><li>An experienced agent, I have been selling real estate full-time since 2005<\\/li><li>Auto-prospecting so you never miss a new listing to the market<\\/li><li>Weekly check-in updates to make sure you are happy<\\/li><li><strong>And much more!<\\/strong><\\/li><\\/ul>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>When you work with me, you are working with a one-of-a-kind agent who simply offers my clients more than other agents. Please read through the client reviews on this site to get a good feel of how I treat my clients. You will never regret hiring me as your agent!<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<h2>Sellers<\\/h2>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>When the time comes to put your home on the market, hiring a real estate professional is a crucial step in the right direction. I realize that there are many home-selling options, but the tried and true method of hiring an agent is still the best option by far. It is proven to be the safest way to sell as well as being the way to generate the highest selling price.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>But, just listing with an agent is not enough! You have to list with an agent who knows how to market and embraces today\'s trends like social media to reach the masses. You want your home exposed everywhere to reach every possible buyer. But you also want to work with an agent who will be enjoyable to work with and one who has great services and benefits to offer you. That agent is me!<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>I would love to talk to you about listing your home but for now, here is a brief summary of what I offer to sellers:<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>An agent with 24\\/7 availability<\\/li><li>An agent with an A+ BBB rating<\\/li><li>An agent with more recommendations than any other agent in Central Iowa (Trulia.com)<\\/li><li>A professional photographer will take the photos of your home<\\/li><li>A Professional videographer will do a video of your home (Certain price ranges only)<\\/li><li>Professional staging assistance, my interior designer will make sure your home is ready to go<\\/li><li><a href=\\\"http:\\/\\/local.charterhouseiowa.dev\\/truck#entry:9:url\\\">Free Moving Truck <\\/a>(Based on availability, first come, first served)<\\/li><li>Professional moving services (at 7% plan while staying in central Iowa)<\\/li><li>Weekly check-in updates to make sure you are happy<\\/li><li>Social Media marketing<\\/li><li><strong>And much more!<\\/strong><\\/li><\\/ul>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>No other agent offers their clients more! You will love your selling experience, I promise. I realize one size does not fit all when it comes to selling your home. I want to talk to you about your specific situation and see if I can help.<\\/p>\"}}},\"3\":\"\"}}','2015-12-28 00:58:00','2015-12-28 00:58:00','b69ce752-2cdf-4f2c-a4e1-1d0f637b0c72'),
	(69,36,14,1,'en_us',6,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Buyers & Sellers\",\"slug\":\"buyers-sellers\",\"postDate\":1450587650,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":{\"59\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>So you are are thinking about buying or selling. That is great, here are a few things you need to know!<\\/p>\\r\\n\\r\\n<h2>Buyers<\\/h2>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Buying a home is a big purchase, one of the biggest you will ever make. Why in the world would anyone want to make that purchase without the help of a professional real estate agent? Beats me, but many try. The most important thing to remember is that when you are buying a home, you DO NOT PAY YOUR AGENT! The seller of the home pays the entire commission and that commission is split between the agent selling the home and the agent representing the buyer. There is no reason to not have your own agent representing you and working hard on your behalf.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>I have talked to many buyers who have said something like \\\"When we bought, we just used the agent who had the house listed.\\\" If you are thinking about doing this, <strong>use extreme caution!<\\/strong>&nbsp;This should only be done if you have complete trust in the agent. If you just met them, this is a terrible idea! This situation is called Dual Agency and it happens when one agent represents both parties. That agent is trying to get the most money for the seller and also get you the best price. How can they do this? It is tricky and again, should only be done if you have total faith in that agent.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>The way to avoid this is to hire me! Remember, I can show you <em>all<\\/em> homes, not just the ones that I list.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>So when you are my buyer what do you get? Here is a summary of what my buyers get when they work with me:<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>An agent with 24\\/7 availability<\\/li><li>An agent with a A+ BBB rating<\\/li><li>An agent with more recommendations than any other agent in Central Iowa (Trulia.com)<\\/li><li><a href=\\\"http:\\/\\/local.charterhouseiowa.dev\\/truck#entry:9:url\\\">Free Moving Truck<\\/a> (based on availability, first come, first served)<\\/li><li>Access to my interior designer if you need help or suggestion on how to furnish your new home<\\/li><li>Lender recommendations, I can get you to the right lender who will get you closed on time and with no surprises<\\/li><li>An experienced agent, I have been selling real estate full-time since 2005<\\/li><li>Auto-prospecting so you never miss a new listing to the market<\\/li><li>Weekly check-in updates to make sure you are happy<\\/li><li><strong>And much more!<\\/strong><\\/li><\\/ul>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>When you work with me, you are working with a one-of-a-kind agent who simply offers my clients more than other agents. Please read through the client reviews on this site to get a good feel of how I treat my clients. You will never regret hiring me as your agent!<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<h2>Sellers<\\/h2>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>When the time comes to put your home on the market, hiring a real estate professional is a crucial step in the right direction. I realize that there are many home-selling options, but the tried and true method of hiring an agent is still the best option by far. It is proven to be the safest way to sell as well as being the way to generate the highest selling price.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>But, just listing with an agent is not enough! You have to list with an agent who knows how to market and embraces today\'s trends like social media to reach the masses. You want your home exposed everywhere to reach every possible buyer. But you also want to work with an agent who will be enjoyable to work with and one who has great services and benefits to offer you. That agent is me!<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>I would love to talk to you about listing your home but for now, here is a brief summary of what I offer to sellers:<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>An agent with 24\\/7 availability<\\/li><li>An agent with an A+ BBB rating<\\/li><li>An agent with more recommendations than any other agent in Central Iowa (Trulia.com)<\\/li><li>A professional photographer will take the photos of your home<\\/li><li>A Professional videographer will do a video of your home (Certain price ranges only)<\\/li><li>Professional staging assistance, my interior designer will make sure your home is ready to go<\\/li><li><a href=\\\"http:\\/\\/local.charterhouseiowa.dev\\/truck#entry:9:url\\\">Free Moving Truck <\\/a>(Based on availability, first come, first served)<\\/li><li>Professional moving services (at 7% plan while staying in central Iowa)<\\/li><li>Weekly check-in updates to make sure you are happy<\\/li><li>Social Media marketing<\\/li><li><strong>And much more!<\\/strong><\\/li><\\/ul>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>No other agent offers their clients more! You will love your selling experience, I promise. I realize one size does not fit all when it comes to selling your home. I want to talk to you about your specific situation and see if I can help.<\\/p>\"}}},\"3\":\"\"}}','2015-12-28 01:02:06','2015-12-28 01:02:06','3651c0e6-b184-401e-98cc-980634751c13'),
	(70,36,14,1,'en_us',7,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Buyers & Sellers\",\"slug\":\"buyers-sellers\",\"postDate\":1450587650,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":{\"59\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<h3>Buying & Selling<\\/h3><p>So you are are thinking about buying or selling. That is great, here are a few things you need to know!<\\/p>\\r\\n\\r\\n<h2>Buyers<\\/h2>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Buying a home is a big purchase, one of the biggest you will ever make. Why in the world would anyone want to make that purchase without the help of a professional real estate agent? Beats me, but many try. The most important thing to remember is that when you are buying a home, you DO NOT PAY YOUR AGENT! The seller of the home pays the entire commission and that commission is split between the agent selling the home and the agent representing the buyer. There is no reason to not have your own agent representing you and working hard on your behalf.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>I have talked to many buyers who have said something like \\\"When we bought, we just used the agent who had the house listed.\\\" If you are thinking about doing this, <strong>use extreme caution!<\\/strong>&nbsp;This should only be done if you have complete trust in the agent. If you just met them, this is a terrible idea! This situation is called Dual Agency and it happens when one agent represents both parties. That agent is trying to get the most money for the seller and also get you the best price. How can they do this? It is tricky and again, should only be done if you have total faith in that agent.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>The way to avoid this is to hire me! Remember, I can show you <em>all<\\/em> homes, not just the ones that I list.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>So when you are my buyer what do you get? Here is a summary of what my buyers get when they work with me:<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>An agent with 24\\/7 availability<\\/li><li>An agent with a A+ BBB rating<\\/li><li>An agent with more recommendations than any other agent in Central Iowa (Trulia.com)<\\/li><li><a href=\\\"http:\\/\\/local.charterhouseiowa.dev\\/truck#entry:9:url\\\">Free Moving Truck<\\/a> (based on availability, first come, first served)<\\/li><li>Access to my interior designer if you need help or suggestion on how to furnish your new home<\\/li><li>Lender recommendations, I can get you to the right lender who will get you closed on time and with no surprises<\\/li><li>An experienced agent, I have been selling real estate full-time since 2005<\\/li><li>Auto-prospecting so you never miss a new listing to the market<\\/li><li>Weekly check-in updates to make sure you are happy<\\/li><li><strong>And much more!<\\/strong><\\/li><\\/ul>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>When you work with me, you are working with a one-of-a-kind agent who simply offers my clients more than other agents. Please read through the client reviews on this site to get a good feel of how I treat my clients. You will never regret hiring me as your agent!<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<h2>Sellers<\\/h2>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>When the time comes to put your home on the market, hiring a real estate professional is a crucial step in the right direction. I realize that there are many home-selling options, but the tried and true method of hiring an agent is still the best option by far. It is proven to be the safest way to sell as well as being the way to generate the highest selling price.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>But, just listing with an agent is not enough! You have to list with an agent who knows how to market and embraces today\'s trends like social media to reach the masses. You want your home exposed everywhere to reach every possible buyer. But you also want to work with an agent who will be enjoyable to work with and one who has great services and benefits to offer you. That agent is me!<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>I would love to talk to you about listing your home but for now, here is a brief summary of what I offer to sellers:<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>An agent with 24\\/7 availability<\\/li><li>An agent with an A+ BBB rating<\\/li><li>An agent with more recommendations than any other agent in Central Iowa (Trulia.com)<\\/li><li>A professional photographer will take the photos of your home<\\/li><li>A Professional videographer will do a video of your home (Certain price ranges only)<\\/li><li>Professional staging assistance, my interior designer will make sure your home is ready to go<\\/li><li><a href=\\\"http:\\/\\/local.charterhouseiowa.dev\\/truck#entry:9:url\\\">Free Moving Truck <\\/a>(Based on availability, first come, first served)<\\/li><li>Professional moving services (at 7% plan while staying in central Iowa)<\\/li><li>Weekly check-in updates to make sure you are happy<\\/li><li>Social Media marketing<\\/li><li><strong>And much more!<\\/strong><\\/li><\\/ul>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>No other agent offers their clients more! You will love your selling experience, I promise. I realize one size does not fit all when it comes to selling your home. I want to talk to you about your specific situation and see if I can help.<\\/p>\"}}},\"3\":\"\"}}','2015-12-28 01:03:42','2015-12-28 01:03:42','cf28f6e5-df26-4af1-aa0a-8639706fdbe5'),
	(71,2,1,1,'en_us',4,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Mark Charter Realty\",\"slug\":\"homepage\",\"postDate\":1450485935,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>s<\\/p>\",\"49\":{\"new1\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>It\\u2019s true, this site doesn\\u2019t have a whole lot of content yet, but don\\u2019t worry. Our web developers have just installed the CMS, and they\\u2019re setting things up for the content editors this very moment. Soon Local.charterhouseiowa.dev will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.<\\/p>\"}}},\"3\":\"\"}}','2015-12-28 01:04:55','2015-12-28 01:04:55','c6882463-1da7-416c-80c7-2a16fc45feed'),
	(72,61,16,1,'en_us',1,'','{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Mortgage - The Tyler Osby Team\",\"slug\":\"mortgage-the-tyler-osby-team\",\"postDate\":1451264919,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"49\":{\"new2\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Tyler Osby is where Mark turns when his clients need a mortgage or a refinance. Tyler is knowledgeable, accountable and trustworthy. He gets the job done every time!<\\/p>\"}}},\"48\":\"\",\"47\":\"(515) 257-6729\",\"46\":\" http:\\/\\/www.wealthwithmortgage.com\"}}','2015-12-28 01:08:39','2015-12-28 01:08:39','ddee499f-eb24-4945-91b8-7519444d30ba'),
	(73,61,16,1,'en_us',2,'','{\"typeId\":\"16\",\"authorId\":\"1\",\"title\":\"Mortgage - The Tyler Osby Team\",\"slug\":\"mortgage-the-tyler-osby-team\",\"postDate\":1451264880,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"49\":{\"62\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"\"}}},\"48\":\"\",\"3\":\"\",\"47\":\"(515) 257-6729\",\"46\":\" http:\\/\\/www.wealthwithmortgage.com\"}}','2015-12-28 01:15:49','2015-12-28 01:15:49','8404347d-2bb1-4d59-896d-1a9ba6759eb0'),
	(74,61,16,1,'en_us',3,'','{\"typeId\":\"16\",\"authorId\":\"1\",\"title\":\"Mortgage - The Tyler Osby Team\",\"slug\":\"mortgage-the-tyler-osby-team\",\"postDate\":1451264880,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"1\":\"<p>Tyler Osby is where Mark turns when his clients need a mortgage or a refinance. Tyler is knowledgeable, accountable and trustworthy. He gets the job done every time!<\\/p>\",\"48\":\"\",\"3\":\"\",\"47\":\"(515) 257-6729\",\"46\":\" http:\\/\\/www.wealthwithmortgage.com\"}}','2015-12-28 01:16:09','2015-12-28 01:16:09','cb748cba-fdf3-46ab-9fa6-c7809e943e53'),
	(75,61,16,1,'en_us',4,'','{\"typeId\":\"16\",\"authorId\":\"1\",\"title\":\"Mortgage - The Tyler Osby Team\",\"slug\":\"mortgage-the-tyler-osby-team\",\"postDate\":1451264880,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"1\":\"<p>Tyler Osby is where Mark turns when his clients need a mortgage or a refinance. Tyler is knowledgeable, accountable and trustworthy. He gets the job done every time!<\\/p>\",\"48\":\"\",\"3\":\"\",\"47\":\"(515) 257-6729\",\"46\":\" http:\\/\\/www.wealthwithmortgage.com\"}}','2015-12-28 01:18:41','2015-12-28 01:18:41','8fbe4989-d4c2-48eb-b40c-a17fd3161406'),
	(76,2,1,1,'en_us',5,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Mark Charter Realty\",\"slug\":\"homepage\",\"postDate\":1450485935,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":{\"60\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"\"}}},\"3\":\"\"}}','2015-12-28 01:22:51','2015-12-28 01:22:51','b51ced5a-6e71-4f93-8c68-03eb770244a0'),
	(77,2,1,1,'en_us',6,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Mark Charter Realty\",\"slug\":\"homepage\",\"postDate\":1450485935,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":{\"60\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vehicula eget diam ac sagittis. Maecenas efficitur aliquet ipsum, id fringilla quam aliquam sit amet. Quisque eu eros nec lectus fermentum aliquet. Curabitur magna est, vulputate ac quam in, malesuada eleifend felis. In eget suscipit libero. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla facilisi. Etiam tempor facilisis nisi at tempus. Morbi felis felis, eleifend ac nulla ut, dignissim sagittis nisl. Aenean tempus finibus dignissim. Nulla tempus fringilla ante, sed tempus arcu placerat sit amet. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.<\\/p>\"}}},\"3\":\"\"}}','2015-12-28 01:23:47','2015-12-28 01:23:47','7de5a5eb-d9ff-45d0-87e9-c30b901d6322'),
	(78,5,6,1,'en_us',3,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Partners\",\"slug\":\"partners\",\"postDate\":1450486386,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"\",\"49\":{\"new1\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>It\\u2019s true, this site doesn\\u2019t have a whole lot of content yet, but don\\u2019t worry. Our web developers have just installed the CMS, and they\\u2019re setting things up for the content editors this very moment. Soon Local.charterhouseiowa.dev will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.<\\/p>\"}}},\"3\":\"\"}}','2015-12-28 01:27:07','2015-12-28 01:27:07','2d6df5c5-0b95-457e-8492-7bc6b8ccb8ff'),
	(79,5,6,1,'en_us',4,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Partners\",\"slug\":\"partners\",\"postDate\":1450486386,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":{\"63\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"\"}}},\"3\":\"\"}}','2015-12-28 01:28:04','2015-12-28 01:28:04','c7f94a19-92b3-48a5-bf8b-9e9772c42bb5'),
	(80,5,6,1,'en_us',5,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Partners\",\"slug\":\"partners\",\"postDate\":1450486386,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":{\"63\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Maecenas accumsan metus non turpis mattis laoreet. Donec luctus tincidunt sem, vel feugiat sapien ornare ac. Mauris in purus dui. Nullam commodo ut elit quis blandit. Quisque tristique ullamcorper nunc, et elementum odio vestibulum vel. Proin sit amet sollicitudin ligula, sit amet mollis libero. Vestibulum vulputate nec nisl et aliquam. Suspendisse finibus nec nulla sit amet feugiat.<\\/p>\"}}},\"3\":\"\"}}','2015-12-28 01:28:25','2015-12-28 01:28:25','ce1f6b12-6eed-40f8-ab1f-8f5066c96f24'),
	(81,5,6,1,'en_us',6,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Partners\",\"slug\":\"partners\",\"postDate\":1450486386,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":{\"63\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Maecenas accumsan metus non turpis mattis laoreet. Donec luctus tincidunt sem, vel feugiat sapien ornare ac. Mauris in purus dui. Nullam commodo ut elit quis blandit. Quisque tristique ullamcorper nunc, et elementum odio vestibulum vel. Proin sit amet sollicitudin ligula, sit amet mollis libero. Vestibulum vulputate nec nisl et aliquam. Suspendisse finibus nec nulla sit amet feugiat.<\\/p>\"}},\"new1\":{\"type\":\"quote\",\"enabled\":\"1\",\"fields\":{\"quote\":\"<p>Testing<\\/p>\",\"quoteAttribution\":\"Me\"}}},\"3\":\"\"}}','2015-12-28 01:28:51','2015-12-28 01:28:51','73c4a5b2-f517-479a-8c7d-19263eb1a5ec'),
	(82,5,6,1,'en_us',7,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Partners\",\"slug\":\"partners\",\"postDate\":1450486386,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":{\"63\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Maecenas accumsan metus non turpis mattis laoreet. Donec luctus tincidunt sem, vel feugiat sapien ornare ac. Mauris in purus dui. Nullam commodo ut elit quis blandit. Quisque tristique ullamcorper nunc, et elementum odio vestibulum vel. Proin sit amet sollicitudin ligula, sit amet mollis libero. Vestibulum vulputate nec nisl et aliquam. Suspendisse finibus nec nulla sit amet feugiat.<\\/p>\"}}},\"3\":\"\"}}','2015-12-28 01:29:00','2015-12-28 01:29:00','f60c434d-7910-43cf-9e1d-a9d6e14e7f9a'),
	(83,17,4,1,'en_us',7,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Amazing golf course home with too many amenities to list!\",\"slug\":\"amazing-golf-course-home-with-too-many-amenities-to-list\",\"postDate\":1450493520,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"1210 NE 42nd St\",\"street2\":\"\",\"city\":\"Ankeny\",\"state\":\"IA\",\"zip\":\"50021\",\"country\":\"United States\",\"lat\":\"41.76575850\",\"lng\":\"-93.58654680\"},\"8\":\"4.5\",\"9\":\"5\",\"49\":{\"new1\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Maecenas accumsan metus non turpis mattis laoreet. Donec luctus tincidunt sem, vel feugiat sapien ornare ac. Mauris in purus dui. Nullam commodo ut elit quis blandit. Quisque tristique ullamcorper nunc, et elementum odio vestibulum vel. Proin sit amet sollicitudin ligula, sit amet mollis libero. Vestibulum vulputate nec nisl et aliquam. Suspendisse finibus nec nulla sit amet feugiat.<\\/p>\"}}},\"10\":\"\",\"20\":\"\",\"11\":\"504907\",\"3\":\"\",\"17\":\"3 Car Garage\",\"18\":\"$849,900\",\"12\":\"singleFamily\",\"13\":\"\",\"14\":\"2,426\",\"19\":\"yZRyXkGqD2c\",\"21\":\"\"}}','2015-12-28 01:29:55','2015-12-28 01:29:55','835a6577-6a71-4db4-b896-ae05d6313968'),
	(84,18,4,1,'en_us',12,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Absolutely amazing, modern rural-meets-urban set up just outside of Grimes!! A must see at $719,000!\",\"slug\":\"absolutely-amazing-modern-rural-meets-urban-set-up-just-outside-of-grimes\",\"postDate\":1450539000,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"7965 NW 142nd St\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"United States\",\"lat\":\"41.70576500\",\"lng\":\"-93.81369100\"},\"8\":\"4\",\"9\":\"5\",\"49\":{\"new1\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Maecenas accumsan metus non turpis mattis laoreet. Donec luctus tincidunt sem, vel feugiat sapien ornare ac. Mauris in purus dui. Nullam commodo ut elit quis blandit. Quisque tristique ullamcorper nunc, et elementum odio vestibulum vel. Proin sit amet sollicitudin ligula, sit amet mollis libero. Vestibulum vulputate nec nisl et aliquam. Suspendisse finibus nec nulla sit amet feugiat.<\\/p>\"}}},\"10\":\"\",\"20\":\"1Hw5ay9jY7y\",\"11\":\"506142\",\"3\":\"\",\"17\":\"3 car attached garage - 2 car detached garage\",\"18\":\"$719,000\",\"12\":\"singleFamily\",\"13\":\"\",\"14\":\"2,700\",\"19\":\"\",\"21\":\"1990\"}}','2015-12-28 01:30:06','2015-12-28 01:30:06','310dc293-0507-4e56-8f03-66aeb38a836d'),
	(85,4,5,1,'en_us',3,'','{\"typeId\":\"5\",\"authorId\":null,\"title\":\"Properties\",\"slug\":\"properties\",\"postDate\":1450486881,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis elit massa, ornare et purus a, semper sagittis diam. In condimentum, ipsum eget luctus feugiat, est nisi ullamcorper diam, elementum placerat sapien neque nec ex. Duis pulvinar metus mauris, non commodo mauris cursus vel. Phasellus a ligula in lorem tincidunt semper. Phasellus sed nisl quis eros malesuada egestas eu ultrices massa. Cras commodo velit in arcu vehicula, et egestas magna ullamcorper. Donec sagittis viverra mi vitae mattis. Nulla semper sapien ut consequat dignissim. Nam augue sem, luctus eu dictum et, feugiat eget turpis. Nullam sit amet maximus ante. Pellentesque ut risus semper, fermentum erat id, auctor leo. Etiam cursus facilisis justo, quis interdum enim bibendum in. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque feugiat massa quis enim gravida, vel auctor nisi efficitur.<\\/p>\",\"3\":\"\"}}','2015-12-28 01:30:31','2015-12-28 01:30:31','d283bf74-9c65-4b19-97d2-2a1ac197b489'),
	(86,17,4,1,'en_us',8,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Amazing golf course home with too many amenities to list!\",\"slug\":\"amazing-golf-course-home-with-too-many-amenities-to-list\",\"postDate\":1450493520,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"1210 NE 42nd St\",\"street2\":\"\",\"city\":\"Ankeny\",\"state\":\"IA\",\"zip\":\"50021\",\"country\":\"United States\",\"lat\":\"41.76575850\",\"lng\":\"-93.58654680\"},\"8\":\"4.5\",\"9\":\"5\",\"49\":{\"65\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Maecenas accumsan metus non turpis mattis laoreet. Donec luctus tincidunt sem, vel feugiat sapien ornare ac. Mauris in purus dui. Nullam commodo ut elit quis blandit. Quisque tristique ullamcorper nunc, et elementum odio vestibulum vel. Proin sit amet sollicitudin ligula, sit amet mollis libero. Vestibulum vulputate nec nisl et aliquam. Suspendisse finibus nec nulla sit amet feugiat.<\\/p>\"}}},\"57\":\"\",\"10\":\"\",\"20\":\"\",\"11\":\"504907\",\"3\":\"\",\"17\":\"3 Car Garage\",\"18\":\"$849,900\",\"54\":[\"67\"],\"12\":\"singleFamily\",\"55\":\"available\",\"13\":\"\",\"14\":\"2,426\",\"19\":\"yZRyXkGqD2c\",\"21\":\"\"}}','2016-01-06 23:58:35','2016-01-06 23:58:35','63dc3506-81b7-4f32-be35-2a070daa6215'),
	(166,17,4,1,'en_us',9,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Amazing golf course home with too many amenities to list!\",\"slug\":\"amazing-golf-course-home-with-too-many-amenities-to-list\",\"postDate\":1450493520,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"1210 NE 42nd St\",\"street2\":\"\",\"city\":\"Ankeny\",\"state\":\"IA\",\"zip\":\"50021\",\"country\":\"United States\",\"lat\":\"41.76575850\",\"lng\":\"-93.58654680\"},\"8\":\"4.5\",\"9\":\"5\",\"49\":{\"65\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Maecenas accumsan metus non turpis mattis laoreet. Donec luctus tincidunt sem, vel feugiat sapien ornare ac. Mauris in purus dui. Nullam commodo ut elit quis blandit. Quisque tristique ullamcorper nunc, et elementum odio vestibulum vel. Proin sit amet sollicitudin ligula, sit amet mollis libero. Vestibulum vulputate nec nisl et aliquam. Suspendisse finibus nec nulla sit amet feugiat.<\\/p>\"}}},\"57\":\"\",\"10\":\"\",\"20\":\"\",\"11\":\"504907\",\"3\":\"Golf course house.\",\"17\":\"3 Car Garage\",\"18\":\"$849,900\",\"54\":[\"67\"],\"12\":\"singleFamily\",\"55\":\"available\",\"13\":\"\",\"14\":\"2,426\",\"19\":\"yZRyXkGqD2c\",\"21\":\"\"}}','2016-01-13 03:54:34','2016-01-13 03:54:34','7e4dd475-ad07-4182-9571-9fc6c972c78a'),
	(167,17,4,1,'en_us',10,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Amazing golf course home with too many amenities to list!\",\"slug\":\"amazing-golf-course-home-with-too-many-amenities-to-list\",\"postDate\":1450493520,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"1210 NE 42nd St\",\"street2\":\"\",\"city\":\"Ankeny\",\"state\":\"IA\",\"zip\":\"50021\",\"country\":\"United States\",\"lat\":\"41.76575850\",\"lng\":\"-93.58654680\"},\"8\":\"4.5\",\"9\":\"5\",\"49\":{\"65\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Maecenas accumsan metus non turpis mattis laoreet. Donec luctus tincidunt sem, vel feugiat sapien ornare ac. Mauris in purus dui. Nullam commodo ut elit quis blandit. Quisque tristique ullamcorper nunc, et elementum odio vestibulum vel. Proin sit amet sollicitudin ligula, sit amet mollis libero. Vestibulum vulputate nec nisl et aliquam. Suspendisse finibus nec nulla sit amet feugiat.<\\/p>\"}}},\"57\":\"\",\"10\":\"\",\"20\":\"\",\"11\":\"504907\",\"3\":\"Golf course house.\",\"17\":\"3 Car Garage\",\"18\":\"849,900\",\"54\":[\"67\"],\"12\":\"singleFamily\",\"55\":\"available\",\"13\":\"\",\"14\":\"2,426\",\"19\":\"yZRyXkGqD2c\",\"21\":\"\"}}','2016-01-13 04:02:49','2016-01-13 04:02:49','7981ff00-daa9-4dc8-a95c-b2913682ebfe'),
	(168,80,4,1,'en_us',1,'','{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Amazing Grimes home with plenty of room to roam!!\",\"slug\":\"amazing-grimes-home-with-plenty-of-room-to-roam\",\"postDate\":1452661400,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"11559 NW Timberbrooke Ln, Grimes\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"\",\"lat\":\"\",\"lng\":\"\"},\"8\":\"3.5\",\"9\":\"5\",\"49\":{\"new1\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Stunning 2 story home on an unbelievable piece of ground, all located in an ultra convenient location within Johnston schools. As you stand in the 2 story living room, taking in the view, you feel like you are taking in the beauty of a park. Come explore and enjoy all of the landscaping in place, including your own bridge, that leads to your shop\\/art studio\\/clubhouse on the back of the lot! You can also make a massive fire to enjoy a fall evening in the outdoor fireplace. Inside, notice the quality! Built sturdy by Unique Homes, there are tons of details to enjoy. The main floor library is a stunning room and you will love the vaulted entry. The kitchen is fresh and updated with brand new granite counters and appliances. The top floor contains 3 beds and a laundry. The master is HUGE, as is the bath! The multi level basement is perfect for entertaining and has tons of character. Other features - 3 season room, security, central vac, multiple decks, heated garage and more. Come and see!<\\/p>\"}}},\"57\":\"\",\"10\":\"\",\"20\":\"UwmwWVAhRe3\",\"11\":\"507193\",\"3\":\"\",\"17\":\"3 car garage\",\"18\":\"699,900\",\"54\":\"\",\"12\":\"singleFamily\",\"55\":\"available\",\"13\":\"\",\"14\":\"3,209\",\"19\":\"\",\"21\":\"1993\"}}','2016-01-13 05:03:20','2016-01-13 05:03:20','c59e89af-e69e-4cc0-8848-5abc3496c7ff'),
	(169,80,4,1,'en_us',2,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Amazing Grimes home with plenty of room to roam\",\"slug\":\"amazing-grimes-home-with-plenty-of-room-to-roam\",\"postDate\":1452661380,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"11559 NW Timberbrooke Ln, Grimes\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"\",\"lat\":\"\",\"lng\":\"\"},\"8\":\"3.5\",\"9\":\"5\",\"49\":{\"81\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Stunning 2 story home on an unbelievable piece of ground, all located in an ultra convenient location within Johnston schools. As you stand in the 2 story living room, taking in the view, you feel like you are taking in the beauty of a park. Come explore and enjoy all of the landscaping in place, including your own bridge, that leads to your shop\\/art studio\\/clubhouse on the back of the lot!&nbsp;<\\/p><p>You can also make a massive fire to enjoy a fall evening in the outdoor fireplace. Inside, notice the quality! Built sturdy by Unique Homes, there are tons of details to enjoy. The main floor library is a stunning room and you will love the vaulted entry. The kitchen is fresh and updated with brand new granite counters and appliances. The top floor contains 3 beds and a laundry.&nbsp;<\\/p><p>The master is HUGE, as is the bath! The multi level basement is perfect for entertaining and has tons of character. Other features - 3 season room, security, central vac, multiple decks, heated garage and more. Come and see!<\\/p>\"}}},\"57\":\"\",\"10\":\"\",\"20\":\"UwmwWVAhRe3\",\"11\":\"507193\",\"3\":\"\",\"17\":\"3 car garage\",\"18\":\"699,900\",\"54\":\"\",\"12\":\"singleFamily\",\"55\":\"available\",\"13\":\"\",\"14\":\"3,209\",\"19\":\"\",\"21\":\"1993\"}}','2016-01-13 05:05:11','2016-01-13 05:05:11','4d1f06b8-a4cb-4a3f-9ce6-56c4201f8ec4'),
	(170,80,4,1,'en_us',3,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Amazing Grimes home with plenty of room to roam\",\"slug\":\"amazing-grimes-home-with-plenty-of-room-to-roam\",\"postDate\":1452661380,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"11559 NW Timberbrooke Ln, Grimes\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"\",\"lat\":\"\",\"lng\":\"\"},\"8\":\"3.5\",\"9\":\"5\",\"49\":{\"81\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Stunning 2 story home on an unbelievable piece of ground, all located in an ultra convenient location within Johnston schools. As you stand in the 2 story living room, taking in the view, you feel like you are taking in the beauty of a park. Come explore and enjoy all of the landscaping in place, including your own bridge, that leads to your shop\\/art studio\\/clubhouse on the back of the lot!&nbsp;<\\/p>\\r\\n\\r\\n<p>You can also make a massive fire to enjoy a fall evening in the outdoor fireplace. Inside, notice the quality! Built sturdy by Unique Homes, there are tons of details to enjoy. The main floor library is a stunning room and you will love the vaulted entry. The kitchen is fresh and updated with brand new granite counters and appliances. The top floor contains 3 beds and a laundry.&nbsp;<\\/p>\\r\\n\\r\\n<p>The master is HUGE, as is the bath! The multi level basement is perfect for entertaining and has tons of character. Other features - 3 season room, security, central vac, multiple decks, heated garage and more. Come and see!<\\/p>\"}},\"new1\":{\"type\":\"quote\",\"enabled\":\"1\",\"fields\":{\"quote\":\"<p>This property is the shit.<\\/p>\",\"quoteAttribution\":\"Unknown\"}}},\"57\":\"\",\"10\":\"\",\"20\":\"UwmwWVAhRe3\",\"11\":\"507193\",\"3\":\"\",\"17\":\"3 car garage\",\"18\":\"699,900\",\"54\":\"\",\"12\":\"singleFamily\",\"55\":\"available\",\"13\":\"\",\"14\":\"3,209\",\"19\":\"\",\"21\":\"1993\"}}','2016-01-13 05:05:40','2016-01-13 05:05:40','57666fcf-c413-4a7c-9f5c-cb6baf72cc2b'),
	(171,80,4,1,'en_us',4,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Amazing Grimes home with plenty of room to roaM!\",\"slug\":\"amazing-grimes-home-with-plenty-of-room-to-roam\",\"postDate\":1452661380,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"11559 NW Timberbrooke Ln, Grimes\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"\",\"lat\":\"\",\"lng\":\"\"},\"8\":\"3.5\",\"9\":\"5\",\"49\":{\"81\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Stunning 2 story home on an unbelievable piece of ground, all located in an ultra convenient location within Johnston schools. As you stand in the 2 story living room, taking in the view, you feel like you are taking in the beauty of a park. Come explore and enjoy all of the landscaping in place, including your own bridge, that leads to your shop\\/art studio\\/clubhouse on the back of the lot!&nbsp;<\\/p>\\r\\n\\r\\n<p>You can also make a massive fire to enjoy a fall evening in the outdoor fireplace. Inside, notice the quality! Built sturdy by Unique Homes, there are tons of details to enjoy. The main floor library is a stunning room and you will love the vaulted entry. The kitchen is fresh and updated with brand new granite counters and appliances. The top floor contains 3 beds and a laundry.&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>The master is HUGE, as is the bath! The multi level basement is perfect for entertaining and has tons of character. Other features - 3 season room, security, central vac, multiple decks, heated garage and more. Come and see!<\\/p>\"}},\"82\":{\"type\":\"quote\",\"enabled\":\"1\",\"fields\":{\"quote\":\"<p>This property is the shit.<\\/p>\",\"quoteAttribution\":\"Unknown\"}}},\"57\":\"\",\"10\":\"\",\"20\":\"UwmwWVAhRe3\",\"11\":\"507193\",\"3\":\"\",\"17\":\"3 car garage\",\"18\":\"699,900\",\"54\":\"\",\"12\":\"singleFamily\",\"55\":\"available\",\"13\":\"\",\"14\":\"3,209\",\"19\":\"\",\"21\":\"1993\"}}','2016-01-13 05:06:17','2016-01-13 05:06:17','3d0f0cf3-d0fe-455c-a062-cabcad71c7e2'),
	(172,80,4,1,'en_us',5,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"MAmazing Grimes home with plenty of room to roam!\",\"slug\":\"amazing-grimes-home-with-plenty-of-room-to-roam\",\"postDate\":1452661380,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"11559 NW Timberbrooke Ln, Grimes\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"\",\"lat\":\"\",\"lng\":\"\"},\"8\":\"3.5\",\"9\":\"5\",\"49\":{\"81\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Stunning 2 story home on an unbelievable piece of ground, all located in an ultra convenient location within Johnston schools. As you stand in the 2 story living room, taking in the view, you feel like you are taking in the beauty of a park. Come explore and enjoy all of the landscaping in place, including your own bridge, that leads to your shop\\/art studio\\/clubhouse on the back of the lot!&nbsp;<\\/p>\\r\\n\\r\\n<p>You can also make a massive fire to enjoy a fall evening in the outdoor fireplace. Inside, notice the quality! Built sturdy by Unique Homes, there are tons of details to enjoy. The main floor library is a stunning room and you will love the vaulted entry. The kitchen is fresh and updated with brand new granite counters and appliances. The top floor contains 3 beds and a laundry.&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>The master is HUGE, as is the bath! The multi level basement is perfect for entertaining and has tons of character. Other features - 3 season room, security, central vac, multiple decks, heated garage and more. Come and see!<\\/p>\"}},\"82\":{\"type\":\"quote\",\"enabled\":\"1\",\"fields\":{\"quote\":\"<p>This property is the shit.<\\/p>\",\"quoteAttribution\":\"Unknown\"}}},\"57\":\"\",\"10\":\"\",\"20\":\"UwmwWVAhRe3\",\"11\":\"507193\",\"3\":\"\",\"17\":\"3 car garage\",\"18\":\"699,900\",\"54\":\"\",\"12\":\"singleFamily\",\"55\":\"available\",\"13\":\"\",\"14\":\"3,209\",\"19\":\"\",\"21\":\"1993\"}}','2016-01-13 05:06:25','2016-01-13 05:06:25','efd88d06-fc5a-45bb-b6b6-146ba6c884e1'),
	(173,17,4,1,'en_us',11,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Amazing golf course home with too many amenities to list!\",\"slug\":\"amazing-golf-course-home-with-too-many-amenities-to-list\",\"postDate\":1450493520,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"1210 NE 42nd St\",\"street2\":\"\",\"city\":\"Ankeny\",\"state\":\"IA\",\"zip\":\"50021\",\"country\":\"United States\",\"lat\":\"41.76575850\",\"lng\":\"-93.58654680\"},\"8\":\"4.5\",\"9\":\"1\",\"49\":{\"65\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Maecenas accumsan metus non turpis mattis laoreet. Donec luctus tincidunt sem, vel feugiat sapien ornare ac. Mauris in purus dui. Nullam commodo ut elit quis blandit. Quisque tristique ullamcorper nunc, et elementum odio vestibulum vel. Proin sit amet sollicitudin ligula, sit amet mollis libero. Vestibulum vulputate nec nisl et aliquam. Suspendisse finibus nec nulla sit amet feugiat.<\\/p>\"}}},\"57\":\"\",\"10\":\"\",\"20\":\"\",\"11\":\"504907\",\"3\":\"Golf course house.\",\"17\":\"3 Car Garage\",\"18\":\"849,900\",\"54\":[\"67\"],\"12\":\"singleFamily\",\"55\":\"available\",\"13\":\"\",\"14\":\"2,426\",\"19\":\"yZRyXkGqD2c\",\"21\":\"\"}}','2016-01-14 02:36:11','2016-01-14 02:36:11','72d05d14-9325-437e-81de-217f9f10902e'),
	(174,80,4,1,'en_us',6,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Amazing Grimes home with plenty of room to roam!\",\"slug\":\"amazing-grimes-home-with-plenty-of-room-to-roam\",\"postDate\":1452661380,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"11559 NW Timberbrooke Ln\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"\",\"lat\":\"\",\"lng\":\"\"},\"8\":\"3.5\",\"9\":\"5\",\"49\":{\"81\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Stunning 2 story home on an unbelievable piece of ground, all located in an ultra convenient location within Johnston schools. As you stand in the 2 story living room, taking in the view, you feel like you are taking in the beauty of a park. Come explore and enjoy all of the landscaping in place, including your own bridge, that leads to your shop\\/art studio\\/clubhouse on the back of the lot!&nbsp;<\\/p>\\r\\n\\r\\n<p>You can also make a massive fire to enjoy a fall evening in the outdoor fireplace. Inside, notice the quality! Built sturdy by Unique Homes, there are tons of details to enjoy. The main floor library is a stunning room and you will love the vaulted entry. The kitchen is fresh and updated with brand new granite counters and appliances. The top floor contains 3 beds and a laundry.&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>The master is HUGE, as is the bath! The multi level basement is perfect for entertaining and has tons of character. Other features - 3 season room, security, central vac, multiple decks, heated garage and more. Come and see!<\\/p>\"}},\"82\":{\"type\":\"quote\",\"enabled\":\"1\",\"fields\":{\"quote\":\"<p>This property is the shit.<\\/p>\",\"quoteAttribution\":\"Unknown\"}}},\"57\":\"\",\"10\":\"\",\"20\":\"UwmwWVAhRe3\",\"11\":\"507193\",\"3\":\"\",\"17\":\"3 car garage\",\"18\":\"699,900\",\"54\":\"\",\"12\":\"singleFamily\",\"55\":\"available\",\"13\":\"\",\"14\":\"3,209\",\"19\":\"\",\"21\":\"1993\"}}','2016-01-14 03:38:06','2016-01-14 03:38:06','5507d17c-7aa6-4482-a996-918617efef15'),
	(175,2,1,1,'en_us',7,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Mark Charter Realty\",\"slug\":\"homepage\",\"postDate\":1450485935,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":\"\",\"3\":\"\"}}','2016-01-14 03:48:33','2016-01-14 03:48:33','7820a3b0-90a5-4409-a11c-71877f654bae'),
	(176,2,1,1,'en_us',8,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Mark Charter Realty\",\"slug\":\"homepage\",\"postDate\":1450485935,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":\"\",\"66\":[\"83\"],\"3\":\"\"}}','2016-01-14 03:56:36','2016-01-14 03:56:36','75667b3e-9d90-4cf4-9b6b-c6dcce6840ae'),
	(177,2,1,1,'en_us',9,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Mark Charter Realty\",\"slug\":\"homepage\",\"postDate\":1450485935,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"67\":{\"new1\":{\"type\":\"buyerContent\",\"enabled\":\"1\",\"fields\":{\"icon\":\"\",\"headline\":\"24\\/7 Availability\",\"bodyCopy\":\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit sapiente repellendus, delectus a veniam provident maxime distinctio ipsum sint nulla incidunt vel dolorum ea dolore!<\\/p>\"}}},\"66\":[\"83\"],\"3\":\"\"}}','2016-01-14 04:40:36','2016-01-14 04:40:36','d903b792-6deb-46ba-8da8-617cbc87e623'),
	(178,2,1,1,'en_us',10,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Mark Charter Realty\",\"slug\":\"homepage\",\"postDate\":1450485935,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"67\":{\"84\":{\"type\":\"buyerContent\",\"enabled\":\"1\",\"fields\":{\"icon\":[\"85\"],\"headline\":\"24\\/7 Availability\",\"bodyCopy\":\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit sapiente repellendus, delectus a veniam provident maxime distinctio ipsum sint nulla incidunt vel dolorum ea dolore!<\\/p>\"}}},\"66\":[\"83\"],\"3\":\"\"}}','2016-01-14 04:44:09','2016-01-14 04:44:09','1068bac6-8a16-4f4a-a728-f9fb16953a4f'),
	(179,2,1,1,'en_us',11,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Mark Charter Realty\",\"slug\":\"homepage\",\"postDate\":1450485935,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"67\":{\"84\":{\"type\":\"buyerContent\",\"enabled\":\"1\",\"fields\":{\"icon\":[\"85\"],\"headline\":\"24\\/7 Availability\",\"bodyCopy\":\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit sapiente repellendus, delectus a veniam provident maxime distinctio ipsum sint nulla incidunt vel dolorum ea dolore!<\\/p>\"}},\"new1\":{\"type\":\"buyerContent\",\"enabled\":\"1\",\"fields\":{\"icon\":[\"86\"],\"headline\":\"Access to Interior Designer\",\"bodyCopy\":\"<p>Repellat ea sunt saepe libero repellendus itaque autem omnis, dolore! Reiciendis voluptatibus qui neque doloremque nisi. Ullam illum nihil iure magnam ex, sint, nostrum mollitia?<\\/p>\"}}},\"66\":[\"83\"],\"3\":\"\"}}','2016-01-14 04:46:00','2016-01-14 04:46:00','53229549-7c69-46df-93ba-2a59f1a5eef1'),
	(180,2,1,1,'en_us',12,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Mark Charter Realty\",\"slug\":\"homepage\",\"postDate\":1450485935,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"67\":{\"84\":{\"type\":\"buyerContent\",\"enabled\":\"1\",\"fields\":{\"icon\":[\"85\"],\"headline\":\"24\\/7 Availability\",\"bodyCopy\":\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit sapiente repellendus, delectus a veniam provident maxime distinctio ipsum sint nulla incidunt vel dolorum ea dolore!<\\/p>\"}},\"87\":{\"type\":\"buyerContent\",\"enabled\":\"1\",\"fields\":{\"icon\":[\"86\"],\"headline\":\"Access to Interior Designer\",\"bodyCopy\":\"<p>Repellat ea sunt saepe libero repellendus itaque autem omnis, dolore! Reiciendis voluptatibus qui neque doloremque nisi. Ullam illum nihil iure magnam ex, sint, nostrum mollitia?<\\/p>\"}},\"new1\":{\"type\":\"buyerContent\",\"enabled\":\"1\",\"fields\":{\"icon\":[\"88\"],\"headline\":\"Lender Recommendations\",\"bodyCopy\":\"<p>Ab dignissimos hic sequi! Porro asperiores, quis reprehenderit, sit ratione rem esse, non suscipit laborum ipsa cumque corporis in? Optio, libero, enim. Quaerat, voluptates, nesciunt.<\\/p>\"}}},\"66\":[\"83\"],\"3\":\"\"}}','2016-01-14 04:47:44','2016-01-14 04:47:44','666a4462-c7d3-4690-a056-1898f6351ed0'),
	(181,2,1,1,'en_us',13,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Mark Charter Realty\",\"slug\":\"homepage\",\"postDate\":1450485935,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"67\":{\"87\":{\"type\":\"buyerContent\",\"enabled\":\"1\",\"fields\":{\"icon\":[\"86\"],\"headline\":\"Access to Interior Designer\",\"bodyCopy\":\"<p>Repellat ea sunt saepe libero repellendus itaque autem omnis, dolore! Reiciendis voluptatibus qui neque doloremque nisi. Ullam illum nihil iure magnam ex, sint, nostrum mollitia?<\\/p>\"}},\"84\":{\"type\":\"buyerContent\",\"enabled\":\"1\",\"fields\":{\"icon\":[\"85\"],\"headline\":\"24\\/7 Availability\",\"bodyCopy\":\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit sapiente repellendus, delectus a veniam provident maxime distinctio ipsum sint nulla incidunt vel dolorum ea dolore!<\\/p>\"}},\"89\":{\"type\":\"buyerContent\",\"enabled\":\"1\",\"fields\":{\"icon\":[\"88\"],\"headline\":\"Lender Recommendations\",\"bodyCopy\":\"<p>Ab dignissimos hic sequi! Porro asperiores, quis reprehenderit, sit ratione rem esse, non suscipit laborum ipsa cumque corporis in? Optio, libero, enim. Quaerat, voluptates, nesciunt.<\\/p>\"}}},\"66\":[\"83\"],\"3\":\"\"}}','2016-01-14 04:48:02','2016-01-14 04:48:02','2a0ce5dd-0ce1-4c89-9c69-3b2045dbe3c7'),
	(182,2,1,1,'en_us',14,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Mark Charter Realty\",\"slug\":\"homepage\",\"postDate\":1450485935,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"67\":{\"84\":{\"type\":\"buyerContent\",\"enabled\":\"1\",\"fields\":{\"icon\":[\"85\"],\"headline\":\"24\\/7 Availability\",\"bodyCopy\":\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit sapiente repellendus, delectus a veniam provident maxime distinctio ipsum sint nulla incidunt vel dolorum ea dolore!<\\/p>\"}},\"87\":{\"type\":\"buyerContent\",\"enabled\":\"1\",\"fields\":{\"icon\":[\"86\"],\"headline\":\"Access to Interior Designer\",\"bodyCopy\":\"<p>Repellat ea sunt saepe libero repellendus itaque autem omnis, dolore! Reiciendis voluptatibus qui neque doloremque nisi. Ullam illum nihil iure magnam ex, sint, nostrum mollitia?<\\/p>\"}},\"89\":{\"type\":\"buyerContent\",\"enabled\":\"1\",\"fields\":{\"icon\":[\"88\"],\"headline\":\"Lender Recommendations\",\"bodyCopy\":\"<p>Ab dignissimos hic sequi! Porro asperiores, quis reprehenderit, sit ratione rem esse, non suscipit laborum ipsa cumque corporis in? Optio, libero, enim. Quaerat, voluptates, nesciunt.<\\/p>\"}}},\"66\":[\"83\"],\"3\":\"\"}}','2016-01-14 04:48:11','2016-01-14 04:48:11','3d9d1ed9-681a-424f-aba5-d30c6f00ecc9'),
	(183,4,5,1,'en_us',4,'','{\"typeId\":\"5\",\"authorId\":null,\"title\":\"Listings\",\"slug\":\"properties\",\"postDate\":1452744149,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis elit massa, ornare et purus a, semper sagittis diam. In condimentum, ipsum eget luctus feugiat, est nisi ullamcorper diam, elementum placerat sapien neque nec ex. Duis pulvinar metus mauris, non commodo mauris cursus vel. Phasellus a ligula in lorem tincidunt semper. Phasellus sed nisl quis eros malesuada egestas eu ultrices massa. Cras commodo velit in arcu vehicula, et egestas magna ullamcorper. Donec sagittis viverra mi vitae mattis. Nulla semper sapien ut consequat dignissim. Nam augue sem, luctus eu dictum et, feugiat eget turpis. Nullam sit amet maximus ante. Pellentesque ut risus semper, fermentum erat id, auctor leo. Etiam cursus facilisis justo, quis interdum enim bibendum in. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque feugiat massa quis enim gravida, vel auctor nisi efficitur.<\\/p>\",\"3\":\"\"}}','2016-01-14 04:55:14','2016-01-14 04:55:14','b8891c6e-088f-4457-ab78-ba8287f52c33'),
	(184,4,5,1,'en_us',5,'','{\"typeId\":\"5\",\"authorId\":null,\"title\":\"Listings\",\"slug\":\"properties\",\"postDate\":1452744149,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"\",\"3\":\"\"}}','2016-01-14 04:55:32','2016-01-14 04:55:32','c62b8d00-a498-4f60-b35a-24d23924ef3a'),
	(185,4,5,1,'en_us',6,'','{\"typeId\":\"5\",\"authorId\":null,\"title\":\"Listings\",\"slug\":\"properties\",\"postDate\":1452744149,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"\",\"66\":[\"90\"],\"3\":\"\"}}','2016-01-14 05:01:04','2016-01-14 05:01:04','d3d96ef3-6bd9-4786-88c7-f6352b292dd5'),
	(186,17,4,1,'en_us',12,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Amazing golf course home with too many amenities to list!\",\"slug\":\"504907\",\"postDate\":1450493520,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"1210 NE 42nd St\",\"street2\":\"\",\"city\":\"Ankeny\",\"state\":\"IA\",\"zip\":\"50021\",\"country\":\"United States\",\"lat\":\"41.76575850\",\"lng\":\"-93.58654680\"},\"8\":\"4.5\",\"9\":\"1\",\"49\":{\"65\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Maecenas accumsan metus non turpis mattis laoreet. Donec luctus tincidunt sem, vel feugiat sapien ornare ac. Mauris in purus dui. Nullam commodo ut elit quis blandit. Quisque tristique ullamcorper nunc, et elementum odio vestibulum vel. Proin sit amet sollicitudin ligula, sit amet mollis libero. Vestibulum vulputate nec nisl et aliquam. Suspendisse finibus nec nulla sit amet feugiat.<\\/p>\"}}},\"57\":\"\",\"10\":\"\",\"20\":\"\",\"11\":\"504907\",\"3\":\"Golf course house.\",\"17\":\"3 Car Garage\",\"18\":\"849,900\",\"54\":[\"67\"],\"12\":\"singleFamily\",\"55\":\"available\",\"13\":\"\",\"14\":\"2,426\",\"19\":\"yZRyXkGqD2c\",\"21\":\"\"}}','2016-01-14 05:09:52','2016-01-14 05:09:52','9aa95793-742a-4473-857c-614a4ba7e978'),
	(187,18,4,1,'en_us',13,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Absolutely amazing, modern rural-meets-urban set up just outside of Grimes!! A must see at $719,000!\",\"slug\":\"506142\",\"postDate\":1450539000,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"7965 NW 142nd St\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"United States\",\"lat\":\"41.70576500\",\"lng\":\"-93.81369100\"},\"8\":\"4\",\"9\":\"5\",\"49\":{\"66\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Maecenas accumsan metus non turpis mattis laoreet. Donec luctus tincidunt sem, vel feugiat sapien ornare ac. Mauris in purus dui. Nullam commodo ut elit quis blandit. Quisque tristique ullamcorper nunc, et elementum odio vestibulum vel. Proin sit amet sollicitudin ligula, sit amet mollis libero. Vestibulum vulputate nec nisl et aliquam. Suspendisse finibus nec nulla sit amet feugiat.<\\/p>\"}}},\"57\":\"\",\"10\":\"\",\"20\":\"1Hw5ay9jY7y\",\"11\":\"506142\",\"3\":\"\",\"17\":\"3 car attached garage - 2 car detached garage\",\"18\":\"$719,000\",\"54\":\"\",\"12\":\"singleFamily\",\"55\":\"available\",\"13\":\"\",\"14\":\"2,700\",\"19\":\"\",\"21\":\"1990\"}}','2016-01-14 05:10:18','2016-01-14 05:10:18','0ee0f1c2-c801-4e7b-8de5-d8c7969b8926'),
	(188,80,4,1,'en_us',7,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Amazing Grimes home with plenty of room to roam!\",\"slug\":\"507193\",\"postDate\":1452661380,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"11559 NW Timberbrooke Ln\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"\",\"lat\":\"\",\"lng\":\"\"},\"8\":\"3.5\",\"9\":\"5\",\"49\":{\"81\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Stunning 2 story home on an unbelievable piece of ground, all located in an ultra convenient location within Johnston schools. As you stand in the 2 story living room, taking in the view, you feel like you are taking in the beauty of a park. Come explore and enjoy all of the landscaping in place, including your own bridge, that leads to your shop\\/art studio\\/clubhouse on the back of the lot!&nbsp;<\\/p>\\r\\n\\r\\n<p>You can also make a massive fire to enjoy a fall evening in the outdoor fireplace. Inside, notice the quality! Built sturdy by Unique Homes, there are tons of details to enjoy. The main floor library is a stunning room and you will love the vaulted entry. The kitchen is fresh and updated with brand new granite counters and appliances. The top floor contains 3 beds and a laundry.&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>The master is HUGE, as is the bath! The multi level basement is perfect for entertaining and has tons of character. Other features - 3 season room, security, central vac, multiple decks, heated garage and more. Come and see!<\\/p>\"}},\"82\":{\"type\":\"quote\",\"enabled\":\"1\",\"fields\":{\"quote\":\"<p>This property is the shit.<\\/p>\",\"quoteAttribution\":\"Unknown\"}}},\"57\":\"\",\"10\":\"\",\"20\":\"UwmwWVAhRe3\",\"11\":\"507193\",\"3\":\"\",\"17\":\"3 car garage\",\"18\":\"699,900\",\"54\":\"\",\"12\":\"singleFamily\",\"55\":\"available\",\"13\":\"\",\"14\":\"3,209\",\"19\":\"\",\"21\":\"1993\"}}','2016-01-14 05:10:41','2016-01-14 05:10:41','da70b7cc-e9b5-4304-9358-f866b9c2cf8f'),
	(189,91,4,1,'en_us',1,'','{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Another Awesome House!\",\"slug\":\"another-awesome-house\",\"postDate\":1452748291,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"\",\"street2\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\",\"country\":\"\",\"lat\":\"\",\"lng\":\"\"},\"8\":\"select\",\"9\":\"\",\"49\":\"\",\"57\":\"\",\"10\":\"\",\"20\":\"\",\"11\":\"\",\"3\":\"\",\"17\":\"\",\"18\":\"\",\"54\":\"\",\"12\":\"\",\"55\":\"available\",\"13\":\"\",\"14\":\"\",\"19\":\"\",\"21\":\"\"}}','2016-01-14 05:11:31','2016-01-14 05:11:31','76a750c9-9083-45c6-ad20-93452f827d6a'),
	(190,91,4,1,'en_us',2,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Another Awesome House!\",\"slug\":\"another-awesome-house\",\"postDate\":1452748260,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"3014 SW Prairie View Rd\",\"street2\":\"\",\"city\":\"Ankeny\",\"state\":\"IA\",\"zip\":\"50023\",\"country\":\"United States\",\"lat\":\"41.699004\",\"lng\":\"-93.6446\"},\"8\":\"select\",\"9\":\"\",\"49\":\"\",\"57\":\"\",\"10\":\"\",\"20\":\"\",\"11\":\"12345\",\"3\":\"\",\"17\":\"\",\"18\":\"\",\"12\":\"\",\"55\":\"available\",\"13\":\"\",\"14\":\"\",\"19\":\"\",\"21\":\"\"}}','2016-01-14 05:11:56','2016-01-14 05:11:56','42e37570-b818-4a4f-8c7e-db9395895fac'),
	(191,2,1,1,'en_us',15,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Mark Charter Realty\",\"slug\":\"homepage\",\"postDate\":1450485935,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"66\":[\"83\"],\"3\":\"\",\"74\":[\"92\"]}}','2016-01-16 00:54:26','2016-01-16 00:54:26','0c3c41e9-520d-4636-9596-8752fdb3221e'),
	(192,4,5,1,'en_us',7,'','{\"typeId\":\"5\",\"authorId\":null,\"title\":\"Listings\",\"slug\":\"properties\",\"postDate\":1452744149,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":\"\",\"75\":[\"90\"],\"3\":\"\"}}','2016-01-16 01:59:54','2016-01-16 01:59:54','3d4e0abc-6e95-44f3-9dbf-0fb28e3332ee'),
	(193,17,4,1,'en_us',13,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Amazing golf course home with too many amenities to list!\",\"slug\":\"504907\",\"postDate\":1450493520,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"1210 NE 42nd St\",\"street2\":\"\",\"city\":\"Ankeny\",\"state\":\"IA\",\"zip\":\"50021\",\"country\":\"United States\",\"lat\":\"41.76575850\",\"lng\":\"-93.58654680\"},\"8\":\"4.5\",\"9\":\"1\",\"49\":{\"65\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Maecenas accumsan metus non turpis mattis laoreet. Donec luctus tincidunt sem, vel feugiat sapien ornare ac. Mauris in purus dui. Nullam commodo ut elit quis blandit. Quisque tristique ullamcorper nunc, et elementum odio vestibulum vel. Proin sit amet sollicitudin ligula, sit amet mollis libero. Vestibulum vulputate nec nisl et aliquam. Suspendisse finibus nec nulla sit amet feugiat.<\\/p>\"}}},\"57\":\"\",\"10\":\"\",\"20\":\"\",\"11\":\"504907\",\"3\":\"Golf course house.\",\"17\":\"3 Car Garage\",\"18\":\"849,900\",\"54\":[\"67\"],\"12\":\"singleFamily\",\"55\":\"available\",\"13\":\"\",\"14\":\"2,426\",\"19\":\"yZRyXkGqD2c\",\"21\":\"\"}}','2016-01-16 02:02:22','2016-01-16 02:02:22','77dea2fd-b3e5-4c6b-b3d9-8e5468f70997'),
	(194,18,4,1,'en_us',14,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Absolutely amazing, modern rural-meets-urban set up just outside of Grimes!! A must see at $719,000!\",\"slug\":\"506142\",\"postDate\":1450539000,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"7965 NW 142nd St\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"United States\",\"lat\":\"41.70576500\",\"lng\":\"-93.81369100\"},\"8\":\"4\",\"9\":\"5\",\"49\":{\"66\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Maecenas accumsan metus non turpis mattis laoreet. Donec luctus tincidunt sem, vel feugiat sapien ornare ac. Mauris in purus dui. Nullam commodo ut elit quis blandit. Quisque tristique ullamcorper nunc, et elementum odio vestibulum vel. Proin sit amet sollicitudin ligula, sit amet mollis libero. Vestibulum vulputate nec nisl et aliquam. Suspendisse finibus nec nulla sit amet feugiat.<\\/p>\"}}},\"57\":\"\",\"10\":\"\",\"20\":\"1Hw5ay9jY7y\",\"11\":\"506142\",\"3\":\"\",\"17\":\"3 car attached garage - 2 car detached garage\",\"18\":\"719,000\",\"54\":\"\",\"12\":\"singleFamily\",\"55\":\"available\",\"13\":\"\",\"14\":\"2,700\",\"19\":\"\",\"21\":\"1990\"}}','2016-01-16 02:02:33','2016-01-16 02:02:33','64571510-7adf-45e0-ba6a-e7370484e988'),
	(195,80,4,1,'en_us',8,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Amazing Grimes home with plenty of room to roam!\",\"slug\":\"507193\",\"postDate\":1452661380,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"11559 NW Timberbrooke Ln\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"\",\"lat\":\"\",\"lng\":\"\"},\"8\":\"3.5\",\"9\":\"5\",\"49\":{\"81\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Stunning 2 story home on an unbelievable piece of ground, all located in an ultra convenient location within Johnston schools. As you stand in the 2 story living room, taking in the view, you feel like you are taking in the beauty of a park. Come explore and enjoy all of the landscaping in place, including your own bridge, that leads to your shop\\/art studio\\/clubhouse on the back of the lot!&nbsp;<\\/p>\\r\\n\\r\\n<p>You can also make a massive fire to enjoy a fall evening in the outdoor fireplace. Inside, notice the quality! Built sturdy by Unique Homes, there are tons of details to enjoy. The main floor library is a stunning room and you will love the vaulted entry. The kitchen is fresh and updated with brand new granite counters and appliances. The top floor contains 3 beds and a laundry.&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>The master is HUGE, as is the bath! The multi level basement is perfect for entertaining and has tons of character. Other features - 3 season room, security, central vac, multiple decks, heated garage and more. Come and see!<\\/p>\"}},\"82\":{\"type\":\"quote\",\"enabled\":\"1\",\"fields\":{\"quote\":\"<p>This property is the shit.<\\/p>\",\"quoteAttribution\":\"Unknown\"}}},\"57\":\"\",\"10\":\"\",\"20\":\"UwmwWVAhRe3\",\"11\":\"507193\",\"3\":\"\",\"17\":\"3 car garage\",\"18\":\"699,900\",\"54\":\"\",\"12\":\"singleFamily\",\"55\":\"available\",\"13\":\"\",\"14\":\"3,209\",\"19\":\"\",\"21\":\"1993\"}}','2016-01-16 02:02:39','2016-01-16 02:02:39','7b93103e-5d87-4c6e-817e-17b9cc8bf950'),
	(196,91,4,1,'en_us',3,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Another Awesome House!\",\"slug\":\"another-awesome-house\",\"postDate\":1452748260,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"3014 SW Prairie View Rd\",\"street2\":\"\",\"city\":\"Ankeny\",\"state\":\"IA\",\"zip\":\"50023\",\"country\":\"United States\",\"lat\":\"41.69900400\",\"lng\":\"-93.64460000\"},\"8\":\"select\",\"9\":\"\",\"49\":\"\",\"57\":\"\",\"10\":\"\",\"20\":\"\",\"11\":\"12345\",\"3\":\"\",\"17\":\"\",\"18\":\"100,000\",\"54\":\"\",\"12\":\"\",\"55\":\"available\",\"13\":\"\",\"14\":\"\",\"19\":\"\",\"21\":\"\"}}','2016-01-16 02:02:53','2016-01-16 02:02:53','0644f27b-7a99-42b4-a76e-f26fa543cbe1'),
	(197,91,4,1,'en_us',4,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Another Awesome House!\",\"slug\":\"another-awesome-house\",\"postDate\":1452748260,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"3014 SW Prairieview Rd\",\"street2\":\"\",\"city\":\"Ankeny\",\"state\":\"IA\",\"zip\":\"50023\",\"country\":\"United States\",\"lat\":\"41.69900400\",\"lng\":\"-93.64460000\"},\"8\":\"select\",\"9\":\"\",\"49\":\"\",\"57\":\"\",\"10\":\"\",\"20\":\"\",\"11\":\"12345\",\"3\":\"\",\"17\":\"\",\"18\":\"100,000\",\"54\":\"\",\"12\":\"\",\"55\":\"available\",\"13\":\"\",\"14\":\"\",\"19\":\"\",\"21\":\"\"}}','2016-01-16 02:03:09','2016-01-16 02:03:09','2cc27f4d-5a27-4494-bf10-bf9e79ec8b0b'),
	(198,22,13,1,'en_us',4,'','{\"typeId\":\"13\",\"authorId\":null,\"title\":\"Sellers\",\"slug\":\"ways-to-sell\",\"postDate\":1452910230,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":[]}','2016-01-16 02:10:47','2016-01-16 02:10:47','5f22c4e9-0655-40e4-a8f1-cdb317df1e1c'),
	(199,22,13,1,'en_us',5,'','{\"typeId\":\"13\",\"authorId\":null,\"title\":\"Sellers\",\"slug\":\"ways-to-sell\",\"postDate\":1452910230,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":{\"new1\":{\"type\":\"headline\",\"enabled\":\"1\",\"fields\":{\"headline\":\"Three Ways to List Your Home!\"}},\"new2\":{\"type\":\"subHeadline\",\"enabled\":\"1\",\"fields\":{\"subHeadline\":\"You Choose. The Power Is Yours\"}}},\"75\":\"\",\"3\":\"\"}}','2016-01-16 02:15:19','2016-01-16 02:15:19','a0627d99-9596-4194-aa93-abe63ee5ffac'),
	(200,22,13,1,'en_us',6,'','{\"typeId\":\"13\",\"authorId\":null,\"title\":\"Sellers\",\"slug\":\"ways-to-sell\",\"postDate\":1452910230,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":{\"93\":{\"type\":\"headline\",\"enabled\":\"1\",\"fields\":{\"headline\":\"Three Ways to List Your Home!\"}},\"94\":{\"type\":\"subHeadline\",\"enabled\":\"1\",\"fields\":{\"subHeadline\":\"You Choose. The Power Is Yours\"}}},\"75\":[\"90\"],\"3\":\"\"}}','2016-01-16 02:17:48','2016-01-16 02:17:48','08408b87-b315-4cfb-a17b-9f398dde4d6e'),
	(201,22,13,1,'en_us',7,'','{\"typeId\":\"13\",\"authorId\":null,\"title\":\"Sellers\",\"slug\":\"ways-to-sell\",\"postDate\":1452910230,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":{\"93\":{\"type\":\"headline\",\"enabled\":\"1\",\"fields\":{\"headline\":\"Three Ways to List Your Home!\"}},\"94\":{\"type\":\"subHeadline\",\"enabled\":\"1\",\"fields\":{\"subHeadline\":\"You Choose. The Power Is Yours\"}}},\"75\":[\"90\"],\"3\":\"\"}}','2016-01-16 02:20:22','2016-01-16 02:20:22','6884b5ec-e03e-4c6a-ba1b-c5f7b332ff65'),
	(202,6,7,1,'en_us',3,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Team\",\"slug\":\"team\",\"postDate\":1450486453,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":\"\",\"75\":[\"90\"],\"3\":\"\"}}','2016-01-16 03:49:25','2016-01-16 03:49:25','7f644ac5-be03-4b79-8b44-886000c6c223'),
	(203,7,8,1,'en_us',4,'','{\"typeId\":\"8\",\"authorId\":null,\"title\":\"Testimonials\",\"slug\":\"reviews\",\"postDate\":1452748665,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":\"\",\"75\":[\"90\"],\"3\":\"\"}}','2016-01-16 03:54:03','2016-01-16 03:54:03','ff0111c3-42a4-4d28-a3c3-039c432960f6'),
	(204,7,8,1,'en_us',5,'','{\"typeId\":\"8\",\"authorId\":null,\"title\":\"Reviews\",\"slug\":\"reviews\",\"postDate\":1452748665,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":\"\",\"75\":[\"90\"],\"3\":\"\"}}','2016-01-16 03:55:08','2016-01-16 03:55:08','f33baa06-0c06-4138-8d6d-e7ef6abf3ea1'),
	(205,38,15,1,'en_us',8,'','{\"typeId\":null,\"authorId\":null,\"title\":\"About Mark Charter\",\"slug\":\"about\",\"postDate\":1450641789,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":{\"56\":{\"type\":\"headline\",\"enabled\":\"1\",\"fields\":{\"headline\":\"Meet Mark and his family\"}},\"55\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Mark started his real estate career in 2005 and has been doing it full-time since then. Over time Mark realized that the majority of real estate agents are the same. They say the same things, they market the same way, and they treat clients the same. Mark did not like this so he set about creating his own unique way of practicing real estate.<\\/p>\\r\\n\\r\\n<p>In 2014, Mark debuted a three tier pricing plan for listing clients, giving them the choice on how they want to list their home. This is another example of how Mark never accepts the status quo in real estate and is always looking to make things better for his clients.<\\/p>\"}},\"57\":{\"type\":\"quote\",\"enabled\":\"1\",\"fields\":{\"quote\":\"<p>One of Mark Charter\\u2019s greatest characteristics is his giving nature.<\\/p>\",\"quoteAttribution\":\"Adam Carroll\"}},\"58\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Mark\'s overall goal on every deal is simple. He wants to provide such great service that after the deal is done, that client will happily provide a positive review of the transaction. This goal is what makes Mark one of the most recommended agents in Central Iowa.<\\/p>\\r\\n\\r\\n<p>When you add it all up, when you take into account what Mark offers clients in terms of services, marketing and care, Mark truly feels he is the best option available in the market today and he hopes you will experience this for yourself very soon!<\\/p>\"}}},\"75\":[\"90\"],\"3\":\"\"}}','2016-01-16 03:57:00','2016-01-16 03:57:00','926bfebc-2222-4c52-b7d6-8db901bb329a'),
	(206,5,6,1,'en_us',8,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Partners\",\"slug\":\"partners\",\"postDate\":1450486386,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":{\"63\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Maecenas accumsan metus non turpis mattis laoreet. Donec luctus tincidunt sem, vel feugiat sapien ornare ac. Mauris in purus dui. Nullam commodo ut elit quis blandit. Quisque tristique ullamcorper nunc, et elementum odio vestibulum vel. Proin sit amet sollicitudin ligula, sit amet mollis libero. Vestibulum vulputate nec nisl et aliquam. Suspendisse finibus nec nulla sit amet feugiat.<\\/p>\"}}},\"75\":[\"90\"],\"3\":\"\"}}','2016-01-16 04:08:11','2016-01-16 04:08:11','fe55b08c-a00f-413b-8c7b-edf5aed6af0f'),
	(207,61,16,1,'en_us',5,'','{\"typeId\":\"16\",\"authorId\":\"1\",\"title\":\"Mortgage - The Tyler Osby Team\",\"slug\":\"mortgage-the-tyler-osby-team\",\"postDate\":1451264880,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"1\":\"<p>Tyler Osby is where Mark turns when his clients need a mortgage or a refinance. Tyler is knowledgeable, accountable and trustworthy. He gets the job done every time!<\\/p>\",\"48\":[\"98\"],\"3\":\"\",\"47\":\"(515) 257-6729\",\"46\":\" http:\\/\\/www.wealthwithmortgage.com\"}}','2016-01-16 04:14:16','2016-01-16 04:14:16','a4fb2780-c893-4bd8-bc2c-410056a56752'),
	(208,61,16,1,'en_us',6,'','{\"typeId\":\"16\",\"authorId\":\"1\",\"title\":\"Mortgage - The Tyler Osby Team\",\"slug\":\"mortgage-the-tyler-osby-team\",\"postDate\":1451264880,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"1\":\"<p>Tyler Osby is where Mark turns when his clients need a mortgage or a refinance. Tyler is knowledgeable, accountable and trustworthy. He gets the job done every time!<\\/p>\",\"48\":\"\",\"3\":\"\",\"47\":\"(515) 257-6729\",\"46\":\" http:\\/\\/www.wealthwithmortgage.com\"}}','2016-01-16 04:18:28','2016-01-16 04:18:28','ca687882-24a6-4002-827c-afa3dd899b5e'),
	(209,61,16,1,'en_us',7,'','{\"typeId\":\"16\",\"authorId\":\"1\",\"title\":\"Mortgage - The Tyler Osby Team\",\"slug\":\"mortgage-the-tyler-osby-team\",\"postDate\":1451264880,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"1\":\"<p>Tyler Osby is where Mark turns when his clients need a mortgage or a refinance. Tyler is knowledgeable, accountable and trustworthy. He gets the job done every time!<\\/p>\",\"48\":[\"99\"],\"3\":\"\",\"47\":\"(515) 257-6729\",\"46\":\" http:\\/\\/www.wealthwithmortgage.com\"}}','2016-01-16 04:19:26','2016-01-16 04:19:26','1baea34e-f457-4a71-876f-768802cb15d3'),
	(210,18,4,1,'en_us',15,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Absolutely amazing, modern rural-meets-urban set up just outside of Grimes!! A must see at $719,000!\",\"slug\":\"506142\",\"postDate\":1450539000,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"7965 NW 142nd St\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"United States\",\"lat\":\"41.70576500\",\"lng\":\"-93.81369100\"},\"8\":\"4\",\"9\":\"5\",\"49\":{\"66\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Maecenas accumsan metus non turpis mattis laoreet. Donec luctus tincidunt sem, vel feugiat sapien ornare ac. Mauris in purus dui. Nullam commodo ut elit quis blandit. Quisque tristique ullamcorper nunc, et elementum odio vestibulum vel. Proin sit amet sollicitudin ligula, sit amet mollis libero. Vestibulum vulputate nec nisl et aliquam. Suspendisse finibus nec nulla sit amet feugiat.<\\/p>\"}}},\"57\":[\"103\",\"104\",\"105\"],\"10\":\"\",\"20\":\"1Hw5ay9jY7y\",\"11\":\"506142\",\"3\":\"\",\"17\":\"3 car attached garage - 2 car detached garage\",\"18\":\"719,000\",\"78\":\"\",\"54\":\"\",\"12\":\"singleFamily\",\"55\":\"available\",\"13\":\"\",\"14\":\"2,700\",\"19\":\"\",\"21\":\"1990\"}}','2016-01-17 16:00:29','2016-01-17 16:00:29','fc07cc00-2905-4973-bc61-77aab2e5d8d5'),
	(211,17,4,1,'en_us',14,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Amazing golf course home with too many amenities to list!\",\"slug\":\"504907\",\"postDate\":1450493520,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"1210 NE 42nd St\",\"street2\":\"\",\"city\":\"Ankeny\",\"state\":\"IA\",\"zip\":\"50021\",\"country\":\"United States\",\"lat\":\"41.76575850\",\"lng\":\"-93.58654680\"},\"8\":\"4.5\",\"9\":\"1\",\"49\":{\"65\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Maecenas accumsan metus non turpis mattis laoreet. Donec luctus tincidunt sem, vel feugiat sapien ornare ac. Mauris in purus dui. Nullam commodo ut elit quis blandit. Quisque tristique ullamcorper nunc, et elementum odio vestibulum vel. Proin sit amet sollicitudin ligula, sit amet mollis libero. Vestibulum vulputate nec nisl et aliquam. Suspendisse finibus nec nulla sit amet feugiat.<\\/p>\"}}},\"57\":[\"106\",\"107\",\"108\"],\"10\":\"\",\"20\":\"\",\"11\":\"504907\",\"3\":\"Golf course house.\",\"17\":\"3 Car Garage\",\"18\":\"849,900\",\"78\":\"\",\"54\":[\"67\"],\"12\":\"singleFamily\",\"55\":\"available\",\"13\":\"\",\"14\":\"2,426\",\"19\":\"yZRyXkGqD2c\",\"21\":\"\"}}','2016-01-17 16:04:49','2016-01-17 16:04:49','e1668200-519d-449d-8905-e675690928d8'),
	(212,17,4,1,'en_us',15,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Amazing golf course home with too many amenities to list!\",\"slug\":\"504907\",\"postDate\":1450493520,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"1210 NE 42nd St\",\"street2\":\"\",\"city\":\"Ankeny\",\"state\":\"IA\",\"zip\":\"50021\",\"country\":\"United States\",\"lat\":\"41.76575850\",\"lng\":\"-93.58654680\"},\"8\":\"4.5\",\"9\":\"1\",\"49\":{\"65\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Maecenas accumsan metus non turpis mattis laoreet. Donec luctus tincidunt sem, vel feugiat sapien ornare ac. Mauris in purus dui. Nullam commodo ut elit quis blandit. Quisque tristique ullamcorper nunc, et elementum odio vestibulum vel. Proin sit amet sollicitudin ligula, sit amet mollis libero. Vestibulum vulputate nec nisl et aliquam. Suspendisse finibus nec nulla sit amet feugiat.<\\/p>\"}}},\"57\":[\"108\",\"107\",\"106\"],\"10\":\"\",\"20\":\"\",\"11\":\"504907\",\"3\":\"Golf course house.\",\"17\":\"3 Car Garage\",\"18\":\"849,900\",\"78\":\"\",\"54\":[\"67\"],\"12\":\"singleFamily\",\"55\":\"available\",\"13\":\"\",\"14\":\"2,426\",\"19\":\"yZRyXkGqD2c\",\"21\":\"\"}}','2016-01-17 16:05:04','2016-01-17 16:05:04','6b663405-407b-4977-b406-1087c1a1cc78'),
	(213,80,4,1,'en_us',9,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Amazing Grimes home with plenty of room to roam!\",\"slug\":\"507193\",\"postDate\":1452661380,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"11559 NW Timberbrooke Ln\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"\",\"lat\":\"\",\"lng\":\"\"},\"8\":\"3.5\",\"9\":\"5\",\"49\":{\"81\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Stunning 2 story home on an unbelievable piece of ground, all located in an ultra convenient location within Johnston schools. As you stand in the 2 story living room, taking in the view, you feel like you are taking in the beauty of a park. Come explore and enjoy all of the landscaping in place, including your own bridge, that leads to your shop\\/art studio\\/clubhouse on the back of the lot!&nbsp;<\\/p>\\r\\n\\r\\n<p>You can also make a massive fire to enjoy a fall evening in the outdoor fireplace. Inside, notice the quality! Built sturdy by Unique Homes, there are tons of details to enjoy. The main floor library is a stunning room and you will love the vaulted entry. The kitchen is fresh and updated with brand new granite counters and appliances. The top floor contains 3 beds and a laundry.&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>The master is HUGE, as is the bath! The multi level basement is perfect for entertaining and has tons of character. Other features - 3 season room, security, central vac, multiple decks, heated garage and more. Come and see!<\\/p>\"}},\"82\":{\"type\":\"quote\",\"enabled\":\"1\",\"fields\":{\"quote\":\"<p>This property is the shit.<\\/p>\",\"quoteAttribution\":\"Unknown\"}}},\"57\":[\"109\",\"110\",\"111\"],\"10\":\"\",\"20\":\"UwmwWVAhRe3\",\"11\":\"507193\",\"3\":\"\",\"17\":\"3 car garage\",\"18\":\"699,900\",\"78\":\"\",\"54\":\"\",\"12\":\"singleFamily\",\"55\":\"available\",\"13\":\"\",\"14\":\"3,209\",\"19\":\"\",\"21\":\"1993\"}}','2016-01-17 16:07:12','2016-01-17 16:07:12','f7da0c1b-7775-47ed-a561-ac359c719cbd'),
	(214,9,10,1,'en_us',3,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Truck\",\"slug\":\"truck\",\"postDate\":1450487151,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":{\"new1\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur elit justo, tempus et eros ut, elementum ultrices ipsum. Donec et rhoncus elit, quis auctor felis. Ut cursus elementum elementum. Sed et ornare risus. Phasellus a mollis magna, ut tincidunt tellus. Nulla justo metus, varius sit amet velit in, volutpat rutrum turpis. Integer pellentesque ultrices tellus nec efficitur.<\\/p>\"}}},\"75\":[\"90\"],\"3\":\"\"}}','2016-01-17 22:16:03','2016-01-17 22:16:03','778b0cce-b9cc-4c7d-b05e-a986e9563476'),
	(215,9,10,1,'en_us',4,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Truck\",\"slug\":\"truck\",\"postDate\":1450487151,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":{\"112\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur elit justo, tempus et eros ut, elementum ultrices ipsum. Donec et rhoncus elit, quis auctor felis. Ut cursus elementum elementum. Sed et ornare risus. Phasellus a mollis magna, ut tincidunt tellus. Nulla justo metus, varius sit amet velit in, volutpat rutrum turpis. Integer pellentesque ultrices tellus nec efficitur.<\\/p>\"}},\"new1\":{\"type\":\"headline\",\"enabled\":\"1\",\"fields\":{\"headline\":\"This truck is legit.\"}}},\"75\":[\"90\"],\"3\":\"\"}}','2016-01-17 22:18:51','2016-01-17 22:18:51','d45610c9-d825-449f-bf6a-7c11d171c66f'),
	(235,132,3,1,'en_us',1,NULL,'{\"typeId\":1,\"authorId\":null,\"title\":\"Damon Gentry\",\"slug\":\"damon-gentry\",\"postDate\":1453074311,\"expiryDate\":1453160711,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"\",\"5\":\"damonjentree@gmail.com\",\"61\":\"Damon\",\"62\":\"Gentry\",\"7\":\"5153714708\"}}','2016-01-17 23:45:11','2016-01-17 23:45:11','c88bd325-f798-4758-a0ee-2af315a1f099');

/*!40000 ALTER TABLE `entryversions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fieldgroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fieldgroups`;

CREATE TABLE `fieldgroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fieldgroups_name_unq_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `fieldgroups` WRITE;
/*!40000 ALTER TABLE `fieldgroups` DISABLE KEYS */;

INSERT INTO `fieldgroups` (`id`, `name`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Default','2015-12-19 00:45:35','2015-12-19 00:45:35','9bdb4644-e7f2-44c5-8873-16b9251c195b'),
	(2,'Metadata','2015-12-19 01:15:53','2015-12-19 01:15:53','ddc378ed-2b9b-41e1-88f8-d8804c13b1c3'),
	(3,'Reservations','2015-12-19 01:34:50','2015-12-19 01:34:50','4c9be1fd-53c9-476a-b9fd-5d4ff9f7fec8'),
	(4,'Property','2015-12-19 01:48:09','2015-12-19 01:48:09','ce6faef5-724c-4ba6-b900-10f52fdbed99'),
	(5,'Testimonials','2015-12-19 02:25:13','2015-12-19 02:30:57','5808ae43-a3dd-4e2b-8988-63c9b3cef773'),
	(6,'Social Media','2015-12-20 04:19:49','2015-12-20 04:19:49','90173653-e25a-4b0a-976a-c3cc25b93e11'),
	(7,'Selling Options','2015-12-20 04:41:11','2016-01-16 03:32:06','d9785488-3dd1-4637-8613-36529f872ba9'),
	(8,'Team Members','2015-12-20 20:23:52','2015-12-20 20:23:52','c53e342a-3446-42db-a90a-fcd7889def27'),
	(9,'Partners','2015-12-20 20:46:16','2015-12-20 20:46:16','c2468c7b-8ecc-4f70-b231-e3d6c72303a2');

/*!40000 ALTER TABLE `fieldgroups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fieldlayoutfields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fieldlayoutfields`;

CREATE TABLE `fieldlayoutfields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layoutId` int(11) NOT NULL,
  `tabId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `required` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sortOrder` tinyint(4) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fieldlayoutfields_layoutId_fieldId_unq_idx` (`layoutId`,`fieldId`),
  KEY `fieldlayoutfields_sortOrder_idx` (`sortOrder`),
  KEY `fieldlayoutfields_tabId_fk` (`tabId`),
  KEY `fieldlayoutfields_fieldId_fk` (`fieldId`),
  CONSTRAINT `fieldlayoutfields_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fieldlayoutfields_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fieldlayoutfields_tabId_fk` FOREIGN KEY (`tabId`) REFERENCES `fieldlayouttabs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `fieldlayoutfields` WRITE;
/*!40000 ALTER TABLE `fieldlayoutfields` DISABLE KEYS */;

INSERT INTO `fieldlayoutfields` (`id`, `layoutId`, `tabId`, `fieldId`, `required`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(2,5,2,1,1,1,'2015-12-19 00:45:35','2015-12-19 00:45:35','2312e57a-8508-44a3-a168-5025221b514e'),
	(3,5,2,2,0,2,'2015-12-19 00:45:35','2015-12-19 00:45:35','d8357c0a-8b6a-4d6e-b337-49d03c17ec44'),
	(30,22,22,1,0,1,'2015-12-19 01:44:56','2015-12-19 01:44:56','6f125e4d-04de-4e30-ae02-2ade0dcadd48'),
	(31,22,23,3,0,1,'2015-12-19 01:44:56','2015-12-19 01:44:56','6ffcd773-f102-489e-a6d5-ce05dc931d2f'),
	(68,32,39,1,0,1,'2015-12-19 02:31:59','2015-12-19 02:31:59','2315377d-dde5-4349-8066-9af63d5b12e2'),
	(69,32,39,15,0,2,'2015-12-19 02:31:59','2015-12-19 02:31:59','894c12ea-9ddb-4706-a18c-7cd97b7214b0'),
	(70,32,40,3,0,1,'2015-12-19 02:31:59','2015-12-19 02:31:59','3ebaef39-7b0b-4365-b9c0-6575f3203381'),
	(197,58,78,32,0,1,'2015-12-20 04:51:51','2015-12-20 04:51:51','e3446787-6487-4fb8-9e0f-cfc563caf786'),
	(198,59,79,33,0,1,'2015-12-20 04:51:51','2015-12-20 04:51:51','42875d37-17fa-4005-8501-9bd139973ae6'),
	(199,60,80,34,0,1,'2015-12-20 04:51:51','2015-12-20 04:51:51','b61bb0cb-3401-4b9c-a598-233716f3543e'),
	(213,68,89,39,0,1,'2015-12-20 14:57:48','2015-12-20 14:57:48','8b97c479-0d27-4ab0-b77a-93f8e331890a'),
	(224,74,94,41,0,1,'2015-12-20 20:39:56','2015-12-20 20:39:56','e47003ba-b941-459c-8b3b-59587a799a75'),
	(225,74,94,43,0,2,'2015-12-20 20:39:56','2015-12-20 20:39:56','5e7f59a5-ffc8-4038-b9aa-eaab6b601be1'),
	(226,74,94,42,0,3,'2015-12-20 20:39:56','2015-12-20 20:39:56','932ee411-4f9b-411d-8e0a-c271cbc41a51'),
	(227,74,94,45,0,4,'2015-12-20 20:39:56','2015-12-20 20:39:56','4aaeab92-19dd-41db-852d-5ae812de6ec6'),
	(228,74,94,44,0,5,'2015-12-20 20:39:56','2015-12-20 20:39:56','1575ddf8-b4a6-495e-bb2b-8393e9655878'),
	(251,88,110,49,0,1,'2015-12-28 00:58:09','2015-12-28 00:58:09','c1110979-566d-4d3f-a41d-cd8b80c38ddd'),
	(252,88,111,3,0,1,'2015-12-28 00:58:09','2015-12-28 00:58:09','865d24f6-acb8-44c4-8af7-08d52bfa55b1'),
	(256,90,114,49,0,1,'2015-12-28 01:04:27','2015-12-28 01:04:27','d49a82ed-15a8-4c4a-8e53-58ee61fbf1ef'),
	(257,90,115,3,0,1,'2015-12-28 01:04:27','2015-12-28 01:04:27','33b9e9ff-f33f-41a9-9436-ef94fb27d8a7'),
	(275,97,126,1,0,1,'2015-12-28 01:16:04','2015-12-28 01:16:04','3183c8ba-9c08-4e60-9d48-d16dd45bee54'),
	(276,97,126,48,0,2,'2015-12-28 01:16:04','2015-12-28 01:16:04','a75d6c60-16fd-490a-9707-6fa5841dc33e'),
	(277,97,126,47,0,3,'2015-12-28 01:16:04','2015-12-28 01:16:04','833fb520-09e1-4df2-aae8-39d789942a51'),
	(278,97,126,46,0,4,'2015-12-28 01:16:04','2015-12-28 01:16:04','a0fe7385-28d3-46ef-874c-0f4a86cf1393'),
	(279,97,127,3,0,1,'2015-12-28 01:16:04','2015-12-28 01:16:04','383ad105-4c52-4bc7-8f0b-a72448d58d84'),
	(388,111,153,1,0,1,'2016-01-07 00:12:12','2016-01-07 00:12:12','0a850230-c0cc-43e5-bfd8-617b05cc69cb'),
	(389,111,153,15,0,2,'2016-01-07 00:12:12','2016-01-07 00:12:12','dbcbe78d-1364-4d90-9346-436799ec35fc'),
	(427,119,165,22,0,1,'2016-01-08 21:39:20','2016-01-08 21:39:20','eda740d0-ef44-47c8-be72-c3a8574ed46d'),
	(428,119,165,6,0,2,'2016-01-08 21:39:20','2016-01-08 21:39:20','10c30fdb-a6b0-4ece-92fa-6f8358553b59'),
	(429,119,165,63,0,3,'2016-01-08 21:39:20','2016-01-08 21:39:20','c99725ca-13f7-4571-9079-e123e1efab7c'),
	(430,119,165,23,0,4,'2016-01-08 21:39:20','2016-01-08 21:39:20','9de2001b-741b-46db-8bcf-45301e8fb526'),
	(431,119,165,64,0,5,'2016-01-08 21:39:20','2016-01-08 21:39:20','8d2f9af2-7b87-4d0f-889c-25f967d20b4d'),
	(435,121,168,49,0,1,'2016-01-12 01:59:06','2016-01-12 01:59:06','af29a487-c0d8-4883-802d-f15b85368eaf'),
	(436,121,169,3,0,1,'2016-01-12 01:59:06','2016-01-12 01:59:06','74782630-3ebd-4bf9-901a-50afb56d0ffa'),
	(439,124,172,1,0,1,'2016-01-12 02:04:13','2016-01-12 02:04:13','7edb44a4-d7cf-433a-ae39-a631a3a6c1d2'),
	(451,126,176,61,0,1,'2016-01-12 04:17:42','2016-01-12 04:17:42','1f942c49-9b67-4ce2-87cb-996fb4b4b196'),
	(452,126,176,62,0,2,'2016-01-12 04:17:42','2016-01-12 04:17:42','3eec1b54-8558-44ac-9e50-749c28aa3142'),
	(453,126,176,5,0,3,'2016-01-12 04:17:42','2016-01-12 04:17:42','3981a379-38cf-47b2-b744-8261c81f9cbd'),
	(454,126,176,7,0,4,'2016-01-12 04:17:42','2016-01-12 04:17:42','1ebab22e-9575-4267-ba61-65b290fa7287'),
	(455,126,176,6,0,5,'2016-01-12 04:17:42','2016-01-12 04:17:42','78cd7ae9-331d-4738-8293-8a73e4a4458e'),
	(456,126,176,1,0,6,'2016-01-12 04:17:42','2016-01-12 04:17:42','4fb9dd4f-0123-4920-b512-e9f17373d78a'),
	(457,126,177,58,0,1,'2016-01-12 04:17:42','2016-01-12 04:17:42','deea4002-8e73-4d74-986a-9b3ccd851211'),
	(458,126,177,65,0,2,'2016-01-12 04:17:42','2016-01-12 04:17:42','d206e3f7-6d36-4b7a-855b-c1437983ee61'),
	(459,126,177,59,0,3,'2016-01-12 04:17:42','2016-01-12 04:17:42','31cd34ca-2188-4e0e-9401-2b50ed9ed516'),
	(460,126,177,60,0,4,'2016-01-12 04:17:42','2016-01-12 04:17:42','aa00ddd7-044c-4a75-85bf-9746ffbe9fd1'),
	(461,126,178,4,0,1,'2016-01-12 04:17:42','2016-01-12 04:17:42','4c7ebe13-94d8-45fe-a87a-63dd725d77b0'),
	(513,148,208,66,0,1,'2016-01-16 00:50:41','2016-01-16 00:50:41','1c9763c4-d2d4-4e0f-8dc9-adb63b882677'),
	(514,148,208,74,0,2,'2016-01-16 00:50:41','2016-01-16 00:50:41','5b89f1b6-210e-4921-8c58-c37d42d5850b'),
	(515,148,209,3,0,1,'2016-01-16 00:50:41','2016-01-16 00:50:41','49057f75-c27e-4171-a78f-475cd38d4836'),
	(519,150,212,75,0,1,'2016-01-16 02:00:35','2016-01-16 02:00:35','07a3e64c-9242-41b5-ba25-34e2c7eb1534'),
	(520,150,213,3,0,1,'2016-01-16 02:00:35','2016-01-16 02:00:35','149459f9-0387-436a-989f-061addfa3f2f'),
	(524,154,216,71,0,1,'2016-01-16 02:13:21','2016-01-16 02:13:21','f9b97df4-6346-47f1-9097-05048f1db086'),
	(525,154,216,72,0,2,'2016-01-16 02:13:21','2016-01-16 02:13:21','db22bdd1-6066-4de9-8f97-6bcee08f14a8'),
	(526,154,216,73,0,3,'2016-01-16 02:13:21','2016-01-16 02:13:21','a335d718-92c8-44da-9c22-9a702a96dbae'),
	(532,159,221,50,0,1,'2016-01-16 02:14:05','2016-01-16 02:14:05','f29093a5-6c47-4bd3-b9bf-20b517499369'),
	(533,160,222,76,0,1,'2016-01-16 02:14:05','2016-01-16 02:14:05','b1c22c76-a07a-4deb-af15-5f6faf357f0e'),
	(534,161,223,51,0,1,'2016-01-16 02:14:05','2016-01-16 02:14:05','73bc2b19-e499-452c-be93-9757c9d63592'),
	(535,162,224,52,0,1,'2016-01-16 02:14:05','2016-01-16 02:14:05','142fd548-3485-4c67-9342-a0e3a839d430'),
	(536,162,224,53,0,2,'2016-01-16 02:14:05','2016-01-16 02:14:05','d33f29ce-4b97-4f39-bec2-e2d8a121fdc4'),
	(549,166,230,75,0,1,'2016-01-16 03:37:12','2016-01-16 03:37:12','f7150e05-fbe9-4336-a9e8-c832d3915797'),
	(550,166,230,49,0,2,'2016-01-16 03:37:12','2016-01-16 03:37:12','8da1b5db-9feb-4271-8f22-062606be5591'),
	(551,166,231,3,0,1,'2016-01-16 03:37:12','2016-01-16 03:37:12','4d5eb1fc-bc04-4eb3-97d1-cd2a76fd2e55'),
	(566,171,236,28,0,1,'2016-01-16 03:39:54','2016-01-16 03:39:54','3f7d29b5-8205-4550-ba2d-080fc541835d'),
	(567,171,236,35,0,2,'2016-01-16 03:39:54','2016-01-16 03:39:54','6114f660-0536-4eb7-909b-8599ef09a823'),
	(568,171,236,36,0,3,'2016-01-16 03:39:54','2016-01-16 03:39:54','c3341c66-5a85-46f1-a8a8-b71b525415c9'),
	(569,172,237,75,0,1,'2016-01-16 03:49:06','2016-01-16 03:49:06','a392be33-0f9b-4f3b-9d95-2a6231ecb428'),
	(570,172,237,49,0,2,'2016-01-16 03:49:06','2016-01-16 03:49:06','3772bec1-54da-49e3-9265-4c7e93ca3b91'),
	(571,172,238,3,0,1,'2016-01-16 03:49:06','2016-01-16 03:49:06','f1a16a74-e6f6-43b9-a3db-6a6056bf0ca8'),
	(572,173,239,75,0,1,'2016-01-16 03:53:47','2016-01-16 03:53:47','586d6e4e-4d8a-42b3-9bd0-3b9a288679ed'),
	(573,173,239,49,0,2,'2016-01-16 03:53:47','2016-01-16 03:53:47','a5ce34c7-9222-40a4-8c43-b221aee7e193'),
	(574,173,240,3,0,1,'2016-01-16 03:53:47','2016-01-16 03:53:47','b09728f9-1829-4283-8f79-270811f15a00'),
	(575,174,241,75,0,1,'2016-01-16 03:56:43','2016-01-16 03:56:43','cc550502-13b7-4f8f-a9f6-705a8783ed2d'),
	(576,174,241,49,0,2,'2016-01-16 03:56:43','2016-01-16 03:56:43','e3f5dd62-f90b-4480-a5b8-5ef609a3052f'),
	(577,174,242,3,0,1,'2016-01-16 03:56:43','2016-01-16 03:56:43','5862528c-ad7d-4c8e-a0d4-1d3703e6f9b0'),
	(578,175,243,75,0,1,'2016-01-16 04:07:30','2016-01-16 04:07:30','ef6ecba6-194b-4c46-bd28-94d3b784db05'),
	(579,175,243,49,0,2,'2016-01-16 04:07:30','2016-01-16 04:07:30','d5a24971-ca9d-49a5-b0d3-302f531ccd3c'),
	(580,175,244,3,0,1,'2016-01-16 04:07:30','2016-01-16 04:07:30','d3aeadef-4e35-478b-8c87-04fb44413160'),
	(581,177,245,49,0,1,'2016-01-16 05:22:16','2016-01-16 05:22:16','b567d7c1-3973-4d6d-95ec-f0fdd3bfd0b4'),
	(582,177,246,57,0,1,'2016-01-16 05:22:16','2016-01-16 05:22:16','9cddfc9e-43e8-4b09-b79c-78fe97f49bff'),
	(583,177,246,6,0,2,'2016-01-16 05:22:16','2016-01-16 05:22:16','d892c672-74a5-479a-8687-1952a08f6e6f'),
	(584,177,246,11,0,3,'2016-01-16 05:22:16','2016-01-16 05:22:16','d2c21157-4157-4ea5-8e6d-771eba71563f'),
	(585,177,246,18,0,4,'2016-01-16 05:22:16','2016-01-16 05:22:16','8f8f2365-0b39-43f2-a99f-47447ebeca11'),
	(586,177,246,21,0,5,'2016-01-16 05:22:16','2016-01-16 05:22:16','0a430c9c-41d6-411c-8eba-74f51faadc38'),
	(587,177,246,14,0,6,'2016-01-16 05:22:16','2016-01-16 05:22:16','141ad707-afb5-4a87-9616-c913dca9698c'),
	(588,177,246,9,0,7,'2016-01-16 05:22:16','2016-01-16 05:22:16','1685f623-8201-4287-91a2-a7c9a03f6b2d'),
	(589,177,246,8,0,8,'2016-01-16 05:22:16','2016-01-16 05:22:16','7c1e8a85-e620-48b7-b797-f0717c84c4c2'),
	(590,177,246,12,0,9,'2016-01-16 05:22:16','2016-01-16 05:22:16','098e015d-16d2-41e7-8bec-21a91ac206fe'),
	(591,177,246,17,0,10,'2016-01-16 05:22:16','2016-01-16 05:22:16','dbdd0159-1a85-4b99-a5d7-d02c31ded188'),
	(592,177,246,19,0,11,'2016-01-16 05:22:16','2016-01-16 05:22:16','4fda0cad-d319-4808-b8c7-199fecfa418e'),
	(593,177,246,20,0,12,'2016-01-16 05:22:16','2016-01-16 05:22:16','9e90b63a-528d-442e-8292-dee1ef536f51'),
	(594,177,246,54,0,13,'2016-01-16 05:22:16','2016-01-16 05:22:16','611005f5-3934-44f0-8b3b-89013203b843'),
	(595,177,246,55,0,14,'2016-01-16 05:22:16','2016-01-16 05:22:16','3e2e670a-b5c4-47f1-b11e-cbf8f944360d'),
	(596,177,246,10,0,15,'2016-01-16 05:22:16','2016-01-16 05:22:16','b276dd20-fa6c-4d49-80c7-f7a331d1ce67'),
	(597,177,246,13,0,16,'2016-01-16 05:22:16','2016-01-16 05:22:16','eba68890-1bc8-44c7-887a-1896aa2dadb2'),
	(598,177,246,78,0,17,'2016-01-16 05:22:16','2016-01-16 05:22:16','9cbfd8b6-c26f-47d4-861f-82839de9a5da'),
	(599,177,247,3,0,1,'2016-01-16 05:22:16','2016-01-16 05:22:16','96bb8da6-d78f-4a4c-8d5e-eac66e53fe39'),
	(600,178,248,75,0,1,'2016-01-17 22:15:41','2016-01-17 22:15:41','b23bc0ba-e4b3-452d-b614-c16a6964a44d'),
	(601,178,248,49,0,2,'2016-01-17 22:15:41','2016-01-17 22:15:41','66e2dd50-657a-4a0d-a374-2ff008cda5da'),
	(602,178,249,3,0,1,'2016-01-17 22:15:41','2016-01-17 22:15:41','36684b2f-b8f2-420c-ad59-26b6476921d3');

/*!40000 ALTER TABLE `fieldlayoutfields` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fieldlayouts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fieldlayouts`;

CREATE TABLE `fieldlayouts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fieldlayouts_type_idx` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `fieldlayouts` WRITE;
/*!40000 ALTER TABLE `fieldlayouts` DISABLE KEYS */;

INSERT INTO `fieldlayouts` (`id`, `type`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Tag','2015-12-19 00:45:35','2015-12-19 00:45:35','ef0c26f0-3ba9-47b6-8c53-822217960096'),
	(5,'Entry','2015-12-19 00:45:35','2015-12-19 00:45:35','aadf50b7-07cd-4fbf-90b1-002c2e424ad9'),
	(22,'Entry','2015-12-19 01:44:56','2015-12-19 01:44:56','dc6df7b1-2aac-4d79-805e-27fa3aad14be'),
	(32,'Entry','2015-12-19 02:31:59','2015-12-19 02:31:59','392c4bb3-b0d6-4931-a983-0484af1e0a1a'),
	(46,'GlobalSet','2015-12-20 04:19:31','2015-12-20 04:19:31','9a8b4868-cb79-42df-95e5-6777eac12848'),
	(58,'MatrixBlock','2015-12-20 04:51:51','2015-12-20 04:51:51','8ce10c0d-f214-4068-bcec-f17597199236'),
	(59,'MatrixBlock','2015-12-20 04:51:51','2015-12-20 04:51:51','7385c88c-b222-45b5-855f-35702489bee1'),
	(60,'MatrixBlock','2015-12-20 04:51:51','2015-12-20 04:51:51','76c447e5-2e22-43e6-997a-7812f26f6227'),
	(68,'GlobalSet','2015-12-20 14:57:48','2015-12-20 14:57:48','fff33e0b-8c13-407f-85b0-9d5374217555'),
	(74,'Category','2015-12-20 20:39:56','2015-12-20 20:39:56','f5ef4cb4-631c-4ac1-bd6b-964181fe7d12'),
	(88,'Entry','2015-12-28 00:58:09','2015-12-28 00:58:09','f2a3d6b3-29fb-4aee-a9ba-17766bf10f80'),
	(90,'Entry','2015-12-28 01:04:27','2015-12-28 01:04:27','d9ac7506-84a1-4910-bd66-d98a21b6c819'),
	(97,'Entry','2015-12-28 01:16:04','2015-12-28 01:16:04','816f8faa-ffed-4332-a839-c6f0ec2daafc'),
	(109,'Asset','2016-01-06 23:55:58','2016-01-06 23:55:58','19c85235-6e09-47fa-9fcc-89023b961f8c'),
	(111,'Category','2016-01-07 00:12:12','2016-01-07 00:12:12','5afc7fc0-f443-4179-89f9-dd3bf433f74e'),
	(119,'GlobalSet','2016-01-08 21:39:20','2016-01-08 21:39:20','e7e26773-4149-4321-a285-42feee6a23ff'),
	(121,'Entry','2016-01-12 01:59:06','2016-01-12 01:59:06','08a5ff34-069c-4150-8b22-f5541fa712d2'),
	(124,'GlobalSet','2016-01-12 02:04:13','2016-01-12 02:04:13','80b0faf0-ec40-4ac4-94e5-6747288e618a'),
	(126,'Entry','2016-01-12 04:17:42','2016-01-12 04:17:42','986838c1-e3cf-4a1c-9a35-c6c1c2f2a810'),
	(128,'Asset','2016-01-14 03:52:05','2016-01-14 03:52:05','5df6ff5d-f9c2-41d4-9986-9a6ae3f2ee01'),
	(148,'Entry','2016-01-16 00:50:41','2016-01-16 00:50:41','29d4d273-1849-40c5-8d65-6c658a538173'),
	(150,'Entry','2016-01-16 02:00:35','2016-01-16 02:00:35','2149d527-db30-42a5-8f6b-b9c304518968'),
	(154,'MatrixBlock','2016-01-16 02:13:21','2016-01-16 02:13:21','7e1f1f39-7fae-4ec2-905e-9f30654309fc'),
	(159,'MatrixBlock','2016-01-16 02:14:05','2016-01-16 02:14:05','87faea68-5eb7-4d42-af8e-8b15fb1c16af'),
	(160,'MatrixBlock','2016-01-16 02:14:05','2016-01-16 02:14:05','55b5a90f-fae2-498b-bdea-3b8990de6299'),
	(161,'MatrixBlock','2016-01-16 02:14:05','2016-01-16 02:14:05','a15979dc-bfbc-4a57-9f2e-a40bd3ff96fb'),
	(162,'MatrixBlock','2016-01-16 02:14:05','2016-01-16 02:14:05','4980cf4e-51d1-40ee-8de3-c6b2bcda2ec8'),
	(166,'Entry','2016-01-16 03:37:12','2016-01-16 03:37:12','b599a69a-9e38-44c8-b131-c2a13b23beb4'),
	(171,'Category','2016-01-16 03:39:54','2016-01-16 03:39:54','f85c6d08-b95c-4833-a8d8-1a7300f98591'),
	(172,'Entry','2016-01-16 03:49:06','2016-01-16 03:49:06','7dba3c2e-8c2f-4ada-86d0-a0276d54be2f'),
	(173,'Entry','2016-01-16 03:53:47','2016-01-16 03:53:47','c629a0c4-28a9-441a-983c-2cccaaf31155'),
	(174,'Entry','2016-01-16 03:56:43','2016-01-16 03:56:43','3e4c2d37-d2c3-4f44-9fbb-0a43440d61c0'),
	(175,'Entry','2016-01-16 04:07:30','2016-01-16 04:07:30','5d84be0e-3182-4df1-a8eb-646c38fd8d97'),
	(176,'Asset','2016-01-16 04:15:51','2016-01-16 04:15:51','dc89cb16-01b3-4a79-a36b-a7e376d06da2'),
	(177,'Entry','2016-01-16 05:22:16','2016-01-16 05:22:16','fa46daac-3404-4798-93c3-23bf898f170c'),
	(178,'Entry','2016-01-17 22:15:41','2016-01-17 22:15:41','8cee86af-abeb-4951-aac9-36f1fa2bb3b1');

/*!40000 ALTER TABLE `fieldlayouts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fieldlayouttabs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fieldlayouttabs`;

CREATE TABLE `fieldlayouttabs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layoutId` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` tinyint(4) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fieldlayouttabs_sortOrder_idx` (`sortOrder`),
  KEY `fieldlayouttabs_layoutId_fk` (`layoutId`),
  CONSTRAINT `fieldlayouttabs_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `fieldlayouttabs` WRITE;
/*!40000 ALTER TABLE `fieldlayouttabs` DISABLE KEYS */;

INSERT INTO `fieldlayouttabs` (`id`, `layoutId`, `name`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(2,5,'Content',1,'2015-12-19 00:45:35','2015-12-19 00:45:35','0877ff73-4bb9-4fd7-9b3b-56263e83deb7'),
	(22,22,'Content',1,'2015-12-19 01:44:56','2015-12-19 01:44:56','a32d6c2c-a9c4-4586-9836-cab943b55623'),
	(23,22,'Metadata',2,'2015-12-19 01:44:56','2015-12-19 01:44:56','1027ea8c-dc1e-4e09-89eb-34beb7aeaa31'),
	(39,32,'Testimonial Details',1,'2015-12-19 02:31:59','2015-12-19 02:31:59','1a4fb6b2-9392-43cb-8971-a514127cc229'),
	(40,32,'Metadata',2,'2015-12-19 02:31:59','2015-12-19 02:31:59','b888e974-851b-48d1-b203-1187434005f8'),
	(78,58,'Content',1,'2015-12-20 04:51:51','2015-12-20 04:51:51','bd31b3ca-638e-4e5d-8f9e-3960301f20dc'),
	(79,59,'Content',1,'2015-12-20 04:51:51','2015-12-20 04:51:51','e8b78b5a-71b3-488b-a328-ab735c7546a8'),
	(80,60,'Content',1,'2015-12-20 04:51:51','2015-12-20 04:51:51','78bab9b4-f112-4c32-a141-897ef9f3e96d'),
	(89,68,'Content',1,'2015-12-20 14:57:48','2015-12-20 14:57:48','fc5dd8f4-50d1-423c-a39e-8b9e84f6e7be'),
	(94,74,'Team Member Details',1,'2015-12-20 20:39:56','2015-12-20 20:39:56','97f21dfa-c13e-4d08-9833-70aeb5010b22'),
	(110,88,'Content',1,'2015-12-28 00:58:09','2015-12-28 00:58:09','26835350-b9fb-42d2-b06b-f27dade29919'),
	(111,88,'Metadata',2,'2015-12-28 00:58:09','2015-12-28 00:58:09','554bef70-811b-4941-b122-44f138a9298d'),
	(114,90,'Content',1,'2015-12-28 01:04:27','2015-12-28 01:04:27','f4ef7aad-5156-400a-b3b1-a4cdf29456ac'),
	(115,90,'Metadata',2,'2015-12-28 01:04:27','2015-12-28 01:04:27','0a325c60-9cc2-434f-a933-62060abc1422'),
	(126,97,'Content',1,'2015-12-28 01:16:04','2015-12-28 01:16:04','cd785750-cf63-42b2-874f-f127c2010bbc'),
	(127,97,'Metadata',2,'2015-12-28 01:16:04','2015-12-28 01:16:04','cfe2ea24-5b92-4d55-85e4-c6a67697080c'),
	(153,111,'Testimonials',1,'2016-01-07 00:12:12','2016-01-07 00:12:12','43d71d1f-3d55-482c-b2b7-dbaeb0f72008'),
	(165,119,'Content',1,'2016-01-08 21:39:20','2016-01-08 21:39:20','f66cc958-2ea4-4f7e-8f34-81e5993a2ea4'),
	(168,121,'Content',1,'2016-01-12 01:59:06','2016-01-12 01:59:06','a641efa1-486f-4ade-9012-6a02c9df7acf'),
	(169,121,'Metadata',2,'2016-01-12 01:59:06','2016-01-12 01:59:06','34fb3c5c-4988-490c-89ca-bc31d3841099'),
	(172,124,'Content',1,'2016-01-12 02:04:13','2016-01-12 02:04:13','465c40e2-7e31-4bb7-9385-0f750bfd616e'),
	(176,126,'Reservation Details',1,'2016-01-12 04:17:42','2016-01-12 04:17:42','869051dc-6785-4b39-b8b0-9eae6dac894c'),
	(177,126,'Confirmation Details',2,'2016-01-12 04:17:42','2016-01-12 04:17:42','3db5a3c8-b806-4b4d-b9dd-71cc85dfe717'),
	(178,126,'Comments',3,'2016-01-12 04:17:42','2016-01-12 04:17:42','3d45dd65-b7b8-4fd4-9e4b-978fe986d2f6'),
	(208,148,'Content',1,'2016-01-16 00:50:41','2016-01-16 00:50:41','2c143a7d-003c-4adc-8eaa-5fdc8e91c38f'),
	(209,148,'Metadata',2,'2016-01-16 00:50:41','2016-01-16 00:50:41','727c8e57-6ba8-4732-bc23-5c6bd05dae5e'),
	(212,150,'Content',1,'2016-01-16 02:00:35','2016-01-16 02:00:35','f34b0a70-8334-45fb-9429-f6949ce9a18c'),
	(213,150,'Metadata',2,'2016-01-16 02:00:35','2016-01-16 02:00:35','b1a02974-6e08-4646-b719-bc24b62dcb4b'),
	(216,154,'Content',1,'2016-01-16 02:13:21','2016-01-16 02:13:21','91fdf75e-e9f1-4d76-a179-7650e81c0ba1'),
	(221,159,'Content',1,'2016-01-16 02:14:05','2016-01-16 02:14:05','81acc663-865f-4af4-9bef-35f8806a0f5d'),
	(222,160,'Content',1,'2016-01-16 02:14:05','2016-01-16 02:14:05','0cedbede-d0f6-406b-9be1-60817f0675d2'),
	(223,161,'Content',1,'2016-01-16 02:14:05','2016-01-16 02:14:05','579625a3-9018-4fb2-a3aa-56429693fdf2'),
	(224,162,'Content',1,'2016-01-16 02:14:05','2016-01-16 02:14:05','a7df6ad0-d191-4a4a-844e-010e04cb2f01'),
	(230,166,'Content',1,'2016-01-16 03:37:12','2016-01-16 03:37:12','231c515f-e1ac-440f-861c-2ce155cd255f'),
	(231,166,'Metadata',2,'2016-01-16 03:37:12','2016-01-16 03:37:12','480318a0-70ec-49c5-bbe0-274d8b2fd8aa'),
	(236,171,'Selling Options',1,'2016-01-16 03:39:54','2016-01-16 03:39:54','54c60377-0743-4476-8994-573c1da69467'),
	(237,172,'Content',1,'2016-01-16 03:49:06','2016-01-16 03:49:06','4f3797cc-db52-41f6-a6bb-4f92e1b9fb64'),
	(238,172,'Metadata',2,'2016-01-16 03:49:06','2016-01-16 03:49:06','7181d94e-7343-4d8a-9af8-70b1a14b07d4'),
	(239,173,'Content',1,'2016-01-16 03:53:47','2016-01-16 03:53:47','1ed9ff44-7245-46d1-bb09-e3f623c11bce'),
	(240,173,'Metadata',2,'2016-01-16 03:53:47','2016-01-16 03:53:47','fcc2b6ac-da01-417b-a7aa-d4d62fbdcd98'),
	(241,174,'Default',1,'2016-01-16 03:56:43','2016-01-16 03:56:43','06211b8c-f50e-4ac2-abc8-65287578639d'),
	(242,174,'Metadata',2,'2016-01-16 03:56:43','2016-01-16 03:56:43','b4e2a318-c6e9-472a-893e-e54feecee288'),
	(243,175,'Content',1,'2016-01-16 04:07:30','2016-01-16 04:07:30','ee1ec09b-5613-4efe-a2d0-145d38ca9bc8'),
	(244,175,'Metadata',2,'2016-01-16 04:07:30','2016-01-16 04:07:30','29b5cff3-960d-4941-b43a-7d0ab2b642d3'),
	(245,177,'Content',1,'2016-01-16 05:22:16','2016-01-16 05:22:16','780655bf-4f97-464a-a90d-77239e67a606'),
	(246,177,'Property Details',2,'2016-01-16 05:22:16','2016-01-16 05:22:16','3dd43bd8-8f97-4d2f-a65d-ab369599fba4'),
	(247,177,'Metadata',3,'2016-01-16 05:22:16','2016-01-16 05:22:16','56ad0a30-8640-48ee-b135-437ec3f20bda'),
	(248,178,'Content',1,'2016-01-17 22:15:41','2016-01-17 22:15:41','aeb2ab7f-2d1b-475c-a008-beb0a16f4c35'),
	(249,178,'Metadata',2,'2016-01-17 22:15:41','2016-01-17 22:15:41','f92d6ab8-8d53-4245-9a8c-8fb6900a6e47');

/*!40000 ALTER TABLE `fieldlayouttabs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fields`;

CREATE TABLE `fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(58) COLLATE utf8_unicode_ci NOT NULL,
  `context` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'global',
  `instructions` text COLLATE utf8_unicode_ci,
  `translatable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fields_handle_context_unq_idx` (`handle`,`context`),
  KEY `fields_context_idx` (`context`),
  KEY `fields_groupId_fk` (`groupId`),
  CONSTRAINT `fields_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `fieldgroups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `fields` WRITE;
/*!40000 ALTER TABLE `fields` DISABLE KEYS */;

INSERT INTO `fields` (`id`, `groupId`, `name`, `handle`, `context`, `instructions`, `translatable`, `type`, `settings`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'Body','body','global',NULL,1,'RichText','{\"configFile\":\"Standard.json\",\"columnType\":\"text\"}','2015-12-19 00:45:35','2015-12-19 00:45:35','f12c5cda-a50e-46f7-b8d6-f8f71bc729e2'),
	(2,1,'Tags','tags','global',NULL,0,'Tags','{\"source\":\"taggroup:1\"}','2015-12-19 00:45:35','2015-12-19 00:45:35','394d6e3d-fdf4-4276-b052-186289f83725'),
	(3,2,'Page Description','pageDescription','global','Best under 155 characters, the page description attribute provides concise explanations of the contents of the web page. Page descriptions are commonly used on search engine result pages (SERPs) to display preview snippets for a given page.',0,'PlainText','{\"placeholder\":\"Here is a description of the applicable page.\",\"maxLength\":\"155\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-19 01:16:51','2015-12-19 01:16:51','99df8b7c-bdbb-4fc7-98c0-e85741f652f6'),
	(4,3,'Comments','comments','global','Use this field to save any comments about this contact. This field is not visible to the public.',0,'RichText','{\"configFile\":\"Standard.json\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"\",\"columnType\":\"text\"}','2015-12-19 01:35:15','2015-12-19 01:35:15','2e31507f-fa4b-446c-99ac-82b8028c0c29'),
	(5,3,'Email','email','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-19 01:39:24','2015-12-19 01:39:24','1bf9b05a-a20a-4a24-b4e1-bcf61f9f771d'),
	(6,1,'Address','address','global','',0,'SmartMap_Address','{\"layout\":{\"street1\":{\"width\":100,\"enable\":1},\"street2\":{\"width\":100,\"enable\":1},\"city\":{\"width\":50,\"enable\":1},\"state\":{\"width\":15,\"enable\":1},\"zip\":{\"width\":35,\"enable\":1},\"country\":{\"width\":100,\"enable\":0},\"lat\":{\"width\":50,\"enable\":0},\"lng\":{\"width\":50,\"enable\":0}}}','2015-12-19 01:40:42','2015-12-19 01:40:42','0991f5f1-a151-40b5-81dc-2f6d4a78729c'),
	(7,3,'Phone Number','phone','global','',0,'PlainText','{\"placeholder\":\"(123) 456-7890\",\"maxLength\":\"14\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-19 01:41:25','2015-12-19 01:41:25','9e0f257f-3823-4dc2-8f80-9a54f281676a'),
	(8,4,'Baths','baths','global','How many bathrooms does this property have?',0,'Dropdown','{\"options\":[{\"label\":\"Select\",\"value\":\"select\",\"default\":\"\"},{\"label\":\"1\",\"value\":\"1\",\"default\":\"\"},{\"label\":\"1.5\",\"value\":\"1.5\",\"default\":\"\"},{\"label\":\"2\",\"value\":\"2\",\"default\":\"\"},{\"label\":\"2.5\",\"value\":\"2.5\",\"default\":\"\"},{\"label\":\"3\",\"value\":\"3\",\"default\":\"\"},{\"label\":\"3.5\",\"value\":\"3.5\",\"default\":\"\"},{\"label\":\"4\",\"value\":\"4\",\"default\":\"\"},{\"label\":\"4.5\",\"value\":\"4.5\",\"default\":\"\"},{\"label\":\"5\",\"value\":\"5\",\"default\":\"\"}]}','2015-12-19 01:49:03','2015-12-19 01:49:03','bdecaabb-c7c7-47a2-ab3a-d63b3c5a2c67'),
	(9,4,'Beds','beds','global','How many bedrooms does this property have?',0,'Dropdown','{\"options\":[{\"label\":\"Select\",\"value\":\"\",\"default\":\"\"},{\"label\":\"1\",\"value\":\"1\",\"default\":\"\"},{\"label\":\"2\",\"value\":\"2\",\"default\":\"\"},{\"label\":\"3\",\"value\":\"3\",\"default\":\"\"},{\"label\":\"4\",\"value\":\"5\",\"default\":\"\"},{\"label\":\"5\",\"value\":\"5\",\"default\":\"\"},{\"label\":\"6\",\"value\":\"6\",\"default\":\"\"}]}','2015-12-19 01:49:48','2015-12-19 01:49:48','dcc5a984-abc4-4228-9783-b71b09bd5ab6'),
	(10,4,'Foreclosure','foreclosure','global','Select this option if the property is in foreclosure.',0,'Lightswitch','{\"default\":\"\"}','2015-12-19 01:50:09','2016-01-16 05:28:09','b777f679-eb4c-4014-93ac-812a1c22fc13'),
	(11,4,'MLS Number','mlsNumber','global','Enter the MLS number here. The system will generate the full url automatically.',0,'PlainText','{\"placeholder\":\"504907\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-19 01:50:23','2015-12-19 15:36:46','f381fe3f-620d-4091-ae02-cb37deb05c73'),
	(12,4,'Property Type','propertyType','global','Choose what type of property this is.',0,'Dropdown','{\"options\":[{\"label\":\"Select\",\"value\":\"\",\"default\":\"1\"},{\"label\":\"Single Family\",\"value\":\"singleFamily\",\"default\":\"\"}]}','2015-12-19 01:50:58','2016-01-13 04:17:32','82349bc5-e270-4754-88e4-e64c724fac8e'),
	(13,4,'Short Sale','shortSale','global','Select this option if this property will be classified as a short sale.',0,'Lightswitch','{\"default\":\"\"}','2015-12-19 01:51:12','2016-01-16 05:27:40','1ef38866-d2cd-4d4d-a71a-96a1e9ca7422'),
	(14,4,'Square Feet','squareFeet','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-19 01:51:25','2015-12-19 01:51:25','e8d19888-44ba-4926-8b48-f65ad01441fe'),
	(15,5,'Attribution','attribution','global','Provide testimonial attribution here.',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-19 02:31:18','2016-01-07 00:21:18','dd4e1e3c-9972-446e-95f8-6fba64f9e301'),
	(17,4,'Parking','parking','global','What type of parking does this property have?',0,'PlainText','{\"placeholder\":\"3 Car Garage\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-19 03:16:32','2015-12-19 03:16:32','b488cbdd-facb-445e-8d23-b68377d385cf'),
	(18,4,'Price','price','global','Enter the listing price for this property. You do not need to include the \"$\".',0,'PlainText','{\"placeholder\":\"$849,900\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-19 15:06:05','2016-01-13 04:03:05','36c97ac2-1c58-4e00-8b49-af45642b6797'),
	(19,4,'Video URL','videoUrl','global','Enter the video ID for this property. The system will generate the full url automatically.',0,'PlainText','{\"placeholder\":\"yZRyXkGqD2c\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-19 15:12:26','2015-12-19 15:36:52','0414a4c3-f8f5-4994-ba6a-e8dddb55ee02'),
	(20,4,'Matterport URL','matterportUrl','global','Enter the Matterport video ID for this property. The system will generate the full url automatically.',0,'PlainText','{\"placeholder\":\"1Hw5ay9jY7y\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-19 15:32:05','2015-12-19 15:36:58','67f2bc5f-03db-415f-8fef-4a3d4ade4706'),
	(21,4,'Year Built','yearBuilt','global','What year was this property built?',0,'PlainText','{\"placeholder\":\"2003\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-19 16:05:06','2015-12-19 16:05:06','fb08ef31-c3f9-4a02-ab3d-a6da2c788de0'),
	(22,1,'Office Title','officeTitle','global','',0,'PlainText','{\"placeholder\":\"Remax Real Estate Concepts\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-20 04:17:37','2015-12-20 04:17:37','d1d6e057-8b17-4285-b704-9c9efb033549'),
	(23,1,'Office Phone','officePhone','global','This is the primary phone contact. It is also what is listed under \"Cell\" in the footer of the website.',0,'PlainText','{\"placeholder\":\"(123) 456-7890\",\"maxLength\":\"14\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-20 04:18:33','2016-01-08 21:35:56','81c7631f-fea6-49bc-b5fa-08b945d6870e'),
	(24,6,'Facebook','facebook','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-20 04:20:40','2015-12-20 04:20:40','e65e1429-3526-44cb-9954-d64ececd5cf7'),
	(25,6,'Twitter','twitter','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-20 04:21:52','2015-12-20 04:21:52','4b5ebbc8-b438-4d04-b09e-7fa28531cad5'),
	(26,6,'LinkedIn','linkedin','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-20 04:22:12','2015-12-20 04:22:12','85bf3994-3dd1-40ee-92d4-9fd56df0b52f'),
	(27,6,'YouTube','youtube','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-20 04:22:23','2015-12-20 04:22:23','853d095b-4558-4b32-b222-1d372fdafaba'),
	(28,7,'Option Description','optionDescription','global','Provide a brief description about this selling option.',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-20 04:43:57','2016-01-16 03:18:31','3411aec3-fd54-4b2e-ad50-9635f6cae6fc'),
	(32,NULL,'Description','description','matrixBlockType:2','Provide a description of this option.',0,'RichText','{\"configFile\":\"Standard.json\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"\",\"columnType\":\"text\"}','2015-12-20 04:51:08','2015-12-20 04:51:51','0f6efc07-58be-494b-98c5-55f3402b1e7b'),
	(33,NULL,'Details','optionDetails','matrixBlockType:3','Provide what this selling option includes.',0,'RichText','{\"configFile\":\"Standard.json\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"\",\"columnType\":\"text\"}','2015-12-20 04:51:08','2015-12-20 04:51:51','b97e0a9a-2703-48ff-ac4f-8cc4752a6d5a'),
	(34,NULL,'Commission','optionCommission','matrixBlockType:4','Select the commission rate for this option.',0,'Dropdown','{\"options\":[{\"label\":\"Select\",\"value\":\"\",\"default\":\"1\"},{\"label\":\"4\",\"value\":\"4\",\"default\":\"\"},{\"label\":\"5\",\"value\":\"5\",\"default\":\"\"},{\"label\":\"6\",\"value\":\"6\",\"default\":\"\"},{\"label\":\"7\",\"value\":\"7\",\"default\":\"\"},{\"label\":\"8\",\"value\":\"8\",\"default\":\"\"}]}','2015-12-20 04:51:08','2015-12-20 04:51:51','dfd012d0-d91d-4686-a9bb-8af885ef647c'),
	(35,7,'Commission','optionCommission','global','Choose the commission rate for this option.',0,'Dropdown','{\"options\":[{\"label\":\"Select\",\"value\":\"\",\"default\":\"1\"},{\"label\":\"2\",\"value\":\"2\",\"default\":\"\"},{\"label\":\"3\",\"value\":\"3\",\"default\":\"\"},{\"label\":\"4\",\"value\":\"4\",\"default\":\"\"},{\"label\":\"5\",\"value\":\"5\",\"default\":\"\"},{\"label\":\"6\",\"value\":\"6\",\"default\":\"\"},{\"label\":\"7\",\"value\":\"7\",\"default\":\"\"},{\"label\":\"8\",\"value\":\"8\",\"default\":\"\"},{\"label\":\"9\",\"value\":\"9\",\"default\":\"\"}]}','2015-12-20 14:38:12','2015-12-20 14:38:12','e20dd876-8935-4164-8ff0-666075ea7049'),
	(36,7,'Option Benefits','optionBenefits','global','Choose the features that are available for this option. To include the feature, check it. You can also reorder any item by dragging it.',0,'Table','{\"columns\":{\"col1\":{\"heading\":\"Feature\",\"handle\":\"feature\",\"width\":\"\",\"type\":\"singleline\"},\"col2\":{\"heading\":\"Available\",\"handle\":\"available\",\"width\":\"\",\"type\":\"checkbox\"}},\"defaults\":{\"row1\":{\"col1\":\"3 Month Listing\",\"col2\":\"\"},\"row2\":{\"col1\":\"Full MLS Exposure\",\"col2\":\"\"},\"row3\":{\"col1\":\"Professional Photos\",\"col2\":\"\"},\"row4\":{\"col1\":\"Social Media Marketing\",\"col2\":\"\"},\"row5\":{\"col1\":\"Sign and Lockbox\",\"col2\":\"\"},\"row6\":{\"col1\":\"Facebook Targeted Marketing\",\"col2\":\"\"},\"row7\":{\"col1\":\"3d Matterport Virtual Tour\",\"col2\":\"\"},\"row8\":{\"col1\":\"Access to 15ft. Truck\",\"col2\":\"\"},\"row9\":{\"col1\":\"Free Pest Inspection\",\"col2\":\"\"},\"row10\":{\"col1\":\"$1,000 \\\"offer\\\" Guarantee!\",\"col2\":\"\"},\"row11\":{\"col1\":\"Free Storage\",\"col2\":\"\"},\"row12\":{\"col1\":\"Full Move Package\",\"col2\":\"\"}}}','2015-12-20 14:39:58','2016-01-17 04:04:53','c2ab6649-a94e-4ad5-aea0-f6380fd44243'),
	(37,7,'Option Icon','optionIcon','global','Upload an icon for this option.',0,'Assets','{\"useSingleFolder\":\"\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"limit\":\"\",\"viewMode\":\"list\",\"selectionLabel\":\"\"}','2015-12-20 14:40:27','2015-12-21 02:53:22','822ca102-29fc-4699-9192-ef2da02a039b'),
	(39,1,'Disclaimer','markCharterDisclaimer','global','Mark Charter disclaimer language goes here.',0,'RichText','{\"configFile\":\"Standard.json\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"\",\"columnType\":\"text\"}','2015-12-20 14:57:35','2015-12-20 15:02:34','8c1890bc-50ba-4aa2-86c0-df2097d9fd08'),
	(41,8,'Title/Position','teamMemberTitle','global','This team members title.',0,'PlainText','{\"placeholder\":\"Owner \\/ Realtor\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-20 20:24:51','2015-12-20 20:27:36','f468c425-0ef6-4116-bbd5-a6943e861cb8'),
	(42,8,'Phone','teamMemberPhone','global','The team member\'s phone number.',0,'PlainText','{\"placeholder\":\"(123) 456-7890\",\"maxLength\":\"14\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-20 20:25:39','2015-12-20 20:25:39','82584adc-4593-451c-be06-a82943d2efbf'),
	(43,8,'Email','teamMemberEmail','global','The team member\'s email address.',0,'PlainText','{\"placeholder\":\"name@charterhouseiowa.com\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-20 20:26:15','2015-12-20 20:26:15','69ae1a84-355f-49b7-bf44-0c72b712b07b'),
	(44,8,'Bio','teamMemberBio','global','Provide a brief bio for this team member.',0,'RichText','{\"configFile\":\"Standard.json\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"\",\"columnType\":\"text\"}','2015-12-20 20:28:46','2015-12-20 20:28:46','2cfb8421-4c01-4f21-9313-ed00386f3492'),
	(45,8,'Photo','teamMemberPhoto','global','Upload a photo of this team member.',0,'Assets','{\"useSingleFolder\":\"\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"1\",\"allowedKinds\":[\"image\"],\"limit\":\"1\",\"viewMode\":\"list\",\"selectionLabel\":\"Add an image\"}','2015-12-20 20:39:35','2015-12-20 20:39:35','3813b2d6-21f9-4600-ae63-09b1c566ff55'),
	(46,9,'Website','partnerWebsite','global','',0,'PlainText','{\"placeholder\":\"http:\\/\\/www.website.com\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-20 20:46:49','2015-12-20 20:46:49','09a0aa5c-736f-463d-9bc6-44257358d37a'),
	(47,9,'Phone Number','partnerPhone','global','',0,'PlainText','{\"placeholder\":\"(123) 456-7890\",\"maxLength\":\"14\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-20 20:47:12','2015-12-20 20:47:12','1373cabd-805a-4fd2-87a7-e06b9148a0fe'),
	(48,9,'Logo','partnerLogo','global','Upload a logo for this partner',0,'Assets','{\"useSingleFolder\":\"1\",\"sources\":\"*\",\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"3\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"1\",\"allowedKinds\":[\"image\"],\"limit\":\"1\",\"viewMode\":\"list\",\"selectionLabel\":\"Add a logo\"}','2015-12-20 20:56:18','2016-01-16 04:18:17','88083212-5080-4edf-92c3-03fd649cbcdd'),
	(49,1,'Content Builder','matrix','global','Use this feature to build out the content for this page. You can arrange fields in any order to create a different layout.',0,'Matrix','{\"maxBlocks\":null}','2015-12-28 00:51:11','2016-01-16 02:14:05','513a5d1b-db33-4b08-bfcd-79b0e9ff044b'),
	(50,NULL,'Headline','headline','matrixBlockType:5','Use this field to create a primary headline.',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-28 00:51:11','2016-01-16 02:14:05','1d650798-bb86-46bf-a35d-56df36711c03'),
	(51,NULL,'Body','bodyCopy','matrixBlockType:6','Use this field to enter a block of content.',0,'RichText','{\"configFile\":\"Standard.json\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"\",\"columnType\":\"text\"}','2015-12-28 00:51:11','2016-01-16 02:14:05','e0e56807-215b-4bee-b943-021feb3270ea'),
	(52,NULL,'Quote','quote','matrixBlockType:7','Use this field to create a pull quote.',0,'RichText','{\"configFile\":\"Simple.json\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"\",\"columnType\":\"text\"}','2015-12-28 00:51:11','2016-01-16 02:14:05','0b51a680-4907-4fb3-84d5-c51761a2383f'),
	(53,NULL,'Quote Attribution','quoteAttribution','matrixBlockType:7','Provide quote attribution here.',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-28 00:51:11','2016-01-16 02:14:05','dc079e1e-9a89-4106-a5cc-3618100a909e'),
	(54,4,'Property Images','propertyImages','global','Upload images of this property here. You can double click any image to edit the title.',0,'Assets','{\"useSingleFolder\":\"1\",\"sources\":\"*\",\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"1\",\"singleUploadLocationSubpath\":\"{mlsNumber}\",\"restrictFiles\":\"1\",\"allowedKinds\":[\"image\"],\"limit\":\"\",\"viewMode\":\"list\",\"selectionLabel\":\"Add an image\"}','2016-01-06 16:53:18','2016-01-06 23:53:41','b720a19b-e6f2-4b6e-a359-bad66ac4a2b0'),
	(55,4,'Sale Status','saleStatus','global','Choose the current sale status of this property.',0,'Dropdown','{\"options\":[{\"label\":\"Select\",\"value\":\"\",\"default\":\"\"},{\"label\":\"Available\",\"value\":\"available\",\"default\":\"1\"},{\"label\":\"Pending\",\"value\":\"pending\",\"default\":\"\"},{\"label\":\"Sold\",\"value\":\"sold\",\"default\":\"\"}]}','2016-01-06 17:00:58','2016-01-06 17:00:58','43c486d4-0f84-4032-90b0-07a94a2f7230'),
	(57,4,'Featured Images','featuredImages','global','Upload up to three images for the hero slider. These are the large image(s) that will show at the top of property page. For best results, upload an image that is at least 1298 x 780. ',0,'Assets','{\"useSingleFolder\":\"1\",\"sources\":\"*\",\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"1\",\"singleUploadLocationSubpath\":\"{mlsNumber}\",\"restrictFiles\":\"1\",\"allowedKinds\":[\"image\"],\"limit\":\"3\",\"viewMode\":\"list\",\"selectionLabel\":\"Add an image\"}','2016-01-06 17:21:35','2016-01-17 15:57:56','be66cef7-2596-4ed6-a567-71804a48fe89'),
	(58,3,'Reservation Status','reservationStatus','global','Select this option to approve the reservation time. The system will notify the customer of the approval.',0,'Lightswitch','{\"default\":\"\"}','2016-01-07 00:28:08','2016-01-07 00:28:08','11c294c9-afd8-4c5e-a5f5-4d28cd0505a7'),
	(59,3,'Electronic Signature','electronicSignature','global','This is the electronic signature of the customer reserving the truck.',0,'PlainText','{\"placeholder\":\"Tyler Durden\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-01-07 04:28:08','2016-01-08 02:14:53','1754bf01-6aa6-4ffa-84db-145e2b02fc2f'),
	(60,3,'Signature Date','signatureDate','global','The date the e-signature was captured.',0,'Date','{\"minuteIncrement\":\"30\",\"showDate\":1,\"showTime\":0}','2016-01-07 04:29:22','2016-01-07 04:29:22','324da0b4-0e1c-4d65-82e3-b0a055f79cdb'),
	(61,3,'First Name','firstName','global','',0,'PlainText','{\"placeholder\":\"Tyler\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-01-08 02:11:21','2016-01-08 02:11:32','84958425-9594-4214-89b3-e50519cc753a'),
	(62,3,'Last Name','lastName','global','',0,'PlainText','{\"placeholder\":\"Durden\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-01-08 02:11:51','2016-01-08 02:11:51','e2a07dc7-e1c3-4e04-ab04-f01f64b1560e'),
	(63,1,'Office Email','officeEmail','global','This is the primary phone contact. It is also what is listed under \"Email\" in the footer of the website.',0,'PlainText','{\"placeholder\":\"name@email.com\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-01-08 21:35:30','2016-01-08 21:35:49','6129b906-9980-4946-83bb-3425087c3fda'),
	(64,1,'License Number','licenseNumber','global','This is the real estate license number. You do not need to include the \"#\" symbol.',0,'PlainText','{\"placeholder\":\"56865000\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-01-08 21:39:08','2016-01-08 21:39:08','87879660-4313-4a27-86b8-f0af59506b17'),
	(65,3,'Agree To Terms','agreeToRentalTerms','global','This signifies the client agrees to the terms of the rental agreement.',0,'Checkboxes','{\"options\":[{\"label\":\"Agree to terms\",\"value\":\"yes\",\"default\":\"\"}]}','2016-01-12 02:09:56','2016-01-12 02:13:26','cdcd85db-f161-4ef3-b616-a27d157d261d'),
	(66,1,'Buyer Hero Image','buyerHeroImage','global','This is the large image that will show in the background of the \"buyer\" home page. For best results, upload an image at least 1298 x 780. ',0,'Assets','{\"useSingleFolder\":\"1\",\"sources\":\"*\",\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"2\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"1\",\"allowedKinds\":[\"image\"],\"limit\":\"1\",\"viewMode\":\"list\",\"selectionLabel\":\"Add an image\"}','2016-01-14 03:51:19','2016-01-16 00:47:51','de8339b9-b55c-4a43-8943-9b4296ef4032'),
	(67,1,'Buyer Content Builder','buyerContentBuilderMatrix','global','Use this feature to build out the content for this page. You can arrange fields in any order to create a different layout.',0,'Matrix','{\"maxBlocks\":\"3\"}','2016-01-14 04:25:21','2016-01-16 02:13:21','52ef9b4d-cfab-4e3c-afb2-09f8d7382450'),
	(71,NULL,'Icon','icon','matrixBlockType:11','Upload an icon for this block of content.',0,'Assets','{\"useSingleFolder\":\"1\",\"sources\":\"*\",\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"2\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"1\",\"allowedKinds\":[\"image\"],\"limit\":\"1\",\"viewMode\":\"list\",\"selectionLabel\":\"Add an image\"}','2016-01-14 04:34:42','2016-01-16 02:13:21','9181205a-9300-4f11-9018-853f34c86ce6'),
	(72,NULL,'Headline','headline','matrixBlockType:11','Use this field to create a primary headline.',0,'PlainText','{\"placeholder\":\"24\\/7 Availability\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-01-14 04:34:42','2016-01-16 02:13:21','f6eea946-5361-4336-b97e-d9bd6f55177e'),
	(73,NULL,'Body','bodyCopy','matrixBlockType:11','Use this field to enter a block of content.',0,'RichText','{\"configFile\":\"Standard.json\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"\",\"columnType\":\"text\"}','2016-01-14 04:34:42','2016-01-16 02:13:21','f67faf8c-69fa-4250-8a63-b0320a95ea23'),
	(74,1,'Seller Hero Image','sellerHeroImage','global','This is the large image that will show in the background of the \"seller\" home page. For best results, upload an image at least 1298 x 780. ',0,'Assets','{\"useSingleFolder\":\"1\",\"sources\":\"*\",\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"2\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"1\",\"allowedKinds\":[\"image\"],\"limit\":\"1\",\"viewMode\":\"list\",\"selectionLabel\":\"Add an image\"}','2016-01-16 00:49:57','2016-01-16 00:49:57','42e5a762-7046-4c25-8da6-689f97f4ba1d'),
	(75,1,'Hero Image','heroImage','global','This is the large image that will show in the background at the top of each page. For best results, upload an image that is at least 1298 x 780. ',0,'Assets','{\"useSingleFolder\":\"1\",\"sources\":\"*\",\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"2\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"1\",\"allowedKinds\":[\"image\"],\"limit\":\"1\",\"viewMode\":\"list\",\"selectionLabel\":\"Add an image\"}','2016-01-16 01:56:54','2016-01-17 15:56:27','bd68f644-1aa7-48ce-b1b2-1aa7776eda0d'),
	(76,NULL,'Sub Headline','subHeadline','matrixBlockType:12','Use this field to create a sub-headline.',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-01-16 02:13:45','2016-01-16 02:14:05','b1894a62-b36e-49ec-a868-39b694e9e5e0'),
	(78,4,'Price Reduced','priceReduced','global','Select this option if the price has been reduced. ',0,'Lightswitch','{\"default\":\"\"}','2016-01-16 05:21:54','2016-01-16 05:25:01','efa209d5-4dc4-491a-ae87-a348cb061c8a');

/*!40000 ALTER TABLE `fields` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table globalsets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `globalsets`;

CREATE TABLE `globalsets` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fieldLayoutId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `globalsets_name_unq_idx` (`name`),
  UNIQUE KEY `globalsets_handle_unq_idx` (`handle`),
  KEY `globalsets_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `globalsets_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `globalsets_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `globalsets` WRITE;
/*!40000 ALTER TABLE `globalsets` DISABLE KEYS */;

INSERT INTO `globalsets` (`id`, `name`, `handle`, `fieldLayoutId`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(19,'Office Information','officeInformation',119,'2015-12-19 21:06:00','2016-01-08 21:39:20','9ba6f9f7-11c5-4a2a-8b58-4dda030f6a39'),
	(20,'Social Media','socialMedia',46,'2015-12-20 04:19:31','2015-12-20 04:19:31','544fbd30-0bcd-4a78-b23c-3ca02abbcf25'),
	(37,'Disclaimer','disclaimer',68,'2015-12-20 14:56:56','2015-12-20 14:57:48','24a9377e-82c3-4e26-b03f-b7e7cc0c111c'),
	(79,'Truck Rental Agreement','truckRentalAgreement',124,'2016-01-12 02:03:44','2016-01-12 02:04:13','fe35395b-8f4a-4c95-8917-3d623df63e4d');

/*!40000 ALTER TABLE `globalsets` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table info
# ------------------------------------------------------------

DROP TABLE IF EXISTS `info`;

CREATE TABLE `info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `build` int(11) unsigned NOT NULL,
  `schemaVersion` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `releaseDate` datetime NOT NULL,
  `edition` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `siteName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `siteUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timezone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `on` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `maintenance` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `track` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `info` WRITE;
/*!40000 ALTER TABLE `info` DISABLE KEYS */;

INSERT INTO `info` (`id`, `version`, `build`, `schemaVersion`, `releaseDate`, `edition`, `siteName`, `siteUrl`, `timezone`, `on`, `maintenance`, `track`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'2.5',2759,'2.5.11','2016-01-14 20:20:03',0,'Charter House Real Estate','http://local.charterhouseiowa.dev','America/Chicago',1,0,'stable','2015-12-19 00:45:31','2016-01-15 01:39:42','f76a2a20-257e-4f36-a3cb-7f9bcd807ce4');

/*!40000 ALTER TABLE `info` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table locales
# ------------------------------------------------------------

DROP TABLE IF EXISTS `locales`;

CREATE TABLE `locales` (
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` tinyint(4) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`locale`),
  KEY `locales_sortOrder_idx` (`sortOrder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `locales` WRITE;
/*!40000 ALTER TABLE `locales` DISABLE KEYS */;

INSERT INTO `locales` (`locale`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	('en_us',1,'2015-12-19 00:45:31','2015-12-19 00:45:31','3e363787-542c-47f4-9e94-6d0cacafe461');

/*!40000 ALTER TABLE `locales` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table matrixblocks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `matrixblocks`;

CREATE TABLE `matrixblocks` (
  `id` int(11) NOT NULL,
  `ownerId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `typeId` int(11) DEFAULT NULL,
  `sortOrder` tinyint(4) DEFAULT NULL,
  `ownerLocale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `matrixblocks_ownerId_idx` (`ownerId`),
  KEY `matrixblocks_fieldId_idx` (`fieldId`),
  KEY `matrixblocks_typeId_idx` (`typeId`),
  KEY `matrixblocks_sortOrder_idx` (`sortOrder`),
  KEY `matrixblocks_ownerLocale_fk` (`ownerLocale`),
  CONSTRAINT `matrixblocks_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixblocks_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixblocks_ownerId_fk` FOREIGN KEY (`ownerId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixblocks_ownerLocale_fk` FOREIGN KEY (`ownerLocale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `matrixblocks_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `matrixblocktypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `matrixblocks` WRITE;
/*!40000 ALTER TABLE `matrixblocks` DISABLE KEYS */;

INSERT INTO `matrixblocks` (`id`, `ownerId`, `fieldId`, `typeId`, `sortOrder`, `ownerLocale`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(55,38,49,6,2,NULL,'2015-12-28 00:53:16','2016-01-16 03:57:00','6e271221-e033-4dfe-a491-76602329a975'),
	(56,38,49,5,1,NULL,'2015-12-28 00:55:12','2016-01-16 03:57:00','1f95b295-5062-4efd-95e7-ff9cc5106154'),
	(57,38,49,7,3,NULL,'2015-12-28 00:55:54','2016-01-16 03:57:00','94c1864d-4cbe-4d7b-8bf0-73b3d90b8e20'),
	(58,38,49,6,4,NULL,'2015-12-28 00:57:07','2016-01-16 03:57:00','47becd02-18d1-4110-b84b-93d83cf8a9dd'),
	(59,36,49,6,1,NULL,'2015-12-28 00:57:53','2015-12-28 01:03:42','8281b1d7-7e2f-4572-a8f3-a9bf7ae0943f'),
	(62,61,49,6,1,NULL,'2015-12-28 01:08:39','2015-12-28 01:15:49','3943fb3d-8043-4569-a8cc-22bd404beff4'),
	(63,5,49,6,1,NULL,'2015-12-28 01:27:07','2016-01-16 04:08:11','70e50044-5623-439e-8b83-31231cd2ea52'),
	(65,17,49,6,1,NULL,'2015-12-28 01:29:55','2016-01-17 16:05:04','733b58fd-efb3-4e06-88a1-47b1d564d3ec'),
	(66,18,49,6,1,NULL,'2015-12-28 01:30:06','2016-01-17 16:00:29','6b8c72be-97a1-40d9-a458-3cba444097fe'),
	(81,80,49,6,1,NULL,'2016-01-13 05:03:20','2016-01-17 16:07:12','389630fc-c69d-4203-9aec-1779ece57467'),
	(82,80,49,7,2,NULL,'2016-01-13 05:05:40','2016-01-17 16:07:12','1f5cbec8-e10a-49dd-a0fd-167babeb8af3'),
	(84,2,67,11,1,NULL,'2016-01-14 04:40:36','2016-01-14 04:48:11','db2059cf-1f84-4c39-aa31-bfc95a68ed6d'),
	(87,2,67,11,2,NULL,'2016-01-14 04:46:00','2016-01-14 04:48:11','a96f7109-fdfa-4d04-9fda-cfb7350388eb'),
	(89,2,67,11,3,NULL,'2016-01-14 04:47:44','2016-01-14 04:48:11','4bfffb24-7218-4332-9176-0e3811135957'),
	(93,22,49,5,1,NULL,'2016-01-16 02:15:19','2016-01-16 02:20:22','89e18000-6b6a-4b08-af46-c9ac429a3aaf'),
	(94,22,49,12,2,NULL,'2016-01-16 02:15:19','2016-01-16 02:20:22','0aa20152-b955-47fd-93da-ad7856cb3c87'),
	(112,9,49,6,1,NULL,'2016-01-17 22:16:03','2016-01-17 22:18:51','30cca020-762f-4a4a-99d7-9744cc7cd750'),
	(113,9,49,5,2,NULL,'2016-01-17 22:18:51','2016-01-17 22:18:51','7ca76643-f27f-4822-9345-4ea76290e194');

/*!40000 ALTER TABLE `matrixblocks` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table matrixblocktypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `matrixblocktypes`;

CREATE TABLE `matrixblocktypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` tinyint(4) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `matrixblocktypes_name_fieldId_unq_idx` (`name`,`fieldId`),
  UNIQUE KEY `matrixblocktypes_handle_fieldId_unq_idx` (`handle`,`fieldId`),
  KEY `matrixblocktypes_fieldId_fk` (`fieldId`),
  KEY `matrixblocktypes_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `matrixblocktypes_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixblocktypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `matrixblocktypes` WRITE;
/*!40000 ALTER TABLE `matrixblocktypes` DISABLE KEYS */;

INSERT INTO `matrixblocktypes` (`id`, `fieldId`, `fieldLayoutId`, `name`, `handle`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(2,28,58,'Description','description',1,'2015-12-20 04:51:08','2015-12-20 04:51:51','7a412cca-fef5-4ae3-94b9-fa0bc972193b'),
	(3,28,59,'Details','details',2,'2015-12-20 04:51:08','2015-12-20 04:51:51','da10b97b-f9e2-4bbf-998c-82ea01903255'),
	(4,28,60,'Commission','commission',3,'2015-12-20 04:51:08','2015-12-20 04:51:51','014ca1d3-9bc4-4e14-8627-a2b995868265'),
	(5,49,159,'Headline','headline',1,'2015-12-28 00:51:11','2016-01-16 02:14:05','5a7dec2a-982e-40bf-b581-d50e4adb2ec6'),
	(6,49,161,'Body','body',3,'2015-12-28 00:51:11','2016-01-16 02:14:05','4744ff05-cd2c-45c8-ac4e-5c48b3461414'),
	(7,49,162,'Quote','quote',4,'2015-12-28 00:51:11','2016-01-16 02:14:05','ddcb9f5a-ad17-459b-b118-a1c613d1d769'),
	(11,67,154,'Buyer Content','buyerContent',1,'2016-01-14 04:34:42','2016-01-16 02:13:21','cf8697b2-6665-42a2-bfb6-37dc211636ce'),
	(12,49,160,'Sub Headline','subHeadline',2,'2016-01-16 02:13:45','2016-01-16 02:14:05','c9cb6802-5389-4c02-8795-31367625afbc');

/*!40000 ALTER TABLE `matrixblocktypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table matrixcontent_buyercontentbuildermatrix
# ------------------------------------------------------------

DROP TABLE IF EXISTS `matrixcontent_buyercontentbuildermatrix`;

CREATE TABLE `matrixcontent_buyercontentbuildermatrix` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `field_headline_headline` text COLLATE utf8_unicode_ci,
  `field_body_bodyCopy` text COLLATE utf8_unicode_ci,
  `field_buyerContent_headline` text COLLATE utf8_unicode_ci,
  `field_buyerContent_bodyCopy` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `matrixcontent_buyercontentbuildermatrix_elementId_locale_unq_idx` (`elementId`,`locale`),
  KEY `matrixcontent_buyercontentbuildermatrix_locale_idx` (`locale`),
  CONSTRAINT `matrixcontent_buyercontentbuildermatrix_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixcontent_buyercontentbuildermatrix_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `matrixcontent_buyercontentbuildermatrix` WRITE;
/*!40000 ALTER TABLE `matrixcontent_buyercontentbuildermatrix` DISABLE KEYS */;

INSERT INTO `matrixcontent_buyercontentbuildermatrix` (`id`, `elementId`, `locale`, `field_headline_headline`, `field_body_bodyCopy`, `field_buyerContent_headline`, `field_buyerContent_bodyCopy`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,84,'en_us',NULL,NULL,'24/7 Availability','<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit sapiente repellendus, delectus a veniam provident maxime distinctio ipsum sint nulla incidunt vel dolorum ea dolore!</p>','2016-01-14 04:40:36','2016-01-14 04:48:11','228f1e69-d096-4590-9a21-9d75a8a835c8'),
	(2,87,'en_us',NULL,NULL,'Access to Interior Designer','<p>Repellat ea sunt saepe libero repellendus itaque autem omnis, dolore! Reiciendis voluptatibus qui neque doloremque nisi. Ullam illum nihil iure magnam ex, sint, nostrum mollitia?</p>','2016-01-14 04:46:00','2016-01-14 04:48:11','3c0743ed-8e31-48c0-95cf-34b290daca7a'),
	(3,89,'en_us',NULL,NULL,'Lender Recommendations','<p>Ab dignissimos hic sequi! Porro asperiores, quis reprehenderit, sit ratione rem esse, non suscipit laborum ipsa cumque corporis in? Optio, libero, enim. Quaerat, voluptates, nesciunt.</p>','2016-01-14 04:47:44','2016-01-14 04:48:11','5a56de7f-02c1-40b7-a0c3-ab31a6d1e678');

/*!40000 ALTER TABLE `matrixcontent_buyercontentbuildermatrix` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table matrixcontent_matrix
# ------------------------------------------------------------

DROP TABLE IF EXISTS `matrixcontent_matrix`;

CREATE TABLE `matrixcontent_matrix` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `field_headline_headline` text COLLATE utf8_unicode_ci,
  `field_body_bodyCopy` text COLLATE utf8_unicode_ci,
  `field_quote_quote` text COLLATE utf8_unicode_ci,
  `field_quote_quoteAttribution` text COLLATE utf8_unicode_ci,
  `field_subHeadline_subHeadline` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `matrixcontent_matrix_elementId_locale_unq_idx` (`elementId`,`locale`),
  KEY `matrixcontent_matrix_locale_fk` (`locale`),
  CONSTRAINT `matrixcontent_matrix_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixcontent_matrix_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `matrixcontent_matrix` WRITE;
/*!40000 ALTER TABLE `matrixcontent_matrix` DISABLE KEYS */;

INSERT INTO `matrixcontent_matrix` (`id`, `elementId`, `locale`, `field_headline_headline`, `field_body_bodyCopy`, `field_quote_quote`, `field_quote_quoteAttribution`, `field_subHeadline_subHeadline`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,55,'en_us',NULL,'<p>Mark started his real estate career in 2005 and has been doing it full-time since then. Over time Mark realized that the majority of real estate agents are the same. They say the same things, they market the same way, and they treat clients the same. Mark did not like this so he set about creating his own unique way of practicing real estate.</p>\r\n\r\n<p>In 2014, Mark debuted a three tier pricing plan for listing clients, giving them the choice on how they want to list their home. This is another example of how Mark never accepts the status quo in real estate and is always looking to make things better for his clients.</p>',NULL,NULL,NULL,'2015-12-28 00:53:16','2016-01-16 03:57:00','8d40fa8c-9548-444f-9c36-7bf625201f72'),
	(2,56,'en_us','Meet Mark and his family',NULL,NULL,NULL,NULL,'2015-12-28 00:55:12','2016-01-16 03:57:00','4d26cf89-2431-484f-85e1-fa5545dfb506'),
	(3,57,'en_us',NULL,NULL,'<p>One of Mark Charter’s greatest characteristics is his giving nature.</p>','Adam Carroll',NULL,'2015-12-28 00:55:54','2016-01-16 03:57:00','cac6c346-3bdb-43b5-b42a-03ee97e6a22a'),
	(4,58,'en_us',NULL,'<p>Mark\'s overall goal on every deal is simple. He wants to provide such great service that after the deal is done, that client will happily provide a positive review of the transaction. This goal is what makes Mark one of the most recommended agents in Central Iowa.</p>\r\n\r\n<p>When you add it all up, when you take into account what Mark offers clients in terms of services, marketing and care, Mark truly feels he is the best option available in the market today and he hopes you will experience this for yourself very soon!</p>',NULL,NULL,NULL,'2015-12-28 00:57:07','2016-01-16 03:57:00','22fb9ebf-a6ca-4056-a653-dfcbd336f520'),
	(5,59,'en_us',NULL,'<h3>Buying & Selling</h3><p>So you are are thinking about buying or selling. That is great, here are a few things you need to know!</p>\r\n\r\n<h2>Buyers</h2>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<p>Buying a home is a big purchase, one of the biggest you will ever make. Why in the world would anyone want to make that purchase without the help of a professional real estate agent? Beats me, but many try. The most important thing to remember is that when you are buying a home, you DO NOT PAY YOUR AGENT! The seller of the home pays the entire commission and that commission is split between the agent selling the home and the agent representing the buyer. There is no reason to not have your own agent representing you and working hard on your behalf.<br></p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<p>I have talked to many buyers who have said something like \"When we bought, we just used the agent who had the house listed.\" If you are thinking about doing this, <strong>use extreme caution!</strong>&nbsp;This should only be done if you have complete trust in the agent. If you just met them, this is a terrible idea! This situation is called Dual Agency and it happens when one agent represents both parties. That agent is trying to get the most money for the seller and also get you the best price. How can they do this? It is tricky and again, should only be done if you have total faith in that agent.<br></p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<p>The way to avoid this is to hire me! Remember, I can show you <em>all</em> homes, not just the ones that I list.<br></p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<p>So when you are my buyer what do you get? Here is a summary of what my buyers get when they work with me:<br></p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<ul><li>An agent with 24/7 availability</li><li>An agent with a A+ BBB rating</li><li>An agent with more recommendations than any other agent in Central Iowa (Trulia.com)</li><li><a href=\"{entry:9:url}\">Free Moving Truck</a> (based on availability, first come, first served)</li><li>Access to my interior designer if you need help or suggestion on how to furnish your new home</li><li>Lender recommendations, I can get you to the right lender who will get you closed on time and with no surprises</li><li>An experienced agent, I have been selling real estate full-time since 2005</li><li>Auto-prospecting so you never miss a new listing to the market</li><li>Weekly check-in updates to make sure you are happy</li><li><strong>And much more!</strong></li></ul>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<p>When you work with me, you are working with a one-of-a-kind agent who simply offers my clients more than other agents. Please read through the client reviews on this site to get a good feel of how I treat my clients. You will never regret hiring me as your agent!</p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<h2>Sellers</h2>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<p>When the time comes to put your home on the market, hiring a real estate professional is a crucial step in the right direction. I realize that there are many home-selling options, but the tried and true method of hiring an agent is still the best option by far. It is proven to be the safest way to sell as well as being the way to generate the highest selling price.<br></p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<p>But, just listing with an agent is not enough! You have to list with an agent who knows how to market and embraces today\'s trends like social media to reach the masses. You want your home exposed everywhere to reach every possible buyer. But you also want to work with an agent who will be enjoyable to work with and one who has great services and benefits to offer you. That agent is me!<br></p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<p>I would love to talk to you about listing your home but for now, here is a brief summary of what I offer to sellers:<br></p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<ul><li>An agent with 24/7 availability</li><li>An agent with an A+ BBB rating</li><li>An agent with more recommendations than any other agent in Central Iowa (Trulia.com)</li><li>A professional photographer will take the photos of your home</li><li>A Professional videographer will do a video of your home (Certain price ranges only)</li><li>Professional staging assistance, my interior designer will make sure your home is ready to go</li><li><a href=\"{entry:9:url}\">Free Moving Truck </a>(Based on availability, first come, first served)</li><li>Professional moving services (at 7% plan while staying in central Iowa)</li><li>Weekly check-in updates to make sure you are happy</li><li>Social Media marketing</li><li><strong>And much more!</strong></li></ul>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<p>No other agent offers their clients more! You will love your selling experience, I promise. I realize one size does not fit all when it comes to selling your home. I want to talk to you about your specific situation and see if I can help.</p>',NULL,NULL,NULL,'2015-12-28 00:57:53','2015-12-28 01:03:42','0e33105a-a303-4d12-ab5f-01a41c317bfa'),
	(7,62,'en_us',NULL,'',NULL,NULL,NULL,'2015-12-28 01:08:39','2015-12-28 01:15:49','e58ea0fd-a087-47ab-8215-437381d1faba'),
	(8,63,'en_us',NULL,'<p>Maecenas accumsan metus non turpis mattis laoreet. Donec luctus tincidunt sem, vel feugiat sapien ornare ac. Mauris in purus dui. Nullam commodo ut elit quis blandit. Quisque tristique ullamcorper nunc, et elementum odio vestibulum vel. Proin sit amet sollicitudin ligula, sit amet mollis libero. Vestibulum vulputate nec nisl et aliquam. Suspendisse finibus nec nulla sit amet feugiat.</p>',NULL,NULL,NULL,'2015-12-28 01:27:07','2016-01-16 04:08:11','17588caa-0fa4-444f-8371-540339131034'),
	(10,65,'en_us',NULL,'<p>Maecenas accumsan metus non turpis mattis laoreet. Donec luctus tincidunt sem, vel feugiat sapien ornare ac. Mauris in purus dui. Nullam commodo ut elit quis blandit. Quisque tristique ullamcorper nunc, et elementum odio vestibulum vel. Proin sit amet sollicitudin ligula, sit amet mollis libero. Vestibulum vulputate nec nisl et aliquam. Suspendisse finibus nec nulla sit amet feugiat.</p>',NULL,NULL,NULL,'2015-12-28 01:29:55','2016-01-17 16:05:04','9cf27522-44b5-437f-be0f-5ae3e7f09997'),
	(11,66,'en_us',NULL,'<p>Maecenas accumsan metus non turpis mattis laoreet. Donec luctus tincidunt sem, vel feugiat sapien ornare ac. Mauris in purus dui. Nullam commodo ut elit quis blandit. Quisque tristique ullamcorper nunc, et elementum odio vestibulum vel. Proin sit amet sollicitudin ligula, sit amet mollis libero. Vestibulum vulputate nec nisl et aliquam. Suspendisse finibus nec nulla sit amet feugiat.</p>',NULL,NULL,NULL,'2015-12-28 01:30:06','2016-01-17 16:00:29','e0e1efdd-e56b-4395-b315-af7f76733def'),
	(14,81,'en_us',NULL,'<p>Stunning 2 story home on an unbelievable piece of ground, all located in an ultra convenient location within Johnston schools. As you stand in the 2 story living room, taking in the view, you feel like you are taking in the beauty of a park. Come explore and enjoy all of the landscaping in place, including your own bridge, that leads to your shop/art studio/clubhouse on the back of the lot!&nbsp;</p>\r\n\r\n<p>You can also make a massive fire to enjoy a fall evening in the outdoor fireplace. Inside, notice the quality! Built sturdy by Unique Homes, there are tons of details to enjoy. The main floor library is a stunning room and you will love the vaulted entry. The kitchen is fresh and updated with brand new granite counters and appliances. The top floor contains 3 beds and a laundry.&nbsp;</p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<p>The master is HUGE, as is the bath! The multi level basement is perfect for entertaining and has tons of character. Other features - 3 season room, security, central vac, multiple decks, heated garage and more. Come and see!</p>',NULL,NULL,NULL,'2016-01-13 05:03:20','2016-01-17 16:07:12','d7797734-22e4-40a8-9d76-9ebd94b242fe'),
	(15,82,'en_us',NULL,NULL,'<p>This property is the shit.</p>','Unknown',NULL,'2016-01-13 05:05:40','2016-01-17 16:07:12','f3ffbf65-1549-4855-8e58-92b5a1767df0'),
	(16,93,'en_us','Three Ways to List Your Home!',NULL,NULL,NULL,NULL,'2016-01-16 02:15:19','2016-01-16 02:20:22','7a56a5fa-dc4b-43bc-8e14-fea415cc0d39'),
	(17,94,'en_us',NULL,NULL,NULL,NULL,'You Choose. The Power Is Yours','2016-01-16 02:15:19','2016-01-16 02:20:22','88bf9f56-9a48-4c06-8f1c-384b7bef34da'),
	(18,112,'en_us',NULL,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur elit justo, tempus et eros ut, elementum ultrices ipsum. Donec et rhoncus elit, quis auctor felis. Ut cursus elementum elementum. Sed et ornare risus. Phasellus a mollis magna, ut tincidunt tellus. Nulla justo metus, varius sit amet velit in, volutpat rutrum turpis. Integer pellentesque ultrices tellus nec efficitur.</p>',NULL,NULL,NULL,'2016-01-17 22:16:03','2016-01-17 22:18:51','cd90c6d3-d5dd-4b74-97ef-2618f72aeeb4'),
	(19,113,'en_us','This truck is legit.',NULL,NULL,NULL,NULL,'2016-01-17 22:18:51','2016-01-17 22:18:51','8bfdbec0-08e6-4042-aecd-ebf93f10280e');

/*!40000 ALTER TABLE `matrixcontent_matrix` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table matrixcontent_option
# ------------------------------------------------------------

DROP TABLE IF EXISTS `matrixcontent_option`;

CREATE TABLE `matrixcontent_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `field_option_optionDescription` text COLLATE utf8_unicode_ci,
  `field_option_optionDetails` text COLLATE utf8_unicode_ci,
  `field_option_commission` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `field_description_description` text COLLATE utf8_unicode_ci,
  `field_details_optionDetails` text COLLATE utf8_unicode_ci,
  `field_commission_optionCommission` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `matrixcontent_option_elementId_locale_unq_idx` (`elementId`,`locale`),
  KEY `matrixcontent_option_locale_fk` (`locale`),
  CONSTRAINT `matrixcontent_option_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixcontent_option_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pluginId` int(11) DEFAULT NULL,
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `applyTime` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `migrations_version_unq_idx` (`version`),
  KEY `migrations_pluginId_fk` (`pluginId`),
  CONSTRAINT `migrations_pluginId_fk` FOREIGN KEY (`pluginId`) REFERENCES `plugins` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `pluginId`, `version`, `applyTime`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,NULL,'m000000_000000_base','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','07a47072-975e-4cc7-909b-444f650dc00f'),
	(2,NULL,'m140730_000001_add_filename_and_format_to_transformindex','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','3c4c2c0f-fbb3-42e5-8915-4bafa41e60a6'),
	(3,NULL,'m140815_000001_add_format_to_transforms','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','e505f9ed-8b6f-488c-a4f6-5aa52c13ab76'),
	(4,NULL,'m140822_000001_allow_more_than_128_items_per_field','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','78fd6ba9-d2dc-4f0d-8db9-732fbe9c3deb'),
	(5,NULL,'m140829_000001_single_title_formats','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','08ca8fd2-4030-46ef-aede-1dd7cc946392'),
	(6,NULL,'m140831_000001_extended_cache_keys','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','64446a12-32bc-4ea4-80ea-bd6749d7c731'),
	(7,NULL,'m140922_000001_delete_orphaned_matrix_blocks','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','39de3ff9-0e00-4cf2-853e-86ef5a303228'),
	(8,NULL,'m141008_000001_elements_index_tune','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','3aa86c52-5928-4d1b-bee0-781c94eb81c5'),
	(9,NULL,'m141009_000001_assets_source_handle','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','54b6c432-a1e2-4d99-891d-14a9fd18c1bf'),
	(10,NULL,'m141024_000001_field_layout_tabs','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','c4f514d5-c7d1-4796-85c4-dbc53c52468d'),
	(11,NULL,'m141030_000000_plugin_schema_versions','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','e24b10c1-8ce1-49aa-a48e-afd8420b27b4'),
	(12,NULL,'m141030_000001_drop_structure_move_permission','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','f6fd87ed-89ee-42e6-9df5-2cac7fef5873'),
	(13,NULL,'m141103_000001_tag_titles','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','56a82d00-bd49-4d72-9cd8-2e46d31875f7'),
	(14,NULL,'m141109_000001_user_status_shuffle','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','910dbe95-f9eb-43ad-9cc9-717bb671ee3a'),
	(15,NULL,'m141126_000001_user_week_start_day','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','2c3c6968-cffb-44fb-b78f-d2039d32192d'),
	(16,NULL,'m150210_000001_adjust_user_photo_size','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','77db2619-1795-4ad5-869f-8f15f84a5efe'),
	(17,NULL,'m150724_000001_adjust_quality_settings','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','62741671-57da-4370-9b25-8d655023e131'),
	(18,NULL,'m150827_000000_element_index_settings','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','5db59ed3-34cc-4e9a-9843-4fc39c6c0c22'),
	(19,NULL,'m150918_000001_add_colspan_to_widgets','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','20860b09-7e07-441a-a32d-038839f3bf4b'),
	(20,NULL,'m151007_000000_clear_asset_caches','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','cb137b61-3c6c-4bd2-9494-3a10f7ef266b'),
	(21,NULL,'m151109_000000_text_url_formats','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','c306b315-32db-42ea-a2e4-7d28f4a7d886'),
	(22,NULL,'m151110_000000_move_logo','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','b9223446-293e-4b11-a154-412e6741e112'),
	(23,NULL,'m151117_000000_adjust_image_widthheight','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','42bde0c8-f2db-485f-a809-54e8a17a6435'),
	(24,NULL,'m151127_000000_clear_license_key_status','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','b6e5fc6a-accc-48a8-9c32-7ccbbe68e3cf'),
	(25,NULL,'m151127_000000_plugin_license_keys','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','1ad0d20d-3003-4b53-ae04-e7cfaf140ae6'),
	(26,NULL,'m151130_000000_update_pt_widget_feeds','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','a213fc5e-84d0-4a53-a3ea-5604fe26fc9f'),
	(27,2,'m140330_000000_smartMap_addCountrySubfield','2015-12-19 01:40:29','2015-12-19 01:40:29','2015-12-19 01:40:29','43424278-9c41-4325-bd78-ebac3a231b83'),
	(28,2,'m140330_000001_smartMap_autofillCountryForExistingAddresses','2015-12-19 01:40:29','2015-12-19 01:40:29','2015-12-19 01:40:29','f1c37216-359c-48a2-a02b-a96a43173755'),
	(29,2,'m140811_000001_smartMap_changeHandleToFieldId','2015-12-19 01:40:29','2015-12-19 01:40:29','2015-12-19 01:40:29','bb2cab8a-aa63-4ac2-9ada-13def5415e41'),
	(30,2,'m150329_000000_smartMap_splitGoogleApiKeys','2015-12-19 01:40:29','2015-12-19 01:40:29','2015-12-19 01:40:29','33c04201-eff3-40de-9c48-4a26c2d5e56f'),
	(31,2,'m150331_000000_smartMap_reorganizeGeolocationOptions','2015-12-19 01:40:29','2015-12-19 01:40:29','2015-12-19 01:40:29','29f28914-80b5-4026-9880-2e6355ef0583');

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table plugins
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plugins`;

CREATE TABLE `plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `version` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `schemaVersion` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `licenseKey` char(24) COLLATE utf8_unicode_ci DEFAULT NULL,
  `licenseKeyStatus` enum('valid','invalid','mismatched','unknown') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unknown',
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `settings` text COLLATE utf8_unicode_ci,
  `installDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `plugins` WRITE;
/*!40000 ALTER TABLE `plugins` DISABLE KEYS */;

INSERT INTO `plugins` (`id`, `class`, `version`, `schemaVersion`, `licenseKey`, `licenseKeyStatus`, `enabled`, `settings`, `installDate`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(2,'SmartMap','2.3.1','2.3.0',NULL,'unknown',1,NULL,'2015-12-19 01:40:29','2015-12-19 01:40:29','2016-01-17 01:52:47','0b869af0-a5fb-41c6-890a-5deb656eb2d3'),
	(15,'ContactForm','1.0',NULL,NULL,'unknown',1,'{\"toEmail\":\"damonjentree@gmail.com\",\"adminSubject\":\"New message from Charter House Real Estate\",\"guestSubject\":\"Hello from Charter House Real Estate\",\"welcomeEmailMessage\":\"<p>Hi {{name}} &mdash; thank you for reaching out to us!<br>We wanted to drop you a quick note that we\'ve received your information and we\'ll be in touch very soon!<\\/p><p>Regards,<br>Charter House Real Estate<\\/p>\"}','2016-01-17 03:25:13','2016-01-17 03:25:13','2016-01-17 03:25:23','63c19df7-b2c8-457c-895b-6e8fe3daf28d'),
	(22,'ListingInquiry','1.0',NULL,NULL,'unknown',1,'{\"toEmail\":\"damonjentree@gmail.com, damon@mccormickcompany.com\",\"adminSubject\":\"New listing inquiry from Charter House Real Estate\",\"guestSubject\":\"Hello from Charter House Real Estate\",\"welcomeEmailMessage\":\"<p>Hi {{firstName}} &mdash; thank you for reaching out to us!<br>We wanted to drop you a quick note that we\'ve received your information and we\'ll be in touch very soon!<\\/p><p>Regards,<br>Charter House Real Estate<\\/p>\"}','2016-01-17 19:18:03','2016-01-17 19:18:03','2016-01-17 19:18:16','65c61f27-6393-4f54-ac14-33e2bbaff287'),
	(25,'Reservation','1.0',NULL,NULL,'unknown',1,'{\"toEmail\":\"damonjentree@gmail.com\",\"adminSubject\":\"New truck reservation from Charter House Real Estate\"}','2016-01-17 23:51:48','2016-01-17 23:51:48','2016-01-17 23:51:56','b2dd9852-1ea8-4553-9d5b-734bf99c199c');

/*!40000 ALTER TABLE `plugins` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table rackspaceaccess
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rackspaceaccess`;

CREATE TABLE `rackspaceaccess` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `connectionKey` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `storageUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cdnUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `rackspaceaccess_connectionKey_unq_idx` (`connectionKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table relations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `relations`;

CREATE TABLE `relations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `sourceId` int(11) NOT NULL,
  `sourceLocale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `targetId` int(11) NOT NULL,
  `sortOrder` smallint(6) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `relations_fieldId_sourceId_sourceLocale_targetId_unq_idx` (`fieldId`,`sourceId`,`sourceLocale`,`targetId`),
  KEY `relations_sourceId_fk` (`sourceId`),
  KEY `relations_sourceLocale_fk` (`sourceLocale`),
  KEY `relations_targetId_fk` (`targetId`),
  CONSTRAINT `relations_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `relations_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `relations_sourceLocale_fk` FOREIGN KEY (`sourceLocale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `relations_targetId_fk` FOREIGN KEY (`targetId`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `relations` WRITE;
/*!40000 ALTER TABLE `relations` DISABLE KEYS */;

INSERT INTO `relations` (`id`, `fieldId`, `sourceId`, `sourceLocale`, `targetId`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(21,71,84,NULL,85,1,'2016-01-14 04:48:11','2016-01-14 04:48:11','7dc7297b-3bf8-4ff5-acc8-498ffc51d6bd'),
	(22,71,87,NULL,86,1,'2016-01-14 04:48:11','2016-01-14 04:48:11','27e29c1b-43bb-427d-a802-7f6a930d901a'),
	(23,71,89,NULL,88,1,'2016-01-14 04:48:11','2016-01-14 04:48:11','a1ce82b2-82fe-46ee-b981-b88502eb6b07'),
	(24,66,4,NULL,90,1,'2016-01-14 05:01:04','2016-01-14 05:01:04','7ead82b9-7e3e-4d4a-a32e-ee46e3614893'),
	(26,66,2,NULL,83,1,'2016-01-16 00:54:26','2016-01-16 00:54:26','4111a862-9358-429b-9881-f47ae1efaf92'),
	(27,74,2,NULL,92,1,'2016-01-16 00:54:26','2016-01-16 00:54:26','d00bb316-0ca4-486b-ac30-f148fb199468'),
	(28,75,4,NULL,90,1,'2016-01-16 01:59:54','2016-01-16 01:59:54','695f7041-5b41-46ef-a2a8-c7ccfcc60060'),
	(31,75,22,NULL,90,1,'2016-01-16 02:20:22','2016-01-16 02:20:22','f3987a80-f96a-40e0-b319-3bdb750fc7ff'),
	(32,75,6,NULL,90,1,'2016-01-16 03:49:25','2016-01-16 03:49:25','3465e18b-9c1b-4c05-bc9d-33875d9ca121'),
	(34,75,7,NULL,90,1,'2016-01-16 03:55:08','2016-01-16 03:55:08','c8714161-d375-4a3a-9f9d-3654a17d9fc3'),
	(35,75,38,NULL,90,1,'2016-01-16 03:57:00','2016-01-16 03:57:00','209fce9b-85d2-478e-af7c-3366e38577a3'),
	(36,75,5,NULL,90,1,'2016-01-16 04:08:11','2016-01-16 04:08:11','26363f50-f4b1-45e8-8c50-49a2f465a1b6'),
	(38,48,61,NULL,99,1,'2016-01-16 04:19:26','2016-01-16 04:19:26','982e7886-bef1-4eae-ae6b-a5df3adbf2a5'),
	(39,57,18,NULL,103,1,'2016-01-17 16:00:29','2016-01-17 16:00:29','91493c0a-0f4c-43e2-87ba-dc84602e5a89'),
	(40,57,18,NULL,104,2,'2016-01-17 16:00:29','2016-01-17 16:00:29','8867899d-b3a5-46a4-918b-278b48ade3d6'),
	(41,57,18,NULL,105,3,'2016-01-17 16:00:29','2016-01-17 16:00:29','3aa20390-5df7-469d-a5cf-b1ff70c16e3b'),
	(46,57,17,NULL,108,1,'2016-01-17 16:05:04','2016-01-17 16:05:04','80fae750-2774-48fb-8bad-f8a252453fc7'),
	(47,57,17,NULL,107,2,'2016-01-17 16:05:04','2016-01-17 16:05:04','e4d80d6a-a523-40f3-8bae-07d5fc30dcb9'),
	(48,57,17,NULL,106,3,'2016-01-17 16:05:04','2016-01-17 16:05:04','b247392e-3496-4711-ae1d-defed71f5ace'),
	(49,54,17,NULL,67,1,'2016-01-17 16:05:04','2016-01-17 16:05:04','4ce73cb0-2c87-4a5c-b422-5e98449a3aea'),
	(50,57,80,NULL,109,1,'2016-01-17 16:07:12','2016-01-17 16:07:12','7f932a69-1849-4654-b059-59b8cec9de94'),
	(51,57,80,NULL,110,2,'2016-01-17 16:07:12','2016-01-17 16:07:12','9b718b40-801b-461c-b520-ff1d06096734'),
	(52,57,80,NULL,111,3,'2016-01-17 16:07:12','2016-01-17 16:07:12','a7cdef68-412c-4398-93d8-0e946e144acc'),
	(54,75,9,NULL,90,1,'2016-01-17 22:18:51','2016-01-17 22:18:51','8c5638cd-18d8-4d34-851b-3d4c8b636ce3');

/*!40000 ALTER TABLE `relations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table routes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `routes`;

CREATE TABLE `routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `urlParts` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `urlPattern` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` tinyint(4) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `routes_urlPattern_unq_idx` (`urlPattern`),
  KEY `routes_locale_idx` (`locale`),
  CONSTRAINT `routes_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table searchindex
# ------------------------------------------------------------

DROP TABLE IF EXISTS `searchindex`;

CREATE TABLE `searchindex` (
  `elementId` int(11) NOT NULL,
  `attribute` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `fieldId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`elementId`,`attribute`,`fieldId`,`locale`),
  FULLTEXT KEY `searchindex_keywords_idx` (`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `searchindex` WRITE;
/*!40000 ALTER TABLE `searchindex` DISABLE KEYS */;

INSERT INTO `searchindex` (`elementId`, `attribute`, `fieldId`, `locale`, `keywords`)
VALUES
	(1,'username',0,'en_us',' markcharter '),
	(1,'firstname',0,'en_us',' mark '),
	(1,'lastname',0,'en_us',' charter '),
	(1,'fullname',0,'en_us',' mark charter '),
	(1,'email',0,'en_us',' damonjentree gmail com '),
	(1,'slug',0,'en_us',''),
	(2,'slug',0,'en_us',' homepage '),
	(2,'title',0,'en_us',' mark charter realty '),
	(2,'field',1,'en_us',' s '),
	(2,'field',49,'en_us',''),
	(86,'extension',0,'en_us',' jpg '),
	(86,'kind',0,'en_us',' image '),
	(86,'slug',0,'en_us',''),
	(86,'title',0,'en_us',' access '),
	(3,'field',1,'en_us',' craft is the cms that s powering local charterhouseiowa dev it s beautiful powerful flexible and easy to use and it s made by pixel tonic we can t wait to dive in and see what it s capable of this is even more captivating content which you couldn t see on the news index page because it was entered after a page break and the news index template only likes to show the content on the first page craft a nice alternative to word if you re making a website '),
	(3,'field',2,'en_us',''),
	(3,'slug',0,'en_us',''),
	(3,'title',0,'en_us',' we just installed craft '),
	(4,'slug',0,'en_us',' properties '),
	(4,'title',0,'en_us',' listings '),
	(5,'slug',0,'en_us',' partners '),
	(5,'title',0,'en_us',' partners '),
	(6,'slug',0,'en_us',' team '),
	(6,'title',0,'en_us',' team '),
	(7,'slug',0,'en_us',' reviews '),
	(7,'title',0,'en_us',' reviews '),
	(8,'slug',0,'en_us',' reservation success '),
	(8,'title',0,'en_us',' reservation success '),
	(9,'slug',0,'en_us',' truck '),
	(9,'title',0,'en_us',' truck '),
	(91,'title',0,'en_us',' another awesome house '),
	(91,'slug',0,'en_us',' another awesome house '),
	(4,'field',1,'en_us',''),
	(2,'field',3,'en_us',''),
	(91,'field',54,'en_us',''),
	(91,'field',55,'en_us',' available '),
	(91,'field',10,'en_us',' 0 '),
	(91,'field',13,'en_us',' 0 '),
	(91,'field',3,'en_us',''),
	(4,'field',3,'en_us',''),
	(6,'field',1,'en_us',' it s true this site doesn t have a whole lot of content yet but don t worry our web developers have just installed the cms and they re setting things up for the content editors this very moment soon local charterhouseiowa dev will be an oasis of fresh perspectives sharp analyses and astute opinions that will keep you coming back again and again '),
	(6,'field',3,'en_us',''),
	(7,'field',1,'en_us',' it s true this site doesn t have a whole lot of content yet but don t worry our web developers have just installed the cms and they re setting things up for the content editors this very moment soon local charterhouseiowa dev will be an oasis of fresh perspectives sharp analyses and astute opinions that will keep you coming back again and again '),
	(7,'field',3,'en_us',''),
	(5,'field',1,'en_us',''),
	(5,'field',49,'en_us',' maecenas accumsan metus non turpis mattis laoreet donec luctus tincidunt sem vel feugiat sapien ornare ac mauris in purus dui nullam commodo ut elit quis blandit quisque tristique ullamcorper nunc et elementum odio vestibulum vel proin sit amet sollicitudin ligula sit amet mollis libero vestibulum vulputate nec nisl et aliquam suspendisse finibus nec nulla sit amet feugiat '),
	(5,'field',3,'en_us',''),
	(38,'field',75,'en_us',' home1 '),
	(80,'field',49,'en_us',' stunning 2 story home on an unbelievable piece of ground all located in an ultra convenient location within johnston schools as you stand in the 2 story living room taking in the view you feel like you are taking in the beauty of a park come explore and enjoy all of the landscaping in place including your own bridge that leads to your shop art studio clubhouse on the back of the lot you can also make a massive fire to enjoy a fall evening in the outdoor fireplace inside notice the quality built sturdy by unique homes there are tons of details to enjoy the main floor library is a stunning room and you will love the vaulted entry the kitchen is fresh and updated with brand new granite counters and appliances the top floor contains 3 beds and a laundry the master is huge as is the bath the multi level basement is perfect for entertaining and has tons of character other features 3 season room security central vac multiple decks heated garage and more come and see this property is the shit unknown '),
	(9,'field',49,'en_us',' lorem ipsum dolor sit amet consectetur adipiscing elit curabitur elit justo tempus et eros ut elementum ultrices ipsum donec et rhoncus elit quis auctor felis ut cursus elementum elementum sed et ornare risus phasellus a mollis magna ut tincidunt tellus nulla justo metus varius sit amet velit in volutpat rutrum turpis integer pellentesque ultrices tellus nec efficitur this truck is legit '),
	(19,'field',63,'en_us',' mark charterhouseiowa com '),
	(19,'field',64,'en_us',' 56865000 '),
	(79,'field',1,'en_us',' api conformance is highly measurable and suppliers who claim it must be included in any derivative version prepared by licensee in the absence of modifications or portions thereof in both source code notice required by applicable law under this license this customary commercial license in ways explicitly requested by the copyright holder explicitly and prominently states near the copyright holder saying it may not copy modify and distribute verbatim copies of this license this license has been made available you are responsible for ensuring that the copyright holder provides the work including but not limited to broadcasting communication and various recording media if any provision of this license published by licensor no one other than as may be filtered to exclude very small or irrelevant contributions this applies to any actual or alleged intellectual property rights needed if any legal entity exercising permissions granted on that web page by copying installing or otherwise make it clear that any modifications thereto inability to comply due to statute or regulation if it is impossible for you to the `latex format a program or other tangible form a copy of the license or at your option c you may not impose any terms that apply to the extent that any such warranty support indemnity or liability obligation is offered by you to the terms of this license agreement in other words go ahead and share nethack but don t try to stop anyone else of these rights for example a page is available under the new version '),
	(79,'slug',0,'en_us',''),
	(9,'field',75,'en_us',' home1 '),
	(38,'field',49,'en_us',' meet mark and his family mark started his real estate career in 2005 and has been doing it full time since then over time mark realized that the majority of real estate agents are the same they say the same things they market the same way and they treat clients the same mark did not like this so he set about creating his own unique way of practicing real estate in 2014 mark debuted a three tier pricing plan for listing clients giving them the choice on how they want to list their home this is another example of how mark never accepts the status quo in real estate and is always looking to make things better for his clients one of mark charter s greatest characteristics is his giving nature adam carroll mark s overall goal on every deal is simple he wants to provide such great service that after the deal is done that client will happily provide a positive review of the transaction this goal is what makes mark one of the most recommended agents in central iowa when you add it all up when you take into account what mark offers clients in terms of services marketing and care mark truly feels he is the best option available in the market today and he hopes you will experience this for yourself very soon '),
	(9,'field',1,'en_us',' lorem ipsum dolor sit amet consectetur adipiscing elit curabitur mattis sollicitudin felis consectetur eleifend mi ornare sed vivamus cursus tincidunt ultrices nam id tincidunt purus morbi lectus eros molestie non dolor ut tincidunt fermentum odio vivamus consectetur molestie lorem ac varius '),
	(8,'field',1,'en_us',' we got ya covered and you got my truck '),
	(8,'field',3,'en_us',''),
	(17,'field',1,'en_us',' stunning ankeny home located on otter creek golf course this 4 bed 4 5 bath former show home is loaded with features and amenities from top to bottom try building this home today for less than a million dollars it all starts with location you will love cul de sac living close to i35 making a commute a breeze step through the front door and you will be wowed by the view there is not one but two ponds directly behind the home the outside is just as gorgeous as the inside full landscaping flagstone entry exterior lighting huge patio and a wraparound deck with outdoor kitchen truly put this home among ankeny s best inside the quality is top notch beautiful knotty alder trim acacia hardwood viking appliances and a beautiful stone fireplace are impossible to miss the master is an oasis with beautiful materials need an amazing basement sunken family room barn wood floors huge wet bar hidden room and golf cart garage await too much to list email for all features '),
	(17,'field',11,'en_us',' 504907 '),
	(17,'field',6,'en_us',' 1210 ne 42nd st ankeny ia 50021 united states 41 76575850 93 58654680 6403 57288807 '),
	(17,'field',12,'en_us',' singlefamily '),
	(17,'field',14,'en_us',' 2 426 '),
	(17,'field',9,'en_us',' 1 '),
	(17,'field',8,'en_us',' 4 5 '),
	(17,'field',10,'en_us',' 0 '),
	(17,'field',13,'en_us',' 0 '),
	(17,'field',3,'en_us',' golf course house '),
	(17,'slug',0,'en_us',' 504907 '),
	(91,'field',11,'en_us',' 12345 '),
	(17,'title',0,'en_us',' amazing golf course home with too many amenities to list '),
	(17,'field',16,'en_us',' http www realtor com realestateandhomes search mlslid=504907 '),
	(17,'field',17,'en_us',' 3 car garage '),
	(17,'field',18,'en_us',' 849 900 '),
	(17,'field',19,'en_us',' yzryxkgqd2c '),
	(18,'field',1,'en_us',' call your builder and call off the meeting this beats new construction and is the house you have been waiting on to hit the market this stunning grimes acreage sits on 2 3 acres and is just a mile north of dcg high school awesome location pull into the long driveway and enjoy the massive front yard with trees stunning curb appeal this 5 bedroom could be 6 home has a floor plan you have not seen before the attention to detail and woodwork is stunning built ins and window seats with storage are found everywhere the master bedroom is on the main level and offers a gorgeous bath with carrera marble and a vey large closet the main floor laundry is also huge you will find a second full laundry room in the finished basement along with a second kitchen one of the beds is separated from the rest of the house and is perfect for an office or teen room too many amenities to list them all 5 garage stallsgeothermalhigh end kitchencovered deckso much more a rare find '),
	(18,'field',11,'en_us',' 506142 '),
	(18,'field',16,'en_us',' http www realtor com realestateandhomes search mlslid=506142 '),
	(18,'field',18,'en_us',' 719 000 '),
	(18,'field',14,'en_us',' 2 700 '),
	(18,'field',6,'en_us',' 7965 nw 142nd st grimes ia 50111 united states 41 70576500 93 81369100 6415 45239364 '),
	(18,'field',9,'en_us',' 5 '),
	(18,'field',8,'en_us',' 4 '),
	(18,'field',12,'en_us',' singlefamily '),
	(18,'field',17,'en_us',' 3 car attached garage 2 car detached garage '),
	(18,'field',19,'en_us',''),
	(18,'field',10,'en_us',' 0 '),
	(18,'field',13,'en_us',' 0 '),
	(18,'field',3,'en_us',''),
	(18,'slug',0,'en_us',' 506142 '),
	(91,'field',49,'en_us',''),
	(91,'field',57,'en_us',''),
	(18,'title',0,'en_us',' absolutely amazing modern rural meets urban set up just outside of grimes a must see at $719 000 '),
	(18,'field',20,'en_us',' 1hw5ay9jy7y '),
	(18,'field',21,'en_us',' 1990 '),
	(19,'slug',0,'en_us',''),
	(19,'field',6,'en_us',' 3602 ne otter view circle ankeny ia 50021 united states 41 76183000 93 57202570 6402 83642357 '),
	(19,'field',22,'en_us',' remax real estate concepts '),
	(19,'field',5,'en_us',' mark charterhouseiowa com '),
	(20,'slug',0,'en_us',''),
	(19,'field',23,'en_us',' 515 864 6444 '),
	(21,'slug',0,'en_us',' contact '),
	(21,'title',0,'en_us',' contact mark '),
	(21,'field',1,'en_us',''),
	(21,'field',3,'en_us',''),
	(22,'slug',0,'en_us',' ways to sell '),
	(22,'title',0,'en_us',' sellers '),
	(103,'filename',0,'en_us',' featured 1 jpg '),
	(100,'field',35,'en_us',' 5 '),
	(100,'field',36,'en_us',' 3 month listing 1 3 month listing 1 full mls exposure 1 full mls exposure 1 professional photos 1 professional photos 1 social media marketing 1 social media marketing 1 sign and lockbox 1 sign and lockbox 1 facebook targeted marketing facebook targeted marketing 3d matterport virtual tour 3d matterport virtual tour access to 15ft truck access to 15ft truck free pest inspection free pest inspection $1 000 offer guarantee $1 000 offer guarantee free storage free storage full move package full move package '),
	(102,'field',35,'en_us',' 6 '),
	(102,'field',36,'en_us',' 3 month listing 1 3 month listing 1 full mls exposure 1 full mls exposure 1 professional photos 1 professional photos 1 social media marketing 1 social media marketing 1 sign and lockbox 1 sign and lockbox 1 facebook targeted marketing 1 facebook targeted marketing 1 3d matterport virtual tour 1 3d matterport virtual tour 1 access to 15ft truck 1 access to 15ft truck 1 free pest inspection 1 free pest inspection 1 $1 000 offer guarantee 1 $1 000 offer guarantee 1 free storage free storage full move package full move package '),
	(101,'field',35,'en_us',' 7 '),
	(101,'field',36,'en_us',' 3 month listing 1 3 month listing 1 full mls exposure 1 full mls exposure 1 professional photos 1 professional photos 1 social media marketing 1 social media marketing 1 sign and lockbox 1 sign and lockbox 1 facebook targeted marketing 1 facebook targeted marketing 1 3d matterport virtual tour 1 3d matterport virtual tour 1 access to 15ft truck 1 access to 15ft truck 1 free pest inspection 1 free pest inspection 1 $1 000 offer guarantee 1 $1 000 offer guarantee 1 free storage 1 free storage 1 full move package 1 full move package 1 '),
	(36,'slug',0,'en_us',' buyers sellers '),
	(36,'title',0,'en_us',' buyers sellers '),
	(36,'field',1,'en_us',''),
	(69,'field',15,'en_us',' bethany wilcke '),
	(69,'slug',0,'en_us',' i was so lucky to find them '),
	(69,'title',0,'en_us',' i was so lucky to find them '),
	(70,'field',1,'en_us',' mark was phenomenal to work with during the sale of our home his no nonsense tell it how it is attitude is the only reason we got our home sold we had a unique situation with our home based on the size garage and price mark was the only person that told us what we needed to hear instead of just fluffing us with what we wanted to hear mark is a great communicator and we always knew what was expected of us through the entire process thank you mark '),
	(70,'field',15,'en_us',' jeremy orr '),
	(70,'slug',0,'en_us',' mark was phenomenal to work with '),
	(70,'title',0,'en_us',' mark was phenomenal to work with '),
	(80,'field',57,'en_us',' featured 1 featured 2 featured 3 '),
	(36,'field',3,'en_us',''),
	(85,'title',0,'en_us',' 24 '),
	(86,'filename',0,'en_us',' access jpg '),
	(84,'slug',0,'en_us',''),
	(85,'filename',0,'en_us',' 24 jpg '),
	(85,'extension',0,'en_us',' jpg '),
	(85,'kind',0,'en_us',' image '),
	(85,'slug',0,'en_us',''),
	(83,'filename',0,'en_us',' buyer hero jpg '),
	(61,'field',49,'en_us',''),
	(61,'field',3,'en_us',''),
	(61,'field',48,'en_us',' tyler osby '),
	(61,'field',46,'en_us',' http www wealthwithmortgage com '),
	(61,'field',47,'en_us',' 515 257 6729 '),
	(61,'slug',0,'en_us',' mortgage the tyler osby team '),
	(61,'title',0,'en_us',' mortgage the tyler osby team '),
	(62,'field',51,'en_us',''),
	(61,'field',1,'en_us',' tyler osby is where mark turns when his clients need a mortgage or a refinance tyler is knowledgeable accountable and trustworthy he gets the job done every time '),
	(62,'slug',0,'en_us',''),
	(100,'title',0,'en_us',' pro go plan '),
	(104,'filename',0,'en_us',' featured 2 jpg '),
	(100,'slug',0,'en_us',' pro go plan '),
	(102,'slug',0,'en_us',' pro plus plan '),
	(101,'slug',0,'en_us',' pro elite plan '),
	(22,'field',1,'en_us',''),
	(22,'field',3,'en_us',''),
	(22,'field',75,'en_us',' home1 '),
	(22,'field',38,'en_us',''),
	(37,'slug',0,'en_us',''),
	(37,'field',39,'en_us',' mark charter is a licensed agent with re max real estate concepts in the state of iowa b if you have received this information and are already listed with an agent please disregard this as it is not meant to intrude on and existing seller agent relationship full moves are available to clients that are staying within central iowa '),
	(38,'slug',0,'en_us',' about '),
	(38,'title',0,'en_us',' about mark charter '),
	(38,'field',1,'en_us',' mark started his real estate career in 2005 and has been doing it full time since then over time mark realized that the majority of real estate agents are the same they say the same things they market the same way and they treat clients the same mark did not like this so he set about creating his own unique way of practicing real estate in 2014 mark debuted a three tier pricing plan for listing clients giving them the choice on how they want to list their home this is another example of how mark never accepts the status quo in real estate and is always looking to make things better for his clients mark s overall goal on every deal is simple he wants to provide such great service that after the deal is done that client will happily provide a positive review of the transaction this goal is what makes mark one of the most recommended agents in central iowa when you add it all up when you take into account what mark offers clients in terms of services marketing and care mark truly feels he is the best option available in the market today and he hopes you will experience this for yourself very soon '),
	(38,'field',3,'en_us',''),
	(39,'field',40,'en_us',''),
	(39,'field',41,'en_us',' owner realtor '),
	(39,'field',43,'en_us',' mark charterhouseiowa com '),
	(39,'field',42,'en_us',' 515 864 6444 '),
	(39,'slug',0,'en_us',' mark charter '),
	(39,'title',0,'en_us',' mark charter '),
	(39,'field',44,'en_us',' mark charter is iowa s most recommended agent on sites like trulia com and angieslist com and is one of the top selling agents in central iowa mark believes in constantly looking for ways to put his clients first and accomplishes this through things like short listing contracts flexible commission plans and great perks that benefit his clients mark is married to katie and has a son named seton and a daughter named hollis '),
	(40,'field',41,'en_us',' buyer specialist '),
	(40,'field',43,'en_us',' amy charterhouseiowa com '),
	(40,'field',42,'en_us',' 515 822 0171 '),
	(40,'field',44,'en_us',' amy meyer is a fantastic addition to the mark charter team talented dedicated and hard working amy s role is to help buyers find the perfect home they are looking for '),
	(40,'slug',0,'en_us',' amy meyer '),
	(40,'title',0,'en_us',' amy meyer '),
	(41,'field',41,'en_us',' business manager client coordinator '),
	(41,'field',43,'en_us',' nic charterhouseiowa com '),
	(41,'field',42,'en_us',' 515 371 1973 '),
	(41,'field',44,'en_us',' nic s role is making sure the client is happy he helps buyers and seller with all the details of a move things like lining up use of the mark charter real estate moving truck scheduling cleaners or anything else that you might need he is a vital part of the team and is here to help you '),
	(41,'slug',0,'en_us',' nic meyer '),
	(41,'title',0,'en_us',' nic meyer '),
	(42,'field',41,'en_us',' photographer '),
	(42,'field',43,'en_us',''),
	(42,'field',42,'en_us',''),
	(42,'field',44,'en_us',' jake boyd is central iowa s premier real estate photographer and owns jake boyd photo jordan and jason are also valuable members of the team as well mark trusts jake boyd photo to shoot all of his listing photos '),
	(42,'slug',0,'en_us',' jake boyd photo '),
	(42,'title',0,'en_us',' jake boyd photo '),
	(69,'field',1,'en_us',' before finding mark and amy i had had a lot of bad experiences with realtors from the minute i contacted them about possibly being my realtor i was in constant communication with them i was never left wondering what was going on i was paired with amy and she is fantastic she listened to not only what i was looking for but what i was going to use spaces for and looked for those things in the places i wanted to view she helped me to pro and con the spaces i was looking on based on what she had heard me say about other spaces she was in constant contact with me about setting up showings and if there was a delay in getting an answer from someone was letting me know what the hold up was i have never met a more caring team to work with and when they say you re part of the family now you are as a first time home buyer i had lots of questions both for amy and for the seller and i got every single one answered i never once felt like there was another goal that the team was working for other than to make sure i was happy with what i was getting while the people are top notch it s also amazing to me that you can use the moving truck for free and they had my new home cleaned before i moved in it saved me so much time and hassle not to mention cost i was so lucky to find them '),
	(68,'title',0,'en_us',' one of mark charter s greatest characteristics is his giving nature '),
	(68,'field',15,'en_us',' adam carroll '),
	(68,'slug',0,'en_us',' one of mark charters greatest characteristics is his giving nature '),
	(17,'field',57,'en_us',' featured 3 featured 2 featured 1 '),
	(17,'field',54,'en_us',' house '),
	(17,'field',55,'en_us',' available '),
	(68,'field',1,'en_us',' one of mark charter s greatest characteristics is his giving nature he always goes above and beyond for his clients to make sure that their experience in buying or selling a home is first rate while not just an experienced real estate agent mark is also extremely knowledgeable about the investment real estate market as well he is a career agent that absolutely without a doubt has your best interests in mind '),
	(67,'title',0,'en_us',' house '),
	(67,'filename',0,'en_us',' house jpg '),
	(67,'extension',0,'en_us',' jpg '),
	(67,'kind',0,'en_us',' image '),
	(67,'slug',0,'en_us',''),
	(90,'filename',0,'en_us',' home1 jpg '),
	(90,'extension',0,'en_us',' jpg '),
	(90,'kind',0,'en_us',' image '),
	(90,'slug',0,'en_us',''),
	(90,'title',0,'en_us',' home1 '),
	(4,'field',66,'en_us',' home1 '),
	(91,'field',21,'en_us',''),
	(91,'field',14,'en_us',''),
	(91,'field',9,'en_us',''),
	(91,'field',8,'en_us',' select '),
	(91,'field',12,'en_us',''),
	(91,'field',17,'en_us',''),
	(91,'field',19,'en_us',''),
	(91,'field',20,'en_us',''),
	(66,'slug',0,'en_us',''),
	(66,'field',51,'en_us',' maecenas accumsan metus non turpis mattis laoreet donec luctus tincidunt sem vel feugiat sapien ornare ac mauris in purus dui nullam commodo ut elit quis blandit quisque tristique ullamcorper nunc et elementum odio vestibulum vel proin sit amet sollicitudin ligula sit amet mollis libero vestibulum vulputate nec nisl et aliquam suspendisse finibus nec nulla sit amet feugiat '),
	(65,'slug',0,'en_us',''),
	(18,'field',49,'en_us',' maecenas accumsan metus non turpis mattis laoreet donec luctus tincidunt sem vel feugiat sapien ornare ac mauris in purus dui nullam commodo ut elit quis blandit quisque tristique ullamcorper nunc et elementum odio vestibulum vel proin sit amet sollicitudin ligula sit amet mollis libero vestibulum vulputate nec nisl et aliquam suspendisse finibus nec nulla sit amet feugiat '),
	(17,'field',21,'en_us',''),
	(17,'field',20,'en_us',''),
	(65,'field',51,'en_us',' maecenas accumsan metus non turpis mattis laoreet donec luctus tincidunt sem vel feugiat sapien ornare ac mauris in purus dui nullam commodo ut elit quis blandit quisque tristique ullamcorper nunc et elementum odio vestibulum vel proin sit amet sollicitudin ligula sit amet mollis libero vestibulum vulputate nec nisl et aliquam suspendisse finibus nec nulla sit amet feugiat '),
	(63,'slug',0,'en_us',''),
	(17,'field',49,'en_us',' maecenas accumsan metus non turpis mattis laoreet donec luctus tincidunt sem vel feugiat sapien ornare ac mauris in purus dui nullam commodo ut elit quis blandit quisque tristique ullamcorper nunc et elementum odio vestibulum vel proin sit amet sollicitudin ligula sit amet mollis libero vestibulum vulputate nec nisl et aliquam suspendisse finibus nec nulla sit amet feugiat '),
	(83,'extension',0,'en_us',' jpg '),
	(63,'field',51,'en_us',' maecenas accumsan metus non turpis mattis laoreet donec luctus tincidunt sem vel feugiat sapien ornare ac mauris in purus dui nullam commodo ut elit quis blandit quisque tristique ullamcorper nunc et elementum odio vestibulum vel proin sit amet sollicitudin ligula sit amet mollis libero vestibulum vulputate nec nisl et aliquam suspendisse finibus nec nulla sit amet feugiat '),
	(83,'kind',0,'en_us',' image '),
	(83,'slug',0,'en_us',' hero '),
	(83,'title',0,'en_us',' buyer hero '),
	(2,'field',66,'en_us',' buyer hero '),
	(91,'field',18,'en_us',' 100 000 '),
	(18,'field',57,'en_us',' featured 1 featured 2 featured 3 '),
	(91,'field',6,'en_us',' 91 6 3014 sw prairieview rd ankeny ia 50023 united states 41 69900400 93 64460000 6406 75758889 '),
	(18,'field',54,'en_us',''),
	(18,'field',55,'en_us',' available '),
	(2,'field',67,'en_us',' lorem ipsum dolor sit amet consectetur adipisicing elit fugit sapiente repellendus delectus a veniam provident maxime distinctio ipsum sint nulla incidunt vel dolorum ea dolore 24 7 availability 24 repellat ea sunt saepe libero repellendus itaque autem omnis dolore reiciendis voluptatibus qui neque doloremque nisi ullam illum nihil iure magnam ex sint nostrum mollitia access to interior designer access ab dignissimos hic sequi porro asperiores quis reprehenderit sit ratione rem esse non suscipit laborum ipsa cumque corporis in optio libero enim quaerat voluptates nesciunt lender recommendations lender '),
	(84,'field',71,'en_us',' 24 '),
	(84,'field',72,'en_us',' 24 7 availability '),
	(84,'field',73,'en_us',' lorem ipsum dolor sit amet consectetur adipisicing elit fugit sapiente repellendus delectus a veniam provident maxime distinctio ipsum sint nulla incidunt vel dolorum ea dolore '),
	(9,'field',3,'en_us',''),
	(55,'field',51,'en_us',' mark started his real estate career in 2005 and has been doing it full time since then over time mark realized that the majority of real estate agents are the same they say the same things they market the same way and they treat clients the same mark did not like this so he set about creating his own unique way of practicing real estate in 2014 mark debuted a three tier pricing plan for listing clients giving them the choice on how they want to list their home this is another example of how mark never accepts the status quo in real estate and is always looking to make things better for his clients '),
	(58,'field',51,'en_us',' mark s overall goal on every deal is simple he wants to provide such great service that after the deal is done that client will happily provide a positive review of the transaction this goal is what makes mark one of the most recommended agents in central iowa when you add it all up when you take into account what mark offers clients in terms of services marketing and care mark truly feels he is the best option available in the market today and he hopes you will experience this for yourself very soon '),
	(55,'slug',0,'en_us',''),
	(56,'field',50,'en_us',' meet mark and his family '),
	(56,'slug',0,'en_us',''),
	(57,'field',52,'en_us',' one of mark charter s greatest characteristics is his giving nature '),
	(57,'field',53,'en_us',' adam carroll '),
	(57,'slug',0,'en_us',''),
	(58,'slug',0,'en_us',''),
	(36,'field',49,'en_us',' buying sellingso you are are thinking about buying or selling that is great here are a few things you need to know buyers buying a home is a big purchase one of the biggest you will ever make why in the world would anyone want to make that purchase without the help of a professional real estate agent beats me but many try the most important thing to remember is that when you are buying a home you do not pay your agent the seller of the home pays the entire commission and that commission is split between the agent selling the home and the agent representing the buyer there is no reason to not have your own agent representing you and working hard on your behalf i have talked to many buyers who have said something like when we bought we just used the agent who had the house listed if you are thinking about doing this use extreme caution this should only be done if you have complete trust in the agent if you just met them this is a terrible idea this situation is called dual agency and it happens when one agent represents both parties that agent is trying to get the most money for the seller and also get you the best price how can they do this it is tricky and again should only be done if you have total faith in that agent the way to avoid this is to hire me remember i can show you all homes not just the ones that i list so when you are my buyer what do you get here is a summary of what my buyers get when they work with me an agent with 24 7 availabilityan agent with a a bbb ratingan agent with more recommendations than any other agent in central iowa trulia com free moving truck based on availability first come first served access to my interior designer if you need help or suggestion on how to furnish your new homelender recommendations i can get you to the right lender who will get you closed on time and with no surprisesan experienced agent i have been selling real estate full time since 2005auto prospecting so you never miss a new listing to the marketweekly check in updates to make sure you are happyand much more when you work with me you are working with a one of a kind agent who simply offers my clients more than other agents please read through the client reviews on this site to get a good feel of how i treat my clients you will never regret hiring me as your agent sellers when the time comes to put your home on the market hiring a real estate professional is a crucial step in the right direction i realize that there are many home selling options but the tried and true method of hiring an agent is still the best option by far it is proven to be the safest way to sell as well as being the way to generate the highest selling price but just listing with an agent is not enough you have to list with an agent who knows how to market and embraces today s trends like social media to reach the masses you want your home exposed everywhere to reach every possible buyer but you also want to work with an agent who will be enjoyable to work with and one who has great services and benefits to offer you that agent is me i would love to talk to you about listing your home but for now here is a brief summary of what i offer to sellers an agent with 24 7 availabilityan agent with an a bbb ratingan agent with more recommendations than any other agent in central iowa trulia com a professional photographer will take the photos of your homea professional videographer will do a video of your home certain price ranges only professional staging assistance my interior designer will make sure your home is ready to gofree moving truck based on availability first come first served professional moving services at 7% plan while staying in central iowa weekly check in updates to make sure you are happysocial media marketingand much more no other agent offers their clients more you will love your selling experience i promise i realize one size does not fit all when it comes to selling your home i want to talk to you about your specific situation and see if i can help '),
	(59,'field',51,'en_us',' buying sellingso you are are thinking about buying or selling that is great here are a few things you need to know buyers buying a home is a big purchase one of the biggest you will ever make why in the world would anyone want to make that purchase without the help of a professional real estate agent beats me but many try the most important thing to remember is that when you are buying a home you do not pay your agent the seller of the home pays the entire commission and that commission is split between the agent selling the home and the agent representing the buyer there is no reason to not have your own agent representing you and working hard on your behalf i have talked to many buyers who have said something like when we bought we just used the agent who had the house listed if you are thinking about doing this use extreme caution this should only be done if you have complete trust in the agent if you just met them this is a terrible idea this situation is called dual agency and it happens when one agent represents both parties that agent is trying to get the most money for the seller and also get you the best price how can they do this it is tricky and again should only be done if you have total faith in that agent the way to avoid this is to hire me remember i can show you all homes not just the ones that i list so when you are my buyer what do you get here is a summary of what my buyers get when they work with me an agent with 24 7 availabilityan agent with a a bbb ratingan agent with more recommendations than any other agent in central iowa trulia com free moving truck based on availability first come first served access to my interior designer if you need help or suggestion on how to furnish your new homelender recommendations i can get you to the right lender who will get you closed on time and with no surprisesan experienced agent i have been selling real estate full time since 2005auto prospecting so you never miss a new listing to the marketweekly check in updates to make sure you are happyand much more when you work with me you are working with a one of a kind agent who simply offers my clients more than other agents please read through the client reviews on this site to get a good feel of how i treat my clients you will never regret hiring me as your agent sellers when the time comes to put your home on the market hiring a real estate professional is a crucial step in the right direction i realize that there are many home selling options but the tried and true method of hiring an agent is still the best option by far it is proven to be the safest way to sell as well as being the way to generate the highest selling price but just listing with an agent is not enough you have to list with an agent who knows how to market and embraces today s trends like social media to reach the masses you want your home exposed everywhere to reach every possible buyer but you also want to work with an agent who will be enjoyable to work with and one who has great services and benefits to offer you that agent is me i would love to talk to you about listing your home but for now here is a brief summary of what i offer to sellers an agent with 24 7 availabilityan agent with an a bbb ratingan agent with more recommendations than any other agent in central iowa trulia com a professional photographer will take the photos of your homea professional videographer will do a video of your home certain price ranges only professional staging assistance my interior designer will make sure your home is ready to gofree moving truck based on availability first come first served professional moving services at 7% plan while staying in central iowa weekly check in updates to make sure you are happysocial media marketingand much more no other agent offers their clients more you will love your selling experience i promise i realize one size does not fit all when it comes to selling your home i want to talk to you about your specific situation and see if i can help '),
	(59,'slug',0,'en_us',''),
	(80,'field',6,'en_us',' 11559 nw timberbrooke ln grimes ia 50111 '),
	(80,'field',11,'en_us',' 507193 '),
	(80,'field',18,'en_us',' 699 900 '),
	(80,'field',21,'en_us',' 1993 '),
	(80,'field',14,'en_us',' 3 209 '),
	(80,'field',9,'en_us',' 5 '),
	(80,'field',8,'en_us',' 3 5 '),
	(80,'field',12,'en_us',' singlefamily '),
	(80,'field',17,'en_us',' 3 car garage '),
	(80,'field',19,'en_us',''),
	(80,'field',20,'en_us',' uwmwwvahre3 '),
	(80,'field',54,'en_us',''),
	(80,'field',55,'en_us',' available '),
	(80,'field',10,'en_us',' 0 '),
	(80,'field',13,'en_us',' 0 '),
	(80,'field',3,'en_us',''),
	(80,'slug',0,'en_us',' 507193 '),
	(80,'title',0,'en_us',' amazing grimes home with plenty of room to roam '),
	(81,'field',51,'en_us',' stunning 2 story home on an unbelievable piece of ground all located in an ultra convenient location within johnston schools as you stand in the 2 story living room taking in the view you feel like you are taking in the beauty of a park come explore and enjoy all of the landscaping in place including your own bridge that leads to your shop art studio clubhouse on the back of the lot you can also make a massive fire to enjoy a fall evening in the outdoor fireplace inside notice the quality built sturdy by unique homes there are tons of details to enjoy the main floor library is a stunning room and you will love the vaulted entry the kitchen is fresh and updated with brand new granite counters and appliances the top floor contains 3 beds and a laundry the master is huge as is the bath the multi level basement is perfect for entertaining and has tons of character other features 3 season room security central vac multiple decks heated garage and more come and see '),
	(81,'slug',0,'en_us',''),
	(82,'field',52,'en_us',' this property is the shit '),
	(82,'field',53,'en_us',' unknown '),
	(82,'slug',0,'en_us',''),
	(87,'field',71,'en_us',' access '),
	(87,'field',72,'en_us',' access to interior designer '),
	(87,'field',73,'en_us',' repellat ea sunt saepe libero repellendus itaque autem omnis dolore reiciendis voluptatibus qui neque doloremque nisi ullam illum nihil iure magnam ex sint nostrum mollitia '),
	(87,'slug',0,'en_us',''),
	(88,'filename',0,'en_us',' lender jpg '),
	(88,'extension',0,'en_us',' jpg '),
	(88,'kind',0,'en_us',' image '),
	(88,'slug',0,'en_us',''),
	(88,'title',0,'en_us',' lender '),
	(89,'field',71,'en_us',' lender '),
	(89,'field',72,'en_us',' lender recommendations '),
	(89,'field',73,'en_us',' ab dignissimos hic sequi porro asperiores quis reprehenderit sit ratione rem esse non suscipit laborum ipsa cumque corporis in optio libero enim quaerat voluptates nesciunt '),
	(89,'slug',0,'en_us',''),
	(92,'filename',0,'en_us',' seller hero jpg '),
	(92,'extension',0,'en_us',' jpg '),
	(92,'kind',0,'en_us',' image '),
	(92,'slug',0,'en_us',''),
	(92,'title',0,'en_us',' seller hero '),
	(2,'field',74,'en_us',' seller hero '),
	(102,'title',0,'en_us',' pro plus plan '),
	(101,'title',0,'en_us',' pro elite plan '),
	(4,'field',75,'en_us',' home1 '),
	(4,'field',49,'en_us',''),
	(22,'field',49,'en_us',' three ways to list your home you choose the power is yours '),
	(93,'field',50,'en_us',' three ways to list your home '),
	(93,'slug',0,'en_us',''),
	(94,'field',76,'en_us',' you choose the power is yours '),
	(94,'slug',0,'en_us',''),
	(103,'extension',0,'en_us',' jpg '),
	(103,'kind',0,'en_us',' image '),
	(103,'slug',0,'en_us',''),
	(103,'title',0,'en_us',' featured 1 '),
	(100,'field',28,'en_us',' the perfect plan to keep it simple this seller does not require much and likes to do things themselves just list with a pro and go '),
	(101,'field',28,'en_us',' for the seller who wants the very best maximum marketing $$$ s maximum convince all in one the elite is unrivaled in the marketplace '),
	(102,'field',28,'en_us',' this popular standard plan packs a punch great marketing and perks all at a common commission rate in the market great service plus more '),
	(6,'field',75,'en_us',' home1 '),
	(6,'field',49,'en_us',''),
	(7,'field',75,'en_us',' home1 '),
	(7,'field',49,'en_us',''),
	(5,'field',75,'en_us',' home1 '),
	(98,'filename',0,'en_us',' tyler osby jpg '),
	(98,'extension',0,'en_us',' jpg '),
	(98,'kind',0,'en_us',' image '),
	(98,'slug',0,'en_us',''),
	(98,'title',0,'en_us',' tyler osby '),
	(99,'filename',0,'en_us',' tyler osby jpg '),
	(99,'extension',0,'en_us',' jpg '),
	(99,'kind',0,'en_us',' image '),
	(99,'slug',0,'en_us',''),
	(99,'title',0,'en_us',' tyler osby '),
	(109,'filename',0,'en_us',' featured 1 jpg '),
	(17,'field',78,'en_us',' 0 '),
	(106,'filename',0,'en_us',' featured 1 jpg '),
	(18,'field',78,'en_us',' 0 '),
	(80,'field',78,'en_us',' 0 '),
	(91,'field',78,'en_us',' 0 '),
	(104,'extension',0,'en_us',' jpg '),
	(104,'kind',0,'en_us',' image '),
	(104,'slug',0,'en_us',''),
	(104,'title',0,'en_us',' featured 2 '),
	(105,'filename',0,'en_us',' featured 3 jpg '),
	(105,'extension',0,'en_us',' jpg '),
	(105,'kind',0,'en_us',' image '),
	(105,'slug',0,'en_us',''),
	(105,'title',0,'en_us',' featured 3 '),
	(106,'extension',0,'en_us',' jpg '),
	(106,'kind',0,'en_us',' image '),
	(106,'slug',0,'en_us',''),
	(106,'title',0,'en_us',' featured 1 '),
	(107,'filename',0,'en_us',' featured 2 jpg '),
	(107,'extension',0,'en_us',' jpg '),
	(107,'kind',0,'en_us',' image '),
	(107,'slug',0,'en_us',''),
	(107,'title',0,'en_us',' featured 2 '),
	(108,'filename',0,'en_us',' featured 3 jpg '),
	(108,'extension',0,'en_us',' jpg '),
	(108,'kind',0,'en_us',' image '),
	(108,'slug',0,'en_us',''),
	(108,'title',0,'en_us',' featured 3 '),
	(109,'extension',0,'en_us',' jpg '),
	(109,'kind',0,'en_us',' image '),
	(109,'slug',0,'en_us',''),
	(109,'title',0,'en_us',' featured 1 '),
	(110,'filename',0,'en_us',' featured 2 jpg '),
	(110,'extension',0,'en_us',' jpg '),
	(110,'kind',0,'en_us',' image '),
	(110,'slug',0,'en_us',''),
	(110,'title',0,'en_us',' featured 2 '),
	(111,'filename',0,'en_us',' featured 3 jpg '),
	(111,'extension',0,'en_us',' jpg '),
	(111,'kind',0,'en_us',' image '),
	(111,'slug',0,'en_us',''),
	(111,'title',0,'en_us',' featured 3 '),
	(112,'field',51,'en_us',' lorem ipsum dolor sit amet consectetur adipiscing elit curabitur elit justo tempus et eros ut elementum ultrices ipsum donec et rhoncus elit quis auctor felis ut cursus elementum elementum sed et ornare risus phasellus a mollis magna ut tincidunt tellus nulla justo metus varius sit amet velit in volutpat rutrum turpis integer pellentesque ultrices tellus nec efficitur '),
	(112,'slug',0,'en_us',''),
	(113,'field',50,'en_us',' this truck is legit '),
	(113,'slug',0,'en_us',''),
	(132,'field',4,'en_us',''),
	(132,'slug',0,'en_us',''),
	(132,'title',0,'en_us',' damon gentry '),
	(132,'field',60,'en_us',''),
	(132,'field',61,'en_us',' damon '),
	(132,'field',62,'en_us',' gentry '),
	(132,'field',5,'en_us',' damonjentree gmail com '),
	(132,'field',7,'en_us',' 5153714708 '),
	(132,'field',6,'en_us',''),
	(132,'field',1,'en_us',''),
	(132,'field',58,'en_us',' 0 '),
	(132,'field',65,'en_us',''),
	(132,'field',59,'en_us','');

/*!40000 ALTER TABLE `searchindex` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sections
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sections`;

CREATE TABLE `sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('single','channel','structure') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'channel',
  `hasUrls` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `template` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enableVersioning` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sections_name_unq_idx` (`name`),
  UNIQUE KEY `sections_handle_unq_idx` (`handle`),
  KEY `sections_structureId_fk` (`structureId`),
  CONSTRAINT `sections_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `sections` WRITE;
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;

INSERT INTO `sections` (`id`, `structureId`, `name`, `handle`, `type`, `hasUrls`, `template`, `enableVersioning`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,NULL,'Homepage','homepage','single',1,'index',1,'2015-12-19 00:45:35','2015-12-19 00:45:35','7f150a74-5770-4c0f-8731-e7d31b45e9cc'),
	(2,NULL,'News','news','channel',1,'news/_entry',1,'2015-12-19 00:45:35','2015-12-19 00:45:35','6edd160b-e0ce-42d1-94eb-371aa2f4cd37'),
	(3,1,'Truck Reservations','truckReservations','structure',1,'truck/_confirmation',1,'2015-12-19 00:51:17','2016-01-12 02:01:50','5bf0a186-c6cf-4b6f-8501-0ffdaa705d39'),
	(4,2,'Listing','listing','structure',1,'listings/_entry',1,'2015-12-19 00:51:40','2016-01-17 15:58:13','48b6a8e5-aaa5-4187-ace7-3fb0ca5d0c0d'),
	(5,NULL,'Listings','listings','single',1,'listings',1,'2015-12-19 00:52:08','2016-01-14 04:02:29','3f73d159-1185-481c-af11-c0043929efe4'),
	(6,NULL,'Partners','partners','single',1,'partners',1,'2015-12-19 00:53:06','2015-12-19 00:53:06','352fab42-9fbe-4676-9b77-5c89c463a039'),
	(7,NULL,'Team','team','single',1,'team',1,'2015-12-19 00:54:13','2015-12-19 00:54:13','bb99b444-6c6d-4b25-9a0f-7ee60a948adb'),
	(8,NULL,'Reviews','reviews','single',1,'reviews',1,'2015-12-19 00:54:52','2016-01-14 05:17:45','b40a3f83-5a09-412d-97a0-c96f6881f46e'),
	(9,NULL,'Reservation Success','reservationSuccess','single',1,'truck/_success',1,'2015-12-19 00:55:30','2015-12-19 00:55:30','fb32ec3b-f589-48a2-a279-ee874fa999d8'),
	(10,NULL,'Truck','truck','single',1,'truck',1,'2015-12-19 01:05:51','2015-12-19 01:05:51','5fc2f0d1-a580-405e-a76a-dda23d23d828'),
	(12,NULL,'Contact','contact','single',1,'contact',1,'2015-12-20 04:23:50','2015-12-20 04:23:50','cabab400-43b3-4425-8c57-116fd274be3a'),
	(13,NULL,'Sellers','sellers','single',1,'sellers',1,'2015-12-20 04:39:21','2016-01-16 02:10:30','bc0c2ff5-310a-4734-bb96-53de679be8ed'),
	(14,NULL,'Buyers & Sellers','buyersSellers','single',1,'buyers-sellers',1,'2015-12-20 05:00:50','2015-12-20 05:00:50','dda9ff39-74b4-4509-a0d2-70f1556a6b6e'),
	(15,NULL,'About','about','single',1,'about',1,'2015-12-20 20:03:09','2015-12-20 20:03:09','e37c152e-1162-4482-b2c6-c7492ccc32ef'),
	(16,8,'Partner','partner','structure',0,NULL,1,'2015-12-28 01:06:31','2015-12-28 01:06:31','86813916-1a64-49e5-87fa-a6ee9eda54f5');

/*!40000 ALTER TABLE `sections` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sections_i18n
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sections_i18n`;

CREATE TABLE `sections_i18n` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sectionId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `enabledByDefault` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `urlFormat` text COLLATE utf8_unicode_ci,
  `nestedUrlFormat` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sections_i18n_sectionId_locale_unq_idx` (`sectionId`,`locale`),
  KEY `sections_i18n_locale_fk` (`locale`),
  CONSTRAINT `sections_i18n_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sections_i18n_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `sections_i18n` WRITE;
/*!40000 ALTER TABLE `sections_i18n` DISABLE KEYS */;

INSERT INTO `sections_i18n` (`id`, `sectionId`, `locale`, `enabledByDefault`, `urlFormat`, `nestedUrlFormat`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'en_us',1,'__home__',NULL,'2015-12-19 00:45:35','2015-12-19 00:45:35','5167aa15-b94f-4dca-8b06-702da5b0b454'),
	(2,2,'en_us',1,'news/{postDate.year}/{slug}',NULL,'2015-12-19 00:45:35','2015-12-19 00:45:35','037441d2-8558-4051-945e-9500b09cea02'),
	(3,3,'en_us',0,'reservation/confirmation/{slug}',NULL,'2015-12-19 00:51:17','2016-01-12 02:00:56','ac2cf4a2-48c5-4fdb-8716-68d314ead2ea'),
	(4,4,'en_us',0,'listings/{mlsNumber}','{parent.uri}/{slug}','2015-12-19 00:51:40','2016-01-17 15:58:13','de995385-b829-4f42-87c5-b472b7f7466c'),
	(5,5,'en_us',0,'listings',NULL,'2015-12-19 00:52:08','2016-01-14 04:02:29','24fc049d-1d8c-4b55-8d2a-14fbb4208871'),
	(6,6,'en_us',0,'partners',NULL,'2015-12-19 00:53:06','2015-12-19 00:53:06','a3615636-4c64-4ca2-9ac0-0c6c687714c9'),
	(7,7,'en_us',0,'team',NULL,'2015-12-19 00:54:13','2015-12-19 00:54:13','a724981b-f928-4a06-b355-01e90002da7e'),
	(8,8,'en_us',0,'reviews',NULL,'2015-12-19 00:54:52','2016-01-14 05:17:45','786fbb36-57b3-4bbf-9bb7-59fc58c2d417'),
	(9,9,'en_us',0,'truck/reserved',NULL,'2015-12-19 00:55:30','2015-12-19 00:55:30','460cf05e-dd3b-4d63-b70e-0b2a14744903'),
	(10,10,'en_us',0,'truck',NULL,'2015-12-19 01:05:51','2015-12-19 01:05:51','58fc884a-13fb-453a-84de-614f923fbece'),
	(12,12,'en_us',0,'contact',NULL,'2015-12-20 04:23:50','2015-12-20 04:23:50','f43359ce-e556-45ef-93b2-deabc70a7035'),
	(13,13,'en_us',0,'sellers',NULL,'2015-12-20 04:39:21','2016-01-16 02:09:25','e063f83f-964b-4089-95e5-71df720741ba'),
	(14,14,'en_us',0,'buyers-and-sellers',NULL,'2015-12-20 05:00:50','2015-12-20 05:00:50','dcc3326e-1c5e-4480-87c7-eceacc88e21c'),
	(15,15,'en_us',0,'about',NULL,'2015-12-20 20:03:09','2015-12-20 20:03:09','2ec729bc-214a-4f5e-b250-0262935c917e'),
	(16,16,'en_us',0,NULL,NULL,'2015-12-28 01:06:31','2015-12-28 01:06:31','07c30822-e2fe-4acc-9a47-ab0e1842dd0f');

/*!40000 ALTER TABLE `sections_i18n` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sessions`;

CREATE TABLE `sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `token` char(100) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `sessions_uid_idx` (`uid`),
  KEY `sessions_token_idx` (`token`),
  KEY `sessions_dateUpdated_idx` (`dateUpdated`),
  KEY `sessions_userId_fk` (`userId`),
  CONSTRAINT `sessions_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;

INSERT INTO `sessions` (`id`, `userId`, `token`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'240dd64d8d8d2eb71109bb89c0a0ec36858a385bczozMjoiRW8wQTg3NjVGOVk3aFVqSVNPVFBtVlY3ZjFHWk9yM3oiOw==','2015-12-19 00:45:35','2015-12-19 00:45:35','5e9fd262-0735-4ca6-8269-fbd8e87478c2'),
	(2,1,'e6aeae021186724a1e8b9e0018504314fbecdc45czozMjoifmdMSzdQRjFxRjZhTEtBZXJBUHBDNFF3emszWUVmcHgiOw==','2015-12-19 14:57:18','2015-12-19 14:57:18','2ce114c4-780a-433d-862c-bcadf36e0832'),
	(3,1,'9ede0e28d74cdb95d6e9b532746a6f00403ee066czozMjoiTzYxQmVHWDhpN0xDNll6bTd3R05ZdTR4VndBOGNkWGgiOw==','2015-12-19 20:57:36','2015-12-19 20:57:36','15e20660-843e-4ee5-b789-976d69ece407'),
	(4,1,'4b52ac09e75b19584fd7075e862f1e802e918083czozMjoiQ3RuWUZxVTZ0UUNpMn5JVWRtdGl0MVJTR2w5R2Z1MWIiOw==','2015-12-20 04:07:32','2015-12-20 04:07:32','47138b1f-5a3e-4dfd-8eee-30dfd2807927'),
	(5,1,'fc1508ad9ce2a2d039d3e3f6f6af16db0d41e4cbczozMjoiTWlVcU53dGMzaXJHcDlzbWd6TFp1RHJMc1liOThFU0kiOw==','2015-12-20 14:32:28','2015-12-20 14:32:28','fedb406d-b643-40d3-90f2-40454cace395'),
	(6,1,'14b66e71923b33c3cfdc25dfd818dd76366f5bccczozMjoia2tvV3FqSlJkU3dVY1c2OVVidGlTX3lTNE1jdUxDOVYiOw==','2015-12-20 20:02:53','2015-12-20 20:02:53','f787a71b-6ea5-4aa9-b31f-9f2603b38450'),
	(7,1,'f264355fa37e027d81d569d8dcab95264ed00e8fczozMjoicW1XNjhmMkpJQnlxelM4Z0d1UDdST1p3ZlF4dVEwWDMiOw==','2015-12-21 02:18:24','2015-12-21 02:18:24','cf5ad6e2-d335-40e3-a7a8-f1e5074091d8'),
	(8,1,'0ae8ce425bdfd57e56b2d7757fcfda110cd8323dczozMjoiTWl3OXVHOVlyN01hYUVYSnZGRWk0b0p5dGFzakVZcGsiOw==','2015-12-21 20:40:22','2015-12-21 20:40:22','e7bf794f-e2de-4d10-b094-4d5a90a60a11'),
	(9,1,'1e21aba3063660611561070e95892eb8440bc04eczozMjoiNlV5U2d2aXB6U2Z4X35iWThFc0NNU0dEMnF3RFh1bkEiOw==','2015-12-22 22:55:48','2015-12-22 22:55:48','e6c4ed06-8583-4e48-9499-6702d2674fec'),
	(10,1,'348cbaa057055b4bce2ca914b7ed5e61029b2ae6czozMjoiWHBnVzdwaWlpVzNPQkxiRmN0a29VSkkzQjYzeFM3bzIiOw==','2015-12-28 00:47:45','2015-12-28 00:47:45','5acf5b88-7d2b-41b2-a6df-9cb1f84d1bcc'),
	(11,1,'84ab26107308f5b5d89b485d7e58c87afaada30bczozMjoiNGNpSWlxbTlvbUtwN0R1dWNVdXo3Nmo2X0R0SDcwWFgiOw==','2015-12-31 22:05:57','2015-12-31 22:05:57','9fedf335-b419-41ef-9f91-29ff6d80cd02'),
	(12,1,'0e14ea930645f0cf16981b7075071c0f3860864bczozMjoiNUpPYlhCQnFUY1doeF9sSGw2bTNXM1RlN3pUZ1VQZ1IiOw==','2016-01-06 16:51:49','2016-01-06 16:51:49','fed78e14-57f9-48a0-8b05-5b83143b02a1'),
	(13,1,'5085cd3c4b77190fdbb9e3f51944aef6b2085982czozMjoidjV4WFlVUlVPQWtvUjR+eDA4dFQwY0FVZWpzQVVWemciOw==','2016-01-06 23:44:26','2016-01-06 23:44:26','07c31eed-0da2-40d7-8b14-b4036f6583a9'),
	(14,1,'3e3c0f2aa649923ba49a9ff390f8a6ce369cb036czozMjoiWXFKSG9GQTVza0l0R1YzYTZPVkYzVXVhYzVweGRTRlQiOw==','2016-01-07 03:20:15','2016-01-07 03:20:15','6b2e6433-f744-4ed3-bffb-dc0130f27c9f'),
	(15,1,'bf4bfc77ac69ca391aaaee134bda579284f666c4czozMjoiV2pFVUI4OXZRWXRsTVlrYk5nb1Z+czU2UEdmUXBMQWYiOw==','2016-01-07 16:15:41','2016-01-07 16:15:41','8575839f-a788-4bda-aa27-20e5326fbefc'),
	(16,1,'2fcd8cb8ea382659662905d61e515a76579eb048czozMjoibXFTWlJmQXk0YkQ4dEZvQUlPZXd+ZjB1SjVmTnNlWGUiOw==','2016-01-07 23:37:39','2016-01-07 23:37:39','258cf763-3338-4ed6-8188-6aae7ba65839'),
	(17,1,'34e8ee19c16f3de7f3c11ec5c1c4fc4a23c581e7czozMjoiSXhOR3JlSldlSDQwbjhNNHVaSlRGUEpjaGVmRGhDfkUiOw==','2016-01-08 01:45:09','2016-01-08 01:45:09','7047b0c0-5672-4a8e-8362-4d9d9daeb42d'),
	(18,1,'6c9ef15295d43b18ee0da7f40b6d5ab753eba2f7czozMjoiY35LU2pIdTlPa2ltUmtham5vRkNlT1p5V0hhV1dHbVkiOw==','2016-01-08 03:25:29','2016-01-08 03:25:29','b876f36f-0a5f-478e-9981-c1cb9747f512'),
	(19,1,'41158ddf70e562a499669eecc747e1ece66bd273czozMjoibFFCMVJXQlRONHJrY1ZrM1ZpRVlPbHlDM0lPaHFZWEkiOw==','2016-01-08 12:44:42','2016-01-08 12:44:42','4602a1be-6fd8-4508-8a74-874e4d26702b'),
	(20,1,'be87853523ccb2159357b60b1eb5d7e09829ca45czozMjoidjZDS3FpdV81eGlTcDJjRjZnVEY1VE1jY3dVdkdPMkMiOw==','2016-01-08 21:29:26','2016-01-08 21:29:26','4a94dd0e-86d7-4935-9d91-7862de4523af'),
	(21,1,'53450297378e5ea4b478303243541eb08a332489czozMjoicUhaU2xMaWdyVHVfWlJic1l0U1BIU0l3dWhsVEJYWXoiOw==','2016-01-11 01:22:04','2016-01-11 01:22:04','d97a8f24-f2e2-49f5-9def-9eaaaf66c251'),
	(22,1,'a4b954d175fcc96796c77bcbb5f52cda23d6349fczozMjoiVH54V01USWlIQlpqVlhZd3hHX2xnVkxCOUpTUHE2M1YiOw==','2016-01-11 02:52:25','2016-01-11 02:52:25','53c40fa8-9f39-40d6-ac6a-bd3b67e8e01d'),
	(23,1,'76a72da78f9326aacf3e5ba630459447c7bb5a1cczozMjoiTGRSdURveTg1Y243dVhxamlyc3B0SEdqdnZZdX5FWHMiOw==','2016-01-11 23:52:52','2016-01-11 23:52:52','67781428-d515-4515-ba8f-bcf92bb4aeae'),
	(24,1,'283f5e34697665ed9d6bb69da9fbd5bb454c583bczozMjoiM3AzRWZNdjA5VEw2dHRsYVRyc3BwU0FMTFJ0cXZwUVEiOw==','2016-01-12 01:40:08','2016-01-12 01:53:48','ed2c8278-d14e-4534-bd37-e50f8cdc09fd'),
	(25,1,'f1e10cb52d835480103765230e2109a6246e0a7dczozMjoiTE1lb2hKZEl4b2dKanl0S2ZINUxLTTNuMk96dEZRdmkiOw==','2016-01-12 04:15:59','2016-01-12 04:15:59','be3e040b-7215-45b6-a14d-5a3c2d09b2bb'),
	(26,1,'e35a810067e532ac3f0b5270684aeab4ea2781ebczozMjoicFdodVJyTEh5MTJiN2o0R1RuNFhaM3AyZlBwMlFfNk4iOw==','2016-01-13 03:54:14','2016-01-13 03:54:14','dddd941c-9cf8-4c63-a166-2741a8ca664f'),
	(27,1,'427bbf24b0c24991178a491862c6d2d98d4c6213czozMjoidE5fRXVVT1dTdDU2bjgyZEZ4cX5TOGpoUWNYNX5QZkIiOw==','2016-01-14 02:29:15','2016-01-14 02:29:15','9755b848-a84c-4fa8-8f0e-e8e13ab2afe7'),
	(28,1,'f1b075525aa959986c688c4f0820b472cf1eb374czozMjoiNUp1VXRtdTlncU1rTm5Id3ozMk1ZNEx5ZTVNaXJnMVoiOw==','2016-01-14 03:37:38','2016-01-14 03:37:38','171e5496-f4c4-4290-83e4-6fdb6fdf62e5'),
	(29,1,'38387dcca64f2bf7008d6b7a200f8211dd470ac5czozMjoia2llWWtfM2ZYN3lxNG1hfmcxRnlLNnBQYktQbEhHQzMiOw==','2016-01-14 14:29:47','2016-01-14 14:29:47','cb45aa1b-87e4-417b-9641-8c3b753238ba'),
	(30,1,'b9a67fa95516b0bcbe903db53fdc98ddc01b593bczozMjoiUWFxOWExdWhvSnRiVE5oMVBrT1Q1UDhmM1NzdUN0aEYiOw==','2016-01-15 01:39:26','2016-01-15 01:39:26','317a405d-e583-45a8-ad00-cb202ff76165'),
	(31,1,'ff2392974885b94357b006d6182928c413919c89czozMjoidlphWnljOU9DMWxSUklLT0J6ZE9iQkRwdmlkN1c4Y0wiOw==','2016-01-16 00:47:18','2016-01-16 00:47:18','052450f5-d258-4215-ac7a-a1bbf34f44e7'),
	(32,1,'cc64cac5d6cc456a7fbe567326f4a86f5f8e8ea2czozMjoiNERWOVRMSDhUflBTfnd0M05wbVlnanFKNVh5YXZiZVIiOw==','2016-01-17 01:47:09','2016-01-17 01:47:09','0aec7bae-a67c-4444-a724-1dd2f40b7c51'),
	(33,1,'71b05fcf5a307fd4ce590af36a248735223e481eczozMjoick9IamVQRFM4dFg1YXVvX2x4TzdYellONWU3Q3EyQzkiOw==','2016-01-17 14:05:25','2016-01-17 14:05:25','53ac88b2-4ac9-4641-b7aa-583b955be0aa'),
	(34,1,'45588edb98d2396c5ad892429a43e1a2b09dc925czozMjoidn5zVmg1SkR4UW5FREtrTHJERmdobGlTaXBoQW5CfjUiOw==','2016-01-17 22:15:09','2016-01-17 22:15:09','1f40cc77-ff22-4579-97ed-7c952d62c664');

/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table shunnedmessages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shunnedmessages`;

CREATE TABLE `shunnedmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `shunnedmessages_userId_message_unq_idx` (`userId`,`message`),
  CONSTRAINT `shunnedmessages_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table smartmap_addresses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `smartmap_addresses`;

CREATE TABLE `smartmap_addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `street1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` decimal(12,8) DEFAULT NULL,
  `lng` decimal(12,8) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `smartmap_addresses_elementId_fk` (`elementId`),
  KEY `smartmap_addresses_fieldId_fk` (`fieldId`),
  CONSTRAINT `smartmap_addresses_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `smartmap_addresses_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `smartmap_addresses` WRITE;
/*!40000 ALTER TABLE `smartmap_addresses` DISABLE KEYS */;

INSERT INTO `smartmap_addresses` (`id`, `elementId`, `fieldId`, `street1`, `street2`, `city`, `state`, `zip`, `country`, `lat`, `lng`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(2,17,6,'1210 NE 42nd St',NULL,'Ankeny','IA','50021','United States',41.76575850,-93.58654680,'2015-12-19 02:52:17','2016-01-17 16:05:04','6f1eeb0a-4d6c-4d3d-99ea-c5b83ac122cc'),
	(3,18,6,'7965 NW 142nd St',NULL,'Grimes','IA','50111','United States',41.70576500,-93.81369100,'2015-12-19 15:30:35','2016-01-17 16:00:29','cd688567-ad6d-43e4-99a2-d52a77dceb4d'),
	(4,19,6,'3602 NE Otter View Circle',NULL,'Ankeny','IA','50021','United States',41.76183000,-93.57202570,'2015-12-19 21:07:35','2016-01-08 21:50:23','c960dcfa-c56c-4401-b1a9-44200347a755'),
	(8,80,6,'11559 NW Timberbrooke Ln',NULL,'Grimes','IA','50111',NULL,NULL,NULL,'2016-01-13 05:03:20','2016-01-17 16:07:12','7b1bc458-c9ce-4484-ab61-124408bd4a55'),
	(9,91,6,'3014 SW Prairieview Rd',NULL,'Ankeny','IA','50023','United States',41.69900400,-93.64460000,'2016-01-14 05:11:31','2016-01-16 02:03:09','73875f8a-ba9e-4d15-ad7d-1f7d6a9a6b55');

/*!40000 ALTER TABLE `smartmap_addresses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table structureelements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `structureelements`;

CREATE TABLE `structureelements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `elementId` int(11) DEFAULT NULL,
  `root` int(11) unsigned DEFAULT NULL,
  `lft` int(11) unsigned NOT NULL,
  `rgt` int(11) unsigned NOT NULL,
  `level` smallint(6) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `structureelements_structureId_elementId_unq_idx` (`structureId`,`elementId`),
  KEY `structureelements_root_idx` (`root`),
  KEY `structureelements_lft_idx` (`lft`),
  KEY `structureelements_rgt_idx` (`rgt`),
  KEY `structureelements_level_idx` (`level`),
  KEY `structureelements_elementId_fk` (`elementId`),
  CONSTRAINT `structureelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `structureelements_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `structureelements` WRITE;
/*!40000 ALTER TABLE `structureelements` DISABLE KEYS */;

INSERT INTO `structureelements` (`id`, `structureId`, `elementId`, `root`, `lft`, `rgt`, `level`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,NULL,1,1,4,0,'2015-12-19 01:33:52','2015-12-19 01:33:52','5b432d0f-162c-4f13-8728-1896c92e3714'),
	(4,3,NULL,4,1,2,0,'2015-12-19 01:53:53','2015-12-19 01:53:53','de40f022-8974-442d-abe4-9bc24b66a676'),
	(11,2,NULL,11,1,10,0,'2015-12-19 02:52:17','2015-12-19 02:52:17','4924e465-d94e-4560-ac61-753117cd0e85'),
	(12,2,17,11,2,3,1,'2015-12-19 02:52:17','2015-12-19 02:52:17','6d772b2f-b7a5-4ff6-bb3c-0df367546fc2'),
	(13,2,18,11,4,5,1,'2015-12-19 15:30:35','2015-12-19 15:30:35','a3bb2aae-1e30-40db-b6ef-0d71ac23f6fb'),
	(14,5,NULL,14,1,8,0,'2015-12-20 04:45:14','2015-12-20 04:45:14','f0bdd15b-5a53-435a-963f-f9ddb0580f88'),
	(18,6,NULL,18,1,10,0,'2015-12-20 20:27:05','2015-12-20 20:27:05','9012d51b-1b54-4fc6-9644-b43d9e25d858'),
	(19,6,39,18,2,3,1,'2015-12-20 20:27:05','2015-12-20 20:27:05','917e7543-5674-4783-b396-dac0721d6647'),
	(20,6,40,18,4,5,1,'2015-12-20 20:30:17','2015-12-20 20:30:17','3d78a952-1727-44a8-a9d5-4a9dc83bcd63'),
	(21,6,41,18,6,7,1,'2015-12-20 20:31:04','2015-12-20 20:31:04','846a7fd7-e447-44c9-b968-db08e4ae4e4a'),
	(22,6,42,18,8,9,1,'2015-12-20 20:31:22','2015-12-20 20:31:22','79df1090-1c00-4e2a-b5c9-3b36b5fc552e'),
	(23,7,NULL,23,1,2,0,'2015-12-20 20:45:03','2015-12-20 20:45:03','c1f7188e-ecaf-4b9b-89b7-e47911d46b9f'),
	(36,8,NULL,36,1,4,0,'2015-12-28 01:08:39','2015-12-28 01:08:39','2f1f6687-8062-4214-92bf-bb15cb63bfc4'),
	(37,8,61,36,2,3,1,'2015-12-28 01:08:39','2015-12-28 01:08:39','607e84c4-572a-43fc-8a08-e75a942a6deb'),
	(38,9,NULL,38,1,8,0,'2016-01-07 00:14:03','2016-01-07 00:14:03','dd1ab40f-747c-4e3d-8b67-9f0c6378107a'),
	(39,9,68,38,2,3,1,'2016-01-07 00:14:03','2016-01-07 00:14:03','1bf36290-9e91-4ab4-88a7-f521855577aa'),
	(40,9,69,38,4,5,1,'2016-01-07 00:24:07','2016-01-07 00:24:07','89d07808-bdab-4ffa-bd81-13b4be48d128'),
	(41,9,70,38,6,7,1,'2016-01-07 00:24:35','2016-01-07 00:24:35','6139c811-a5a8-4cd3-b0f6-6985c9ac24e4'),
	(47,2,80,11,6,7,1,'2016-01-13 05:03:20','2016-01-13 05:03:20','68225b8d-50ea-48dd-89aa-e52b02c93741'),
	(48,2,91,11,8,9,1,'2016-01-14 05:11:31','2016-01-14 05:11:31','f9009e9b-878d-4da8-8a2e-db2daf3eae6f'),
	(52,5,100,14,2,3,1,'2016-01-17 04:05:40','2016-01-17 04:05:40','6c7101c1-508f-4e3b-a7db-c9c3454abeef'),
	(53,5,101,14,4,5,1,'2016-01-17 04:08:41','2016-01-17 04:08:41','dac1d16a-ba54-40ac-ab2c-b3c5d69de658'),
	(54,5,102,14,6,7,1,'2016-01-17 04:09:48','2016-01-17 04:09:48','8b78e139-e347-41b3-ae28-bbbbf4c1ce7b'),
	(73,1,132,1,2,3,1,'2016-01-17 23:45:11','2016-01-17 23:45:11','dfb47a7e-834b-47f5-943b-c04285a00fe9');

/*!40000 ALTER TABLE `structureelements` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table structures
# ------------------------------------------------------------

DROP TABLE IF EXISTS `structures`;

CREATE TABLE `structures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `maxLevels` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `structures` WRITE;
/*!40000 ALTER TABLE `structures` DISABLE KEYS */;

INSERT INTO `structures` (`id`, `maxLevels`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'2015-12-19 00:51:17','2016-01-12 02:01:50','c564974b-cb8f-41b8-9960-a93b3d4b9b12'),
	(2,NULL,'2015-12-19 00:51:40','2016-01-17 15:58:13','a56f9635-5114-4341-bea1-c54496d94d94'),
	(3,NULL,'2015-12-19 01:53:33','2015-12-19 01:53:33','f525db9b-ebed-4fae-898d-6070aaadf7c2'),
	(5,NULL,'2015-12-20 04:40:58','2016-01-16 03:39:54','3b1c9aab-dbbc-4955-87a7-64ecabaf0e90'),
	(6,NULL,'2015-12-20 20:23:29','2015-12-20 20:39:56','77b3fc24-125d-40cf-894a-5fe54367b211'),
	(7,NULL,'2015-12-20 20:44:47','2015-12-20 20:56:30','a57ba134-73fc-4161-9825-d23b3b5e34ca'),
	(8,NULL,'2015-12-28 01:06:31','2015-12-28 01:06:31','43826c7c-8952-4b63-bd5c-23a1821caeac'),
	(9,NULL,'2016-01-07 00:11:36','2016-01-07 00:12:12','cd87c131-1094-4a17-acf0-e9ae528887bb');

/*!40000 ALTER TABLE `structures` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table systemsettings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `systemsettings`;

CREATE TABLE `systemsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `systemsettings_category_unq_idx` (`category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `systemsettings` WRITE;
/*!40000 ALTER TABLE `systemsettings` DISABLE KEYS */;

INSERT INTO `systemsettings` (`id`, `category`, `settings`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'email','{\"protocol\":\"smtp\",\"emailAddress\":\"damonjentree@gmail.com\",\"senderName\":\"Mark Charter Realty\",\"smtpAuth\":1,\"username\":\"jplobaito\",\"password\":\"Jpl@57006\",\"smtpSecureTransportType\":\"none\",\"port\":\"587\",\"host\":\"smtp.sendgrid.net\",\"timeout\":\"30\"}','2015-12-19 00:45:35','2016-01-07 23:56:38','2c381dec-f2e4-4a2a-88ae-ed4613297ee8');

/*!40000 ALTER TABLE `systemsettings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table taggroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `taggroups`;

CREATE TABLE `taggroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fieldLayoutId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `taggroups_name_unq_idx` (`name`),
  UNIQUE KEY `taggroups_handle_unq_idx` (`handle`),
  KEY `taggroups_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `taggroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `taggroups` WRITE;
/*!40000 ALTER TABLE `taggroups` DISABLE KEYS */;

INSERT INTO `taggroups` (`id`, `name`, `handle`, `fieldLayoutId`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Default','default',1,'2015-12-19 00:45:35','2015-12-19 00:45:35','bd2467df-480e-44b2-bad0-71cd5c8d7ff4');

/*!40000 ALTER TABLE `taggroups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tags`;

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `tags_groupId_fk` (`groupId`),
  CONSTRAINT `tags_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `taggroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tags_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table tasks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tasks`;

CREATE TABLE `tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `root` int(11) unsigned DEFAULT NULL,
  `lft` int(11) unsigned NOT NULL,
  `rgt` int(11) unsigned NOT NULL,
  `level` smallint(6) unsigned NOT NULL,
  `currentStep` int(11) unsigned DEFAULT NULL,
  `totalSteps` int(11) unsigned DEFAULT NULL,
  `status` enum('pending','error','running') COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `tasks_root_idx` (`root`),
  KEY `tasks_lft_idx` (`lft`),
  KEY `tasks_rgt_idx` (`rgt`),
  KEY `tasks_level_idx` (`level`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table templatecachecriteria
# ------------------------------------------------------------

DROP TABLE IF EXISTS `templatecachecriteria`;

CREATE TABLE `templatecachecriteria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cacheId` int(11) NOT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `criteria` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `templatecachecriteria_cacheId_fk` (`cacheId`),
  KEY `templatecachecriteria_type_idx` (`type`),
  CONSTRAINT `templatecachecriteria_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `templatecaches` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table templatecacheelements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `templatecacheelements`;

CREATE TABLE `templatecacheelements` (
  `cacheId` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  KEY `templatecacheelements_cacheId_fk` (`cacheId`),
  KEY `templatecacheelements_elementId_fk` (`elementId`),
  CONSTRAINT `templatecacheelements_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `templatecaches` (`id`) ON DELETE CASCADE,
  CONSTRAINT `templatecacheelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table templatecaches
# ------------------------------------------------------------

DROP TABLE IF EXISTS `templatecaches`;

CREATE TABLE `templatecaches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cacheKey` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `body` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `templatecaches_expiryDate_cacheKey_locale_path_idx` (`expiryDate`,`cacheKey`,`locale`,`path`),
  KEY `templatecaches_locale_fk` (`locale`),
  CONSTRAINT `templatecaches_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tokens`;

CREATE TABLE `tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `route` text COLLATE utf8_unicode_ci,
  `usageLimit` tinyint(3) unsigned DEFAULT NULL,
  `usageCount` tinyint(3) unsigned DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tokens_token_unq_idx` (`token`),
  KEY `tokens_expiryDate_idx` (`expiryDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table usergroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `usergroups`;

CREATE TABLE `usergroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table usergroups_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `usergroups_users`;

CREATE TABLE `usergroups_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `usergroups_users_groupId_userId_unq_idx` (`groupId`,`userId`),
  KEY `usergroups_users_userId_fk` (`userId`),
  CONSTRAINT `usergroups_users_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `usergroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `usergroups_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table userpermissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `userpermissions`;

CREATE TABLE `userpermissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userpermissions_name_unq_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table userpermissions_usergroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `userpermissions_usergroups`;

CREATE TABLE `userpermissions_usergroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissionId` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userpermissions_usergroups_permissionId_groupId_unq_idx` (`permissionId`,`groupId`),
  KEY `userpermissions_usergroups_groupId_fk` (`groupId`),
  CONSTRAINT `userpermissions_usergroups_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `usergroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `userpermissions_usergroups_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `userpermissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table userpermissions_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `userpermissions_users`;

CREATE TABLE `userpermissions_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissionId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userpermissions_users_permissionId_userId_unq_idx` (`permissionId`,`userId`),
  KEY `userpermissions_users_userId_fk` (`userId`),
  CONSTRAINT `userpermissions_users_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `userpermissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `userpermissions_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `preferredLocale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weekStartDay` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `client` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `suspended` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pending` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `archived` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastLoginDate` datetime DEFAULT NULL,
  `lastLoginAttemptIPAddress` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invalidLoginWindowStart` datetime DEFAULT NULL,
  `invalidLoginCount` tinyint(4) unsigned DEFAULT NULL,
  `lastInvalidLoginDate` datetime DEFAULT NULL,
  `lockoutDate` datetime DEFAULT NULL,
  `verificationCode` char(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verificationCodeIssuedDate` datetime DEFAULT NULL,
  `unverifiedEmail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passwordResetRequired` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastPasswordChangeDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unq_idx` (`username`),
  UNIQUE KEY `users_email_unq_idx` (`email`),
  KEY `users_verificationCode_idx` (`verificationCode`),
  KEY `users_uid_idx` (`uid`),
  KEY `users_preferredLocale_fk` (`preferredLocale`),
  CONSTRAINT `users_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `users_preferredLocale_fk` FOREIGN KEY (`preferredLocale`) REFERENCES `locales` (`locale`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `username`, `photo`, `firstName`, `lastName`, `email`, `password`, `preferredLocale`, `weekStartDay`, `admin`, `client`, `locked`, `suspended`, `pending`, `archived`, `lastLoginDate`, `lastLoginAttemptIPAddress`, `invalidLoginWindowStart`, `invalidLoginCount`, `lastInvalidLoginDate`, `lockoutDate`, `verificationCode`, `verificationCodeIssuedDate`, `unverifiedEmail`, `passwordResetRequired`, `lastPasswordChangeDate`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'markcharter',NULL,'Mark','Charter','damonjentree@gmail.com','$2y$13$GJJzL9T7Tf0TK0x.EolIG.ovL6ewvOhqUU1l7gYgamPgHhRpUmNjK',NULL,0,1,0,0,0,0,0,'2016-01-17 22:15:09','127.0.0.1',NULL,NULL,'2016-01-11 23:52:33',NULL,NULL,NULL,NULL,0,'2015-12-19 00:45:33','2015-12-19 00:45:33','2016-01-17 22:15:09','81f6b86e-53ef-42cd-9e94-70ae234fbf0f');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table widgets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `widgets`;

CREATE TABLE `widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` tinyint(4) DEFAULT NULL,
  `colspan` tinyint(4) unsigned DEFAULT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `widgets_userId_fk` (`userId`),
  CONSTRAINT `widgets_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `widgets` WRITE;
/*!40000 ALTER TABLE `widgets` DISABLE KEYS */;

INSERT INTO `widgets` (`id`, `userId`, `type`, `sortOrder`, `colspan`, `settings`, `enabled`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'RecentEntries',1,NULL,NULL,1,'2015-12-19 00:45:42','2015-12-19 00:45:42','e72ff14a-a709-4d8a-a753-fb739baad71f'),
	(2,1,'GetHelp',2,NULL,NULL,1,'2015-12-19 00:45:42','2015-12-19 00:45:42','8324332c-891f-4293-bba3-23d4860e9656'),
	(3,1,'Updates',3,NULL,NULL,1,'2015-12-19 00:45:42','2015-12-19 00:45:42','e6b92389-4a92-4499-b318-f60b003387bf'),
	(4,1,'Feed',4,NULL,'{\"url\":\"https:\\/\\/craftcms.com\\/news.rss\",\"title\":\"Craft News\"}',1,'2015-12-19 00:45:42','2015-12-19 00:45:42','10dd0609-25ef-4d6c-a7f3-b0d882d962ee');

/*!40000 ALTER TABLE `widgets` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
