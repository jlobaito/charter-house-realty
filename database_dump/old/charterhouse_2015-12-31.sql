# ************************************************************
# Sequel Pro SQL dump
# Version 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.42)
# Database: charterhouse
# Generation Time: 2015-12-31 21:41:08 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table assetfiles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `assetfiles`;

CREATE TABLE `assetfiles` (
  `id` int(11) NOT NULL,
  `sourceId` int(11) DEFAULT NULL,
  `folderId` int(11) NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kind` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unknown',
  `width` int(11) unsigned DEFAULT NULL,
  `height` int(11) unsigned DEFAULT NULL,
  `size` int(11) unsigned DEFAULT NULL,
  `dateModified` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `assetfiles_filename_folderId_unq_idx` (`filename`,`folderId`),
  KEY `assetfiles_sourceId_fk` (`sourceId`),
  KEY `assetfiles_folderId_fk` (`folderId`),
  CONSTRAINT `assetfiles_folderId_fk` FOREIGN KEY (`folderId`) REFERENCES `assetfolders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assetfiles_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assetfiles_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `assetsources` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table assetfolders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `assetfolders`;

CREATE TABLE `assetfolders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentId` int(11) DEFAULT NULL,
  `sourceId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `assetfolders_name_parentId_sourceId_unq_idx` (`name`,`parentId`,`sourceId`),
  KEY `assetfolders_parentId_fk` (`parentId`),
  KEY `assetfolders_sourceId_fk` (`sourceId`),
  CONSTRAINT `assetfolders_parentId_fk` FOREIGN KEY (`parentId`) REFERENCES `assetfolders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assetfolders_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `assetsources` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table assetindexdata
# ------------------------------------------------------------

DROP TABLE IF EXISTS `assetindexdata`;

CREATE TABLE `assetindexdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sessionId` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sourceId` int(10) NOT NULL,
  `offset` int(10) NOT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `size` int(10) DEFAULT NULL,
  `recordId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `assetindexdata_sessionId_sourceId_offset_unq_idx` (`sessionId`,`sourceId`,`offset`),
  KEY `assetindexdata_sourceId_fk` (`sourceId`),
  CONSTRAINT `assetindexdata_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `assetsources` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table assetsources
# ------------------------------------------------------------

DROP TABLE IF EXISTS `assetsources`;

CREATE TABLE `assetsources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `sortOrder` tinyint(4) DEFAULT NULL,
  `fieldLayoutId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `assetsources_name_unq_idx` (`name`),
  UNIQUE KEY `assetsources_handle_unq_idx` (`handle`),
  KEY `assetsources_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `assetsources_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table assettransformindex
# ------------------------------------------------------------

DROP TABLE IF EXISTS `assettransformindex`;

CREATE TABLE `assettransformindex` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fileId` int(11) NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `format` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sourceId` int(11) DEFAULT NULL,
  `fileExists` tinyint(1) DEFAULT NULL,
  `inProgress` tinyint(1) DEFAULT NULL,
  `dateIndexed` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `assettransformindex_sourceId_fileId_location_idx` (`sourceId`,`fileId`,`location`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table assettransforms
# ------------------------------------------------------------

DROP TABLE IF EXISTS `assettransforms`;

CREATE TABLE `assettransforms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mode` enum('stretch','fit','crop') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'crop',
  `position` enum('top-left','top-center','top-right','center-left','center-center','center-right','bottom-left','bottom-center','bottom-right') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'center-center',
  `height` int(10) DEFAULT NULL,
  `width` int(10) DEFAULT NULL,
  `format` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quality` int(10) DEFAULT NULL,
  `dimensionChangeTime` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `assettransforms_name_unq_idx` (`name`),
  UNIQUE KEY `assettransforms_handle_unq_idx` (`handle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `categories_groupId_fk` (`groupId`),
  CONSTRAINT `categories_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `categorygroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `categories_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;

INSERT INTO `categories` (`id`, `groupId`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(23,2,'2015-12-20 04:45:14','2015-12-20 14:46:46','990913d2-73e8-4a4a-acd8-2adc0f413efd'),
	(28,2,'2015-12-20 04:53:50','2015-12-20 14:50:30','b4c7676d-5749-48ec-8958-87c9cb038d38'),
	(32,2,'2015-12-20 04:55:11','2015-12-20 15:02:56','c267eb48-7723-4596-83c4-401511e9e670'),
	(39,3,'2015-12-20 20:27:05','2015-12-20 20:29:17','5b8e1d41-ab08-4de6-9994-680d246f0168'),
	(40,3,'2015-12-20 20:30:17','2015-12-20 20:30:17','9fbd0802-3747-45d4-84ae-d982216254a4'),
	(41,3,'2015-12-20 20:31:04','2015-12-20 20:31:04','145fa1ca-a90e-4593-a8ba-bc08f9bc481d'),
	(42,3,'2015-12-20 20:31:22','2015-12-20 20:31:22','65e3afcf-8b43-4e8f-bad6-7464dda32222');

/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table categorygroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categorygroups`;

CREATE TABLE `categorygroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hasUrls` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `template` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `categorygroups_name_unq_idx` (`name`),
  UNIQUE KEY `categorygroups_handle_unq_idx` (`handle`),
  KEY `categorygroups_structureId_fk` (`structureId`),
  KEY `categorygroups_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `categorygroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `categorygroups_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `categorygroups` WRITE;
/*!40000 ALTER TABLE `categorygroups` DISABLE KEYS */;

INSERT INTO `categorygroups` (`id`, `structureId`, `fieldLayoutId`, `name`, `handle`, `hasUrls`, `template`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(2,5,65,'Ways To Sell','waysToSell',0,NULL,'2015-12-20 04:40:58','2015-12-20 14:40:47','666de006-99ac-4d05-839a-5ff0ab05753d'),
	(3,6,74,'Team Members','teamMembers',0,NULL,'2015-12-20 20:23:29','2015-12-20 20:39:56','ecf25418-ed99-4fa4-998d-50519df7853d');

/*!40000 ALTER TABLE `categorygroups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table categorygroups_i18n
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categorygroups_i18n`;

CREATE TABLE `categorygroups_i18n` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `urlFormat` text COLLATE utf8_unicode_ci,
  `nestedUrlFormat` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `categorygroups_i18n_groupId_locale_unq_idx` (`groupId`,`locale`),
  KEY `categorygroups_i18n_locale_fk` (`locale`),
  CONSTRAINT `categorygroups_i18n_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `categorygroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `categorygroups_i18n_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `categorygroups_i18n` WRITE;
/*!40000 ALTER TABLE `categorygroups_i18n` DISABLE KEYS */;

INSERT INTO `categorygroups_i18n` (`id`, `groupId`, `locale`, `urlFormat`, `nestedUrlFormat`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(2,2,'en_us',NULL,NULL,'2015-12-20 04:40:58','2015-12-20 04:40:58','bb87c0f0-d34b-4c55-8e10-530fde15c481'),
	(3,3,'en_us',NULL,NULL,'2015-12-20 20:23:29','2015-12-20 20:23:29','263fe484-458c-46bf-b8f0-e2c51990e2d2');

/*!40000 ALTER TABLE `categorygroups_i18n` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table content
# ------------------------------------------------------------

DROP TABLE IF EXISTS `content`;

CREATE TABLE `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_body` text COLLATE utf8_unicode_ci,
  `field_pageDescription` varchar(155) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_comments` text COLLATE utf8_unicode_ci,
  `field_email` text COLLATE utf8_unicode_ci,
  `field_phone` varchar(14) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_baths` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_beds` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_foreclosure` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `field_mlsNumber` text COLLATE utf8_unicode_ci,
  `field_shortSale` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `field_squareFeet` text COLLATE utf8_unicode_ci,
  `field_source` text COLLATE utf8_unicode_ci,
  `field_propertyType` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `field_parking` text COLLATE utf8_unicode_ci,
  `field_price` text COLLATE utf8_unicode_ci,
  `field_videoUrl` text COLLATE utf8_unicode_ci,
  `field_matterportUrl` text COLLATE utf8_unicode_ci,
  `field_yearBuilt` text COLLATE utf8_unicode_ci,
  `field_officeTitle` text COLLATE utf8_unicode_ci,
  `field_officePhone` varchar(14) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_facebook` text COLLATE utf8_unicode_ci,
  `field_twitter` text COLLATE utf8_unicode_ci,
  `field_linkedin` text COLLATE utf8_unicode_ci,
  `field_youtube` text COLLATE utf8_unicode_ci,
  `field_optionDescription` text COLLATE utf8_unicode_ci,
  `field_optionCommission` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `field_optionBenefits` text COLLATE utf8_unicode_ci,
  `field_markCharterDisclaimer` text COLLATE utf8_unicode_ci,
  `field_teamMemberTitle` text COLLATE utf8_unicode_ci,
  `field_teamMemberPhone` varchar(14) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_teamMemberEmail` text COLLATE utf8_unicode_ci,
  `field_teamMemberBio` text COLLATE utf8_unicode_ci,
  `field_partnerWebsite` text COLLATE utf8_unicode_ci,
  `field_partnerPhone` varchar(14) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_elementId_locale_unq_idx` (`elementId`,`locale`),
  KEY `content_title_idx` (`title`),
  KEY `content_locale_fk` (`locale`),
  CONSTRAINT `content_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `content_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `content` WRITE;
/*!40000 ALTER TABLE `content` DISABLE KEYS */;

INSERT INTO `content` (`id`, `elementId`, `locale`, `title`, `field_body`, `field_pageDescription`, `field_comments`, `field_email`, `field_phone`, `field_baths`, `field_beds`, `field_foreclosure`, `field_mlsNumber`, `field_shortSale`, `field_squareFeet`, `field_source`, `field_propertyType`, `field_parking`, `field_price`, `field_videoUrl`, `field_matterportUrl`, `field_yearBuilt`, `field_officeTitle`, `field_officePhone`, `field_facebook`, `field_twitter`, `field_linkedin`, `field_youtube`, `field_optionDescription`, `field_optionCommission`, `field_optionBenefits`, `field_markCharterDisclaimer`, `field_teamMemberTitle`, `field_teamMemberPhone`, `field_teamMemberEmail`, `field_teamMemberBio`, `field_partnerWebsite`, `field_partnerPhone`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'en_us',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-12-19 00:45:33','2015-12-28 00:48:24','f7f261c0-8bb8-4c23-a1b0-91d442605c2a'),
	(2,2,'en_us','Mark Charter Realty','<p>s</p>','',NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-12-19 00:45:35','2015-12-28 01:23:47','c031151f-4592-46a9-a340-04526c81cd36'),
	(3,3,'en_us','We just installed Craft!','<p>Craft is the CMS that’s powering Local.charterhouseiowa.dev. It’s beautiful, powerful, flexible, and easy-to-use, and it’s made by Pixel &amp; Tonic. We can’t wait to dive in and see what it’s capable of!</p><!--pagebreak--><p>This is even more captivating content, which you couldn’t see on the News index page because it was entered after a Page Break, and the News index template only likes to show the content on the first page.</p><p>Craft: a nice alternative to Word, if you’re making a website.</p>',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-12-19 00:45:35','2015-12-19 00:45:35','e95ac777-13b6-4229-99d2-10cdd420fe6f'),
	(4,4,'en_us','Properties','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis elit massa, ornare et purus a, semper sagittis diam. In condimentum, ipsum eget luctus feugiat, est nisi ullamcorper diam, elementum placerat sapien neque nec ex. Duis pulvinar metus mauris, non commodo mauris cursus vel. Phasellus a ligula in lorem tincidunt semper. Phasellus sed nisl quis eros malesuada egestas eu ultrices massa. Cras commodo velit in arcu vehicula, et egestas magna ullamcorper. Donec sagittis viverra mi vitae mattis. Nulla semper sapien ut consequat dignissim. Nam augue sem, luctus eu dictum et, feugiat eget turpis. Nullam sit amet maximus ante. Pellentesque ut risus semper, fermentum erat id, auctor leo. Etiam cursus facilisis justo, quis interdum enim bibendum in. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque feugiat massa quis enim gravida, vel auctor nisi efficitur.</p>','',NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-12-19 00:52:08','2015-12-28 01:30:31','bb56f9e8-7abc-4a29-b70b-05449b405b08'),
	(5,5,'en_us','Partners','','',NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-12-19 00:53:06','2015-12-28 01:29:00','1da33283-e59a-44b8-ba30-7e7a6d583227'),
	(6,6,'en_us','Team','<p>It’s true, this site doesn’t have a whole lot of content yet, but don’t worry. Our web developers have just installed the CMS, and they’re setting things up for the content editors this very moment. Soon Local.charterhouseiowa.dev will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.</p>','',NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-12-19 00:54:13','2015-12-19 01:46:50','53c58286-371a-489e-b755-f8447d16afa4'),
	(7,7,'en_us','Testimonials','<p>It’s true, this site doesn’t have a whole lot of content yet, but don’t worry. Our web developers have just installed the CMS, and they’re setting things up for the content editors this very moment. Soon Local.charterhouseiowa.dev will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.</p>','',NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-12-19 00:54:52','2015-12-19 02:29:20','b4ee2574-7077-44e6-a760-dd636041ed69'),
	(8,8,'en_us','Reservation Success','<p>We got ya\' covered - and you got my truck.</p>','',NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-12-19 00:55:30','2015-12-19 02:41:05','534e20b0-0afd-4742-830d-93feb55d08dc'),
	(9,9,'en_us','Truck','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mattis sollicitudin felis, consectetur eleifend mi ornare sed. Vivamus cursus tincidunt ultrices. Nam id tincidunt purus. Morbi lectus eros, molestie non dolor ut, tincidunt fermentum odio. Vivamus consectetur molestie lorem ac varius.</p>','',NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-12-19 01:05:51','2015-12-21 20:41:59','aa2821fa-0af5-4d15-997b-b65b172d2e7d'),
	(13,13,'en_us','One of Mark Charter’s greatest characteristics is his giving nature.','<p>One of Mark Charter\'s greatest characteristics is his giving nature. He always goes above and beyond for his clients to make sure that their experience in buying or selling a home is first rate. While not just an experienced real estate agent, Mark is also extremely knowledgeable about the investment real estate market as well. He is a career agent that absolutely, without a doubt, has your best interests in mind.</p>','',NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,'Adam Carroll','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-12-19 02:30:36','2015-12-19 02:36:01','e19b0e28-568e-4ff3-af22-08526874991f'),
	(14,14,'en_us','I was so lucky to find them!','<p>Before finding Mark and Amy, I had had a lot of bad experiences with realtors. From the minute I contacted them about possibly being my realtor, I was in constant communication with them--I was never left wondering what was going on! I was paired with Amy and she is FANTASTIC. She listened to not only what I was looking for, but what I was going to use spaces for and looked for those things in the places I wanted to view. She helped me to pro and con the spaces I was looking on based on what she had heard me say about other spaces. She was in constant contact with me about setting up showings and if there was a delay in getting an answer from someone, was letting me know what the hold up was.&nbsp;</p><p>I have never met a more caring team to work with and when they say you\'re part of the family now, you are! As a first time home buyer, I had lots of questions both for Amy and for the seller and I got every single one answered. I never once felt like there was another goal that the team was working for other than to make sure I was happy with what I was getting! While the people are top notch, it\'s also amazing to me that you can use the moving truck for free and they had my new home cleaned before I moved in! It saved me so much time and hassle, not to mention cost. I was so lucky to find them!</p>','',NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,'Bethany Wilcke','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-12-19 02:34:49','2015-12-19 02:36:01','7149117c-37e1-4772-ad12-c20524d96f83'),
	(15,15,'en_us','Mark was phenomenal to work with.','<p>Mark was phenomenal to work with during the sale of our home. His no-nonsense, tell-it-how-it is attitude is the only reason we got our home sold! We had a unique situation with our home based on the size, garage and price - Mark was the only person that told us what we needed to hear (instead of just fluffing us with what we wanted to hear). Mark is a great communicator and we always knew what was expected of us through the entire process. Thank you, Mark!</p>','',NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,'Jeremy Orr','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-12-19 02:35:26','2015-12-19 02:36:01','8752be87-9c76-4420-9082-8db18a7c5b4e'),
	(17,17,'en_us','Amazing golf course home with too many amenities to list!','<p>Stunning Ankeny home located on Otter Creek Golf Course. This 4 bed, 4.5 bath former Show Home is loaded with features and amenities from top to bottom. Try building this home today for less than a million dollars! It all starts with location. You will love cul-de-sac living, close to I35 making a commute a breeze. Step through the front door and you will be wowed by the view! There is not one, but TWO ponds directly behind the home. The outside is just as gorgeous as the inside.&nbsp;</p>\r\n\r\n<p>Full landscaping, flagstone entry, exterior lighting, huge patio and a wraparound deck with outdoor kitchen truly put this home among Ankeny\'s best! Inside the quality is top notch. Beautiful knotty alder trim, Acacia hardwood, Viking appliances, and a beautiful stone fireplace are impossible to miss. The master is an oasis, with beautiful materials. Need an amazing basement? Sunken family room, barn wood floors, huge wet bar, hidden room and golf cart garage await. Too much to list, Email for all features!</p>','',NULL,NULL,NULL,'4.5','5',0,'504907',0,'2,426',NULL,'singleFamily','3 Car Garage','$849,900','yZRyXkGqD2c','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-12-19 02:52:17','2015-12-28 01:29:55','4ccd886b-2f4c-4250-8b3f-f85fc67fcdb9'),
	(18,18,'en_us','Absolutely amazing, modern rural-meets-urban set up just outside of Grimes!! A must see at $719,000!','<p>Call your builder and call off the meeting! This beats new construction and is the house you have been waiting on to hit the market! This stunning Grimes acreage sits on 2.3 acres and is just a mile north of DCG High School. Awesome location. Pull into the long driveway and enjoy the massive front yard with trees. STUNNING curb appeal!&nbsp;</p>\r\n\r\n<p>This 5 bedroom (could be 6) home has a floor plan you have not seen before. The attention to detail and woodwork is stunning. Built-ins and window seats with storage are found everywhere. The master bedroom is on the main level and offers a gorgeous bath with Carrera marble, and a vey large closet. The main floor laundry is also huge. You will find a second full laundry room in the finished basement, along with a second kitchen! One of the beds is separated from the rest of the house and is perfect for an office or teen room.&nbsp;</p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<p>Too many amenities to list them all!&nbsp;</p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<ul><li>5 garage stalls</li><li>Geothermal</li><li>High end kitchen</li><li>Covered deck</li><li>So much more -&nbsp;A RARE FIND!</li></ul>','',NULL,NULL,NULL,'4','5',0,'506142',0,'2,700',NULL,'singleFamily','3 car attached garage - 2 car detached garage','$719,000','','1Hw5ay9jY7y','1990',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-12-19 15:30:35','2015-12-28 01:30:06','f0509e79-9c3e-4232-8574-032b4e5a8eca'),
	(19,19,'en_us',NULL,NULL,NULL,NULL,'mark@charterhouseiowa.com',NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'Remax Real Estate Concepts','(515) 864-6444',NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-12-19 21:06:00','2015-12-20 04:19:06','837a1daa-68e8-4d2e-a5eb-f2418a4c82a7'),
	(20,20,'en_us',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-12-20 04:19:31','2015-12-20 04:19:31','3ae86263-7865-4fac-83d9-6016e7b416f2'),
	(21,21,'en_us','Contact Mark','','',NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-12-20 04:23:50','2015-12-20 04:26:39','aece22f3-c5fc-4a91-913f-2e1734904d6d'),
	(22,22,'en_us','3 Ways To List Your Home','','',NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-12-20 04:39:21','2015-12-20 14:56:25','fc1b5871-91c5-4bda-a60b-26e071d183e9'),
	(23,23,'en_us','Pro and Go',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<p>The perfect plan to keep it simple. This seller does not require much, and likes to do things themselves. Just list with a pro...and go!</p>','5','<ul><li>3 month misting</li><li>Full MLS exposure</li><li>Professional photos</li><li>Social media marketing</li><li>Sign and lockbox</li></ul>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-12-20 04:45:14','2015-12-20 14:46:46','6e218d04-55c6-4242-8297-7a913041d7da'),
	(24,28,'en_us','Pro Plus',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<p>This popular standard plan packs a punch! Great marketing and perks, all at common commission rate in the market. Great service plus more!</p>','6','<ul><li>3 month misting</li><li>Full MLS exposure</li><li>Professional photos</li><li>Social media marketing</li><li>Sign and lockbox</li><li>Facebook targeted marketing</li><li>3D Matterport virtual tour</li><li><a href=\"{entry:9:url}\">Access to 15ft. truck</a></li><li>Free pest inspection</li><li>$1,000 \"offer\" guarantee</li></ul>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-12-20 04:53:50','2015-12-20 14:50:30','a017dc9a-f43c-452b-9fc4-69c51ad48233'),
	(25,32,'en_us','Pro Elite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<p>For the seller who wants the very best. Maximum marketing $$$\'s + maximum convince all in one. The Elite is unrivaled in the marketplace!</p>','7','<ul><li>3 month misting</li><li>Full MLS exposure</li><li>Professional photos</li><li>Social media marketing</li><li>Sign and lockbox</li><li>Facebook targeted marketing</li><li>3D Matterport virtual tour</li><li><a href=\"{entry:9:url}\">Access to 15ft. truck</a></li><li>Free pest inspection</li><li>$1,000 \"offer\" guarantee</li><li>Free storage</li><li>Full move package*</li></ul>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-12-20 04:55:11','2015-12-20 15:02:56','d3dfc57a-d79a-4e25-87a6-e8d9ccf02f9c'),
	(26,36,'en_us','Buyers & Sellers','','',NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-12-20 05:00:51','2015-12-28 01:03:42','ac59df68-8977-4bc0-b656-63b9336ac581'),
	(27,37,'en_us',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'<p>Mark Charter is a licensed agent with RE/MAX Real Estate Concepts in the state of Iowa, #B If you have received this information and are already listed with an agent, please disregard this as it is not meant to intrude on and existing seller/agent relationship.</p><p>*Full moves are available to clients that are staying within&nbsp;central Iowa.</p>',NULL,NULL,NULL,NULL,NULL,NULL,'2015-12-20 14:56:56','2015-12-20 15:00:50','ffbbe3cf-7cc5-4fe3-9395-dff2f341f2d3'),
	(28,38,'en_us','About Mark Charter','<p>Mark started his real estate career in 2005 and has been doing it full-time since then. Over time Mark realized that the majority of real estate agents are the same. They say the same things, they market the same way, and they treat clients the same. Mark did not like this so he set about creating his own unique way of practicing real estate.</p><p>In 2014, Mark debuted a three tier pricing plan for listing clients, giving them the choice on how they want to list their home. This is another example of how Mark never accepts the status quo in real estate and is always looking to make things better for his clients.<br></p><p>Mark\'s overall goal on every deal is simple. He wants to provide such great service that after the deal is done, that client will happily provide a positive review of the transaction. This goal is what makes Mark one of the most recommended agents in Central Iowa.<br></p><p>When you add it all up, when you take into account what Mark offers clients in terms of services, marketing and care, Mark truly feels he is the best option available in the market today and he hopes you will experience this for yourself very soon!<br></p>','',NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-12-20 20:03:09','2015-12-28 00:57:07','3c29078b-0fd0-4d35-97a4-88e951e2cdc4'),
	(29,39,'en_us','Mark Charter',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,'Owner / Realtor','(515) 864-6444','mark@charterhouseiowa.com','<p>Mark Charter is Iowa\'s most recommended agent on sites like Trulia.com and Angieslist.com and is one of the top selling agents in central Iowa. Mark believes in constantly looking for ways to put his clients first and accomplishes this through things like short listing contracts, flexible commission plans and great perks that benefit his clients. Mark is Married to Katie and has a son named Seton and a daughter named Hollis.</p>',NULL,NULL,'2015-12-20 20:27:05','2015-12-20 20:29:17','63e92412-b58e-4568-8251-334036c37fdc'),
	(30,40,'en_us','Amy Meyer',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,'Buyer Specialist','(515) 822-0171','amy@charterhouseiowa.com','<p>Amy Meyer is a fantastic addition to the Mark Charter Team. Talented, dedicated and hard-working, Amy\'s role is to help buyers find the perfect home they are looking for.</p>',NULL,NULL,'2015-12-20 20:30:17','2015-12-20 20:30:17','19b9afff-1bec-48db-ac90-f7cebbcb2b68'),
	(31,41,'en_us','Nic Meyer',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,'Business Manager & Client Coordinator','(515) 371-1973','nic@charterhouseiowa.com','<p>Nic\'s role is making sure the client is happy! He helps buyers and seller with all the details of a move . . . things like lining up use of the Mark Charter Real Estate moving truck, scheduling cleaners or anything else that you might need! He is a vital part of the team and is here to help you.</p>',NULL,NULL,'2015-12-20 20:31:04','2015-12-20 20:31:04','597311b7-9bef-4eea-ade8-12488daa270c'),
	(32,42,'en_us','Jake Boyd Photo',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,'Photographer','','','<p>Jake Boyd is Central Iowa\'s premier real estate photographer and owns Jake Boyd Photo. Jordan and Jason are also valuable members of the team as well. Mark trusts Jake Boyd Photo to shoot all of his listing photos.</p>',NULL,NULL,'2015-12-20 20:31:22','2015-12-20 20:31:22','e06169ae-c834-4b65-a77d-cc5a8c6f0331'),
	(45,61,'en_us','Mortgage - The Tyler Osby Team','<p>Tyler Osby is where Mark turns when his clients need a mortgage or a refinance. Tyler is knowledgeable, accountable and trustworthy. He gets the job done every time!</p>','',NULL,NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,' http://www.wealthwithmortgage.com','(515) 257-6729','2015-12-28 01:08:39','2015-12-28 01:18:41','2e8f7fc5-998f-4949-98dd-50a11a362d43');

/*!40000 ALTER TABLE `content` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table deprecationerrors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `deprecationerrors`;

CREATE TABLE `deprecationerrors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fingerprint` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastOccurrence` datetime NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `line` smallint(6) unsigned NOT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `templateLine` smallint(6) unsigned DEFAULT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `traces` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `deprecationerrors_key_fingerprint_unq_idx` (`key`,`fingerprint`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table elementindexsettings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `elementindexsettings`;

CREATE TABLE `elementindexsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `elementindexsettings_type_unq_idx` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table elements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `elements`;

CREATE TABLE `elements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `archived` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `elements_type_idx` (`type`),
  KEY `elements_enabled_idx` (`enabled`),
  KEY `elements_archived_dateCreated_idx` (`archived`,`dateCreated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `elements` WRITE;
/*!40000 ALTER TABLE `elements` DISABLE KEYS */;

INSERT INTO `elements` (`id`, `type`, `enabled`, `archived`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'User',1,0,'2015-12-19 00:45:33','2015-12-28 00:48:24','ab9bc828-a3d1-4732-a422-baf4c5d17443'),
	(2,'Entry',1,0,'2015-12-19 00:45:35','2015-12-28 01:23:47','ad5fb7cb-8c7b-4f18-a668-c39bbf0f6f13'),
	(3,'Entry',1,0,'2015-12-19 00:45:35','2015-12-19 00:45:35','951d1a34-b1d4-4dfb-b655-98bd02e8cb4d'),
	(4,'Entry',1,0,'2015-12-19 00:52:08','2015-12-28 01:30:31','80521552-da7d-48a1-a62b-822b29d8b067'),
	(5,'Entry',1,0,'2015-12-19 00:53:06','2015-12-28 01:29:00','4282024f-cc60-4cb2-8e4f-e924a71330ad'),
	(6,'Entry',1,0,'2015-12-19 00:54:13','2015-12-19 01:46:50','4117134c-6550-4bf5-beee-68eb34d906b8'),
	(7,'Entry',1,0,'2015-12-19 00:54:52','2015-12-19 02:29:20','d2d4bb54-95a2-4c43-afc9-835649f340cb'),
	(8,'Entry',1,0,'2015-12-19 00:55:30','2015-12-19 02:41:05','83978c18-b674-4844-9131-eb04b2335c4c'),
	(9,'Entry',1,0,'2015-12-19 01:05:51','2015-12-21 20:41:59','b63bfe87-2a0c-432b-a19d-3ec53c515699'),
	(13,'Entry',1,0,'2015-12-19 02:30:36','2015-12-19 02:36:01','e2dc4b13-f509-4abf-98e5-5d091c959815'),
	(14,'Entry',1,0,'2015-12-19 02:34:49','2015-12-19 02:36:01','f5f97792-12cf-458c-9f79-aa6f3051be93'),
	(15,'Entry',1,0,'2015-12-19 02:35:26','2015-12-19 02:36:01','406d7276-72c0-48b9-b52c-5568aaa66ad0'),
	(17,'Entry',1,0,'2015-12-19 02:52:17','2015-12-28 01:29:55','09576c8d-53de-4361-b8a3-c3e814554561'),
	(18,'Entry',1,0,'2015-12-19 15:30:35','2015-12-28 01:30:06','e8b9b492-dbbe-414e-8b5c-62419acf479a'),
	(19,'GlobalSet',1,0,'2015-12-19 21:06:00','2015-12-20 04:19:06','9991a733-ccf1-4ccf-9b3b-ec2fd4394352'),
	(20,'GlobalSet',1,0,'2015-12-20 04:19:31','2015-12-20 04:19:31','704536fa-c84f-4714-ba66-0b1e15ca9a27'),
	(21,'Entry',1,0,'2015-12-20 04:23:50','2015-12-20 04:26:39','1cd7a6fa-9994-4ccc-afa5-89fd9ce591f0'),
	(22,'Entry',1,0,'2015-12-20 04:39:21','2015-12-20 14:56:25','98d55f7a-979c-433d-9ff9-026c0c27f569'),
	(23,'Category',1,0,'2015-12-20 04:45:14','2015-12-20 14:46:46','d4afa6a8-367d-4d30-bf77-1165f0f156cd'),
	(25,'MatrixBlock',1,0,'2015-12-20 04:53:04','2015-12-20 04:53:14','cfb87c37-d404-46ce-9fd8-183ba1863228'),
	(26,'MatrixBlock',1,0,'2015-12-20 04:53:04','2015-12-20 04:53:15','b05fbccc-291c-46e9-a805-670beff47314'),
	(27,'MatrixBlock',1,0,'2015-12-20 04:53:04','2015-12-20 04:53:15','6b41701a-6c92-414d-b9b6-99a46370c952'),
	(28,'Category',1,0,'2015-12-20 04:53:50','2015-12-20 14:50:30','6547f45a-f581-40ca-88db-3aa118684dbb'),
	(29,'MatrixBlock',1,0,'2015-12-20 04:53:50','2015-12-20 04:54:19','27c45ce5-9bcf-4976-bd5c-737133ec5ea9'),
	(30,'MatrixBlock',1,0,'2015-12-20 04:53:50','2015-12-20 04:54:19','8ec94276-06d0-417a-b9c9-5261d92cd92d'),
	(31,'MatrixBlock',1,0,'2015-12-20 04:54:19','2015-12-20 04:54:19','6e46e72c-bc02-49e6-a320-d58fe2f65827'),
	(32,'Category',1,0,'2015-12-20 04:55:11','2015-12-20 15:02:56','bf6d1fc9-b862-4010-8b32-95b7d3631c9e'),
	(33,'MatrixBlock',1,0,'2015-12-20 04:55:11','2015-12-20 04:55:11','c26d3556-5e55-4a3c-aae4-7ec5bc6a983e'),
	(34,'MatrixBlock',1,0,'2015-12-20 04:55:11','2015-12-20 04:55:11','6444a468-ae80-4440-9a7b-9fcb1117b0f2'),
	(35,'MatrixBlock',1,0,'2015-12-20 04:55:11','2015-12-20 04:55:11','98313143-5649-49cf-a993-89cf55402c3a'),
	(36,'Entry',1,0,'2015-12-20 05:00:51','2015-12-28 01:03:42','51b33d3d-7c82-41d0-8edb-b396db1e9e17'),
	(37,'GlobalSet',1,0,'2015-12-20 14:56:56','2015-12-20 15:00:50','ac6d3bf4-a7a0-45f4-b800-d73275476c36'),
	(38,'Entry',1,0,'2015-12-20 20:03:09','2015-12-28 00:57:07','f02dea5d-0678-447b-99c4-ab43e186eaec'),
	(39,'Category',1,0,'2015-12-20 20:27:05','2015-12-20 20:29:17','acd424f0-4359-4212-bc7a-73a245aeffc1'),
	(40,'Category',1,0,'2015-12-20 20:30:17','2015-12-20 20:30:17','5ae5dba9-d406-4935-86f9-9215cfe61a50'),
	(41,'Category',1,0,'2015-12-20 20:31:04','2015-12-20 20:31:04','d34801e1-0ded-4495-a238-3fd98a36a23b'),
	(42,'Category',1,0,'2015-12-20 20:31:22','2015-12-20 20:31:22','545c0ed8-7e65-4137-8924-e2e90288eb4a'),
	(55,'MatrixBlock',1,0,'2015-12-28 00:53:16','2015-12-28 00:57:07','407ae79b-ac84-4d12-ab3f-8baefbaee7d3'),
	(56,'MatrixBlock',1,0,'2015-12-28 00:55:12','2015-12-28 00:57:07','a4ac1092-66bd-46d1-96a5-faa12ee16113'),
	(57,'MatrixBlock',1,0,'2015-12-28 00:55:54','2015-12-28 00:57:07','3f141201-319a-4ae0-bc4f-b5dd674d0acc'),
	(58,'MatrixBlock',1,0,'2015-12-28 00:57:07','2015-12-28 00:57:07','63f912b2-5564-4504-ab56-c85e104f3de6'),
	(59,'MatrixBlock',1,0,'2015-12-28 00:57:53','2015-12-28 01:03:42','28d1904a-7bf8-4f84-85c3-7678094928ba'),
	(60,'MatrixBlock',1,0,'2015-12-28 01:04:55','2015-12-28 01:23:47','182f90ff-8d9c-403b-bce9-e7f23964242f'),
	(61,'Entry',1,0,'2015-12-28 01:08:39','2015-12-28 01:18:41','2eec7150-0da7-4b79-8848-33f7231faed0'),
	(62,'MatrixBlock',1,0,'2015-12-28 01:08:39','2015-12-28 01:15:49','6cd4d4ae-61ec-47b3-85fb-d9f25dcf6a8f'),
	(63,'MatrixBlock',1,0,'2015-12-28 01:27:07','2015-12-28 01:29:00','5f253cca-ffbc-4c4c-88c7-4b0279bc8c89'),
	(65,'MatrixBlock',1,0,'2015-12-28 01:29:55','2015-12-28 01:29:55','79dd0ef7-5c33-463a-a42f-f7b7a33fe3ab'),
	(66,'MatrixBlock',1,0,'2015-12-28 01:30:06','2015-12-28 01:30:06','77d70e0e-2d2c-4087-9b3e-38eadd709101');

/*!40000 ALTER TABLE `elements` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table elements_i18n
# ------------------------------------------------------------

DROP TABLE IF EXISTS `elements_i18n`;

CREATE TABLE `elements_i18n` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `elements_i18n_elementId_locale_unq_idx` (`elementId`,`locale`),
  UNIQUE KEY `elements_i18n_uri_locale_unq_idx` (`uri`,`locale`),
  KEY `elements_i18n_slug_locale_idx` (`slug`,`locale`),
  KEY `elements_i18n_enabled_idx` (`enabled`),
  KEY `elements_i18n_locale_fk` (`locale`),
  CONSTRAINT `elements_i18n_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `elements_i18n_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `elements_i18n` WRITE;
/*!40000 ALTER TABLE `elements_i18n` DISABLE KEYS */;

INSERT INTO `elements_i18n` (`id`, `elementId`, `locale`, `slug`, `uri`, `enabled`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'en_us','',NULL,1,'2015-12-19 00:45:33','2015-12-28 00:48:24','af26eb21-c99e-4a2b-bf09-26edd196cf21'),
	(2,2,'en_us','homepage','__home__',1,'2015-12-19 00:45:35','2015-12-28 01:23:47','f1e8c5c7-3dba-4e4c-bbca-b28472687691'),
	(3,3,'en_us','we-just-installed-craft','news/2015/we-just-installed-craft',1,'2015-12-19 00:45:35','2015-12-19 00:45:35','9350db08-b003-48c5-8364-30e0ea306b2d'),
	(4,4,'en_us','properties','properties',1,'2015-12-19 00:52:08','2015-12-28 01:30:31','a394b53a-7672-4074-a438-1367490dd540'),
	(5,5,'en_us','partners','partners',1,'2015-12-19 00:53:06','2015-12-28 01:29:00','fe405578-0f43-460b-8ad1-7930c9315f4f'),
	(6,6,'en_us','team','team',1,'2015-12-19 00:54:13','2015-12-19 01:46:50','2c21a83e-5b84-4e8d-beb9-0d13fb461fd8'),
	(7,7,'en_us','reviews','testimonials',1,'2015-12-19 00:54:52','2015-12-19 02:29:20','ec85b961-a63a-49b9-ba85-cdbc11c35c7f'),
	(8,8,'en_us','reservation-success','truck/reserved',1,'2015-12-19 00:55:30','2015-12-19 02:41:05','a188cdb0-fc88-4857-8eba-2f4cee56af1a'),
	(9,9,'en_us','truck','truck',1,'2015-12-19 01:05:51','2015-12-21 20:41:59','5aead5c8-c135-4cbc-8aac-5059b16878f7'),
	(13,13,'en_us','one-of-mark-charters-greatest-characteristics-is-his-giving-nature',NULL,1,'2015-12-19 02:30:36','2015-12-19 02:36:01','aebb5356-8f52-4b91-b66c-0e16fd343bf6'),
	(14,14,'en_us','i-was-so-lucky-to-find-them',NULL,1,'2015-12-19 02:34:50','2015-12-19 02:36:10','0b04e134-8634-4ca8-a5a4-52bd45fe6ce8'),
	(15,15,'en_us','mark-was-phenomenal-to-work-with',NULL,1,'2015-12-19 02:35:26','2015-12-19 02:36:08','c7810fb0-54d7-40d7-8f88-3c3b0761e922'),
	(17,17,'en_us','amazing-golf-course-home-with-too-many-amenities-to-list','properties/amazing-golf-course-home-with-too-many-amenities-to-list',1,'2015-12-19 02:52:17','2015-12-28 01:29:55','e4fa6a6b-4f34-4c8d-acda-06efe696fc96'),
	(18,18,'en_us','absolutely-amazing-modern-rural-meets-urban-set-up-just-outside-of-grimes','properties/absolutely-amazing-modern-rural-meets-urban-set-up-just-outside-of-grimes',1,'2015-12-19 15:30:35','2015-12-28 01:30:06','ef41d744-2283-4beb-ba71-30715fb68df8'),
	(19,19,'en_us','',NULL,1,'2015-12-19 21:06:00','2015-12-20 04:19:06','991ad00a-891b-4892-b1ba-e6008a40e49d'),
	(20,20,'en_us','',NULL,1,'2015-12-20 04:19:31','2015-12-20 04:19:31','fd7f7628-d347-407e-be76-fb5b5e3dfda8'),
	(21,21,'en_us','contact','contact',1,'2015-12-20 04:23:50','2015-12-20 04:26:39','951b03c9-14b5-4be2-89cd-f8f06c158ef8'),
	(22,22,'en_us','ways-to-sell','ways-to-sell',1,'2015-12-20 04:39:21','2015-12-20 14:56:25','cc121041-bac3-4d5e-8b58-299e21e7ec2e'),
	(23,23,'en_us','pro-and-go',NULL,1,'2015-12-20 04:45:14','2015-12-20 14:46:46','6b56c4c7-61e3-4008-b90d-97ceb8f5fe8d'),
	(25,25,'en_us','',NULL,1,'2015-12-20 04:53:04','2015-12-20 04:53:14','b507b549-468f-4ae1-8daf-d86124eaea0e'),
	(26,26,'en_us','',NULL,1,'2015-12-20 04:53:04','2015-12-20 04:53:15','cc8a64e7-0609-453b-be56-c02bb47fba46'),
	(27,27,'en_us','',NULL,1,'2015-12-20 04:53:04','2015-12-20 04:53:15','fe39a9c7-3e91-4c63-aff9-73cd2d426b20'),
	(28,28,'en_us','pro-plus',NULL,1,'2015-12-20 04:53:50','2015-12-20 14:50:30','1030afe4-f1cd-42e8-92f7-162145faac07'),
	(29,29,'en_us','',NULL,1,'2015-12-20 04:53:50','2015-12-20 04:54:19','986a44e7-7d39-488b-a711-c3e6bc9d196a'),
	(30,30,'en_us','',NULL,1,'2015-12-20 04:53:50','2015-12-20 04:54:19','0a0872ce-62dd-4844-8a8c-848e6dd0a533'),
	(31,31,'en_us','',NULL,1,'2015-12-20 04:54:19','2015-12-20 04:54:19','51475c39-6f61-48cd-9eda-c7783cbdefa7'),
	(32,32,'en_us','pro-elite',NULL,1,'2015-12-20 04:55:11','2015-12-20 15:02:56','81705993-30b0-4ebc-9267-847e51c35b0f'),
	(33,33,'en_us','',NULL,1,'2015-12-20 04:55:11','2015-12-20 04:55:11','aed4123a-8ddc-4fa9-abc0-2cd9cf2b77b0'),
	(34,34,'en_us','',NULL,1,'2015-12-20 04:55:11','2015-12-20 04:55:11','24579d5d-be9e-40b7-b357-72534236bd07'),
	(35,35,'en_us','',NULL,1,'2015-12-20 04:55:11','2015-12-20 04:55:11','eff3c712-42f1-4fb2-abb2-bc3d61bdb658'),
	(36,36,'en_us','buyers-sellers','buyers-and-sellers',1,'2015-12-20 05:00:51','2015-12-28 01:03:42','f42aaf3f-af71-4a75-9dae-7ba604e2d68b'),
	(37,37,'en_us','',NULL,1,'2015-12-20 14:56:56','2015-12-20 15:00:50','4004a54f-5b0a-46e1-b248-3038f5fb5d24'),
	(38,38,'en_us','about','about',1,'2015-12-20 20:03:09','2015-12-28 00:57:07','9365084e-5d82-4dab-9b08-879771588aee'),
	(39,39,'en_us','mark-charter',NULL,1,'2015-12-20 20:27:05','2015-12-20 20:29:17','e6b12353-9da3-4c1f-ac51-797f8dc25d97'),
	(40,40,'en_us','amy-meyer',NULL,1,'2015-12-20 20:30:17','2015-12-20 20:30:18','02448b30-e3b1-4ea1-a3c6-fe88f4c2b3f9'),
	(41,41,'en_us','nic-meyer',NULL,1,'2015-12-20 20:31:04','2015-12-20 20:31:05','8dce9b00-f04d-49ca-9235-62c58f0909d7'),
	(42,42,'en_us','jake-boyd-photo',NULL,1,'2015-12-20 20:31:22','2015-12-20 20:31:23','129def54-ce0d-4113-bfcf-41e2772e0d9b'),
	(55,55,'en_us','',NULL,1,'2015-12-28 00:53:16','2015-12-28 00:57:07','7f491772-f745-4ec6-8bf9-82f91f7b7399'),
	(56,56,'en_us','',NULL,1,'2015-12-28 00:55:12','2015-12-28 00:57:07','882da4bb-46b7-48fe-b87c-7f5e8e2af0a3'),
	(57,57,'en_us','',NULL,1,'2015-12-28 00:55:54','2015-12-28 00:57:07','5c53a104-7ae6-47c7-a732-5095f5b22d15'),
	(58,58,'en_us','',NULL,1,'2015-12-28 00:57:07','2015-12-28 00:57:07','b23e4287-e3a7-440e-90fb-ca28f38e5888'),
	(59,59,'en_us','',NULL,1,'2015-12-28 00:57:53','2015-12-28 01:03:42','dea57ef1-147d-4093-85f7-c8784f95cbb2'),
	(60,60,'en_us','',NULL,1,'2015-12-28 01:04:55','2015-12-28 01:23:47','99fae7e0-07de-41fa-b232-daabf3e0068d'),
	(61,61,'en_us','mortgage-the-tyler-osby-team',NULL,1,'2015-12-28 01:08:39','2015-12-28 01:18:41','63d70650-6588-4383-99cf-8829df6253e9'),
	(62,62,'en_us','',NULL,1,'2015-12-28 01:08:39','2015-12-28 01:15:49','b8977253-ba1f-46ff-a06d-70be5e957f68'),
	(63,63,'en_us','',NULL,1,'2015-12-28 01:27:07','2015-12-28 01:29:00','7727fd40-66ec-4c35-8f65-98db0ffb9ac4'),
	(65,65,'en_us','',NULL,1,'2015-12-28 01:29:55','2015-12-28 01:29:55','529fa7c7-0814-45a3-abb5-4866b5fd0329'),
	(66,66,'en_us','',NULL,1,'2015-12-28 01:30:06','2015-12-28 01:30:06','9a5b5f56-c46d-4796-bdce-1b8c1ecbd875');

/*!40000 ALTER TABLE `elements_i18n` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table emailmessages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `emailmessages`;

CREATE TABLE `emailmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` char(150) COLLATE utf8_unicode_ci NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `emailmessages_key_locale_unq_idx` (`key`,`locale`),
  KEY `emailmessages_locale_fk` (`locale`),
  CONSTRAINT `emailmessages_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table entries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `entries`;

CREATE TABLE `entries` (
  `id` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `typeId` int(11) DEFAULT NULL,
  `authorId` int(11) DEFAULT NULL,
  `postDate` datetime DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `entries_sectionId_idx` (`sectionId`),
  KEY `entries_typeId_idx` (`typeId`),
  KEY `entries_postDate_idx` (`postDate`),
  KEY `entries_expiryDate_idx` (`expiryDate`),
  KEY `entries_authorId_fk` (`authorId`),
  CONSTRAINT `entries_authorId_fk` FOREIGN KEY (`authorId`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entries_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entries_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entries_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `entrytypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `entries` WRITE;
/*!40000 ALTER TABLE `entries` DISABLE KEYS */;

INSERT INTO `entries` (`id`, `sectionId`, `typeId`, `authorId`, `postDate`, `expiryDate`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(2,1,NULL,NULL,'2015-12-19 00:45:35',NULL,'2015-12-19 00:45:35','2015-12-28 01:23:47','1a78c8ca-dc4c-4ce8-a652-13e1558b79df'),
	(3,2,2,1,'2015-12-19 00:45:35',NULL,'2015-12-19 00:45:35','2015-12-19 00:45:35','abe1e365-de2e-46f4-8ac0-3849f263ab37'),
	(4,5,5,NULL,'2015-12-19 01:01:21',NULL,'2015-12-19 00:52:08','2015-12-28 01:30:31','80ad2a46-0790-4108-861a-5b18dd42ee28'),
	(5,6,NULL,NULL,'2015-12-19 00:53:06',NULL,'2015-12-19 00:53:06','2015-12-28 01:29:00','1af7d165-ef79-4b2a-a9b0-b92c71d0f1b5'),
	(6,7,NULL,NULL,'2015-12-19 00:54:13',NULL,'2015-12-19 00:54:13','2015-12-19 01:46:50','d59cec94-a02b-4808-8325-77c1f466621d'),
	(7,8,8,NULL,'2015-12-19 02:26:44',NULL,'2015-12-19 00:54:52','2015-12-19 02:29:20','3a58ec4b-87df-4bf7-9b78-f681af7cb3a5'),
	(8,9,NULL,NULL,'2015-12-19 00:55:30',NULL,'2015-12-19 00:55:30','2015-12-19 02:41:05','bb3c93c6-0089-4c71-9d0d-b8d608a1768e'),
	(9,10,NULL,NULL,'2015-12-19 01:05:51',NULL,'2015-12-19 01:05:51','2015-12-21 20:41:59','8053aa92-b6dd-4d44-9e01-7ae1d1d31af5'),
	(13,11,11,1,'2015-12-19 02:30:00',NULL,'2015-12-19 02:30:36','2015-12-19 02:32:09','1ea48a5e-b304-4218-a8af-6c066314dc9a'),
	(14,11,11,1,'2015-12-19 02:34:49',NULL,'2015-12-19 02:34:50','2015-12-19 02:34:50','86e7f9be-0874-46b0-9f46-40b0c838d367'),
	(15,11,11,1,'2015-12-19 02:35:26',NULL,'2015-12-19 02:35:26','2015-12-19 02:35:26','d1590391-cf16-47e4-aac9-fa09cb06cf84'),
	(17,4,4,1,'2015-12-19 02:52:00',NULL,'2015-12-19 02:52:17','2015-12-28 01:29:55','affd115c-2d4a-428b-829f-b27ff990422f'),
	(18,4,4,1,'2015-12-19 15:30:00',NULL,'2015-12-19 15:30:35','2015-12-28 01:30:06','70be5c17-3380-4563-a0fb-37241b9a0870'),
	(21,12,NULL,NULL,'2015-12-20 04:23:50',NULL,'2015-12-20 04:23:50','2015-12-20 04:26:39','52f3e974-668e-4110-b5ce-c445e1f5f2f7'),
	(22,13,NULL,NULL,'2015-12-20 04:39:21',NULL,'2015-12-20 04:39:21','2015-12-20 14:56:25','9115c449-c946-4407-92cb-1f63bd078782'),
	(36,14,NULL,NULL,'2015-12-20 05:00:50',NULL,'2015-12-20 05:00:51','2015-12-28 01:03:42','2dc853ac-8943-495c-82ba-55f9f93f869d'),
	(38,15,NULL,NULL,'2015-12-20 20:03:09',NULL,'2015-12-20 20:03:09','2015-12-28 00:57:07','ccb9f7c9-3981-4b80-9997-9de6d046c086'),
	(61,16,16,1,'2015-12-28 01:08:00',NULL,'2015-12-28 01:08:39','2015-12-28 01:18:41','661d258f-3ee0-4262-963b-ab1179bf0486');

/*!40000 ALTER TABLE `entries` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table entrydrafts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `entrydrafts`;

CREATE TABLE `entrydrafts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entryId` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `creatorId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notes` tinytext COLLATE utf8_unicode_ci,
  `data` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `entrydrafts_entryId_locale_idx` (`entryId`,`locale`),
  KEY `entrydrafts_sectionId_fk` (`sectionId`),
  KEY `entrydrafts_creatorId_fk` (`creatorId`),
  KEY `entrydrafts_locale_fk` (`locale`),
  CONSTRAINT `entrydrafts_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entrydrafts_entryId_fk` FOREIGN KEY (`entryId`) REFERENCES `entries` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entrydrafts_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `entrydrafts_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table entrytypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `entrytypes`;

CREATE TABLE `entrytypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sectionId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hasTitleField` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `titleLabel` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'Title',
  `titleFormat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sortOrder` tinyint(4) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `entrytypes_name_sectionId_unq_idx` (`name`,`sectionId`),
  UNIQUE KEY `entrytypes_handle_sectionId_unq_idx` (`handle`,`sectionId`),
  KEY `entrytypes_sectionId_fk` (`sectionId`),
  KEY `entrytypes_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `entrytypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `entrytypes_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `entrytypes` WRITE;
/*!40000 ALTER TABLE `entrytypes` DISABLE KEYS */;

INSERT INTO `entrytypes` (`id`, `sectionId`, `fieldLayoutId`, `name`, `handle`, `hasTitleField`, `titleLabel`, `titleFormat`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,92,'Homepage','homepage',1,'Page Title',NULL,NULL,'2015-12-19 00:45:35','2015-12-28 01:05:05','ad406361-f89d-4fb0-8e80-127cfbe42bd9'),
	(2,2,5,'News','news',1,'Title',NULL,NULL,'2015-12-19 00:45:35','2015-12-19 00:45:35','e6ee15f0-82d3-4231-9892-226df210b0e9'),
	(3,3,18,'Reservation','reservation',1,'Full Name',NULL,NULL,'2015-12-19 00:51:17','2015-12-19 01:42:03','a2354d94-bcf8-47d3-a679-33fae058f7d6'),
	(4,4,99,'Property','property',1,'Page Title',NULL,NULL,'2015-12-19 00:51:40','2015-12-28 01:29:18','a8d33aaf-73a6-4e19-8602-8054a92c44de'),
	(5,5,20,'Properties','properties',1,'Page Title',NULL,NULL,'2015-12-19 00:52:08','2015-12-19 01:44:04','baf10729-1552-4951-a280-e6265a6b54a8'),
	(6,6,98,'Partners','partners',1,'Page Title',NULL,NULL,'2015-12-19 00:53:06','2015-12-28 01:27:19','f172a8c6-ff7c-4078-889b-8b455e73a406'),
	(7,7,25,'Team','team',1,'Page Title',NULL,NULL,'2015-12-19 00:54:13','2015-12-19 01:45:41','0ce21136-40b3-4969-a3d7-12259bf8c2ef'),
	(8,8,24,'Reviews','reviews',1,'Page Title',NULL,NULL,'2015-12-19 00:54:52','2015-12-19 01:45:23','58fdb35e-2342-46ae-8c31-a49bba0a15ca'),
	(9,9,22,'Reservation Success','reservationSuccess',1,'Page Title',NULL,NULL,'2015-12-19 00:55:30','2015-12-19 01:44:56','5c6aa4e7-6a20-4884-9053-d5fd5e8877d3'),
	(10,10,81,'Truck','truck',1,'Page Title',NULL,NULL,'2015-12-19 01:05:51','2015-12-21 20:41:28','2b7dd49d-fc4f-4d09-8c92-29014ff0f093'),
	(11,11,32,'Testimonial','testimonial',1,'Headline',NULL,NULL,'2015-12-19 02:24:29','2015-12-19 02:31:59','84020596-a811-4362-ac7e-4dc5db4901c2'),
	(12,12,90,'Contact','contact',1,'Page Title',NULL,NULL,'2015-12-20 04:23:50','2015-12-28 01:04:27','a6319675-048b-44fc-af89-f18e73847ee7'),
	(13,13,101,'Ways To Sell','waysToSell',1,'Page Title',NULL,NULL,'2015-12-20 04:39:21','2015-12-28 01:31:24','1c9ac519-eec9-4290-86dc-63244df89afa'),
	(14,14,88,'Buyers & Sellers','buyersSellers',1,'Page Title',NULL,NULL,'2015-12-20 05:00:50','2015-12-28 00:58:09','a78941ff-1a23-4ae3-acff-9f28a5f0ff2c'),
	(15,15,86,'About','about',1,'Page Title',NULL,NULL,'2015-12-20 20:03:09','2015-12-28 00:53:46','ec23d35f-d212-4806-8ab6-4c8974483556'),
	(16,16,97,'Partner','partner',1,'Page Title',NULL,NULL,'2015-12-28 01:06:31','2015-12-28 01:16:04','5faf26a3-adc9-48b2-b957-202520d326b6');

/*!40000 ALTER TABLE `entrytypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table entryversions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `entryversions`;

CREATE TABLE `entryversions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entryId` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `creatorId` int(11) DEFAULT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `num` smallint(6) unsigned NOT NULL,
  `notes` tinytext COLLATE utf8_unicode_ci,
  `data` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `entryversions_entryId_locale_idx` (`entryId`,`locale`),
  KEY `entryversions_sectionId_fk` (`sectionId`),
  KEY `entryversions_creatorId_fk` (`creatorId`),
  KEY `entryversions_locale_fk` (`locale`),
  CONSTRAINT `entryversions_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `entryversions_entryId_fk` FOREIGN KEY (`entryId`) REFERENCES `entries` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entryversions_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `entryversions_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `entryversions` WRITE;
/*!40000 ALTER TABLE `entryversions` DISABLE KEYS */;

INSERT INTO `entryversions` (`id`, `entryId`, `sectionId`, `creatorId`, `locale`, `num`, `notes`, `data`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,2,1,1,'en_us',1,NULL,'{\"typeId\":\"1\",\"authorId\":null,\"title\":\"Homepage\",\"slug\":\"homepage\",\"postDate\":1450485935,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":[]}','2015-12-19 00:45:35','2015-12-19 00:45:35','40a987ce-14ba-4a6b-955e-3c6a38422e18'),
	(2,2,1,1,'en_us',2,NULL,'{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.charterhouseiowa.dev!\",\"slug\":\"homepage\",\"postDate\":1450485935,\"expiryDate\":null,\"enabled\":\"1\",\"parentId\":null,\"fields\":{\"1\":\"<p>It\\u2019s true, this site doesn\\u2019t have a whole lot of content yet, but don\\u2019t worry. Our web developers have just installed the CMS, and they\\u2019re setting things up for the content editors this very moment. Soon Local.charterhouseiowa.dev will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.<\\/p>\"}}','2015-12-19 00:45:35','2015-12-19 00:45:35','3f461505-42e8-4114-89a4-bde4576a633f'),
	(3,3,2,1,'en_us',1,NULL,'{\"typeId\":\"2\",\"authorId\":\"1\",\"title\":\"We just installed Craft!\",\"slug\":\"we-just-installed-craft\",\"postDate\":1450485935,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":[]}','2015-12-19 00:45:35','2015-12-19 00:45:35','f6debf29-52d7-477f-a777-c8f952d02d93'),
	(4,4,5,1,'en_us',1,NULL,'{\"typeId\":\"5\",\"authorId\":null,\"title\":\"Properties\",\"slug\":\"properties\",\"postDate\":1450486328,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":[]}','2015-12-19 00:52:08','2015-12-19 00:52:08','13cb317d-e5f5-4cfb-8c9a-846b43cfd914'),
	(5,5,6,1,'en_us',1,NULL,'{\"typeId\":\"6\",\"authorId\":null,\"title\":\"Partners\",\"slug\":\"partners\",\"postDate\":1450486386,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":[]}','2015-12-19 00:53:06','2015-12-19 00:53:06','fd35ec51-01a5-44e6-aa7f-1b85b5fbf93b'),
	(6,6,7,1,'en_us',1,NULL,'{\"typeId\":\"7\",\"authorId\":null,\"title\":\"Team\",\"slug\":\"team\",\"postDate\":1450486453,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":[]}','2015-12-19 00:54:13','2015-12-19 00:54:13','0c5cf14d-5ff7-466f-aba7-c142cef411dc'),
	(7,7,8,1,'en_us',1,NULL,'{\"typeId\":\"8\",\"authorId\":null,\"title\":\"Reviews\",\"slug\":\"reviews\",\"postDate\":1450486492,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":[]}','2015-12-19 00:54:52','2015-12-19 00:54:52','0f2e160a-4b2d-48dd-a565-fba9d33757fe'),
	(8,8,9,1,'en_us',1,NULL,'{\"typeId\":\"9\",\"authorId\":null,\"title\":\"Reservation Success\",\"slug\":\"reservation-success\",\"postDate\":1450486530,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":[]}','2015-12-19 00:55:30','2015-12-19 00:55:30','49838030-29df-4c17-9f5d-cb263e1760d5'),
	(9,9,10,1,'en_us',1,NULL,'{\"typeId\":\"10\",\"authorId\":null,\"title\":\"Truck\",\"slug\":\"truck\",\"postDate\":1450487151,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":[]}','2015-12-19 01:05:51','2015-12-19 01:05:51','203b38d8-9cd6-485d-b467-0b78d97c3159'),
	(12,2,1,1,'en_us',3,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Mark Charter Realty\",\"slug\":\"homepage\",\"postDate\":1450485935,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>It\\u2019s true, this site doesn\\u2019t have a whole lot of content yet, but don\\u2019t worry. Our web developers have just installed the CMS, and they\\u2019re setting things up for the content editors this very moment. Soon Local.charterhouseiowa.dev will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.<\\/p>\",\"3\":\"\"}}','2015-12-19 01:46:32','2015-12-19 01:46:32','c68de794-9c7d-4a25-91bb-9e57457b6c5a'),
	(13,4,5,1,'en_us',2,'','{\"typeId\":\"5\",\"authorId\":null,\"title\":\"Properties\",\"slug\":\"properties\",\"postDate\":1450486881,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>It\\u2019s true, this site doesn\\u2019t have a whole lot of content yet, but don\\u2019t worry. Our web developers have just installed the CMS, and they\\u2019re setting things up for the content editors this very moment. Soon Local.charterhouseiowa.dev will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.<\\/p>\",\"3\":\"\"}}','2015-12-19 01:46:42','2015-12-19 01:46:42','ecf7f226-3abb-469f-8d13-66e26ed750a1'),
	(14,6,7,1,'en_us',2,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Team\",\"slug\":\"team\",\"postDate\":1450486453,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>It\\u2019s true, this site doesn\\u2019t have a whole lot of content yet, but don\\u2019t worry. Our web developers have just installed the CMS, and they\\u2019re setting things up for the content editors this very moment. Soon Local.charterhouseiowa.dev will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.<\\/p>\",\"3\":\"\"}}','2015-12-19 01:46:50','2015-12-19 01:46:50','e4710c54-58d6-4fc4-a56a-c87da5dc0dbb'),
	(15,7,8,1,'en_us',2,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Reviews\",\"slug\":\"reviews\",\"postDate\":1450486492,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>It\\u2019s true, this site doesn\\u2019t have a whole lot of content yet, but don\\u2019t worry. Our web developers have just installed the CMS, and they\\u2019re setting things up for the content editors this very moment. Soon Local.charterhouseiowa.dev will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.<\\/p>\",\"3\":\"\"}}','2015-12-19 01:47:00','2015-12-19 01:47:00','3bdbb7be-3230-49e6-aa9f-9ff3198765f4'),
	(16,5,6,1,'en_us',2,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Partners\",\"slug\":\"partners\",\"postDate\":1450486386,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>It\\u2019s true, this site doesn\\u2019t have a whole lot of content yet, but don\\u2019t worry. Our web developers have just installed the CMS, and they\\u2019re setting things up for the content editors this very moment. Soon Local.charterhouseiowa.dev will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.<\\/p>\",\"3\":\"\"}}','2015-12-19 01:47:10','2015-12-19 01:47:10','91f5a661-a43f-489e-8fc0-919c941251ee'),
	(17,7,8,1,'en_us',3,'','{\"typeId\":\"8\",\"authorId\":null,\"title\":\"Testimonials\",\"slug\":\"reviews\",\"postDate\":1450492004,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>It\\u2019s true, this site doesn\\u2019t have a whole lot of content yet, but don\\u2019t worry. Our web developers have just installed the CMS, and they\\u2019re setting things up for the content editors this very moment. Soon Local.charterhouseiowa.dev will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.<\\/p>\",\"3\":\"\"}}','2015-12-19 02:29:20','2015-12-19 02:29:20','21078f3a-4277-4e20-9a92-ecb3bcdf3461'),
	(18,13,11,1,'en_us',1,'','{\"typeId\":null,\"authorId\":\"1\",\"title\":\"One of Mark Charter\\u2019s greatest characteristics is his giving nature.\",\"slug\":\"one-of-mark-charters-greatest-characteristics-is-his-giving-nature\",\"postDate\":1450492236,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>One of Mark Charter\'s greatest characteristics is his giving nature. He always goes above and beyond for his clients to make sure that their experience in buying or selling a home is first rate. While not just an experienced real estate agent, Mark is also extremely knowledgeable about the investment real estate market as well. He is a career agent that absolutely, without a doubt, has your best interests in mind.<\\/p>\",\"3\":\"\"}}','2015-12-19 02:30:36','2015-12-19 02:30:36','7f8b4bc6-bd13-435f-b83a-b2be9a367963'),
	(19,13,11,1,'en_us',2,'','{\"typeId\":\"11\",\"authorId\":\"1\",\"title\":\"One of Mark Charter\\u2019s greatest characteristics is his giving nature.\",\"slug\":\"one-of-mark-charters-greatest-characteristics-is-his-giving-nature\",\"postDate\":1450492200,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>One of Mark Charter\'s greatest characteristics is his giving nature. He always goes above and beyond for his clients to make sure that their experience in buying or selling a home is first rate. While not just an experienced real estate agent, Mark is also extremely knowledgeable about the investment real estate market as well. He is a career agent that absolutely, without a doubt, has your best interests in mind.<\\/p>\",\"3\":\"\",\"15\":\"Adam Carroll\"}}','2015-12-19 02:32:09','2015-12-19 02:32:09','2587d7b9-2f20-4f9c-8f9f-edd9077807f9'),
	(20,14,11,1,'en_us',1,'','{\"typeId\":null,\"authorId\":\"1\",\"title\":\"I was so lucky to find them!\",\"slug\":\"i-was-so-lucky-to-find-them\",\"postDate\":1450492489,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>Before finding Mark and Amy, I had had a lot of bad experiences with realtors. From the minute I contacted them about possibly being my realtor, I was in constant communication with them--I was never left wondering what was going on! I was paired with Amy and she is FANTASTIC. She listened to not only what I was looking for, but what I was going to use spaces for and looked for those things in the places I wanted to view. She helped me to pro and con the spaces I was looking on based on what she had heard me say about other spaces. She was in constant contact with me about setting up showings and if there was a delay in getting an answer from someone, was letting me know what the hold up was.&nbsp;<\\/p><p>I have never met a more caring team to work with and when they say you\'re part of the family now, you are! As a first time home buyer, I had lots of questions both for Amy and for the seller and I got every single one answered. I never once felt like there was another goal that the team was working for other than to make sure I was happy with what I was getting! While the people are top notch, it\'s also amazing to me that you can use the moving truck for free and they had my new home cleaned before I moved in! It saved me so much time and hassle, not to mention cost. I was so lucky to find them!<\\/p>\",\"3\":\"\",\"15\":\"Bethany Wilcke\"}}','2015-12-19 02:34:50','2015-12-19 02:34:50','dc686c9a-11b9-408e-9f08-9ba096f1556a'),
	(21,15,11,1,'en_us',1,'','{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Mark was phenomenal to work with.\",\"slug\":\"mark-was-phenomenal-to-work-with\",\"postDate\":1450492526,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>Mark was phenomenal to work with during the sale of our home. His no-nonsense, tell-it-how-it is attitude is the only reason we got our home sold! We had a unique situation with our home based on the size, garage and price - Mark was the only person that told us what we needed to hear (instead of just fluffing us with what we wanted to hear). Mark is a great communicator and we always knew what was expected of us through the entire process. Thank you, Mark!<\\/p>\",\"3\":\"\",\"15\":\"Jeremy Orr\"}}','2015-12-19 02:35:26','2015-12-19 02:35:26','7a88432f-40a3-4ad3-a25d-6b0b95a539c5'),
	(25,8,9,1,'en_us',2,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Reservation Success\",\"slug\":\"reservation-success\",\"postDate\":1450486530,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>We got ya\' covered - and you got my truck.<\\/p>\",\"3\":\"\"}}','2015-12-19 02:41:05','2015-12-19 02:41:05','72396450-deee-4330-a479-660185321ddf'),
	(26,17,4,1,'en_us',1,'','{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Amazing golf course home with too many amenities to list!\",\"slug\":\"amazing-golf-course-home-with-too-many-amenities-to-list\",\"postDate\":1450493537,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"1210 NE 42nd St\",\"street2\":\"\",\"city\":\"Ankeny\",\"state\":\"IA\",\"zip\":\"50021\",\"country\":\"United States\",\"lat\":\"41.7657585\",\"lng\":\"-93.5865468\"},\"8\":\"4.5\",\"9\":\"5\",\"1\":\"<p>Stunning Ankeny home located on Otter Creek Golf Course. This 4 bed, 4.5 bath former Show Home is loaded with features and amenities from top to bottom. Try building this home today for less than a million dollars! It all starts with location. You will love cul-de-sac living, close to I35 making a commute a breeze. Step through the front door and you will be wowed by the view! There is not one, but TWO ponds directly behind the home. The outside is just as gorgeous as the inside.&nbsp;<\\/p><p>Full landscaping, flagstone entry, exterior lighting, huge patio and a wraparound deck with outdoor kitchen truly put this home among Ankeny\'s best! Inside the quality is top notch. Beautiful knotty alder trim, Acacia hardwood, Viking appliances, and a beautiful stone fireplace are impossible to miss. The master is an oasis, with beautiful materials. Need an amazing basement? Sunken family room, barn wood floors, huge wet bar, hidden room and golf cart garage await. Too much to list, Email for all features!<\\/p>\",\"10\":\"\",\"11\":\"\",\"3\":\"\",\"12\":\"\",\"13\":\"\",\"14\":\"2,426\"}}','2015-12-19 02:52:17','2015-12-19 02:52:17','06da6f8b-92e6-4c16-9194-7eddc4385ffd'),
	(27,17,4,1,'en_us',2,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Amazing golf course home with too many amenities to list!\",\"slug\":\"amazing-golf-course-home-with-too-many-amenities-to-list\",\"postDate\":1450493520,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"1210 NE 42nd St\",\"street2\":\"\",\"city\":\"Ankeny\",\"state\":\"IA\",\"zip\":\"50021\",\"country\":\"United States\",\"lat\":\"41.76575850\",\"lng\":\"-93.58654680\"},\"8\":\"4.5\",\"9\":\"5\",\"1\":\"<p>Stunning Ankeny home located on Otter Creek Golf Course. This 4 bed, 4.5 bath former Show Home is loaded with features and amenities from top to bottom. Try building this home today for less than a million dollars! It all starts with location. You will love cul-de-sac living, close to I35 making a commute a breeze. Step through the front door and you will be wowed by the view! There is not one, but TWO ponds directly behind the home. The outside is just as gorgeous as the inside.&nbsp;<\\/p>\\r\\n\\r\\n<p>Full landscaping, flagstone entry, exterior lighting, huge patio and a wraparound deck with outdoor kitchen truly put this home among Ankeny\'s best! Inside the quality is top notch. Beautiful knotty alder trim, Acacia hardwood, Viking appliances, and a beautiful stone fireplace are impossible to miss. The master is an oasis, with beautiful materials. Need an amazing basement? Sunken family room, barn wood floors, huge wet bar, hidden room and golf cart garage await. Too much to list, Email for all features!<\\/p>\",\"10\":\"\",\"11\":\"\",\"3\":\"\",\"12\":\"\",\"13\":\"\",\"14\":\"2,426\"}}','2015-12-19 03:12:28','2015-12-19 03:12:28','d174e801-531b-4053-80f6-a9caecb75569'),
	(28,17,4,1,'en_us',3,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Amazing golf course home with too many amenities to list!\",\"slug\":\"amazing-golf-course-home-with-too-many-amenities-to-list\",\"postDate\":1450493520,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"1210 NE 42nd St\",\"street2\":\"\",\"city\":\"Ankeny\",\"state\":\"IA\",\"zip\":\"50021\",\"country\":\"United States\",\"lat\":\"41.76575850\",\"lng\":\"-93.58654680\"},\"8\":\"4.5\",\"9\":\"5\",\"1\":\"<p>Stunning Ankeny home located on Otter Creek Golf Course. This 4 bed, 4.5 bath former Show Home is loaded with features and amenities from top to bottom. Try building this home today for less than a million dollars! It all starts with location. You will love cul-de-sac living, close to I35 making a commute a breeze. Step through the front door and you will be wowed by the view! There is not one, but TWO ponds directly behind the home. The outside is just as gorgeous as the inside.&nbsp;<\\/p>\\r\\n\\r\\n<p>Full landscaping, flagstone entry, exterior lighting, huge patio and a wraparound deck with outdoor kitchen truly put this home among Ankeny\'s best! Inside the quality is top notch. Beautiful knotty alder trim, Acacia hardwood, Viking appliances, and a beautiful stone fireplace are impossible to miss. The master is an oasis, with beautiful materials. Need an amazing basement? Sunken family room, barn wood floors, huge wet bar, hidden room and golf cart garage await. Too much to list, Email for all features!<\\/p>\",\"10\":\"\",\"11\":\"\",\"3\":\"\",\"12\":\"singleFamily\",\"13\":\"\",\"14\":\"2,426\"}}','2015-12-19 03:13:24','2015-12-19 03:13:24','cbc470ae-1b57-493b-9153-aac7b70dbb1d'),
	(29,17,4,1,'en_us',4,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Amazing golf course home with too many amenities to list!\",\"slug\":\"amazing-golf-course-home-with-too-many-amenities-to-list\",\"postDate\":1450493520,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"1210 NE 42nd St\",\"street2\":\"\",\"city\":\"Ankeny\",\"state\":\"IA\",\"zip\":\"50021\",\"country\":\"United States\",\"lat\":\"41.76575850\",\"lng\":\"-93.58654680\"},\"8\":\"4.5\",\"9\":\"5\",\"1\":\"<p>Stunning Ankeny home located on Otter Creek Golf Course. This 4 bed, 4.5 bath former Show Home is loaded with features and amenities from top to bottom. Try building this home today for less than a million dollars! It all starts with location. You will love cul-de-sac living, close to I35 making a commute a breeze. Step through the front door and you will be wowed by the view! There is not one, but TWO ponds directly behind the home. The outside is just as gorgeous as the inside.&nbsp;<\\/p>\\r\\n\\r\\n<p>Full landscaping, flagstone entry, exterior lighting, huge patio and a wraparound deck with outdoor kitchen truly put this home among Ankeny\'s best! Inside the quality is top notch. Beautiful knotty alder trim, Acacia hardwood, Viking appliances, and a beautiful stone fireplace are impossible to miss. The master is an oasis, with beautiful materials. Need an amazing basement? Sunken family room, barn wood floors, huge wet bar, hidden room and golf cart garage await. Too much to list, Email for all features!<\\/p>\",\"10\":\"\",\"16\":\"http:\\/\\/www.realtor.com\\/realestateandhomes-search?mlslid=504907\",\"11\":\"504907\",\"3\":\"\",\"12\":\"singleFamily\",\"13\":\"\",\"14\":\"2,426\"}}','2015-12-19 03:15:53','2015-12-19 03:15:53','e2952804-abf0-4394-a81e-e9767ec03978'),
	(30,17,4,1,'en_us',5,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Amazing golf course home with too many amenities to list!\",\"slug\":\"amazing-golf-course-home-with-too-many-amenities-to-list\",\"postDate\":1450493520,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"1210 NE 42nd St\",\"street2\":\"\",\"city\":\"Ankeny\",\"state\":\"IA\",\"zip\":\"50021\",\"country\":\"United States\",\"lat\":\"41.76575850\",\"lng\":\"-93.58654680\"},\"8\":\"4.5\",\"9\":\"5\",\"1\":\"<p>Stunning Ankeny home located on Otter Creek Golf Course. This 4 bed, 4.5 bath former Show Home is loaded with features and amenities from top to bottom. Try building this home today for less than a million dollars! It all starts with location. You will love cul-de-sac living, close to I35 making a commute a breeze. Step through the front door and you will be wowed by the view! There is not one, but TWO ponds directly behind the home. The outside is just as gorgeous as the inside.&nbsp;<\\/p>\\r\\n\\r\\n<p>Full landscaping, flagstone entry, exterior lighting, huge patio and a wraparound deck with outdoor kitchen truly put this home among Ankeny\'s best! Inside the quality is top notch. Beautiful knotty alder trim, Acacia hardwood, Viking appliances, and a beautiful stone fireplace are impossible to miss. The master is an oasis, with beautiful materials. Need an amazing basement? Sunken family room, barn wood floors, huge wet bar, hidden room and golf cart garage await. Too much to list, Email for all features!<\\/p>\",\"10\":\"\",\"16\":\"http:\\/\\/www.realtor.com\\/realestateandhomes-search?mlslid=504907\",\"11\":\"504907\",\"3\":\"\",\"17\":\"3 Car Garage\",\"12\":\"singleFamily\",\"13\":\"\",\"14\":\"2,426\"}}','2015-12-19 03:17:12','2015-12-19 03:17:12','29be9024-cc1a-466e-af26-d1f3a8eb67da'),
	(31,17,4,1,'en_us',6,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Amazing golf course home with too many amenities to list!\",\"slug\":\"amazing-golf-course-home-with-too-many-amenities-to-list\",\"postDate\":1450493520,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"1210 NE 42nd St\",\"street2\":\"\",\"city\":\"Ankeny\",\"state\":\"IA\",\"zip\":\"50021\",\"country\":\"United States\",\"lat\":\"41.76575850\",\"lng\":\"-93.58654680\"},\"8\":\"4.5\",\"9\":\"5\",\"1\":\"<p>Stunning Ankeny home located on Otter Creek Golf Course. This 4 bed, 4.5 bath former Show Home is loaded with features and amenities from top to bottom. Try building this home today for less than a million dollars! It all starts with location. You will love cul-de-sac living, close to I35 making a commute a breeze. Step through the front door and you will be wowed by the view! There is not one, but TWO ponds directly behind the home. The outside is just as gorgeous as the inside.&nbsp;<\\/p>\\r\\n\\r\\n<p>Full landscaping, flagstone entry, exterior lighting, huge patio and a wraparound deck with outdoor kitchen truly put this home among Ankeny\'s best! Inside the quality is top notch. Beautiful knotty alder trim, Acacia hardwood, Viking appliances, and a beautiful stone fireplace are impossible to miss. The master is an oasis, with beautiful materials. Need an amazing basement? Sunken family room, barn wood floors, huge wet bar, hidden room and golf cart garage await. Too much to list, Email for all features!<\\/p>\",\"10\":\"\",\"16\":\"http:\\/\\/www.realtor.com\\/realestateandhomes-search?mlslid=504907\",\"11\":\"504907\",\"3\":\"\",\"17\":\"3 Car Garage\",\"18\":\"$849,900\",\"12\":\"singleFamily\",\"13\":\"\",\"14\":\"2,426\",\"19\":\"yZRyXkGqD2c\"}}','2015-12-19 15:13:12','2015-12-19 15:13:12','1904c559-e2db-4e4d-b070-ad01407651bd'),
	(32,18,4,1,'en_us',1,'','{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Absolutely amazing, modern rural-meets-urban set up just outside of Grimes!! A must see at $719,000!\",\"slug\":\"absolutely-amazing-modern-rural-meets-urban-set-up-just-outside-of-grimes-a-must-see-at-719-000\",\"postDate\":1450539035,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"7965 NW 142nd St\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"United States\",\"lat\":\"41.705765\",\"lng\":\"-93.813691\"},\"8\":\"select\",\"9\":\"\",\"1\":\"<p>Call your builder and call off the meeting! This beats new construction and is the house you have been waiting on to hit the market! This stunning Grimes acreage sits on 2.3 acres and is just a mile north of DCG High School. Awesome location. Pull into the long driveway and enjoy the massive front yard with trees. STUNNING curb appeal!&nbsp;<\\/p><p>This 5 bedroom (could be 6) home has a floor plan you have not seen before. The attention to detail and woodwork is stunning. Built-ins and window seats with storage are found everywhere. The master bedroom is on the main level and offers a gorgeous bath with Carrera marble, and a vey large closet. The main floor laundry is also huge. You will find a second full laundry room in the finished basement, along with a second kitchen! One of the beds is separated from the rest of the house and is perfect for an office or teen room.&nbsp;<\\/p><p>Too many amenities to list them all!&nbsp;<\\/p><ul><li>5 garage stalls<\\/li><li>Geothermal<\\/li><li>High end kitchen<\\/li><li>Covered deck<\\/li><li>So much more -&nbsp;A RARE FIND!<\\/li><\\/ul>\",\"10\":\"\",\"16\":\"\",\"11\":\"\",\"3\":\"\",\"17\":\"\",\"18\":\"$719,000\",\"12\":\"\",\"13\":\"\",\"14\":\"\",\"19\":\"\"}}','2015-12-19 15:30:35','2015-12-19 15:30:35','9a3cd9b3-ff9e-4e78-be79-aa888f1f9cf0'),
	(33,18,4,1,'en_us',2,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Absolutely amazing, modern rural-meets-urban set up just outside of Grimes!! A must see at $719,000!\",\"slug\":\"absolutely-amazing-modern-rural-meets-urban-set-up-just-outside-of-grimes-a-must-see-at-719-000\",\"postDate\":1450539000,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"7965 NW 142nd St\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"United States\",\"lat\":\"41.70576500\",\"lng\":\"-93.81369100\"},\"8\":\"select\",\"9\":\"\",\"1\":\"<p>Call your builder and call off the meeting! This beats new construction and is the house you have been waiting on to hit the market! This stunning Grimes acreage sits on 2.3 acres and is just a mile north of DCG High School. Awesome location. Pull into the long driveway and enjoy the massive front yard with trees. STUNNING curb appeal!&nbsp;<\\/p>\\r\\n\\r\\n<p>This 5 bedroom (could be 6) home has a floor plan you have not seen before. The attention to detail and woodwork is stunning. Built-ins and window seats with storage are found everywhere. The master bedroom is on the main level and offers a gorgeous bath with Carrera marble, and a vey large closet. The main floor laundry is also huge. You will find a second full laundry room in the finished basement, along with a second kitchen! One of the beds is separated from the rest of the house and is perfect for an office or teen room.&nbsp;<\\/p>\\r\\n\\r\\n<p>Too many amenities to list them all!&nbsp;<\\/p>\\r\\n\\r\\n<ul><li>5 garage stalls<\\/li><li>Geothermal<\\/li><li>High end kitchen<\\/li><li>Covered deck<\\/li><li>So much more -&nbsp;A RARE FIND!<\\/li><\\/ul>\",\"10\":\"\",\"16\":\"http:\\/\\/www.realtor.com\\/realestateandhomes-search?mlslid=506142\",\"11\":\"506142\",\"3\":\"\",\"17\":\"\",\"18\":\"$719,000\",\"12\":\"\",\"13\":\"\",\"14\":\"\",\"19\":\"\"}}','2015-12-19 15:35:53','2015-12-19 15:35:53','bd0e88a7-8986-4c12-9b65-fbe952c01f0e'),
	(34,18,4,1,'en_us',3,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Absolutely amazing, modern rural-meets-urban set up just outside of Grimes!! A must see at $719,000!\",\"slug\":\"absolutely-amazing-modern-rural-meets-urban-set-up-just-outside-of-grimes-a-must-see-at-719-000\",\"postDate\":1450539000,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"7965 NW 142nd St\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"United States\",\"lat\":\"41.70576500\",\"lng\":\"-93.81369100\"},\"8\":\"4\",\"9\":\"5\",\"1\":\"<p>Call your builder and call off the meeting! This beats new construction and is the house you have been waiting on to hit the market! This stunning Grimes acreage sits on 2.3 acres and is just a mile north of DCG High School. Awesome location. Pull into the long driveway and enjoy the massive front yard with trees. STUNNING curb appeal!&nbsp;<\\/p>\\r\\n\\r\\n<p>This 5 bedroom (could be 6) home has a floor plan you have not seen before. The attention to detail and woodwork is stunning. Built-ins and window seats with storage are found everywhere. The master bedroom is on the main level and offers a gorgeous bath with Carrera marble, and a vey large closet. The main floor laundry is also huge. You will find a second full laundry room in the finished basement, along with a second kitchen! One of the beds is separated from the rest of the house and is perfect for an office or teen room.&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>Too many amenities to list them all!&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>5 garage stalls<\\/li><li>Geothermal<\\/li><li>High end kitchen<\\/li><li>Covered deck<\\/li><li>So much more -&nbsp;A RARE FIND!<\\/li><\\/ul>\",\"10\":\"\",\"11\":\"506142\",\"3\":\"\",\"17\":\"\",\"18\":\"$719,000\",\"12\":\"singleFamily\",\"13\":\"\",\"14\":\"2,700\",\"19\":\"\"}}','2015-12-19 15:39:28','2015-12-19 15:39:28','9c19377c-0f82-4eb7-b12a-34671ced5426'),
	(35,18,4,1,'en_us',4,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Absolutely amazing, modern rural-meets-urban set up just outside of Grimes!! A must see at $719,000!\",\"slug\":\"absolutely-amazing-modern-rural-meets-urban-set-up-just-outside-of-grimes-a-must-see-at-719-000\",\"postDate\":1450539000,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"7965 NW 142nd St\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"United States\",\"lat\":\"41.70576500\",\"lng\":\"-93.81369100\"},\"8\":\"4\",\"9\":\"5\",\"1\":\"<p>Call your builder and call off the meeting! This beats new construction and is the house you have been waiting on to hit the market! This stunning Grimes acreage sits on 2.3 acres and is just a mile north of DCG High School. Awesome location. Pull into the long driveway and enjoy the massive front yard with trees. STUNNING curb appeal!&nbsp;<\\/p>\\r\\n\\r\\n<p>This 5 bedroom (could be 6) home has a floor plan you have not seen before. The attention to detail and woodwork is stunning. Built-ins and window seats with storage are found everywhere. The master bedroom is on the main level and offers a gorgeous bath with Carrera marble, and a vey large closet. The main floor laundry is also huge. You will find a second full laundry room in the finished basement, along with a second kitchen! One of the beds is separated from the rest of the house and is perfect for an office or teen room.&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Too many amenities to list them all!&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>5 garage stalls<\\/li><li>Geothermal<\\/li><li>High end kitchen<\\/li><li>Covered deck<\\/li><li>So much more -&nbsp;A RARE FIND!<\\/li><\\/ul>\",\"10\":\"\",\"11\":\"506142\",\"3\":\"\",\"17\":\"3 car attached garage - 2 car detached garage\",\"18\":\"$719,000\",\"12\":\"singleFamily\",\"13\":\"\",\"14\":\"2,700\",\"19\":\"\"}}','2015-12-19 15:41:11','2015-12-19 15:41:11','08448998-b7e0-4513-baf9-ac39e6e6f2a3'),
	(36,18,4,1,'en_us',5,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Absolutely amazing, modern rural-meets-urban set up just outside of Grimes!! A must see at $719,000!\",\"slug\":\"absolutely-amazing-modern-rural-meets-urban-set-up-just-outside-of-grimes\",\"postDate\":1450539000,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"7965 NW 142nd St\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"United States\",\"lat\":\"41.70576500\",\"lng\":\"-93.81369100\"},\"8\":\"4\",\"9\":\"5\",\"1\":\"<p>Call your builder and call off the meeting! This beats new construction and is the house you have been waiting on to hit the market! This stunning Grimes acreage sits on 2.3 acres and is just a mile north of DCG High School. Awesome location. Pull into the long driveway and enjoy the massive front yard with trees. STUNNING curb appeal!&nbsp;<\\/p>\\r\\n\\r\\n<p>This 5 bedroom (could be 6) home has a floor plan you have not seen before. The attention to detail and woodwork is stunning. Built-ins and window seats with storage are found everywhere. The master bedroom is on the main level and offers a gorgeous bath with Carrera marble, and a vey large closet. The main floor laundry is also huge. You will find a second full laundry room in the finished basement, along with a second kitchen! One of the beds is separated from the rest of the house and is perfect for an office or teen room.&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Too many amenities to list them all!&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>5 garage stalls<\\/li><li>Geothermal<\\/li><li>High end kitchen<\\/li><li>Covered deck<\\/li><li>So much more -&nbsp;A RARE FIND!<\\/li><\\/ul>\",\"10\":\"\",\"11\":\"506142\",\"3\":\"\",\"17\":\"3 car attached garage - 2 car detached garage\",\"18\":\"$719,000\",\"12\":\"singleFamily\",\"13\":\"\",\"14\":\"2,700\",\"19\":\"\"}}','2015-12-19 15:41:46','2015-12-19 15:41:46','9b4d5c01-4f2b-4c8d-8e65-3f0941019dc4'),
	(37,18,4,1,'en_us',6,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Absolutely amazing, modern rural-meets-urban set up just outside of Grimes!! A must see at $719,000!\",\"slug\":\"absolutely-amazing-modern-rural-meets-urban-set-up-just-outside-of-grimes\",\"postDate\":1450539000,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"7965 NW 142nd St\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"United States\",\"lat\":\"41.70576500\",\"lng\":\"-93.81369100\"},\"8\":\"4\",\"9\":\"5\",\"1\":\"<p>Call your builder and call off the meeting! This beats new construction and is the house you have been waiting on to hit the market! This stunning Grimes acreage sits on 2.3 acres and is just a mile north of DCG High School. Awesome location. Pull into the long driveway and enjoy the massive front yard with trees. STUNNING curb appeal!&nbsp;<\\/p>\\r\\n\\r\\n<p>This 5 bedroom (could be 6) home has a floor plan you have not seen before. The attention to detail and woodwork is stunning. Built-ins and window seats with storage are found everywhere. The master bedroom is on the main level and offers a gorgeous bath with Carrera marble, and a vey large closet. The main floor laundry is also huge. You will find a second full laundry room in the finished basement, along with a second kitchen! One of the beds is separated from the rest of the house and is perfect for an office or teen room.&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Too many amenities to list them all!&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>5 garage stalls<\\/li><li>Geothermal<\\/li><li>High end kitchen<\\/li><li>Covered deck<\\/li><li>So much more -&nbsp;A RARE FIND!<\\/li><\\/ul>\",\"10\":\"\",\"20\":\"1Hw5ay9jY7y\",\"11\":\"506142\",\"3\":\"\",\"17\":\"3 car attached garage - 2 car detached garage\",\"18\":\"$719,000\",\"12\":\"singleFamily\",\"13\":\"\",\"14\":\"2,700\",\"19\":\"\"}}','2015-12-19 15:43:14','2015-12-19 15:43:14','eb919504-763b-49d3-a244-833e69d66001'),
	(38,18,4,1,'en_us',7,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Absolutely amazing, modern rural-meets-urban set up just outside of Grimes!! A must see at $719,000!\",\"slug\":\"absolutely-amazing-modern-rural-meets-urban-set-up-just-outside-of-grimes\",\"postDate\":1450539000,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"7965 NW 142nd St\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"United States\",\"lat\":\"41.70576500\",\"lng\":\"-93.81369100\"},\"8\":\"4\",\"9\":\"5\",\"1\":\"<p>Call your builder and call off the meeting! This beats new construction and is the house you have been waiting on to hit the market! This stunning Grimes acreage sits on 2.3 acres and is just a mile north of DCG High School. Awesome location. Pull into the long driveway and enjoy the massive front yard with trees. STUNNING curb appeal!&nbsp;<\\/p>\\r\\n\\r\\n<p>This 5 bedroom (could be 6) home has a floor plan you have not seen before. The attention to detail and woodwork is stunning. Built-ins and window seats with storage are found everywhere. The master bedroom is on the main level and offers a gorgeous bath with Carrera marble, and a vey large closet. The main floor laundry is also huge. You will find a second full laundry room in the finished basement, along with a second kitchen! One of the beds is separated from the rest of the house and is perfect for an office or teen room.&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Too many amenities to list them all!&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>5 garage stalls<\\/li><li>Geothermal<\\/li><li>High end kitchen<\\/li><li>Covered deck<\\/li><li>So much more -&nbsp;A RARE FIND!<\\/li><\\/ul>\",\"10\":\"\",\"20\":\"1Hw5ay9jY7y\",\"11\":\"506142\",\"3\":\"\",\"17\":\"3 car attached garage - 2 car detached garage\",\"18\":\"$719,000\",\"12\":\"singleFamily\",\"13\":\"\",\"14\":\"2,700\",\"19\":\"\",\"21\":\"1990\"}}','2015-12-19 16:05:36','2015-12-19 16:05:36','fd801323-f4d5-42df-bd55-7fce0b6da4cb'),
	(39,18,4,1,'en_us',8,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Absolutely amazing, modern rural-meets-urban set up just outside of Grimes!! A must see at $719,000!\",\"slug\":\"absolutely-amazing-modern-rural-meets-urban-set-up-just-outside-of-grimes\",\"postDate\":1450539000,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"7965 NW 142nd St\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"United States\",\"lat\":\"41.70576500\",\"lng\":\"-93.81369100\"},\"8\":\"4\",\"9\":\"5\",\"1\":\"<p>Call your builder and call off the meeting! This beats new construction and is the house you have been waiting on to hit the market! This stunning Grimes acreage sits on 2.3 acres and is just a mile north of DCG High School. Awesome location. Pull into the long driveway and enjoy the massive front yard with trees. STUNNING curb appeal!&nbsp;<\\/p>\\r\\n\\r\\n<p>This 5 bedroom (could be 6) home has a floor plan you have not seen before. The attention to detail and woodwork is stunning. Built-ins and window seats with storage are found everywhere. The master bedroom is on the main level and offers a gorgeous bath with Carrera marble, and a vey large closet. The main floor laundry is also huge. You will find a second full laundry room in the finished basement, along with a second kitchen! One of the beds is separated from the rest of the house and is perfect for an office or teen room.&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Too many amenities to list them all!&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>5 garage stalls<\\/li><li>Geothermal<\\/li><li>High end kitchen<\\/li><li>Covered deck<\\/li><li>So much more -&nbsp;A RARE FIND!<\\/li><\\/ul>\",\"10\":\"1\",\"20\":\"1Hw5ay9jY7y\",\"11\":\"506142\",\"3\":\"\",\"17\":\"3 car attached garage - 2 car detached garage\",\"18\":\"$719,000\",\"12\":\"singleFamily\",\"13\":\"\",\"14\":\"2,700\",\"19\":\"\",\"21\":\"1990\"}}','2015-12-19 16:11:35','2015-12-19 16:11:35','5c6a8c6f-6b88-401b-8c3d-b92571ba06f8'),
	(40,18,4,1,'en_us',9,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Absolutely amazing, modern rural-meets-urban set up just outside of Grimes!! A must see at $719,000!\",\"slug\":\"absolutely-amazing-modern-rural-meets-urban-set-up-just-outside-of-grimes\",\"postDate\":1450539000,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"7965 NW 142nd St\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"United States\",\"lat\":\"41.70576500\",\"lng\":\"-93.81369100\"},\"8\":\"4\",\"9\":\"5\",\"1\":\"<p>Call your builder and call off the meeting! This beats new construction and is the house you have been waiting on to hit the market! This stunning Grimes acreage sits on 2.3 acres and is just a mile north of DCG High School. Awesome location. Pull into the long driveway and enjoy the massive front yard with trees. STUNNING curb appeal!&nbsp;<\\/p>\\r\\n\\r\\n<p>This 5 bedroom (could be 6) home has a floor plan you have not seen before. The attention to detail and woodwork is stunning. Built-ins and window seats with storage are found everywhere. The master bedroom is on the main level and offers a gorgeous bath with Carrera marble, and a vey large closet. The main floor laundry is also huge. You will find a second full laundry room in the finished basement, along with a second kitchen! One of the beds is separated from the rest of the house and is perfect for an office or teen room.&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Too many amenities to list them all!&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>5 garage stalls<\\/li><li>Geothermal<\\/li><li>High end kitchen<\\/li><li>Covered deck<\\/li><li>So much more -&nbsp;A RARE FIND!<\\/li><\\/ul>\",\"10\":\"1\",\"20\":\"1Hw5ay9jY7y\",\"11\":\"506142\",\"3\":\"\",\"17\":\"3 car attached garage - 2 car detached garage\",\"18\":\"$719,000\",\"12\":\"singleFamily\",\"13\":\"1\",\"14\":\"2,700\",\"19\":\"\",\"21\":\"1990\"}}','2015-12-19 16:15:37','2015-12-19 16:15:37','043cefec-7344-418c-a075-bdbe41be60d0'),
	(41,18,4,1,'en_us',10,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Absolutely amazing, modern rural-meets-urban set up just outside of Grimes!! A must see at $719,000!\",\"slug\":\"absolutely-amazing-modern-rural-meets-urban-set-up-just-outside-of-grimes\",\"postDate\":1450539000,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"7965 NW 142nd St\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"United States\",\"lat\":\"41.70576500\",\"lng\":\"-93.81369100\"},\"8\":\"4\",\"9\":\"5\",\"1\":\"<p>Call your builder and call off the meeting! This beats new construction and is the house you have been waiting on to hit the market! This stunning Grimes acreage sits on 2.3 acres and is just a mile north of DCG High School. Awesome location. Pull into the long driveway and enjoy the massive front yard with trees. STUNNING curb appeal!&nbsp;<\\/p>\\r\\n\\r\\n<p>This 5 bedroom (could be 6) home has a floor plan you have not seen before. The attention to detail and woodwork is stunning. Built-ins and window seats with storage are found everywhere. The master bedroom is on the main level and offers a gorgeous bath with Carrera marble, and a vey large closet. The main floor laundry is also huge. You will find a second full laundry room in the finished basement, along with a second kitchen! One of the beds is separated from the rest of the house and is perfect for an office or teen room.&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Too many amenities to list them all!&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>5 garage stalls<\\/li><li>Geothermal<\\/li><li>High end kitchen<\\/li><li>Covered deck<\\/li><li>So much more -&nbsp;A RARE FIND!<\\/li><\\/ul>\",\"10\":\"\",\"20\":\"1Hw5ay9jY7y\",\"11\":\"506142\",\"3\":\"\",\"17\":\"3 car attached garage - 2 car detached garage\",\"18\":\"$719,000\",\"12\":\"singleFamily\",\"13\":\"\",\"14\":\"2,700\",\"19\":\"\",\"21\":\"1990\"}}','2015-12-19 16:15:51','2015-12-19 16:15:51','391f2f18-afbb-411a-87cd-ac1ddde43102'),
	(42,18,4,1,'en_us',11,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Absolutely amazing, modern rural-meets-urban set up just outside of Grimes!! A must see at $719,000!\",\"slug\":\"absolutely-amazing-modern-rural-meets-urban-set-up-just-outside-of-grimes\",\"postDate\":1450539000,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"7965 NW 142nd St\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"United States\",\"lat\":\"41.70576500\",\"lng\":\"-93.81369100\"},\"8\":\"4\",\"9\":\"5\",\"1\":\"<p>Call your builder and call off the meeting! This beats new construction and is the house you have been waiting on to hit the market! This stunning Grimes acreage sits on 2.3 acres and is just a mile north of DCG High School. Awesome location. Pull into the long driveway and enjoy the massive front yard with trees. STUNNING curb appeal!&nbsp;<\\/p>\\r\\n\\r\\n<p>This 5 bedroom (could be 6) home has a floor plan you have not seen before. The attention to detail and woodwork is stunning. Built-ins and window seats with storage are found everywhere. The master bedroom is on the main level and offers a gorgeous bath with Carrera marble, and a vey large closet. The main floor laundry is also huge. You will find a second full laundry room in the finished basement, along with a second kitchen! One of the beds is separated from the rest of the house and is perfect for an office or teen room.&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Too many amenities to list them all!&nbsp;<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>5 garage stalls<\\/li><li>Geothermal<\\/li><li>High end kitchen<\\/li><li>Covered deck<\\/li><li>So much more -&nbsp;A RARE FIND!<\\/li><\\/ul>\",\"10\":\"\",\"20\":\"1Hw5ay9jY7y\",\"11\":\"506142\",\"3\":\"\",\"17\":\"3 car attached garage - 2 car detached garage\",\"18\":\"$719,000\",\"12\":\"singleFamily\",\"13\":\"\",\"14\":\"2,700\",\"19\":\"\",\"21\":\"1990\"}}','2015-12-19 16:18:14','2015-12-19 16:18:14','8be44274-fccb-4b5a-aa52-7909f7ef60f4'),
	(43,21,12,1,'en_us',1,NULL,'{\"typeId\":\"12\",\"authorId\":null,\"title\":\"Contact\",\"slug\":\"contact\",\"postDate\":1450585430,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":[]}','2015-12-20 04:23:50','2015-12-20 04:23:50','f12ddb60-0af1-4991-a136-c65683a72bb5'),
	(44,21,12,1,'en_us',2,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Contact\",\"slug\":\"contact\",\"postDate\":1450585430,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"\",\"3\":\"\"}}','2015-12-20 04:26:31','2015-12-20 04:26:31','c37704c6-dd42-4570-a8db-9507d9be15fe'),
	(45,21,12,1,'en_us',3,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Contact Mark\",\"slug\":\"contact\",\"postDate\":1450585430,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"\",\"3\":\"\"}}','2015-12-20 04:26:39','2015-12-20 04:26:39','2046ae16-5266-4dab-b248-9b372b6199d3'),
	(46,22,13,1,'en_us',1,NULL,'{\"typeId\":\"13\",\"authorId\":null,\"title\":\"Ways To Sell\",\"slug\":\"ways-to-sell\",\"postDate\":1450586361,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":[]}','2015-12-20 04:39:21','2015-12-20 04:39:21','814bcd6a-77a4-4395-8a8d-924c4b973d7f'),
	(47,36,14,1,'en_us',1,NULL,'{\"typeId\":\"14\",\"authorId\":null,\"title\":\"Buyers & Sellers\",\"slug\":\"buyers-sellers\",\"postDate\":1450587650,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":[]}','2015-12-20 05:00:51','2015-12-20 05:00:51','11c964ac-6740-432b-94f9-2e43396f4a6d'),
	(48,36,14,1,'en_us',2,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Buyers & Sellers\",\"slug\":\"buyers-sellers\",\"postDate\":1450587650,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>So you are are thinking about buying or selling. That is great, here are a few things you need to know!<\\/p><h2>Buyers<\\/h2><p><strong><\\/strong>Buying a home is a big purchase, one of the biggest you will ever make. Why in the world would anyone want to make that purchase without the help of a professional real estate agent? Beats me, but many try. The most important thing to remember is that when you are buying a home, you DO NOT PAY YOUR AGENT! The seller of the home pays the entire commission and that commission is split between the agent selling the home and the agent representing the buyer. There is no reason to not have your own agent representing you and working hard on your behalf.<br><\\/p><p>I have talked to many buyers who have said something like \\\"When we bought, we just used the agent who had the house listed.\\\" If you are thinking about doing this, <strong>use extreme caution!<\\/strong>&nbsp;This should only be done if you have complete trust in the agent. If you just met them, this is a terrible idea! This situation is called Dual Agency and it happens when one agent represents both parties. That agent is trying to get the most money for the seller and also get you the best price. How can they do this? It is tricky and again, should only be done if you have total faith in that agent.<br><\\/p><p>The way to avoid this is to hire me! Remember, I can show you <em>all<\\/em> homes, not just the ones that I list.<br><\\/p><p>So when you are my buyer what do you get? Here is a summary of what my buyers get when they work with me:<br><\\/p><ul><li>An agent with 24\\/7 availability<\\/li><li>An agent with a A+ BBB rating<\\/li><li>An agent with more recommendations than any other agent in Central Iowa (Trulia.com)<\\/li><li><a href=\\\"http:\\/\\/local.charterhouseiowa.dev\\/truck#entry:9\\\">Free Moving Truck<\\/a> (based on availability, first come, first served)<\\/li><li>Access to my interior designer if you need help or suggestion on how to furnish your new home<\\/li><li>Lender recommendations, I can get you to the right lender who will get you closed on time and with no surprises<\\/li><li>An experienced agent, I have been selling real estate full-time since 2005<\\/li><li>Auto-prospecting so you never miss a new listing to the market<\\/li><li>Weekly check-in updates to make sure you are happy<\\/li><li><strong>And much more!<\\/strong><\\/li><\\/ul><p>When you work with me, you are working with a one-of-a-kind agent who simply offers my clients more than other agents. Please read through the client reviews on this site to get a good feel of how I treat my clients. You will never regret hiring me as your agent!<\\/p><h2>Sellers<\\/h2><p>When the time comes to put your home on the market, hiring a real estate professional is a crucial step in the right direction. I realize that there are many home-selling options, but the tried and true method of hiring an agent is still the best option by far. It is proven to be the safest way to sell as well as being the way to generate the highest selling price.<br><\\/p>\",\"3\":\"\"}}','2015-12-20 05:05:08','2015-12-20 05:05:08','4a8aea66-00f8-4696-a43a-ffa5e6fce613'),
	(49,36,14,1,'en_us',3,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Buyers & Sellers\",\"slug\":\"buyers-sellers\",\"postDate\":1450587650,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>So you are are thinking about buying or selling. That is great, here are a few things you need to know!<\\/p>\\r\\n\\r\\n<h2>Buyers<\\/h2>\\r\\n\\r\\n<p>Buying a home is a big purchase, one of the biggest you will ever make. Why in the world would anyone want to make that purchase without the help of a professional real estate agent? Beats me, but many try. The most important thing to remember is that when you are buying a home, you DO NOT PAY YOUR AGENT! The seller of the home pays the entire commission and that commission is split between the agent selling the home and the agent representing the buyer. There is no reason to not have your own agent representing you and working hard on your behalf.<br><\\/p>\\r\\n\\r\\n<p>I have talked to many buyers who have said something like \\\"When we bought, we just used the agent who had the house listed.\\\" If you are thinking about doing this, <strong>use extreme caution!<\\/strong>&nbsp;This should only be done if you have complete trust in the agent. If you just met them, this is a terrible idea! This situation is called Dual Agency and it happens when one agent represents both parties. That agent is trying to get the most money for the seller and also get you the best price. How can they do this? It is tricky and again, should only be done if you have total faith in that agent.<br><\\/p>\\r\\n\\r\\n<p>The way to avoid this is to hire me! Remember, I can show you <em>all<\\/em> homes, not just the ones that I list.<br><\\/p>\\r\\n\\r\\n<p>So when you are my buyer what do you get? Here is a summary of what my buyers get when they work with me:<br><\\/p>\\r\\n\\r\\n<ul><li>An agent with 24\\/7 availability<\\/li><li>An agent with a A+ BBB rating<\\/li><li>An agent with more recommendations than any other agent in Central Iowa (Trulia.com)<\\/li><li><a href=\\\"http:\\/\\/local.charterhouseiowa.dev\\/truck#entry:9:url\\\">Free Moving Truck<\\/a> (based on availability, first come, first served)<\\/li><li>Access to my interior designer if you need help or suggestion on how to furnish your new home<\\/li><li>Lender recommendations, I can get you to the right lender who will get you closed on time and with no surprises<\\/li><li>An experienced agent, I have been selling real estate full-time since 2005<\\/li><li>Auto-prospecting so you never miss a new listing to the market<\\/li><li>Weekly check-in updates to make sure you are happy<\\/li><li><strong>And much more!<\\/strong><\\/li><\\/ul>\\r\\n\\r\\n<p>When you work with me, you are working with a one-of-a-kind agent who simply offers my clients more than other agents. Please read through the client reviews on this site to get a good feel of how I treat my clients. You will never regret hiring me as your agent!<\\/p>\\r\\n\\r\\n<h2>Sellers<\\/h2>\\r\\n\\r\\n<p>When the time comes to put your home on the market, hiring a real estate professional is a crucial step in the right direction. I realize that there are many home-selling options, but the tried and true method of hiring an agent is still the best option by far. It is proven to be the safest way to sell as well as being the way to generate the highest selling price.<br><\\/p><p>But, just listing with an agent is not enough! You have to list with an agent who knows how to market and embraces today\'s trends like social media to reach the masses. You want your home exposed everywhere to reach every possible buyer. But you also want to work with an agent who will be enjoyable to work with and one who has great services and benefits to offer you. That agent is me!<br><\\/p><p>I would love to talk to you about listing your home but for now, here is a brief summary of what I offer to sellers:<br><\\/p><ul><li>An agent with 24\\/7 availability<\\/li><li>An agent with an A+ BBB rating<\\/li><li>An agent with more recommendations than any other agent in Central Iowa (Trulia.com)<\\/li><li>A professional photographer will take the photos of your home<\\/li><li>A Professional videographer will do a video of your home (Certain price ranges only)<\\/li><li>Professional staging assistance, my interior designer will make sure your home is ready to go<\\/li><li><a href=\\\"http:\\/\\/local.charterhouseiowa.dev\\/truck#entry:9\\\">Free Moving Truck <\\/a>(Based on availability, first come, first served)<\\/li><li>Professional moving services (at 7% plan while staying in central Iowa)<\\/li><li>Weekly check-in updates to make sure you are happy<\\/li><li>Social Media marketing<\\/li><li><strong>And much more!<\\/strong><\\/li><\\/ul><p>No other agent offers their clients more! You will love your selling experience, I promise. I realize one size does not fit all when it comes to selling your home. I want to talk to you about your specific situation and see if I can help.<\\/p>\",\"3\":\"\"}}','2015-12-20 05:08:19','2015-12-20 05:08:19','e7ee117b-0347-422b-b5c6-da19a8ebdd02'),
	(50,22,13,1,'en_us',2,'','{\"typeId\":null,\"authorId\":null,\"title\":\"3 Ways To List Your Home\",\"slug\":\"ways-to-sell\",\"postDate\":1450586361,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"\",\"3\":\"\"}}','2015-12-20 14:54:52','2015-12-20 14:54:52','9f9c0bd5-24b6-4839-8c9e-8c21be29d4aa'),
	(51,22,13,1,'en_us',3,'','{\"typeId\":null,\"authorId\":null,\"title\":\"3 Ways To List Your Home\",\"slug\":\"ways-to-sell\",\"postDate\":1450586361,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"\",\"38\":\"\",\"3\":\"\"}}','2015-12-20 14:56:25','2015-12-20 14:56:25','4ec70fb6-1edb-43e6-bfa8-b73c80de7ab8'),
	(52,38,15,1,'en_us',1,NULL,'{\"typeId\":\"15\",\"authorId\":null,\"title\":\"About\",\"slug\":\"about\",\"postDate\":1450641789,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":[]}','2015-12-20 20:03:09','2015-12-20 20:03:09','1a1dffd8-8b59-46dd-895a-6b8e09977c9e'),
	(53,38,15,1,'en_us',2,'','{\"typeId\":null,\"authorId\":null,\"title\":\"About\",\"slug\":\"about\",\"postDate\":1450641789,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>Mark started his real estate career in 2005 and has been doing it full-time since then. Over time Mark realized that the majority of real estate agents are the same. They say the same things, they market the same way, and they treat clients the same. Mark did not like this so he set about creating his own unique way of practicing real estate.<\\/p><p>In 2014, Mark debuted a three tier pricing plan for listing clients, giving them the choice on how they want to list their home. This is another example of how Mark never accepts the status quo in real estate and is always looking to make things better for his clients.<br><\\/p><p>Mark\'s overall goal on every deal is simple. He wants to provide such great service that after the deal is done, that client will happily provide a positive review of the transaction. This goal is what makes Mark one of the most recommended agents in Central Iowa.<br><\\/p><p>When you add it all up, when you take into account what Mark offers clients in terms of services, marketing and care, Mark truly feels he is the best option available in the market today and he hopes you will experience this for yourself very soon!<br><\\/p>\",\"3\":\"\"}}','2015-12-20 20:04:30','2015-12-20 20:04:30','0805a412-f818-4b7a-970e-3d63a4c0a65f'),
	(61,9,10,1,'en_us',2,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Truck\",\"slug\":\"truck\",\"postDate\":1450487151,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mattis sollicitudin felis, consectetur eleifend mi ornare sed. Vivamus cursus tincidunt ultrices. Nam id tincidunt purus. Morbi lectus eros, molestie non dolor ut, tincidunt fermentum odio. Vivamus consectetur molestie lorem ac varius.<\\/p>\",\"3\":\"\"}}','2015-12-21 20:41:59','2015-12-21 20:41:59','2ab089dd-91ce-4cef-879a-5c2c3f1a1c1c'),
	(62,38,15,1,'en_us',3,'','{\"typeId\":null,\"authorId\":null,\"title\":\"About\",\"slug\":\"about\",\"postDate\":1450641789,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":{\"new1\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Mark started his real estate career in 2005 and has been doing it full-time since then. Over time Mark realized that the majority of real estate agents are the same. They say the same things, they market the same way, and they treat clients the same. Mark did not like this so he set about creating his own unique way of practicing real estate.<\\/p><p>In 2014, Mark debuted a three tier pricing plan for listing clients, giving them the choice on how they want to list their home. This is another example of how Mark never accepts the status quo in real estate and is always looking to make things better for his clients.<\\/p><p><br>Mark\'s overall goal on every deal is simple. He wants to provide such great service that after the deal is done, that client will happily provide a positive review of the transaction. This goal is what makes Mark one of the most recommended agents in Central Iowa.<\\/p><p><br>When you add it all up, when you take into account what Mark offers clients in terms of services, marketing and care, Mark truly feels he is the best option available in the market today and he hopes you will experience this for yourself very soon!<\\/p>\"}}},\"3\":\"\"}}','2015-12-28 00:53:16','2015-12-28 00:53:16','499bc50b-0213-46dd-9d2a-12cf0a925256'),
	(63,38,15,1,'en_us',4,'','{\"typeId\":null,\"authorId\":null,\"title\":\"About Mark Charter\",\"slug\":\"about\",\"postDate\":1450641789,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":{\"new1\":{\"type\":\"headline\",\"enabled\":\"1\",\"fields\":{\"headline\":\"Meet Mark and his family\"}},\"55\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Mark started his real estate career in 2005 and has been doing it full-time since then. Over time Mark realized that the majority of real estate agents are the same. They say the same things, they market the same way, and they treat clients the same. Mark did not like this so he set about creating his own unique way of practicing real estate.<\\/p>\\r\\n\\r\\n<p>In 2014, Mark debuted a three tier pricing plan for listing clients, giving them the choice on how they want to list their home. This is another example of how Mark never accepts the status quo in real estate and is always looking to make things better for his clients.<\\/p>\\r\\n\\r\\n<p><br>Mark\'s overall goal on every deal is simple. He wants to provide such great service that after the deal is done, that client will happily provide a positive review of the transaction. This goal is what makes Mark one of the most recommended agents in Central Iowa.<\\/p>\\r\\n\\r\\n<p><br>When you add it all up, when you take into account what Mark offers clients in terms of services, marketing and care, Mark truly feels he is the best option available in the market today and he hopes you will experience this for yourself very soon!<\\/p>\"}}},\"3\":\"\"}}','2015-12-28 00:55:12','2015-12-28 00:55:12','66da3bcd-f138-43e3-a6e0-7be5372dd924'),
	(64,38,15,1,'en_us',5,'','{\"typeId\":null,\"authorId\":null,\"title\":\"About Mark Charter\",\"slug\":\"about\",\"postDate\":1450641789,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":{\"new1\":{\"type\":\"quote\",\"enabled\":\"1\",\"fields\":{\"quote\":\"<p>One of Mark Charter\\u2019s greatest characteristics is his giving nature.<\\/p>\",\"quoteAttribution\":\"Adam Carroll\"}},\"56\":{\"type\":\"headline\",\"enabled\":\"1\",\"fields\":{\"headline\":\"Meet Mark and his family\"}},\"55\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Mark started his real estate career in 2005 and has been doing it full-time since then. Over time Mark realized that the majority of real estate agents are the same. They say the same things, they market the same way, and they treat clients the same. Mark did not like this so he set about creating his own unique way of practicing real estate.<\\/p>\\r\\n\\r\\n<p>In 2014, Mark debuted a three tier pricing plan for listing clients, giving them the choice on how they want to list their home. This is another example of how Mark never accepts the status quo in real estate and is always looking to make things better for his clients.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p><br>Mark\'s overall goal on every deal is simple. He wants to provide such great service that after the deal is done, that client will happily provide a positive review of the transaction. This goal is what makes Mark one of the most recommended agents in Central Iowa.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p><br>When you add it all up, when you take into account what Mark offers clients in terms of services, marketing and care, Mark truly feels he is the best option available in the market today and he hopes you will experience this for yourself very soon!<\\/p>\"}}},\"3\":\"\"}}','2015-12-28 00:55:54','2015-12-28 00:55:54','58a6acd4-8986-40af-b4af-b3e52581e526'),
	(65,38,15,1,'en_us',6,'','{\"typeId\":null,\"authorId\":null,\"title\":\"About Mark Charter\",\"slug\":\"about\",\"postDate\":1450641789,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":{\"56\":{\"type\":\"headline\",\"enabled\":\"1\",\"fields\":{\"headline\":\"Meet Mark and his family\"}},\"57\":{\"type\":\"quote\",\"enabled\":\"1\",\"fields\":{\"quote\":\"<p>One of Mark Charter\\u2019s greatest characteristics is his giving nature.<\\/p>\",\"quoteAttribution\":\"Adam Carroll\"}},\"55\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Mark started his real estate career in 2005 and has been doing it full-time since then. Over time Mark realized that the majority of real estate agents are the same. They say the same things, they market the same way, and they treat clients the same. Mark did not like this so he set about creating his own unique way of practicing real estate.<\\/p>\\r\\n\\r\\n<p>In 2014, Mark debuted a three tier pricing plan for listing clients, giving them the choice on how they want to list their home. This is another example of how Mark never accepts the status quo in real estate and is always looking to make things better for his clients.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p><br>Mark\'s overall goal on every deal is simple. He wants to provide such great service that after the deal is done, that client will happily provide a positive review of the transaction. This goal is what makes Mark one of the most recommended agents in Central Iowa.<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p><br>When you add it all up, when you take into account what Mark offers clients in terms of services, marketing and care, Mark truly feels he is the best option available in the market today and he hopes you will experience this for yourself very soon!<\\/p>\"}}},\"3\":\"\"}}','2015-12-28 00:56:43','2015-12-28 00:56:43','c0c913a0-2742-4785-9ddb-08cc56a271bc'),
	(66,38,15,1,'en_us',7,'','{\"typeId\":null,\"authorId\":null,\"title\":\"About Mark Charter\",\"slug\":\"about\",\"postDate\":1450641789,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":{\"56\":{\"type\":\"headline\",\"enabled\":\"1\",\"fields\":{\"headline\":\"Meet Mark and his family\"}},\"55\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Mark started his real estate career in 2005 and has been doing it full-time since then. Over time Mark realized that the majority of real estate agents are the same. They say the same things, they market the same way, and they treat clients the same. Mark did not like this so he set about creating his own unique way of practicing real estate.<\\/p>\\r\\n\\r\\n<p>In 2014, Mark debuted a three tier pricing plan for listing clients, giving them the choice on how they want to list their home. This is another example of how Mark never accepts the status quo in real estate and is always looking to make things better for his clients.<\\/p>\"}},\"57\":{\"type\":\"quote\",\"enabled\":\"1\",\"fields\":{\"quote\":\"<p>One of Mark Charter\\u2019s greatest characteristics is his giving nature.<\\/p>\",\"quoteAttribution\":\"Adam Carroll\"}},\"new1\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Mark\'s overall goal on every deal is simple. He wants to provide such great service that after the deal is done, that client will happily provide a positive review of the transaction. This goal is what makes Mark one of the most recommended agents in Central Iowa.<\\/p>\\r\\n\\r\\n<p>When you add it all up, when you take into account what Mark offers clients in terms of services, marketing and care, Mark truly feels he is the best option available in the market today and he hopes you will experience this for yourself very soon!<\\/p>\"}}},\"3\":\"\"}}','2015-12-28 00:57:07','2015-12-28 00:57:07','8f259d23-bd4e-4bed-b3a3-c1ca79e9a742'),
	(67,36,14,1,'en_us',4,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Buyers & Sellers\",\"slug\":\"buyers-sellers\",\"postDate\":1450587650,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>So you are are thinking about buying or selling. That is great, here are a few things you need to know!<\\/p>\\r\\n\\r\\n<h2>Buyers<\\/h2>\\r\\n\\r\\n\\r\\n\\r\\n<p>Buying a home is a big purchase, one of the biggest you will ever make. Why in the world would anyone want to make that purchase without the help of a professional real estate agent? Beats me, but many try. The most important thing to remember is that when you are buying a home, you DO NOT PAY YOUR AGENT! The seller of the home pays the entire commission and that commission is split between the agent selling the home and the agent representing the buyer. There is no reason to not have your own agent representing you and working hard on your behalf.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>I have talked to many buyers who have said something like \\\"When we bought, we just used the agent who had the house listed.\\\" If you are thinking about doing this, <strong>use extreme caution!<\\/strong>&nbsp;This should only be done if you have complete trust in the agent. If you just met them, this is a terrible idea! This situation is called Dual Agency and it happens when one agent represents both parties. That agent is trying to get the most money for the seller and also get you the best price. How can they do this? It is tricky and again, should only be done if you have total faith in that agent.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>The way to avoid this is to hire me! Remember, I can show you <em>all<\\/em> homes, not just the ones that I list.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>So when you are my buyer what do you get? Here is a summary of what my buyers get when they work with me:<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>An agent with 24\\/7 availability<\\/li><li>An agent with a A+ BBB rating<\\/li><li>An agent with more recommendations than any other agent in Central Iowa (Trulia.com)<\\/li><li><a href=\\\"http:\\/\\/local.charterhouseiowa.dev\\/truck#entry:9:url\\\">Free Moving Truck<\\/a> (based on availability, first come, first served)<\\/li><li>Access to my interior designer if you need help or suggestion on how to furnish your new home<\\/li><li>Lender recommendations, I can get you to the right lender who will get you closed on time and with no surprises<\\/li><li>An experienced agent, I have been selling real estate full-time since 2005<\\/li><li>Auto-prospecting so you never miss a new listing to the market<\\/li><li>Weekly check-in updates to make sure you are happy<\\/li><li><strong>And much more!<\\/strong><\\/li><\\/ul>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>When you work with me, you are working with a one-of-a-kind agent who simply offers my clients more than other agents. Please read through the client reviews on this site to get a good feel of how I treat my clients. You will never regret hiring me as your agent!<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<h2>Sellers<\\/h2>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>When the time comes to put your home on the market, hiring a real estate professional is a crucial step in the right direction. I realize that there are many home-selling options, but the tried and true method of hiring an agent is still the best option by far. It is proven to be the safest way to sell as well as being the way to generate the highest selling price.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>But, just listing with an agent is not enough! You have to list with an agent who knows how to market and embraces today\'s trends like social media to reach the masses. You want your home exposed everywhere to reach every possible buyer. But you also want to work with an agent who will be enjoyable to work with and one who has great services and benefits to offer you. That agent is me!<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>I would love to talk to you about listing your home but for now, here is a brief summary of what I offer to sellers:<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>An agent with 24\\/7 availability<\\/li><li>An agent with an A+ BBB rating<\\/li><li>An agent with more recommendations than any other agent in Central Iowa (Trulia.com)<\\/li><li>A professional photographer will take the photos of your home<\\/li><li>A Professional videographer will do a video of your home (Certain price ranges only)<\\/li><li>Professional staging assistance, my interior designer will make sure your home is ready to go<\\/li><li><a href=\\\"http:\\/\\/local.charterhouseiowa.dev\\/truck#entry:9:url\\\">Free Moving Truck <\\/a>(Based on availability, first come, first served)<\\/li><li>Professional moving services (at 7% plan while staying in central Iowa)<\\/li><li>Weekly check-in updates to make sure you are happy<\\/li><li>Social Media marketing<\\/li><li><strong>And much more!<\\/strong><\\/li><\\/ul>\\r\\n\\r\\n\\r\\n\\r\\n<p>No other agent offers their clients more! You will love your selling experience, I promise. I realize one size does not fit all when it comes to selling your home. I want to talk to you about your specific situation and see if I can help.<\\/p>\",\"49\":{\"new1\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>So you are are thinking about buying or selling. That is great, here are a few things you need to know!<\\/p>\\r\\n\\r\\n<h2>Buyers<\\/h2>\\r\\n\\r\\n\\r\\n\\r\\n<p>Buying a home is a big purchase, one of the biggest you will ever make. Why in the world would anyone want to make that purchase without the help of a professional real estate agent? Beats me, but many try. The most important thing to remember is that when you are buying a home, you DO NOT PAY YOUR AGENT! The seller of the home pays the entire commission and that commission is split between the agent selling the home and the agent representing the buyer. There is no reason to not have your own agent representing you and working hard on your behalf.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>I have talked to many buyers who have said something like \\\"When we bought, we just used the agent who had the house listed.\\\" If you are thinking about doing this, <strong>use extreme caution!<\\/strong>&nbsp;This should only be done if you have complete trust in the agent. If you just met them, this is a terrible idea! This situation is called Dual Agency and it happens when one agent represents both parties. That agent is trying to get the most money for the seller and also get you the best price. How can they do this? It is tricky and again, should only be done if you have total faith in that agent.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>The way to avoid this is to hire me! Remember, I can show you <em>all<\\/em> homes, not just the ones that I list.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>So when you are my buyer what do you get? Here is a summary of what my buyers get when they work with me:<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>An agent with 24\\/7 availability<\\/li><li>An agent with a A+ BBB rating<\\/li><li>An agent with more recommendations than any other agent in Central Iowa (Trulia.com)<\\/li><li><a href=\\\"http:\\/\\/local.charterhouseiowa.dev\\/truck#entry:9:url\\\">Free Moving Truck<\\/a> (based on availability, first come, first served)<\\/li><li>Access to my interior designer if you need help or suggestion on how to furnish your new home<\\/li><li>Lender recommendations, I can get you to the right lender who will get you closed on time and with no surprises<\\/li><li>An experienced agent, I have been selling real estate full-time since 2005<\\/li><li>Auto-prospecting so you never miss a new listing to the market<\\/li><li>Weekly check-in updates to make sure you are happy<\\/li><li><strong>And much more!<\\/strong><\\/li><\\/ul>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>When you work with me, you are working with a one-of-a-kind agent who simply offers my clients more than other agents. Please read through the client reviews on this site to get a good feel of how I treat my clients. You will never regret hiring me as your agent!<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<h2>Sellers<\\/h2>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>When the time comes to put your home on the market, hiring a real estate professional is a crucial step in the right direction. I realize that there are many home-selling options, but the tried and true method of hiring an agent is still the best option by far. It is proven to be the safest way to sell as well as being the way to generate the highest selling price.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>But, just listing with an agent is not enough! You have to list with an agent who knows how to market and embraces today\'s trends like social media to reach the masses. You want your home exposed everywhere to reach every possible buyer. But you also want to work with an agent who will be enjoyable to work with and one who has great services and benefits to offer you. That agent is me!<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<p>I would love to talk to you about listing your home but for now, here is a brief summary of what I offer to sellers:<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>An agent with 24\\/7 availability<\\/li><li>An agent with an A+ BBB rating<\\/li><li>An agent with more recommendations than any other agent in Central Iowa (Trulia.com)<\\/li><li>A professional photographer will take the photos of your home<\\/li><li>A Professional videographer will do a video of your home (Certain price ranges only)<\\/li><li>Professional staging assistance, my interior designer will make sure your home is ready to go<\\/li><li><a href=\\\"http:\\/\\/local.charterhouseiowa.dev\\/truck#entry:9:url\\\">Free Moving Truck <\\/a>(Based on availability, first come, first served)<\\/li><li>Professional moving services (at 7% plan while staying in central Iowa)<\\/li><li>Weekly check-in updates to make sure you are happy<\\/li><li>Social Media marketing<\\/li><li><strong>And much more!<\\/strong><\\/li><\\/ul>\\r\\n\\r\\n\\r\\n\\r\\n<p>No other agent offers their clients more! You will love your selling experience, I promise. I realize one size does not fit all when it comes to selling your home. I want to talk to you about your specific situation and see if I can help.<\\/p>\"}}},\"3\":\"\"}}','2015-12-28 00:57:53','2015-12-28 00:57:53','00738543-86a2-4498-a72f-a885bd1dc987'),
	(68,36,14,1,'en_us',5,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Buyers & Sellers\",\"slug\":\"buyers-sellers\",\"postDate\":1450587650,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"\",\"49\":{\"59\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>So you are are thinking about buying or selling. That is great, here are a few things you need to know!<\\/p>\\r\\n\\r\\n<h2>Buyers<\\/h2>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Buying a home is a big purchase, one of the biggest you will ever make. Why in the world would anyone want to make that purchase without the help of a professional real estate agent? Beats me, but many try. The most important thing to remember is that when you are buying a home, you DO NOT PAY YOUR AGENT! The seller of the home pays the entire commission and that commission is split between the agent selling the home and the agent representing the buyer. There is no reason to not have your own agent representing you and working hard on your behalf.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>I have talked to many buyers who have said something like \\\"When we bought, we just used the agent who had the house listed.\\\" If you are thinking about doing this, <strong>use extreme caution!<\\/strong>&nbsp;This should only be done if you have complete trust in the agent. If you just met them, this is a terrible idea! This situation is called Dual Agency and it happens when one agent represents both parties. That agent is trying to get the most money for the seller and also get you the best price. How can they do this? It is tricky and again, should only be done if you have total faith in that agent.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>The way to avoid this is to hire me! Remember, I can show you <em>all<\\/em> homes, not just the ones that I list.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>So when you are my buyer what do you get? Here is a summary of what my buyers get when they work with me:<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>An agent with 24\\/7 availability<\\/li><li>An agent with a A+ BBB rating<\\/li><li>An agent with more recommendations than any other agent in Central Iowa (Trulia.com)<\\/li><li><a href=\\\"http:\\/\\/local.charterhouseiowa.dev\\/truck#entry:9:url\\\">Free Moving Truck<\\/a> (based on availability, first come, first served)<\\/li><li>Access to my interior designer if you need help or suggestion on how to furnish your new home<\\/li><li>Lender recommendations, I can get you to the right lender who will get you closed on time and with no surprises<\\/li><li>An experienced agent, I have been selling real estate full-time since 2005<\\/li><li>Auto-prospecting so you never miss a new listing to the market<\\/li><li>Weekly check-in updates to make sure you are happy<\\/li><li><strong>And much more!<\\/strong><\\/li><\\/ul>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>When you work with me, you are working with a one-of-a-kind agent who simply offers my clients more than other agents. Please read through the client reviews on this site to get a good feel of how I treat my clients. You will never regret hiring me as your agent!<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<h2>Sellers<\\/h2>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>When the time comes to put your home on the market, hiring a real estate professional is a crucial step in the right direction. I realize that there are many home-selling options, but the tried and true method of hiring an agent is still the best option by far. It is proven to be the safest way to sell as well as being the way to generate the highest selling price.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>But, just listing with an agent is not enough! You have to list with an agent who knows how to market and embraces today\'s trends like social media to reach the masses. You want your home exposed everywhere to reach every possible buyer. But you also want to work with an agent who will be enjoyable to work with and one who has great services and benefits to offer you. That agent is me!<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>I would love to talk to you about listing your home but for now, here is a brief summary of what I offer to sellers:<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>An agent with 24\\/7 availability<\\/li><li>An agent with an A+ BBB rating<\\/li><li>An agent with more recommendations than any other agent in Central Iowa (Trulia.com)<\\/li><li>A professional photographer will take the photos of your home<\\/li><li>A Professional videographer will do a video of your home (Certain price ranges only)<\\/li><li>Professional staging assistance, my interior designer will make sure your home is ready to go<\\/li><li><a href=\\\"http:\\/\\/local.charterhouseiowa.dev\\/truck#entry:9:url\\\">Free Moving Truck <\\/a>(Based on availability, first come, first served)<\\/li><li>Professional moving services (at 7% plan while staying in central Iowa)<\\/li><li>Weekly check-in updates to make sure you are happy<\\/li><li>Social Media marketing<\\/li><li><strong>And much more!<\\/strong><\\/li><\\/ul>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>No other agent offers their clients more! You will love your selling experience, I promise. I realize one size does not fit all when it comes to selling your home. I want to talk to you about your specific situation and see if I can help.<\\/p>\"}}},\"3\":\"\"}}','2015-12-28 00:58:00','2015-12-28 00:58:00','b69ce752-2cdf-4f2c-a4e1-1d0f637b0c72'),
	(69,36,14,1,'en_us',6,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Buyers & Sellers\",\"slug\":\"buyers-sellers\",\"postDate\":1450587650,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":{\"59\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>So you are are thinking about buying or selling. That is great, here are a few things you need to know!<\\/p>\\r\\n\\r\\n<h2>Buyers<\\/h2>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Buying a home is a big purchase, one of the biggest you will ever make. Why in the world would anyone want to make that purchase without the help of a professional real estate agent? Beats me, but many try. The most important thing to remember is that when you are buying a home, you DO NOT PAY YOUR AGENT! The seller of the home pays the entire commission and that commission is split between the agent selling the home and the agent representing the buyer. There is no reason to not have your own agent representing you and working hard on your behalf.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>I have talked to many buyers who have said something like \\\"When we bought, we just used the agent who had the house listed.\\\" If you are thinking about doing this, <strong>use extreme caution!<\\/strong>&nbsp;This should only be done if you have complete trust in the agent. If you just met them, this is a terrible idea! This situation is called Dual Agency and it happens when one agent represents both parties. That agent is trying to get the most money for the seller and also get you the best price. How can they do this? It is tricky and again, should only be done if you have total faith in that agent.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>The way to avoid this is to hire me! Remember, I can show you <em>all<\\/em> homes, not just the ones that I list.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>So when you are my buyer what do you get? Here is a summary of what my buyers get when they work with me:<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>An agent with 24\\/7 availability<\\/li><li>An agent with a A+ BBB rating<\\/li><li>An agent with more recommendations than any other agent in Central Iowa (Trulia.com)<\\/li><li><a href=\\\"http:\\/\\/local.charterhouseiowa.dev\\/truck#entry:9:url\\\">Free Moving Truck<\\/a> (based on availability, first come, first served)<\\/li><li>Access to my interior designer if you need help or suggestion on how to furnish your new home<\\/li><li>Lender recommendations, I can get you to the right lender who will get you closed on time and with no surprises<\\/li><li>An experienced agent, I have been selling real estate full-time since 2005<\\/li><li>Auto-prospecting so you never miss a new listing to the market<\\/li><li>Weekly check-in updates to make sure you are happy<\\/li><li><strong>And much more!<\\/strong><\\/li><\\/ul>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>When you work with me, you are working with a one-of-a-kind agent who simply offers my clients more than other agents. Please read through the client reviews on this site to get a good feel of how I treat my clients. You will never regret hiring me as your agent!<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<h2>Sellers<\\/h2>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>When the time comes to put your home on the market, hiring a real estate professional is a crucial step in the right direction. I realize that there are many home-selling options, but the tried and true method of hiring an agent is still the best option by far. It is proven to be the safest way to sell as well as being the way to generate the highest selling price.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>But, just listing with an agent is not enough! You have to list with an agent who knows how to market and embraces today\'s trends like social media to reach the masses. You want your home exposed everywhere to reach every possible buyer. But you also want to work with an agent who will be enjoyable to work with and one who has great services and benefits to offer you. That agent is me!<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>I would love to talk to you about listing your home but for now, here is a brief summary of what I offer to sellers:<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>An agent with 24\\/7 availability<\\/li><li>An agent with an A+ BBB rating<\\/li><li>An agent with more recommendations than any other agent in Central Iowa (Trulia.com)<\\/li><li>A professional photographer will take the photos of your home<\\/li><li>A Professional videographer will do a video of your home (Certain price ranges only)<\\/li><li>Professional staging assistance, my interior designer will make sure your home is ready to go<\\/li><li><a href=\\\"http:\\/\\/local.charterhouseiowa.dev\\/truck#entry:9:url\\\">Free Moving Truck <\\/a>(Based on availability, first come, first served)<\\/li><li>Professional moving services (at 7% plan while staying in central Iowa)<\\/li><li>Weekly check-in updates to make sure you are happy<\\/li><li>Social Media marketing<\\/li><li><strong>And much more!<\\/strong><\\/li><\\/ul>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>No other agent offers their clients more! You will love your selling experience, I promise. I realize one size does not fit all when it comes to selling your home. I want to talk to you about your specific situation and see if I can help.<\\/p>\"}}},\"3\":\"\"}}','2015-12-28 01:02:06','2015-12-28 01:02:06','3651c0e6-b184-401e-98cc-980634751c13'),
	(70,36,14,1,'en_us',7,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Buyers & Sellers\",\"slug\":\"buyers-sellers\",\"postDate\":1450587650,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":{\"59\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<h3>Buying & Selling<\\/h3><p>So you are are thinking about buying or selling. That is great, here are a few things you need to know!<\\/p>\\r\\n\\r\\n<h2>Buyers<\\/h2>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>Buying a home is a big purchase, one of the biggest you will ever make. Why in the world would anyone want to make that purchase without the help of a professional real estate agent? Beats me, but many try. The most important thing to remember is that when you are buying a home, you DO NOT PAY YOUR AGENT! The seller of the home pays the entire commission and that commission is split between the agent selling the home and the agent representing the buyer. There is no reason to not have your own agent representing you and working hard on your behalf.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>I have talked to many buyers who have said something like \\\"When we bought, we just used the agent who had the house listed.\\\" If you are thinking about doing this, <strong>use extreme caution!<\\/strong>&nbsp;This should only be done if you have complete trust in the agent. If you just met them, this is a terrible idea! This situation is called Dual Agency and it happens when one agent represents both parties. That agent is trying to get the most money for the seller and also get you the best price. How can they do this? It is tricky and again, should only be done if you have total faith in that agent.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>The way to avoid this is to hire me! Remember, I can show you <em>all<\\/em> homes, not just the ones that I list.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>So when you are my buyer what do you get? Here is a summary of what my buyers get when they work with me:<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>An agent with 24\\/7 availability<\\/li><li>An agent with a A+ BBB rating<\\/li><li>An agent with more recommendations than any other agent in Central Iowa (Trulia.com)<\\/li><li><a href=\\\"http:\\/\\/local.charterhouseiowa.dev\\/truck#entry:9:url\\\">Free Moving Truck<\\/a> (based on availability, first come, first served)<\\/li><li>Access to my interior designer if you need help or suggestion on how to furnish your new home<\\/li><li>Lender recommendations, I can get you to the right lender who will get you closed on time and with no surprises<\\/li><li>An experienced agent, I have been selling real estate full-time since 2005<\\/li><li>Auto-prospecting so you never miss a new listing to the market<\\/li><li>Weekly check-in updates to make sure you are happy<\\/li><li><strong>And much more!<\\/strong><\\/li><\\/ul>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>When you work with me, you are working with a one-of-a-kind agent who simply offers my clients more than other agents. Please read through the client reviews on this site to get a good feel of how I treat my clients. You will never regret hiring me as your agent!<\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<h2>Sellers<\\/h2>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>When the time comes to put your home on the market, hiring a real estate professional is a crucial step in the right direction. I realize that there are many home-selling options, but the tried and true method of hiring an agent is still the best option by far. It is proven to be the safest way to sell as well as being the way to generate the highest selling price.<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>But, just listing with an agent is not enough! You have to list with an agent who knows how to market and embraces today\'s trends like social media to reach the masses. You want your home exposed everywhere to reach every possible buyer. But you also want to work with an agent who will be enjoyable to work with and one who has great services and benefits to offer you. That agent is me!<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>I would love to talk to you about listing your home but for now, here is a brief summary of what I offer to sellers:<br><\\/p>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<ul><li>An agent with 24\\/7 availability<\\/li><li>An agent with an A+ BBB rating<\\/li><li>An agent with more recommendations than any other agent in Central Iowa (Trulia.com)<\\/li><li>A professional photographer will take the photos of your home<\\/li><li>A Professional videographer will do a video of your home (Certain price ranges only)<\\/li><li>Professional staging assistance, my interior designer will make sure your home is ready to go<\\/li><li><a href=\\\"http:\\/\\/local.charterhouseiowa.dev\\/truck#entry:9:url\\\">Free Moving Truck <\\/a>(Based on availability, first come, first served)<\\/li><li>Professional moving services (at 7% plan while staying in central Iowa)<\\/li><li>Weekly check-in updates to make sure you are happy<\\/li><li>Social Media marketing<\\/li><li><strong>And much more!<\\/strong><\\/li><\\/ul>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n<p>No other agent offers their clients more! You will love your selling experience, I promise. I realize one size does not fit all when it comes to selling your home. I want to talk to you about your specific situation and see if I can help.<\\/p>\"}}},\"3\":\"\"}}','2015-12-28 01:03:42','2015-12-28 01:03:42','cf28f6e5-df26-4af1-aa0a-8639706fdbe5'),
	(71,2,1,1,'en_us',4,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Mark Charter Realty\",\"slug\":\"homepage\",\"postDate\":1450485935,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>s<\\/p>\",\"49\":{\"new1\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>It\\u2019s true, this site doesn\\u2019t have a whole lot of content yet, but don\\u2019t worry. Our web developers have just installed the CMS, and they\\u2019re setting things up for the content editors this very moment. Soon Local.charterhouseiowa.dev will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.<\\/p>\"}}},\"3\":\"\"}}','2015-12-28 01:04:55','2015-12-28 01:04:55','c6882463-1da7-416c-80c7-2a16fc45feed'),
	(72,61,16,1,'en_us',1,'','{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Mortgage - The Tyler Osby Team\",\"slug\":\"mortgage-the-tyler-osby-team\",\"postDate\":1451264919,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"49\":{\"new2\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Tyler Osby is where Mark turns when his clients need a mortgage or a refinance. Tyler is knowledgeable, accountable and trustworthy. He gets the job done every time!<\\/p>\"}}},\"48\":\"\",\"47\":\"(515) 257-6729\",\"46\":\" http:\\/\\/www.wealthwithmortgage.com\"}}','2015-12-28 01:08:39','2015-12-28 01:08:39','ddee499f-eb24-4945-91b8-7519444d30ba'),
	(73,61,16,1,'en_us',2,'','{\"typeId\":\"16\",\"authorId\":\"1\",\"title\":\"Mortgage - The Tyler Osby Team\",\"slug\":\"mortgage-the-tyler-osby-team\",\"postDate\":1451264880,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"49\":{\"62\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"\"}}},\"48\":\"\",\"3\":\"\",\"47\":\"(515) 257-6729\",\"46\":\" http:\\/\\/www.wealthwithmortgage.com\"}}','2015-12-28 01:15:49','2015-12-28 01:15:49','8404347d-2bb1-4d59-896d-1a9ba6759eb0'),
	(74,61,16,1,'en_us',3,'','{\"typeId\":\"16\",\"authorId\":\"1\",\"title\":\"Mortgage - The Tyler Osby Team\",\"slug\":\"mortgage-the-tyler-osby-team\",\"postDate\":1451264880,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"1\":\"<p>Tyler Osby is where Mark turns when his clients need a mortgage or a refinance. Tyler is knowledgeable, accountable and trustworthy. He gets the job done every time!<\\/p>\",\"48\":\"\",\"3\":\"\",\"47\":\"(515) 257-6729\",\"46\":\" http:\\/\\/www.wealthwithmortgage.com\"}}','2015-12-28 01:16:09','2015-12-28 01:16:09','cb748cba-fdf3-46ab-9fa6-c7809e943e53'),
	(75,61,16,1,'en_us',4,'','{\"typeId\":\"16\",\"authorId\":\"1\",\"title\":\"Mortgage - The Tyler Osby Team\",\"slug\":\"mortgage-the-tyler-osby-team\",\"postDate\":1451264880,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"1\":\"<p>Tyler Osby is where Mark turns when his clients need a mortgage or a refinance. Tyler is knowledgeable, accountable and trustworthy. He gets the job done every time!<\\/p>\",\"48\":\"\",\"3\":\"\",\"47\":\"(515) 257-6729\",\"46\":\" http:\\/\\/www.wealthwithmortgage.com\"}}','2015-12-28 01:18:41','2015-12-28 01:18:41','8fbe4989-d4c2-48eb-b40c-a17fd3161406'),
	(76,2,1,1,'en_us',5,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Mark Charter Realty\",\"slug\":\"homepage\",\"postDate\":1450485935,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":{\"60\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"\"}}},\"3\":\"\"}}','2015-12-28 01:22:51','2015-12-28 01:22:51','b51ced5a-6e71-4f93-8c68-03eb770244a0'),
	(77,2,1,1,'en_us',6,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Mark Charter Realty\",\"slug\":\"homepage\",\"postDate\":1450485935,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":{\"60\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vehicula eget diam ac sagittis. Maecenas efficitur aliquet ipsum, id fringilla quam aliquam sit amet. Quisque eu eros nec lectus fermentum aliquet. Curabitur magna est, vulputate ac quam in, malesuada eleifend felis. In eget suscipit libero. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla facilisi. Etiam tempor facilisis nisi at tempus. Morbi felis felis, eleifend ac nulla ut, dignissim sagittis nisl. Aenean tempus finibus dignissim. Nulla tempus fringilla ante, sed tempus arcu placerat sit amet. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.<\\/p>\"}}},\"3\":\"\"}}','2015-12-28 01:23:47','2015-12-28 01:23:47','7de5a5eb-d9ff-45d0-87e9-c30b901d6322'),
	(78,5,6,1,'en_us',3,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Partners\",\"slug\":\"partners\",\"postDate\":1450486386,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"\",\"49\":{\"new1\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>It\\u2019s true, this site doesn\\u2019t have a whole lot of content yet, but don\\u2019t worry. Our web developers have just installed the CMS, and they\\u2019re setting things up for the content editors this very moment. Soon Local.charterhouseiowa.dev will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.<\\/p>\"}}},\"3\":\"\"}}','2015-12-28 01:27:07','2015-12-28 01:27:07','2d6df5c5-0b95-457e-8492-7bc6b8ccb8ff'),
	(79,5,6,1,'en_us',4,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Partners\",\"slug\":\"partners\",\"postDate\":1450486386,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":{\"63\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"\"}}},\"3\":\"\"}}','2015-12-28 01:28:04','2015-12-28 01:28:04','c7f94a19-92b3-48a5-bf8b-9e9772c42bb5'),
	(80,5,6,1,'en_us',5,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Partners\",\"slug\":\"partners\",\"postDate\":1450486386,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":{\"63\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Maecenas accumsan metus non turpis mattis laoreet. Donec luctus tincidunt sem, vel feugiat sapien ornare ac. Mauris in purus dui. Nullam commodo ut elit quis blandit. Quisque tristique ullamcorper nunc, et elementum odio vestibulum vel. Proin sit amet sollicitudin ligula, sit amet mollis libero. Vestibulum vulputate nec nisl et aliquam. Suspendisse finibus nec nulla sit amet feugiat.<\\/p>\"}}},\"3\":\"\"}}','2015-12-28 01:28:25','2015-12-28 01:28:25','ce1f6b12-6eed-40f8-ab1f-8f5066c96f24'),
	(81,5,6,1,'en_us',6,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Partners\",\"slug\":\"partners\",\"postDate\":1450486386,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":{\"63\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Maecenas accumsan metus non turpis mattis laoreet. Donec luctus tincidunt sem, vel feugiat sapien ornare ac. Mauris in purus dui. Nullam commodo ut elit quis blandit. Quisque tristique ullamcorper nunc, et elementum odio vestibulum vel. Proin sit amet sollicitudin ligula, sit amet mollis libero. Vestibulum vulputate nec nisl et aliquam. Suspendisse finibus nec nulla sit amet feugiat.<\\/p>\"}},\"new1\":{\"type\":\"quote\",\"enabled\":\"1\",\"fields\":{\"quote\":\"<p>Testing<\\/p>\",\"quoteAttribution\":\"Me\"}}},\"3\":\"\"}}','2015-12-28 01:28:51','2015-12-28 01:28:51','73c4a5b2-f517-479a-8c7d-19263eb1a5ec'),
	(82,5,6,1,'en_us',7,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Partners\",\"slug\":\"partners\",\"postDate\":1450486386,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"49\":{\"63\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Maecenas accumsan metus non turpis mattis laoreet. Donec luctus tincidunt sem, vel feugiat sapien ornare ac. Mauris in purus dui. Nullam commodo ut elit quis blandit. Quisque tristique ullamcorper nunc, et elementum odio vestibulum vel. Proin sit amet sollicitudin ligula, sit amet mollis libero. Vestibulum vulputate nec nisl et aliquam. Suspendisse finibus nec nulla sit amet feugiat.<\\/p>\"}}},\"3\":\"\"}}','2015-12-28 01:29:00','2015-12-28 01:29:00','f60c434d-7910-43cf-9e1d-a9d6e14e7f9a'),
	(83,17,4,1,'en_us',7,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Amazing golf course home with too many amenities to list!\",\"slug\":\"amazing-golf-course-home-with-too-many-amenities-to-list\",\"postDate\":1450493520,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"1210 NE 42nd St\",\"street2\":\"\",\"city\":\"Ankeny\",\"state\":\"IA\",\"zip\":\"50021\",\"country\":\"United States\",\"lat\":\"41.76575850\",\"lng\":\"-93.58654680\"},\"8\":\"4.5\",\"9\":\"5\",\"49\":{\"new1\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Maecenas accumsan metus non turpis mattis laoreet. Donec luctus tincidunt sem, vel feugiat sapien ornare ac. Mauris in purus dui. Nullam commodo ut elit quis blandit. Quisque tristique ullamcorper nunc, et elementum odio vestibulum vel. Proin sit amet sollicitudin ligula, sit amet mollis libero. Vestibulum vulputate nec nisl et aliquam. Suspendisse finibus nec nulla sit amet feugiat.<\\/p>\"}}},\"10\":\"\",\"20\":\"\",\"11\":\"504907\",\"3\":\"\",\"17\":\"3 Car Garage\",\"18\":\"$849,900\",\"12\":\"singleFamily\",\"13\":\"\",\"14\":\"2,426\",\"19\":\"yZRyXkGqD2c\",\"21\":\"\"}}','2015-12-28 01:29:55','2015-12-28 01:29:55','835a6577-6a71-4db4-b896-ae05d6313968'),
	(84,18,4,1,'en_us',12,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Absolutely amazing, modern rural-meets-urban set up just outside of Grimes!! A must see at $719,000!\",\"slug\":\"absolutely-amazing-modern-rural-meets-urban-set-up-just-outside-of-grimes\",\"postDate\":1450539000,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"6\":{\"street1\":\"7965 NW 142nd St\",\"street2\":\"\",\"city\":\"Grimes\",\"state\":\"IA\",\"zip\":\"50111\",\"country\":\"United States\",\"lat\":\"41.70576500\",\"lng\":\"-93.81369100\"},\"8\":\"4\",\"9\":\"5\",\"49\":{\"new1\":{\"type\":\"body\",\"enabled\":\"1\",\"fields\":{\"bodyCopy\":\"<p>Maecenas accumsan metus non turpis mattis laoreet. Donec luctus tincidunt sem, vel feugiat sapien ornare ac. Mauris in purus dui. Nullam commodo ut elit quis blandit. Quisque tristique ullamcorper nunc, et elementum odio vestibulum vel. Proin sit amet sollicitudin ligula, sit amet mollis libero. Vestibulum vulputate nec nisl et aliquam. Suspendisse finibus nec nulla sit amet feugiat.<\\/p>\"}}},\"10\":\"\",\"20\":\"1Hw5ay9jY7y\",\"11\":\"506142\",\"3\":\"\",\"17\":\"3 car attached garage - 2 car detached garage\",\"18\":\"$719,000\",\"12\":\"singleFamily\",\"13\":\"\",\"14\":\"2,700\",\"19\":\"\",\"21\":\"1990\"}}','2015-12-28 01:30:06','2015-12-28 01:30:06','310dc293-0507-4e56-8f03-66aeb38a836d'),
	(85,4,5,1,'en_us',3,'','{\"typeId\":\"5\",\"authorId\":null,\"title\":\"Properties\",\"slug\":\"properties\",\"postDate\":1450486881,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis elit massa, ornare et purus a, semper sagittis diam. In condimentum, ipsum eget luctus feugiat, est nisi ullamcorper diam, elementum placerat sapien neque nec ex. Duis pulvinar metus mauris, non commodo mauris cursus vel. Phasellus a ligula in lorem tincidunt semper. Phasellus sed nisl quis eros malesuada egestas eu ultrices massa. Cras commodo velit in arcu vehicula, et egestas magna ullamcorper. Donec sagittis viverra mi vitae mattis. Nulla semper sapien ut consequat dignissim. Nam augue sem, luctus eu dictum et, feugiat eget turpis. Nullam sit amet maximus ante. Pellentesque ut risus semper, fermentum erat id, auctor leo. Etiam cursus facilisis justo, quis interdum enim bibendum in. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque feugiat massa quis enim gravida, vel auctor nisi efficitur.<\\/p>\",\"3\":\"\"}}','2015-12-28 01:30:31','2015-12-28 01:30:31','d283bf74-9c65-4b19-97d2-2a1ac197b489');

/*!40000 ALTER TABLE `entryversions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fieldgroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fieldgroups`;

CREATE TABLE `fieldgroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fieldgroups_name_unq_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `fieldgroups` WRITE;
/*!40000 ALTER TABLE `fieldgroups` DISABLE KEYS */;

INSERT INTO `fieldgroups` (`id`, `name`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Default','2015-12-19 00:45:35','2015-12-19 00:45:35','9bdb4644-e7f2-44c5-8873-16b9251c195b'),
	(2,'Metadata','2015-12-19 01:15:53','2015-12-19 01:15:53','ddc378ed-2b9b-41e1-88f8-d8804c13b1c3'),
	(3,'Reservations','2015-12-19 01:34:50','2015-12-19 01:34:50','4c9be1fd-53c9-476a-b9fd-5d4ff9f7fec8'),
	(4,'Property','2015-12-19 01:48:09','2015-12-19 01:48:09','ce6faef5-724c-4ba6-b900-10f52fdbed99'),
	(5,'Testimonials','2015-12-19 02:25:13','2015-12-19 02:30:57','5808ae43-a3dd-4e2b-8988-63c9b3cef773'),
	(6,'Social Media','2015-12-20 04:19:49','2015-12-20 04:19:49','90173653-e25a-4b0a-976a-c3cc25b93e11'),
	(7,'Ways To Sell','2015-12-20 04:41:11','2015-12-20 04:41:11','d9785488-3dd1-4637-8613-36529f872ba9'),
	(8,'Team Members','2015-12-20 20:23:52','2015-12-20 20:23:52','c53e342a-3446-42db-a90a-fcd7889def27'),
	(9,'Partners','2015-12-20 20:46:16','2015-12-20 20:46:16','c2468c7b-8ecc-4f70-b231-e3d6c72303a2');

/*!40000 ALTER TABLE `fieldgroups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fieldlayoutfields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fieldlayoutfields`;

CREATE TABLE `fieldlayoutfields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layoutId` int(11) NOT NULL,
  `tabId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `required` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sortOrder` tinyint(4) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fieldlayoutfields_layoutId_fieldId_unq_idx` (`layoutId`,`fieldId`),
  KEY `fieldlayoutfields_sortOrder_idx` (`sortOrder`),
  KEY `fieldlayoutfields_tabId_fk` (`tabId`),
  KEY `fieldlayoutfields_fieldId_fk` (`fieldId`),
  CONSTRAINT `fieldlayoutfields_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fieldlayoutfields_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fieldlayoutfields_tabId_fk` FOREIGN KEY (`tabId`) REFERENCES `fieldlayouttabs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `fieldlayoutfields` WRITE;
/*!40000 ALTER TABLE `fieldlayoutfields` DISABLE KEYS */;

INSERT INTO `fieldlayoutfields` (`id`, `layoutId`, `tabId`, `fieldId`, `required`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(2,5,2,1,1,1,'2015-12-19 00:45:35','2015-12-19 00:45:35','2312e57a-8508-44a3-a168-5025221b514e'),
	(3,5,2,2,0,2,'2015-12-19 00:45:35','2015-12-19 00:45:35','d8357c0a-8b6a-4d6e-b337-49d03c17ec44'),
	(18,18,13,5,0,1,'2015-12-19 01:42:03','2015-12-19 01:42:03','fda8a1dd-1e05-42f0-940e-615ab37a3e28'),
	(19,18,13,7,0,2,'2015-12-19 01:42:03','2015-12-19 01:42:03','72838b5f-16a3-4801-af77-23803619bd12'),
	(20,18,13,6,0,3,'2015-12-19 01:42:03','2015-12-19 01:42:03','1f9898e7-4587-48c2-820a-33e679904260'),
	(21,18,13,1,0,4,'2015-12-19 01:42:03','2015-12-19 01:42:03','0b3d7984-ddf9-44ad-ab92-678ef70323df'),
	(22,18,14,4,0,1,'2015-12-19 01:42:03','2015-12-19 01:42:03','edd8ee92-6b40-487e-8ddb-c1e7b558ba87'),
	(25,20,17,1,0,1,'2015-12-19 01:44:04','2015-12-19 01:44:04','9d6117c8-e6f6-4679-a233-5c13f367e468'),
	(26,20,18,3,0,1,'2015-12-19 01:44:04','2015-12-19 01:44:04','26a97e13-aa60-49ae-96dd-473218632820'),
	(30,22,22,1,0,1,'2015-12-19 01:44:56','2015-12-19 01:44:56','6f125e4d-04de-4e30-ae02-2ade0dcadd48'),
	(31,22,23,3,0,1,'2015-12-19 01:44:56','2015-12-19 01:44:56','6ffcd773-f102-489e-a6d5-ce05dc931d2f'),
	(32,24,24,1,0,1,'2015-12-19 01:45:23','2015-12-19 01:45:23','44ff6a3b-c2c8-4bb1-9dd4-29703d2f5f9c'),
	(33,24,25,3,0,1,'2015-12-19 01:45:23','2015-12-19 01:45:23','ed267db2-7c4a-4c9b-ad87-cc0082609088'),
	(34,25,26,1,0,1,'2015-12-19 01:45:41','2015-12-19 01:45:41','3cda4063-80db-4efe-bed7-fc4a215bc1a1'),
	(35,25,27,3,0,1,'2015-12-19 01:45:41','2015-12-19 01:45:41','e918e9d0-2a6f-4c22-9c9c-54bbd3718bfd'),
	(68,32,39,1,0,1,'2015-12-19 02:31:59','2015-12-19 02:31:59','2315377d-dde5-4349-8066-9af63d5b12e2'),
	(69,32,39,15,0,2,'2015-12-19 02:31:59','2015-12-19 02:31:59','894c12ea-9ddb-4706-a18c-7cd97b7214b0'),
	(70,32,40,3,0,1,'2015-12-19 02:31:59','2015-12-19 02:31:59','3ebaef39-7b0b-4365-b9c0-6575f3203381'),
	(182,45,68,22,0,1,'2015-12-20 04:18:47','2015-12-20 04:18:47','924f51f4-be14-47ee-9242-4417685eca1f'),
	(183,45,68,6,0,2,'2015-12-20 04:18:47','2015-12-20 04:18:47','d003d197-ed0c-4694-972d-72770de9c289'),
	(184,45,68,5,0,3,'2015-12-20 04:18:47','2015-12-20 04:18:47','7c93d925-ebd8-4a92-b1a6-5deeeafd2f62'),
	(185,45,68,23,0,4,'2015-12-20 04:18:47','2015-12-20 04:18:47','c1971fa9-985c-4c2d-becc-c62fc17cc1c4'),
	(197,58,78,32,0,1,'2015-12-20 04:51:51','2015-12-20 04:51:51','e3446787-6487-4fb8-9e0f-cfc563caf786'),
	(198,59,79,33,0,1,'2015-12-20 04:51:51','2015-12-20 04:51:51','42875d37-17fa-4005-8501-9bd139973ae6'),
	(199,60,80,34,0,1,'2015-12-20 04:51:51','2015-12-20 04:51:51','b61bb0cb-3401-4b9c-a598-233716f3543e'),
	(206,65,86,37,0,1,'2015-12-20 14:40:47','2015-12-20 14:40:47','637f1142-a2c6-480b-be8e-54ef103864f2'),
	(207,65,86,28,0,2,'2015-12-20 14:40:47','2015-12-20 14:40:47','d23274b3-a74c-4822-92d7-d22878f5e5ee'),
	(208,65,86,35,0,3,'2015-12-20 14:40:47','2015-12-20 14:40:47','a8e5a3be-923f-48e2-a2af-e6bf4060cba7'),
	(209,65,86,36,0,4,'2015-12-20 14:40:47','2015-12-20 14:40:47','eb24a86b-08eb-4721-959b-a102d9316695'),
	(213,68,89,39,0,1,'2015-12-20 14:57:48','2015-12-20 14:57:48','8b97c479-0d27-4ab0-b77a-93f8e331890a'),
	(224,74,94,41,0,1,'2015-12-20 20:39:56','2015-12-20 20:39:56','e47003ba-b941-459c-8b3b-59587a799a75'),
	(225,74,94,43,0,2,'2015-12-20 20:39:56','2015-12-20 20:39:56','5e7f59a5-ffc8-4038-b9aa-eaab6b601be1'),
	(226,74,94,42,0,3,'2015-12-20 20:39:56','2015-12-20 20:39:56','932ee411-4f9b-411d-8e0a-c271cbc41a51'),
	(227,74,94,45,0,4,'2015-12-20 20:39:56','2015-12-20 20:39:56','4aaeab92-19dd-41db-852d-5ae812de6ec6'),
	(228,74,94,44,0,5,'2015-12-20 20:39:56','2015-12-20 20:39:56','1575ddf8-b4a6-495e-bb2b-8393e9655878'),
	(238,81,99,1,0,1,'2015-12-21 20:41:28','2015-12-21 20:41:28','5dc0c135-699c-4922-aa9c-65351e9c8689'),
	(239,81,100,3,0,1,'2015-12-21 20:41:28','2015-12-21 20:41:28','07f283a1-5685-4d25-affa-e99e85a36c90'),
	(240,82,101,50,0,1,'2015-12-28 00:51:11','2015-12-28 00:51:11','a46c9136-5f8a-4b9c-9584-803d53cfbe71'),
	(241,83,102,51,0,1,'2015-12-28 00:51:11','2015-12-28 00:51:11','66e57229-8660-4ef3-8dec-f923347e0994'),
	(242,84,103,52,0,1,'2015-12-28 00:51:11','2015-12-28 00:51:11','fdb7e693-ff68-4963-9f61-11f9afb064a5'),
	(243,84,103,53,0,2,'2015-12-28 00:51:11','2015-12-28 00:51:11','a5173056-070a-49ca-bcc1-7b79f84866a1'),
	(246,86,106,49,0,1,'2015-12-28 00:53:46','2015-12-28 00:53:46','cc1c3b65-c733-4e95-8e63-d571412b932f'),
	(247,86,107,3,0,1,'2015-12-28 00:53:46','2015-12-28 00:53:46','f7ae8f2f-46ab-48e5-9dc3-f5f17f88cb51'),
	(251,88,110,49,0,1,'2015-12-28 00:58:09','2015-12-28 00:58:09','c1110979-566d-4d3f-a41d-cd8b80c38ddd'),
	(252,88,111,3,0,1,'2015-12-28 00:58:09','2015-12-28 00:58:09','865d24f6-acb8-44c4-8af7-08d52bfa55b1'),
	(256,90,114,49,0,1,'2015-12-28 01:04:27','2015-12-28 01:04:27','d49a82ed-15a8-4c4a-8e53-58ee61fbf1ef'),
	(257,90,115,3,0,1,'2015-12-28 01:04:27','2015-12-28 01:04:27','33b9e9ff-f33f-41a9-9436-ef94fb27d8a7'),
	(261,92,118,49,0,1,'2015-12-28 01:05:05','2015-12-28 01:05:05','b83512a2-44a6-47df-9af4-a939087fec48'),
	(262,92,119,3,0,1,'2015-12-28 01:05:05','2015-12-28 01:05:05','2386106c-d661-4283-b152-2dd7d8ded999'),
	(275,97,126,1,0,1,'2015-12-28 01:16:04','2015-12-28 01:16:04','3183c8ba-9c08-4e60-9d48-d16dd45bee54'),
	(276,97,126,48,0,2,'2015-12-28 01:16:04','2015-12-28 01:16:04','a75d6c60-16fd-490a-9707-6fa5841dc33e'),
	(277,97,126,47,0,3,'2015-12-28 01:16:04','2015-12-28 01:16:04','833fb520-09e1-4df2-aae8-39d789942a51'),
	(278,97,126,46,0,4,'2015-12-28 01:16:04','2015-12-28 01:16:04','a0fe7385-28d3-46ef-874c-0f4a86cf1393'),
	(279,97,127,3,0,1,'2015-12-28 01:16:04','2015-12-28 01:16:04','383ad105-4c52-4bc7-8f0b-a72448d58d84'),
	(280,98,128,49,0,1,'2015-12-28 01:27:19','2015-12-28 01:27:19','0fa252a0-9490-48b5-ac40-f549b199a174'),
	(281,98,129,3,0,1,'2015-12-28 01:27:19','2015-12-28 01:27:19','5ec0e5d1-881a-4eb2-9e79-c7d4c2c365b0'),
	(282,99,130,49,0,1,'2015-12-28 01:29:18','2015-12-28 01:29:18','798ca64b-c12a-4ae9-87bd-ef945fbf9271'),
	(283,99,131,6,0,1,'2015-12-28 01:29:18','2015-12-28 01:29:18','940bcca5-7221-418d-b9a1-8f93718bd465'),
	(284,99,131,11,0,2,'2015-12-28 01:29:18','2015-12-28 01:29:18','b841005f-d92c-4fd6-9925-5208435eaa9f'),
	(285,99,131,18,0,3,'2015-12-28 01:29:18','2015-12-28 01:29:18','4089b0e9-ed4d-4a0d-842c-72dd64f758a9'),
	(286,99,131,21,0,4,'2015-12-28 01:29:18','2015-12-28 01:29:18','b8f122ce-0be5-4671-8c88-0f13a674e0f8'),
	(287,99,131,14,0,5,'2015-12-28 01:29:18','2015-12-28 01:29:18','80567c12-633f-40d8-8746-4804903da51c'),
	(288,99,131,9,0,6,'2015-12-28 01:29:18','2015-12-28 01:29:18','4cbafe02-57d3-4a32-8b1b-c32b1426117f'),
	(289,99,131,8,0,7,'2015-12-28 01:29:18','2015-12-28 01:29:18','43088550-6f2c-4ecd-b452-538c30154a44'),
	(290,99,131,12,0,8,'2015-12-28 01:29:18','2015-12-28 01:29:18','937e3f06-3b1d-4b32-b0ba-a3dcb25127e6'),
	(291,99,131,17,0,9,'2015-12-28 01:29:18','2015-12-28 01:29:18','ecb0a4de-ceac-4210-9065-017802c47103'),
	(292,99,131,19,0,10,'2015-12-28 01:29:18','2015-12-28 01:29:18','df3cc8fe-4015-4ff9-ab5b-9cf2476e9211'),
	(293,99,131,20,0,11,'2015-12-28 01:29:18','2015-12-28 01:29:18','f1f62054-d590-49d4-957a-509d651b0dca'),
	(294,99,131,10,0,12,'2015-12-28 01:29:18','2015-12-28 01:29:18','01467396-44bd-4978-ae66-fe9b7449fa22'),
	(295,99,131,13,0,13,'2015-12-28 01:29:18','2015-12-28 01:29:18','e128332a-dd44-435c-b7f2-bc4918bc74a0'),
	(296,99,132,3,0,1,'2015-12-28 01:29:18','2015-12-28 01:29:18','08c0f17b-42c8-4c6f-9b7c-dff95bb8a4de'),
	(300,101,135,1,0,1,'2015-12-28 01:31:24','2015-12-28 01:31:24','0ad937fc-76d8-4238-8329-74bccb217c22'),
	(301,101,136,3,0,1,'2015-12-28 01:31:24','2015-12-28 01:31:24','adcfd44c-ef97-4dd8-9800-e0e53d9d4a38');

/*!40000 ALTER TABLE `fieldlayoutfields` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fieldlayouts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fieldlayouts`;

CREATE TABLE `fieldlayouts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fieldlayouts_type_idx` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `fieldlayouts` WRITE;
/*!40000 ALTER TABLE `fieldlayouts` DISABLE KEYS */;

INSERT INTO `fieldlayouts` (`id`, `type`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Tag','2015-12-19 00:45:35','2015-12-19 00:45:35','ef0c26f0-3ba9-47b6-8c53-822217960096'),
	(5,'Entry','2015-12-19 00:45:35','2015-12-19 00:45:35','aadf50b7-07cd-4fbf-90b1-002c2e424ad9'),
	(18,'Entry','2015-12-19 01:42:03','2015-12-19 01:42:03','cbf7ff18-b098-4c3e-bdf4-f516a018a93f'),
	(20,'Entry','2015-12-19 01:44:04','2015-12-19 01:44:04','3b96f6ab-2501-4443-9b28-75e66cc41215'),
	(22,'Entry','2015-12-19 01:44:56','2015-12-19 01:44:56','dc6df7b1-2aac-4d79-805e-27fa3aad14be'),
	(24,'Entry','2015-12-19 01:45:23','2015-12-19 01:45:23','c00c3c59-c07d-45ed-aa76-db88bb206f7d'),
	(25,'Entry','2015-12-19 01:45:41','2015-12-19 01:45:41','f0054822-edca-4f62-8c71-3ec7a7352513'),
	(32,'Entry','2015-12-19 02:31:59','2015-12-19 02:31:59','392c4bb3-b0d6-4931-a983-0484af1e0a1a'),
	(45,'GlobalSet','2015-12-20 04:18:47','2015-12-20 04:18:47','74e13416-e477-4abe-98b2-c16179053a12'),
	(46,'GlobalSet','2015-12-20 04:19:31','2015-12-20 04:19:31','9a8b4868-cb79-42df-95e5-6777eac12848'),
	(58,'MatrixBlock','2015-12-20 04:51:51','2015-12-20 04:51:51','8ce10c0d-f214-4068-bcec-f17597199236'),
	(59,'MatrixBlock','2015-12-20 04:51:51','2015-12-20 04:51:51','7385c88c-b222-45b5-855f-35702489bee1'),
	(60,'MatrixBlock','2015-12-20 04:51:51','2015-12-20 04:51:51','76c447e5-2e22-43e6-997a-7812f26f6227'),
	(65,'Category','2015-12-20 14:40:47','2015-12-20 14:40:47','da0016c8-c9f0-4857-94c8-0d34481a7c5f'),
	(68,'GlobalSet','2015-12-20 14:57:48','2015-12-20 14:57:48','fff33e0b-8c13-407f-85b0-9d5374217555'),
	(74,'Category','2015-12-20 20:39:56','2015-12-20 20:39:56','f5ef4cb4-631c-4ac1-bd6b-964181fe7d12'),
	(81,'Entry','2015-12-21 20:41:28','2015-12-21 20:41:28','9853738e-0b15-4282-9b8a-bb083c1458bb'),
	(82,'MatrixBlock','2015-12-28 00:51:11','2015-12-28 00:51:11','3c2fbb25-5db0-4e23-9cdf-42928bf2f014'),
	(83,'MatrixBlock','2015-12-28 00:51:11','2015-12-28 00:51:11','0d1a2dff-7ec2-4a38-a88f-87e83e1a7525'),
	(84,'MatrixBlock','2015-12-28 00:51:11','2015-12-28 00:51:11','03d3ea99-2bd9-43c3-b849-ca7a1e8abd3f'),
	(86,'Entry','2015-12-28 00:53:46','2015-12-28 00:53:46','84bd2db7-3b50-4934-af2e-f6cc54e8193a'),
	(88,'Entry','2015-12-28 00:58:09','2015-12-28 00:58:09','f2a3d6b3-29fb-4aee-a9ba-17766bf10f80'),
	(90,'Entry','2015-12-28 01:04:27','2015-12-28 01:04:27','d9ac7506-84a1-4910-bd66-d98a21b6c819'),
	(92,'Entry','2015-12-28 01:05:05','2015-12-28 01:05:05','5e2c8bb6-5ce2-4662-a1f5-e7b8785da043'),
	(97,'Entry','2015-12-28 01:16:04','2015-12-28 01:16:04','816f8faa-ffed-4332-a839-c6f0ec2daafc'),
	(98,'Entry','2015-12-28 01:27:19','2015-12-28 01:27:19','f565b100-54b9-4b1f-a07e-493e3346d9c6'),
	(99,'Entry','2015-12-28 01:29:18','2015-12-28 01:29:18','33c1448c-aeb7-4e7d-bd7f-3e2ae0e99c0c'),
	(101,'Entry','2015-12-28 01:31:24','2015-12-28 01:31:24','1e52e91e-169f-4bee-99b3-bc6e727730c4');

/*!40000 ALTER TABLE `fieldlayouts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fieldlayouttabs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fieldlayouttabs`;

CREATE TABLE `fieldlayouttabs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layoutId` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` tinyint(4) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fieldlayouttabs_sortOrder_idx` (`sortOrder`),
  KEY `fieldlayouttabs_layoutId_fk` (`layoutId`),
  CONSTRAINT `fieldlayouttabs_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `fieldlayouttabs` WRITE;
/*!40000 ALTER TABLE `fieldlayouttabs` DISABLE KEYS */;

INSERT INTO `fieldlayouttabs` (`id`, `layoutId`, `name`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(2,5,'Content',1,'2015-12-19 00:45:35','2015-12-19 00:45:35','0877ff73-4bb9-4fd7-9b3b-56263e83deb7'),
	(13,18,'Reservation Details',1,'2015-12-19 01:42:03','2015-12-19 01:42:03','1c9f59cb-606b-45ba-9258-08371e991486'),
	(14,18,'Comments',2,'2015-12-19 01:42:03','2015-12-19 01:42:03','2e1a0762-d083-4953-9200-04cdf049072e'),
	(17,20,'Content',1,'2015-12-19 01:44:04','2015-12-19 01:44:04','384aa049-6b71-43d5-8a51-0aeae373bf26'),
	(18,20,'Metadata',2,'2015-12-19 01:44:04','2015-12-19 01:44:04','70771c17-1b96-4c14-94ba-35f5584d01c9'),
	(22,22,'Content',1,'2015-12-19 01:44:56','2015-12-19 01:44:56','a32d6c2c-a9c4-4586-9836-cab943b55623'),
	(23,22,'Metadata',2,'2015-12-19 01:44:56','2015-12-19 01:44:56','1027ea8c-dc1e-4e09-89eb-34beb7aeaa31'),
	(24,24,'Content',1,'2015-12-19 01:45:23','2015-12-19 01:45:23','40116096-6260-40a5-8a8e-8d8f8329ef95'),
	(25,24,'Metadata',2,'2015-12-19 01:45:23','2015-12-19 01:45:23','aec7114c-9964-4a22-ad7c-66c7c65a1691'),
	(26,25,'Content',1,'2015-12-19 01:45:41','2015-12-19 01:45:41','2855862c-988c-41a4-8b80-86a97c909db0'),
	(27,25,'Metadata',2,'2015-12-19 01:45:41','2015-12-19 01:45:41','39d77979-339d-4c5e-8a1f-6dc3636a2d37'),
	(39,32,'Testimonial Details',1,'2015-12-19 02:31:59','2015-12-19 02:31:59','1a4fb6b2-9392-43cb-8971-a514127cc229'),
	(40,32,'Metadata',2,'2015-12-19 02:31:59','2015-12-19 02:31:59','b888e974-851b-48d1-b203-1187434005f8'),
	(68,45,'Content',1,'2015-12-20 04:18:47','2015-12-20 04:18:47','01c7bc79-de7d-43fa-81b1-2238accadc20'),
	(78,58,'Content',1,'2015-12-20 04:51:51','2015-12-20 04:51:51','bd31b3ca-638e-4e5d-8f9e-3960301f20dc'),
	(79,59,'Content',1,'2015-12-20 04:51:51','2015-12-20 04:51:51','e8b78b5a-71b3-488b-a328-ab735c7546a8'),
	(80,60,'Content',1,'2015-12-20 04:51:51','2015-12-20 04:51:51','78bab9b4-f112-4c32-a141-897ef9f3e96d'),
	(86,65,'Selling Options',1,'2015-12-20 14:40:47','2015-12-20 14:40:47','d8ba13c8-afb2-472b-bb76-90d82b303a1b'),
	(89,68,'Content',1,'2015-12-20 14:57:48','2015-12-20 14:57:48','fc5dd8f4-50d1-423c-a39e-8b9e84f6e7be'),
	(94,74,'Team Member Details',1,'2015-12-20 20:39:56','2015-12-20 20:39:56','97f21dfa-c13e-4d08-9833-70aeb5010b22'),
	(99,81,'Content',1,'2015-12-21 20:41:28','2015-12-21 20:41:28','c20de3db-ec28-4c46-ba7c-0880b3f26e10'),
	(100,81,'Metadata',2,'2015-12-21 20:41:28','2015-12-21 20:41:28','710ba0e6-5e47-487f-958d-f6887c2662c3'),
	(101,82,'Content',1,'2015-12-28 00:51:11','2015-12-28 00:51:11','6539ec31-0095-47f4-816e-60f6601a4dee'),
	(102,83,'Content',1,'2015-12-28 00:51:11','2015-12-28 00:51:11','82cd44e7-6c06-490b-ba4c-09f3a5f748fc'),
	(103,84,'Content',1,'2015-12-28 00:51:11','2015-12-28 00:51:11','f5c94f47-aed7-4aa2-a9aa-b5ed6d7f4d68'),
	(106,86,'Default',1,'2015-12-28 00:53:46','2015-12-28 00:53:46','6c4c1286-1790-480d-b558-ff83da350880'),
	(107,86,'Metadata',2,'2015-12-28 00:53:46','2015-12-28 00:53:46','43c6c781-a560-4ec5-9d7f-436a87a108db'),
	(110,88,'Content',1,'2015-12-28 00:58:09','2015-12-28 00:58:09','26835350-b9fb-42d2-b06b-f27dade29919'),
	(111,88,'Metadata',2,'2015-12-28 00:58:09','2015-12-28 00:58:09','554bef70-811b-4941-b122-44f138a9298d'),
	(114,90,'Content',1,'2015-12-28 01:04:27','2015-12-28 01:04:27','f4ef7aad-5156-400a-b3b1-a4cdf29456ac'),
	(115,90,'Metadata',2,'2015-12-28 01:04:27','2015-12-28 01:04:27','0a325c60-9cc2-434f-a933-62060abc1422'),
	(118,92,'Content',1,'2015-12-28 01:05:05','2015-12-28 01:05:05','6c58be22-4c6d-445b-a952-3810caf78da5'),
	(119,92,'Metadata',2,'2015-12-28 01:05:05','2015-12-28 01:05:05','7d295406-ec7a-4dc7-8e6b-9179a7932b91'),
	(126,97,'Content',1,'2015-12-28 01:16:04','2015-12-28 01:16:04','cd785750-cf63-42b2-874f-f127c2010bbc'),
	(127,97,'Metadata',2,'2015-12-28 01:16:04','2015-12-28 01:16:04','cfe2ea24-5b92-4d55-85e4-c6a67697080c'),
	(128,98,'Content',1,'2015-12-28 01:27:19','2015-12-28 01:27:19','8f427723-e68e-40b7-8135-f46b9602d4d3'),
	(129,98,'Metadata',2,'2015-12-28 01:27:19','2015-12-28 01:27:19','3842faec-05ad-444b-9909-615a2aa069f4'),
	(130,99,'Content',1,'2015-12-28 01:29:18','2015-12-28 01:29:18','4d4e6cf3-3306-44ef-9cff-121b0da8c716'),
	(131,99,'Property Details',2,'2015-12-28 01:29:18','2015-12-28 01:29:18','682f6185-182c-4668-9f28-2063d96e9523'),
	(132,99,'Metadata',3,'2015-12-28 01:29:18','2015-12-28 01:29:18','00fa6f3e-9ed9-4242-8039-5da0d6f0c267'),
	(135,101,'Content',1,'2015-12-28 01:31:24','2015-12-28 01:31:24','c228a280-0077-4fc3-b2f6-872c16f39322'),
	(136,101,'Metadata',2,'2015-12-28 01:31:24','2015-12-28 01:31:24','65e34cc3-9c98-4a76-8974-d531597681d6');

/*!40000 ALTER TABLE `fieldlayouttabs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fields`;

CREATE TABLE `fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(58) COLLATE utf8_unicode_ci NOT NULL,
  `context` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'global',
  `instructions` text COLLATE utf8_unicode_ci,
  `translatable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fields_handle_context_unq_idx` (`handle`,`context`),
  KEY `fields_context_idx` (`context`),
  KEY `fields_groupId_fk` (`groupId`),
  CONSTRAINT `fields_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `fieldgroups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `fields` WRITE;
/*!40000 ALTER TABLE `fields` DISABLE KEYS */;

INSERT INTO `fields` (`id`, `groupId`, `name`, `handle`, `context`, `instructions`, `translatable`, `type`, `settings`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'Body','body','global',NULL,1,'RichText','{\"configFile\":\"Standard.json\",\"columnType\":\"text\"}','2015-12-19 00:45:35','2015-12-19 00:45:35','f12c5cda-a50e-46f7-b8d6-f8f71bc729e2'),
	(2,1,'Tags','tags','global',NULL,0,'Tags','{\"source\":\"taggroup:1\"}','2015-12-19 00:45:35','2015-12-19 00:45:35','394d6e3d-fdf4-4276-b052-186289f83725'),
	(3,2,'Page Description','pageDescription','global','Best under 155 characters, the page description attribute provides concise explanations of the contents of the web page. Page descriptions are commonly used on search engine result pages (SERPs) to display preview snippets for a given page.',0,'PlainText','{\"placeholder\":\"Here is a description of the applicable page.\",\"maxLength\":\"155\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-19 01:16:51','2015-12-19 01:16:51','99df8b7c-bdbb-4fc7-98c0-e85741f652f6'),
	(4,3,'Comments','comments','global','Use this field to save any comments about this contact. This field is not visible to the public.',0,'RichText','{\"configFile\":\"Standard.json\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"\",\"columnType\":\"text\"}','2015-12-19 01:35:15','2015-12-19 01:35:15','2e31507f-fa4b-446c-99ac-82b8028c0c29'),
	(5,3,'Email','email','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-19 01:39:24','2015-12-19 01:39:24','1bf9b05a-a20a-4a24-b4e1-bcf61f9f771d'),
	(6,1,'Address','address','global','',0,'SmartMap_Address','{\"layout\":{\"street1\":{\"width\":100,\"enable\":1},\"street2\":{\"width\":100,\"enable\":1},\"city\":{\"width\":50,\"enable\":1},\"state\":{\"width\":15,\"enable\":1},\"zip\":{\"width\":35,\"enable\":1},\"country\":{\"width\":100,\"enable\":0},\"lat\":{\"width\":50,\"enable\":0},\"lng\":{\"width\":50,\"enable\":0}}}','2015-12-19 01:40:42','2015-12-19 01:40:42','0991f5f1-a151-40b5-81dc-2f6d4a78729c'),
	(7,3,'Phone Number','phone','global','',0,'PlainText','{\"placeholder\":\"(123) 456-7890\",\"maxLength\":\"14\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-19 01:41:25','2015-12-19 01:41:25','9e0f257f-3823-4dc2-8f80-9a54f281676a'),
	(8,4,'Baths','baths','global','How many bathrooms does this property have?',0,'Dropdown','{\"options\":[{\"label\":\"Select\",\"value\":\"select\",\"default\":\"\"},{\"label\":\"1\",\"value\":\"1\",\"default\":\"\"},{\"label\":\"1.5\",\"value\":\"1.5\",\"default\":\"\"},{\"label\":\"2\",\"value\":\"2\",\"default\":\"\"},{\"label\":\"2.5\",\"value\":\"2.5\",\"default\":\"\"},{\"label\":\"3\",\"value\":\"3\",\"default\":\"\"},{\"label\":\"3.5\",\"value\":\"3.5\",\"default\":\"\"},{\"label\":\"4\",\"value\":\"4\",\"default\":\"\"},{\"label\":\"4.5\",\"value\":\"4.5\",\"default\":\"\"},{\"label\":\"5\",\"value\":\"5\",\"default\":\"\"}]}','2015-12-19 01:49:03','2015-12-19 01:49:03','bdecaabb-c7c7-47a2-ab3a-d63b3c5a2c67'),
	(9,4,'Beds','beds','global','How many bedrooms does this property have?',0,'Dropdown','{\"options\":[{\"label\":\"Select\",\"value\":\"\",\"default\":\"\"},{\"label\":\"1\",\"value\":\"1\",\"default\":\"\"},{\"label\":\"2\",\"value\":\"2\",\"default\":\"\"},{\"label\":\"3\",\"value\":\"3\",\"default\":\"\"},{\"label\":\"4\",\"value\":\"5\",\"default\":\"\"},{\"label\":\"5\",\"value\":\"5\",\"default\":\"\"},{\"label\":\"6\",\"value\":\"6\",\"default\":\"\"}]}','2015-12-19 01:49:48','2015-12-19 01:49:48','dcc5a984-abc4-4228-9783-b71b09bd5ab6'),
	(10,4,'Foreclosure','foreclosure','global','',0,'Lightswitch','{\"default\":\"\"}','2015-12-19 01:50:09','2015-12-19 01:50:09','b777f679-eb4c-4014-93ac-812a1c22fc13'),
	(11,4,'MLS Number','mlsNumber','global','Enter the MLS number here. The system will generate the full url automatically.',0,'PlainText','{\"placeholder\":\"504907\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-19 01:50:23','2015-12-19 15:36:46','f381fe3f-620d-4091-ae02-cb37deb05c73'),
	(12,4,'Property Type','propertyType','global','Choose what type of property this is.',0,'Dropdown','{\"options\":[{\"label\":\"Select\",\"value\":\"\",\"default\":\"1\"},{\"label\":\"Single Family\",\"value\":\"singleFamily\",\"default\":\"\"}]}','2015-12-19 01:50:58','2015-12-19 03:13:00','82349bc5-e270-4754-88e4-e64c724fac8e'),
	(13,4,'Short Sale','shortSale','global','',0,'Lightswitch','{\"default\":\"\"}','2015-12-19 01:51:12','2015-12-19 01:51:12','1ef38866-d2cd-4d4d-a71a-96a1e9ca7422'),
	(14,4,'Square Feet','squareFeet','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-19 01:51:25','2015-12-19 01:51:25','e8d19888-44ba-4926-8b48-f65ad01441fe'),
	(15,5,'Attribution','source','global','Provide testimonial attribution here.',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-19 02:31:18','2015-12-28 01:25:02','dd4e1e3c-9972-446e-95f8-6fba64f9e301'),
	(17,4,'Parking','parking','global','What type of parking does this property have?',0,'PlainText','{\"placeholder\":\"3 Car Garage\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-19 03:16:32','2015-12-19 03:16:32','b488cbdd-facb-445e-8d23-b68377d385cf'),
	(18,4,'Price','price','global','Enter the listing price for this property. You will need to include the \"$\" as well.',0,'PlainText','{\"placeholder\":\"$849,900\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-19 15:06:05','2015-12-19 15:14:01','36c97ac2-1c58-4e00-8b49-af45642b6797'),
	(19,4,'Video URL','videoUrl','global','Enter the video ID for this property. The system will generate the full url automatically.',0,'PlainText','{\"placeholder\":\"yZRyXkGqD2c\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-19 15:12:26','2015-12-19 15:36:52','0414a4c3-f8f5-4994-ba6a-e8dddb55ee02'),
	(20,4,'Matterport URL','matterportUrl','global','Enter the Matterport video ID for this property. The system will generate the full url automatically.',0,'PlainText','{\"placeholder\":\"1Hw5ay9jY7y\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-19 15:32:05','2015-12-19 15:36:58','67f2bc5f-03db-415f-8fef-4a3d4ade4706'),
	(21,4,'Year Built','yearBuilt','global','What year was this property built?',0,'PlainText','{\"placeholder\":\"2003\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-19 16:05:06','2015-12-19 16:05:06','fb08ef31-c3f9-4a02-ab3d-a6da2c788de0'),
	(22,1,'Office Title','officeTitle','global','',0,'PlainText','{\"placeholder\":\"Remax Real Estate Concepts\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-20 04:17:37','2015-12-20 04:17:37','d1d6e057-8b17-4285-b704-9c9efb033549'),
	(23,1,'Office Phone','officePhone','global','',0,'PlainText','{\"placeholder\":\"(123) 456-7890\",\"maxLength\":\"14\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-20 04:18:33','2015-12-20 04:18:33','81c7631f-fea6-49bc-b5fa-08b945d6870e'),
	(24,6,'Facebook','facebook','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-20 04:20:40','2015-12-20 04:20:40','e65e1429-3526-44cb-9954-d64ececd5cf7'),
	(25,6,'Twitter','twitter','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-20 04:21:52','2015-12-20 04:21:52','4b5ebbc8-b438-4d04-b09e-7fa28531cad5'),
	(26,6,'LinkedIn','linkedin','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-20 04:22:12','2015-12-20 04:22:12','85bf3994-3dd1-40ee-92d4-9fd56df0b52f'),
	(27,6,'YouTube','youtube','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-20 04:22:23','2015-12-20 04:22:23','853d095b-4558-4b32-b222-1d372fdafaba'),
	(28,7,'Option Description','optionDescription','global','Provide some information about this selling option.',0,'RichText','{\"configFile\":\"Standard.json\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"\",\"columnType\":\"text\"}','2015-12-20 04:43:57','2015-12-20 14:34:53','3411aec3-fd54-4b2e-ad50-9635f6cae6fc'),
	(32,NULL,'Description','description','matrixBlockType:2','Provide a description of this option.',0,'RichText','{\"configFile\":\"Standard.json\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"\",\"columnType\":\"text\"}','2015-12-20 04:51:08','2015-12-20 04:51:51','0f6efc07-58be-494b-98c5-55f3402b1e7b'),
	(33,NULL,'Details','optionDetails','matrixBlockType:3','Provide what this selling option includes.',0,'RichText','{\"configFile\":\"Standard.json\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"\",\"columnType\":\"text\"}','2015-12-20 04:51:08','2015-12-20 04:51:51','b97e0a9a-2703-48ff-ac4f-8cc4752a6d5a'),
	(34,NULL,'Commission','optionCommission','matrixBlockType:4','Select the commission rate for this option.',0,'Dropdown','{\"options\":[{\"label\":\"Select\",\"value\":\"\",\"default\":\"1\"},{\"label\":\"4\",\"value\":\"4\",\"default\":\"\"},{\"label\":\"5\",\"value\":\"5\",\"default\":\"\"},{\"label\":\"6\",\"value\":\"6\",\"default\":\"\"},{\"label\":\"7\",\"value\":\"7\",\"default\":\"\"},{\"label\":\"8\",\"value\":\"8\",\"default\":\"\"}]}','2015-12-20 04:51:08','2015-12-20 04:51:51','dfd012d0-d91d-4686-a9bb-8af885ef647c'),
	(35,7,'Commission','optionCommission','global','Choose the commission rate for this option.',0,'Dropdown','{\"options\":[{\"label\":\"Select\",\"value\":\"\",\"default\":\"1\"},{\"label\":\"2\",\"value\":\"2\",\"default\":\"\"},{\"label\":\"3\",\"value\":\"3\",\"default\":\"\"},{\"label\":\"4\",\"value\":\"4\",\"default\":\"\"},{\"label\":\"5\",\"value\":\"5\",\"default\":\"\"},{\"label\":\"6\",\"value\":\"6\",\"default\":\"\"},{\"label\":\"7\",\"value\":\"7\",\"default\":\"\"},{\"label\":\"8\",\"value\":\"8\",\"default\":\"\"},{\"label\":\"9\",\"value\":\"9\",\"default\":\"\"}]}','2015-12-20 14:38:12','2015-12-20 14:38:12','e20dd876-8935-4164-8ff0-666075ea7049'),
	(36,7,'Option Benefits','optionBenefits','global','Provide the benefits of this option.',0,'RichText','{\"configFile\":\"Standard.json\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"\",\"columnType\":\"text\"}','2015-12-20 14:39:58','2015-12-20 14:39:58','c2ab6649-a94e-4ad5-aea0-f6380fd44243'),
	(37,7,'Option Icon','optionIcon','global','Upload an icon for this option.',0,'Assets','{\"useSingleFolder\":\"\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"limit\":\"\",\"viewMode\":\"list\",\"selectionLabel\":\"\"}','2015-12-20 14:40:27','2015-12-21 02:53:22','822ca102-29fc-4699-9192-ef2da02a039b'),
	(39,1,'Disclaimer','markCharterDisclaimer','global','Mark Charter disclaimer language goes here.',0,'RichText','{\"configFile\":\"Standard.json\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"\",\"columnType\":\"text\"}','2015-12-20 14:57:35','2015-12-20 15:02:34','8c1890bc-50ba-4aa2-86c0-df2097d9fd08'),
	(41,8,'Title/Position','teamMemberTitle','global','This team members title.',0,'PlainText','{\"placeholder\":\"Owner \\/ Realtor\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-20 20:24:51','2015-12-20 20:27:36','f468c425-0ef6-4116-bbd5-a6943e861cb8'),
	(42,8,'Phone','teamMemberPhone','global','The team member\'s phone number.',0,'PlainText','{\"placeholder\":\"(123) 456-7890\",\"maxLength\":\"14\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-20 20:25:39','2015-12-20 20:25:39','82584adc-4593-451c-be06-a82943d2efbf'),
	(43,8,'Email','teamMemberEmail','global','The team member\'s email address.',0,'PlainText','{\"placeholder\":\"name@charterhouseiowa.com\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-20 20:26:15','2015-12-20 20:26:15','69ae1a84-355f-49b7-bf44-0c72b712b07b'),
	(44,8,'Bio','teamMemberBio','global','Provide a brief bio for this team member.',0,'RichText','{\"configFile\":\"Standard.json\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"\",\"columnType\":\"text\"}','2015-12-20 20:28:46','2015-12-20 20:28:46','2cfb8421-4c01-4f21-9313-ed00386f3492'),
	(45,8,'Photo','teamMemberPhoto','global','Upload a photo of this team member.',0,'Assets','{\"useSingleFolder\":\"\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"1\",\"allowedKinds\":[\"image\"],\"limit\":\"1\",\"viewMode\":\"list\",\"selectionLabel\":\"Add an image\"}','2015-12-20 20:39:35','2015-12-20 20:39:35','3813b2d6-21f9-4600-ae63-09b1c566ff55'),
	(46,9,'Website','partnerWebsite','global','',0,'PlainText','{\"placeholder\":\"http:\\/\\/www.website.com\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-20 20:46:49','2015-12-20 20:46:49','09a0aa5c-736f-463d-9bc6-44257358d37a'),
	(47,9,'Phone Number','partnerPhone','global','',0,'PlainText','{\"placeholder\":\"(123) 456-7890\",\"maxLength\":\"14\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-20 20:47:12','2015-12-20 20:47:12','1373cabd-805a-4fd2-87a7-e06b9148a0fe'),
	(48,9,'Logo','partnerLogo','global','Upload a logo for this partner',0,'Assets','{\"useSingleFolder\":\"\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"1\",\"allowedKinds\":[\"image\"],\"limit\":\"1\",\"viewMode\":\"list\",\"selectionLabel\":\"Add a logo\"}','2015-12-20 20:56:18','2015-12-20 20:56:18','88083212-5080-4edf-92c3-03fd649cbcdd'),
	(49,1,'Content Builder','matrix','global','Use this feature to build out the content for this page. You can arrange fields in any order to create a different layout.',0,'Matrix','{\"maxBlocks\":null}','2015-12-28 00:51:11','2015-12-28 00:51:11','513a5d1b-db33-4b08-bfcd-79b0e9ff044b'),
	(50,NULL,'Headline','headline','matrixBlockType:5','Use this field to create a primary headline.',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-28 00:51:11','2015-12-28 00:51:11','1d650798-bb86-46bf-a35d-56df36711c03'),
	(51,NULL,'Body','bodyCopy','matrixBlockType:6','Use this field to enter a block of content.',0,'RichText','{\"configFile\":\"Standard.json\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"\",\"columnType\":\"text\"}','2015-12-28 00:51:11','2015-12-28 00:51:11','e0e56807-215b-4bee-b943-021feb3270ea'),
	(52,NULL,'Quote','quote','matrixBlockType:7','Use this field to create a pull quote.',0,'RichText','{\"configFile\":\"Simple.json\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"\",\"columnType\":\"text\"}','2015-12-28 00:51:11','2015-12-28 00:51:11','0b51a680-4907-4fb3-84d5-c51761a2383f'),
	(53,NULL,'Quote Attribution','quoteAttribution','matrixBlockType:7','Provide quote attribution here.',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2015-12-28 00:51:11','2015-12-28 00:51:11','dc079e1e-9a89-4106-a5cc-3618100a909e');

/*!40000 ALTER TABLE `fields` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table globalsets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `globalsets`;

CREATE TABLE `globalsets` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fieldLayoutId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `globalsets_name_unq_idx` (`name`),
  UNIQUE KEY `globalsets_handle_unq_idx` (`handle`),
  KEY `globalsets_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `globalsets_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `globalsets_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `globalsets` WRITE;
/*!40000 ALTER TABLE `globalsets` DISABLE KEYS */;

INSERT INTO `globalsets` (`id`, `name`, `handle`, `fieldLayoutId`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(19,'Office Information','officeInformation',45,'2015-12-19 21:06:00','2015-12-20 04:18:47','9ba6f9f7-11c5-4a2a-8b58-4dda030f6a39'),
	(20,'Social Media','socialMedia',46,'2015-12-20 04:19:31','2015-12-20 04:19:31','544fbd30-0bcd-4a78-b23c-3ca02abbcf25'),
	(37,'Disclaimer','disclaimer',68,'2015-12-20 14:56:56','2015-12-20 14:57:48','24a9377e-82c3-4e26-b03f-b7e7cc0c111c');

/*!40000 ALTER TABLE `globalsets` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table info
# ------------------------------------------------------------

DROP TABLE IF EXISTS `info`;

CREATE TABLE `info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `build` int(11) unsigned NOT NULL,
  `schemaVersion` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `releaseDate` datetime NOT NULL,
  `edition` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `siteName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `siteUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timezone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `on` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `maintenance` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `track` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `info` WRITE;
/*!40000 ALTER TABLE `info` DISABLE KEYS */;

INSERT INTO `info` (`id`, `version`, `build`, `schemaVersion`, `releaseDate`, `edition`, `siteName`, `siteUrl`, `timezone`, `on`, `maintenance`, `track`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'2.5',2755,'2.5.11','2015-12-16 22:42:32',0,'Mark Charter Realty','http://local.charterhouseiowa.dev','America/Chicago',1,0,'stable','2015-12-19 00:45:31','2015-12-19 02:40:31','f76a2a20-257e-4f36-a3cb-7f9bcd807ce4');

/*!40000 ALTER TABLE `info` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table locales
# ------------------------------------------------------------

DROP TABLE IF EXISTS `locales`;

CREATE TABLE `locales` (
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` tinyint(4) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`locale`),
  KEY `locales_sortOrder_idx` (`sortOrder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `locales` WRITE;
/*!40000 ALTER TABLE `locales` DISABLE KEYS */;

INSERT INTO `locales` (`locale`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	('en_us',1,'2015-12-19 00:45:31','2015-12-19 00:45:31','3e363787-542c-47f4-9e94-6d0cacafe461');

/*!40000 ALTER TABLE `locales` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table matrixblocks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `matrixblocks`;

CREATE TABLE `matrixblocks` (
  `id` int(11) NOT NULL,
  `ownerId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `typeId` int(11) DEFAULT NULL,
  `sortOrder` tinyint(4) DEFAULT NULL,
  `ownerLocale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `matrixblocks_ownerId_idx` (`ownerId`),
  KEY `matrixblocks_fieldId_idx` (`fieldId`),
  KEY `matrixblocks_typeId_idx` (`typeId`),
  KEY `matrixblocks_sortOrder_idx` (`sortOrder`),
  KEY `matrixblocks_ownerLocale_fk` (`ownerLocale`),
  CONSTRAINT `matrixblocks_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixblocks_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixblocks_ownerId_fk` FOREIGN KEY (`ownerId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixblocks_ownerLocale_fk` FOREIGN KEY (`ownerLocale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `matrixblocks_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `matrixblocktypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `matrixblocks` WRITE;
/*!40000 ALTER TABLE `matrixblocks` DISABLE KEYS */;

INSERT INTO `matrixblocks` (`id`, `ownerId`, `fieldId`, `typeId`, `sortOrder`, `ownerLocale`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(25,23,28,2,1,NULL,'2015-12-20 04:53:04','2015-12-20 04:53:15','8a672f73-55e3-4e50-9874-fda40d159ddd'),
	(26,23,28,4,2,NULL,'2015-12-20 04:53:04','2015-12-20 04:53:15','a0f33de7-38d3-415b-98d3-6fae6a9190f3'),
	(27,23,28,3,3,NULL,'2015-12-20 04:53:04','2015-12-20 04:53:15','7000c733-3691-4aee-934c-345f2d5417f9'),
	(29,28,28,2,1,NULL,'2015-12-20 04:53:50','2015-12-20 04:54:19','d2330d40-004f-4e59-b878-bd68ec3cea3e'),
	(30,28,28,4,2,NULL,'2015-12-20 04:53:50','2015-12-20 04:54:19','f8944641-c765-4921-a7b7-d04c08b8d86d'),
	(31,28,28,3,3,NULL,'2015-12-20 04:54:19','2015-12-20 04:54:19','bbe22d20-3176-4dd6-9c0d-68457a304f6f'),
	(33,32,28,2,1,NULL,'2015-12-20 04:55:11','2015-12-20 04:55:11','baf57b96-3ab7-452e-ab73-57f95a3662e2'),
	(34,32,28,3,2,NULL,'2015-12-20 04:55:11','2015-12-20 04:55:11','84901b58-8c00-40c3-bbde-0175a0dbf4f2'),
	(35,32,28,4,3,NULL,'2015-12-20 04:55:11','2015-12-20 04:55:11','453b59cc-255a-4f77-bdaa-ef91eabf989b'),
	(55,38,49,6,2,NULL,'2015-12-28 00:53:16','2015-12-28 00:57:07','6e271221-e033-4dfe-a491-76602329a975'),
	(56,38,49,5,1,NULL,'2015-12-28 00:55:12','2015-12-28 00:57:07','1f95b295-5062-4efd-95e7-ff9cc5106154'),
	(57,38,49,7,3,NULL,'2015-12-28 00:55:54','2015-12-28 00:57:07','94c1864d-4cbe-4d7b-8bf0-73b3d90b8e20'),
	(58,38,49,6,4,NULL,'2015-12-28 00:57:07','2015-12-28 00:57:07','47becd02-18d1-4110-b84b-93d83cf8a9dd'),
	(59,36,49,6,1,NULL,'2015-12-28 00:57:53','2015-12-28 01:03:42','8281b1d7-7e2f-4572-a8f3-a9bf7ae0943f'),
	(60,2,49,6,1,NULL,'2015-12-28 01:04:55','2015-12-28 01:23:47','67b9b07d-f984-462e-98d0-155597ca2f44'),
	(62,61,49,6,1,NULL,'2015-12-28 01:08:39','2015-12-28 01:15:49','3943fb3d-8043-4569-a8cc-22bd404beff4'),
	(63,5,49,6,1,NULL,'2015-12-28 01:27:07','2015-12-28 01:29:00','70e50044-5623-439e-8b83-31231cd2ea52'),
	(65,17,49,6,1,NULL,'2015-12-28 01:29:55','2015-12-28 01:29:55','733b58fd-efb3-4e06-88a1-47b1d564d3ec'),
	(66,18,49,6,1,NULL,'2015-12-28 01:30:06','2015-12-28 01:30:06','6b8c72be-97a1-40d9-a458-3cba444097fe');

/*!40000 ALTER TABLE `matrixblocks` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table matrixblocktypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `matrixblocktypes`;

CREATE TABLE `matrixblocktypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` tinyint(4) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `matrixblocktypes_name_fieldId_unq_idx` (`name`,`fieldId`),
  UNIQUE KEY `matrixblocktypes_handle_fieldId_unq_idx` (`handle`,`fieldId`),
  KEY `matrixblocktypes_fieldId_fk` (`fieldId`),
  KEY `matrixblocktypes_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `matrixblocktypes_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixblocktypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `matrixblocktypes` WRITE;
/*!40000 ALTER TABLE `matrixblocktypes` DISABLE KEYS */;

INSERT INTO `matrixblocktypes` (`id`, `fieldId`, `fieldLayoutId`, `name`, `handle`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(2,28,58,'Description','description',1,'2015-12-20 04:51:08','2015-12-20 04:51:51','7a412cca-fef5-4ae3-94b9-fa0bc972193b'),
	(3,28,59,'Details','details',2,'2015-12-20 04:51:08','2015-12-20 04:51:51','da10b97b-f9e2-4bbf-998c-82ea01903255'),
	(4,28,60,'Commission','commission',3,'2015-12-20 04:51:08','2015-12-20 04:51:51','014ca1d3-9bc4-4e14-8627-a2b995868265'),
	(5,49,82,'Headline','headline',1,'2015-12-28 00:51:11','2015-12-28 00:51:11','5a7dec2a-982e-40bf-b581-d50e4adb2ec6'),
	(6,49,83,'Body','body',2,'2015-12-28 00:51:11','2015-12-28 00:51:11','4744ff05-cd2c-45c8-ac4e-5c48b3461414'),
	(7,49,84,'Quote','quote',3,'2015-12-28 00:51:11','2015-12-28 00:51:11','ddcb9f5a-ad17-459b-b118-a1c613d1d769');

/*!40000 ALTER TABLE `matrixblocktypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table matrixcontent_matrix
# ------------------------------------------------------------

DROP TABLE IF EXISTS `matrixcontent_matrix`;

CREATE TABLE `matrixcontent_matrix` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `field_headline_headline` text COLLATE utf8_unicode_ci,
  `field_body_bodyCopy` text COLLATE utf8_unicode_ci,
  `field_quote_quote` text COLLATE utf8_unicode_ci,
  `field_quote_quoteAttribution` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `matrixcontent_matrix_elementId_locale_unq_idx` (`elementId`,`locale`),
  KEY `matrixcontent_matrix_locale_fk` (`locale`),
  CONSTRAINT `matrixcontent_matrix_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixcontent_matrix_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `matrixcontent_matrix` WRITE;
/*!40000 ALTER TABLE `matrixcontent_matrix` DISABLE KEYS */;

INSERT INTO `matrixcontent_matrix` (`id`, `elementId`, `locale`, `field_headline_headline`, `field_body_bodyCopy`, `field_quote_quote`, `field_quote_quoteAttribution`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,55,'en_us',NULL,'<p>Mark started his real estate career in 2005 and has been doing it full-time since then. Over time Mark realized that the majority of real estate agents are the same. They say the same things, they market the same way, and they treat clients the same. Mark did not like this so he set about creating his own unique way of practicing real estate.</p>\r\n\r\n<p>In 2014, Mark debuted a three tier pricing plan for listing clients, giving them the choice on how they want to list their home. This is another example of how Mark never accepts the status quo in real estate and is always looking to make things better for his clients.</p>',NULL,NULL,'2015-12-28 00:53:16','2015-12-28 00:57:07','8d40fa8c-9548-444f-9c36-7bf625201f72'),
	(2,56,'en_us','Meet Mark and his family',NULL,NULL,NULL,'2015-12-28 00:55:12','2015-12-28 00:57:07','4d26cf89-2431-484f-85e1-fa5545dfb506'),
	(3,57,'en_us',NULL,NULL,'<p>One of Mark Charter’s greatest characteristics is his giving nature.</p>','Adam Carroll','2015-12-28 00:55:54','2015-12-28 00:57:07','cac6c346-3bdb-43b5-b42a-03ee97e6a22a'),
	(4,58,'en_us',NULL,'<p>Mark\'s overall goal on every deal is simple. He wants to provide such great service that after the deal is done, that client will happily provide a positive review of the transaction. This goal is what makes Mark one of the most recommended agents in Central Iowa.</p>\r\n\r\n<p>When you add it all up, when you take into account what Mark offers clients in terms of services, marketing and care, Mark truly feels he is the best option available in the market today and he hopes you will experience this for yourself very soon!</p>',NULL,NULL,'2015-12-28 00:57:07','2015-12-28 00:57:07','22fb9ebf-a6ca-4056-a653-dfcbd336f520'),
	(5,59,'en_us',NULL,'<h3>Buying & Selling</h3><p>So you are are thinking about buying or selling. That is great, here are a few things you need to know!</p>\r\n\r\n<h2>Buyers</h2>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<p>Buying a home is a big purchase, one of the biggest you will ever make. Why in the world would anyone want to make that purchase without the help of a professional real estate agent? Beats me, but many try. The most important thing to remember is that when you are buying a home, you DO NOT PAY YOUR AGENT! The seller of the home pays the entire commission and that commission is split between the agent selling the home and the agent representing the buyer. There is no reason to not have your own agent representing you and working hard on your behalf.<br></p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<p>I have talked to many buyers who have said something like \"When we bought, we just used the agent who had the house listed.\" If you are thinking about doing this, <strong>use extreme caution!</strong>&nbsp;This should only be done if you have complete trust in the agent. If you just met them, this is a terrible idea! This situation is called Dual Agency and it happens when one agent represents both parties. That agent is trying to get the most money for the seller and also get you the best price. How can they do this? It is tricky and again, should only be done if you have total faith in that agent.<br></p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<p>The way to avoid this is to hire me! Remember, I can show you <em>all</em> homes, not just the ones that I list.<br></p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<p>So when you are my buyer what do you get? Here is a summary of what my buyers get when they work with me:<br></p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<ul><li>An agent with 24/7 availability</li><li>An agent with a A+ BBB rating</li><li>An agent with more recommendations than any other agent in Central Iowa (Trulia.com)</li><li><a href=\"{entry:9:url}\">Free Moving Truck</a> (based on availability, first come, first served)</li><li>Access to my interior designer if you need help or suggestion on how to furnish your new home</li><li>Lender recommendations, I can get you to the right lender who will get you closed on time and with no surprises</li><li>An experienced agent, I have been selling real estate full-time since 2005</li><li>Auto-prospecting so you never miss a new listing to the market</li><li>Weekly check-in updates to make sure you are happy</li><li><strong>And much more!</strong></li></ul>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<p>When you work with me, you are working with a one-of-a-kind agent who simply offers my clients more than other agents. Please read through the client reviews on this site to get a good feel of how I treat my clients. You will never regret hiring me as your agent!</p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<h2>Sellers</h2>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<p>When the time comes to put your home on the market, hiring a real estate professional is a crucial step in the right direction. I realize that there are many home-selling options, but the tried and true method of hiring an agent is still the best option by far. It is proven to be the safest way to sell as well as being the way to generate the highest selling price.<br></p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<p>But, just listing with an agent is not enough! You have to list with an agent who knows how to market and embraces today\'s trends like social media to reach the masses. You want your home exposed everywhere to reach every possible buyer. But you also want to work with an agent who will be enjoyable to work with and one who has great services and benefits to offer you. That agent is me!<br></p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<p>I would love to talk to you about listing your home but for now, here is a brief summary of what I offer to sellers:<br></p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<ul><li>An agent with 24/7 availability</li><li>An agent with an A+ BBB rating</li><li>An agent with more recommendations than any other agent in Central Iowa (Trulia.com)</li><li>A professional photographer will take the photos of your home</li><li>A Professional videographer will do a video of your home (Certain price ranges only)</li><li>Professional staging assistance, my interior designer will make sure your home is ready to go</li><li><a href=\"{entry:9:url}\">Free Moving Truck </a>(Based on availability, first come, first served)</li><li>Professional moving services (at 7% plan while staying in central Iowa)</li><li>Weekly check-in updates to make sure you are happy</li><li>Social Media marketing</li><li><strong>And much more!</strong></li></ul>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<p>No other agent offers their clients more! You will love your selling experience, I promise. I realize one size does not fit all when it comes to selling your home. I want to talk to you about your specific situation and see if I can help.</p>',NULL,NULL,'2015-12-28 00:57:53','2015-12-28 01:03:42','0e33105a-a303-4d12-ab5f-01a41c317bfa'),
	(6,60,'en_us',NULL,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vehicula eget diam ac sagittis. Maecenas efficitur aliquet ipsum, id fringilla quam aliquam sit amet. Quisque eu eros nec lectus fermentum aliquet. Curabitur magna est, vulputate ac quam in, malesuada eleifend felis. In eget suscipit libero. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla facilisi. Etiam tempor facilisis nisi at tempus. Morbi felis felis, eleifend ac nulla ut, dignissim sagittis nisl. Aenean tempus finibus dignissim. Nulla tempus fringilla ante, sed tempus arcu placerat sit amet. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>',NULL,NULL,'2015-12-28 01:04:55','2015-12-28 01:23:47','00eb4007-fdd8-4081-b06c-066687691934'),
	(7,62,'en_us',NULL,'',NULL,NULL,'2015-12-28 01:08:39','2015-12-28 01:15:49','e58ea0fd-a087-47ab-8215-437381d1faba'),
	(8,63,'en_us',NULL,'<p>Maecenas accumsan metus non turpis mattis laoreet. Donec luctus tincidunt sem, vel feugiat sapien ornare ac. Mauris in purus dui. Nullam commodo ut elit quis blandit. Quisque tristique ullamcorper nunc, et elementum odio vestibulum vel. Proin sit amet sollicitudin ligula, sit amet mollis libero. Vestibulum vulputate nec nisl et aliquam. Suspendisse finibus nec nulla sit amet feugiat.</p>',NULL,NULL,'2015-12-28 01:27:07','2015-12-28 01:29:00','17588caa-0fa4-444f-8371-540339131034'),
	(10,65,'en_us',NULL,'<p>Maecenas accumsan metus non turpis mattis laoreet. Donec luctus tincidunt sem, vel feugiat sapien ornare ac. Mauris in purus dui. Nullam commodo ut elit quis blandit. Quisque tristique ullamcorper nunc, et elementum odio vestibulum vel. Proin sit amet sollicitudin ligula, sit amet mollis libero. Vestibulum vulputate nec nisl et aliquam. Suspendisse finibus nec nulla sit amet feugiat.</p>',NULL,NULL,'2015-12-28 01:29:55','2015-12-28 01:29:55','9cf27522-44b5-437f-be0f-5ae3e7f09997'),
	(11,66,'en_us',NULL,'<p>Maecenas accumsan metus non turpis mattis laoreet. Donec luctus tincidunt sem, vel feugiat sapien ornare ac. Mauris in purus dui. Nullam commodo ut elit quis blandit. Quisque tristique ullamcorper nunc, et elementum odio vestibulum vel. Proin sit amet sollicitudin ligula, sit amet mollis libero. Vestibulum vulputate nec nisl et aliquam. Suspendisse finibus nec nulla sit amet feugiat.</p>',NULL,NULL,'2015-12-28 01:30:06','2015-12-28 01:30:06','e0e1efdd-e56b-4395-b315-af7f76733def');

/*!40000 ALTER TABLE `matrixcontent_matrix` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table matrixcontent_option
# ------------------------------------------------------------

DROP TABLE IF EXISTS `matrixcontent_option`;

CREATE TABLE `matrixcontent_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `field_option_optionDescription` text COLLATE utf8_unicode_ci,
  `field_option_optionDetails` text COLLATE utf8_unicode_ci,
  `field_option_commission` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `field_description_description` text COLLATE utf8_unicode_ci,
  `field_details_optionDetails` text COLLATE utf8_unicode_ci,
  `field_commission_optionCommission` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `matrixcontent_option_elementId_locale_unq_idx` (`elementId`,`locale`),
  KEY `matrixcontent_option_locale_fk` (`locale`),
  CONSTRAINT `matrixcontent_option_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixcontent_option_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `matrixcontent_option` WRITE;
/*!40000 ALTER TABLE `matrixcontent_option` DISABLE KEYS */;

INSERT INTO `matrixcontent_option` (`id`, `elementId`, `locale`, `field_option_optionDescription`, `field_option_optionDetails`, `field_option_commission`, `field_description_description`, `field_details_optionDetails`, `field_commission_optionCommission`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(2,25,'en_us',NULL,NULL,NULL,'<p>The perfect plan to keep it simple.</p>',NULL,NULL,'2015-12-20 04:53:04','2015-12-20 04:53:14','5498505f-7428-496f-9d83-450ca6c34ba4'),
	(3,26,'en_us',NULL,NULL,NULL,NULL,NULL,'5','2015-12-20 04:53:04','2015-12-20 04:53:15','c5b80a41-c6f8-45a2-869f-a77738115a2b'),
	(4,27,'en_us',NULL,NULL,NULL,NULL,'<ul><li>3 Month Listing</li><li>Full MLS Exposure</li></ul>',NULL,'2015-12-20 04:53:04','2015-12-20 04:53:15','3ed251c2-f987-4ce0-ad09-b29300dec1e4'),
	(5,29,'en_us',NULL,NULL,NULL,'<p>This popular standard plan packs a punch.</p>',NULL,NULL,'2015-12-20 04:53:50','2015-12-20 04:54:19','0e68e57c-2502-400c-afe0-9bb28f53622e'),
	(6,30,'en_us',NULL,NULL,NULL,NULL,NULL,'6','2015-12-20 04:53:50','2015-12-20 04:54:19','6232be06-f6c9-4705-bd64-08245cc2ff30'),
	(7,31,'en_us',NULL,NULL,NULL,NULL,'<ul><li>3 Month Listing</li><li>Full MLS Exposure</li></ul>',NULL,'2015-12-20 04:54:19','2015-12-20 04:54:19','db03a28a-70e6-4e62-805b-fbead19a3975'),
	(8,33,'en_us',NULL,NULL,NULL,'<p>For the seller who wants the very best.</p>',NULL,NULL,'2015-12-20 04:55:11','2015-12-20 04:55:11','de6ee8b7-167f-4854-a672-1f1b1fcab5b7'),
	(9,34,'en_us',NULL,NULL,NULL,NULL,'<ul><li>3 Month Listing</li><li>Full MLS Exposure</li></ul>',NULL,'2015-12-20 04:55:11','2015-12-20 04:55:11','817eb1c9-2b2c-4865-a157-eef0dfcf68a3'),
	(10,35,'en_us',NULL,NULL,NULL,NULL,NULL,'7','2015-12-20 04:55:11','2015-12-20 04:55:11','fe1548b8-68a2-4c55-bf85-ae8e5c7cb929');

/*!40000 ALTER TABLE `matrixcontent_option` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pluginId` int(11) DEFAULT NULL,
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `applyTime` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `migrations_version_unq_idx` (`version`),
  KEY `migrations_pluginId_fk` (`pluginId`),
  CONSTRAINT `migrations_pluginId_fk` FOREIGN KEY (`pluginId`) REFERENCES `plugins` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `pluginId`, `version`, `applyTime`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,NULL,'m000000_000000_base','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','07a47072-975e-4cc7-909b-444f650dc00f'),
	(2,NULL,'m140730_000001_add_filename_and_format_to_transformindex','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','3c4c2c0f-fbb3-42e5-8915-4bafa41e60a6'),
	(3,NULL,'m140815_000001_add_format_to_transforms','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','e505f9ed-8b6f-488c-a4f6-5aa52c13ab76'),
	(4,NULL,'m140822_000001_allow_more_than_128_items_per_field','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','78fd6ba9-d2dc-4f0d-8db9-732fbe9c3deb'),
	(5,NULL,'m140829_000001_single_title_formats','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','08ca8fd2-4030-46ef-aede-1dd7cc946392'),
	(6,NULL,'m140831_000001_extended_cache_keys','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','64446a12-32bc-4ea4-80ea-bd6749d7c731'),
	(7,NULL,'m140922_000001_delete_orphaned_matrix_blocks','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','39de3ff9-0e00-4cf2-853e-86ef5a303228'),
	(8,NULL,'m141008_000001_elements_index_tune','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','3aa86c52-5928-4d1b-bee0-781c94eb81c5'),
	(9,NULL,'m141009_000001_assets_source_handle','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','54b6c432-a1e2-4d99-891d-14a9fd18c1bf'),
	(10,NULL,'m141024_000001_field_layout_tabs','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','c4f514d5-c7d1-4796-85c4-dbc53c52468d'),
	(11,NULL,'m141030_000000_plugin_schema_versions','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','e24b10c1-8ce1-49aa-a48e-afd8420b27b4'),
	(12,NULL,'m141030_000001_drop_structure_move_permission','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','f6fd87ed-89ee-42e6-9df5-2cac7fef5873'),
	(13,NULL,'m141103_000001_tag_titles','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','56a82d00-bd49-4d72-9cd8-2e46d31875f7'),
	(14,NULL,'m141109_000001_user_status_shuffle','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','910dbe95-f9eb-43ad-9cc9-717bb671ee3a'),
	(15,NULL,'m141126_000001_user_week_start_day','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','2c3c6968-cffb-44fb-b78f-d2039d32192d'),
	(16,NULL,'m150210_000001_adjust_user_photo_size','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','77db2619-1795-4ad5-869f-8f15f84a5efe'),
	(17,NULL,'m150724_000001_adjust_quality_settings','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','62741671-57da-4370-9b25-8d655023e131'),
	(18,NULL,'m150827_000000_element_index_settings','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','5db59ed3-34cc-4e9a-9843-4fc39c6c0c22'),
	(19,NULL,'m150918_000001_add_colspan_to_widgets','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','20860b09-7e07-441a-a32d-038839f3bf4b'),
	(20,NULL,'m151007_000000_clear_asset_caches','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','cb137b61-3c6c-4bd2-9494-3a10f7ef266b'),
	(21,NULL,'m151109_000000_text_url_formats','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','c306b315-32db-42ea-a2e4-7d28f4a7d886'),
	(22,NULL,'m151110_000000_move_logo','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','b9223446-293e-4b11-a154-412e6741e112'),
	(23,NULL,'m151117_000000_adjust_image_widthheight','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','42bde0c8-f2db-485f-a809-54e8a17a6435'),
	(24,NULL,'m151127_000000_clear_license_key_status','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','b6e5fc6a-accc-48a8-9c32-7ccbbe68e3cf'),
	(25,NULL,'m151127_000000_plugin_license_keys','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','1ad0d20d-3003-4b53-ae04-e7cfaf140ae6'),
	(26,NULL,'m151130_000000_update_pt_widget_feeds','2015-12-19 00:45:31','2015-12-19 00:45:31','2015-12-19 00:45:31','a213fc5e-84d0-4a53-a3ea-5604fe26fc9f'),
	(27,2,'m140330_000000_smartMap_addCountrySubfield','2015-12-19 01:40:29','2015-12-19 01:40:29','2015-12-19 01:40:29','43424278-9c41-4325-bd78-ebac3a231b83'),
	(28,2,'m140330_000001_smartMap_autofillCountryForExistingAddresses','2015-12-19 01:40:29','2015-12-19 01:40:29','2015-12-19 01:40:29','f1c37216-359c-48a2-a02b-a96a43173755'),
	(29,2,'m140811_000001_smartMap_changeHandleToFieldId','2015-12-19 01:40:29','2015-12-19 01:40:29','2015-12-19 01:40:29','bb2cab8a-aa63-4ac2-9ada-13def5415e41'),
	(30,2,'m150329_000000_smartMap_splitGoogleApiKeys','2015-12-19 01:40:29','2015-12-19 01:40:29','2015-12-19 01:40:29','33c04201-eff3-40de-9c48-4a26c2d5e56f'),
	(31,2,'m150331_000000_smartMap_reorganizeGeolocationOptions','2015-12-19 01:40:29','2015-12-19 01:40:29','2015-12-19 01:40:29','29f28914-80b5-4026-9880-2e6355ef0583');

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table plugins
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plugins`;

CREATE TABLE `plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `version` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `schemaVersion` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `licenseKey` char(24) COLLATE utf8_unicode_ci DEFAULT NULL,
  `licenseKeyStatus` enum('valid','invalid','mismatched','unknown') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unknown',
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `settings` text COLLATE utf8_unicode_ci,
  `installDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `plugins` WRITE;
/*!40000 ALTER TABLE `plugins` DISABLE KEYS */;

INSERT INTO `plugins` (`id`, `class`, `version`, `schemaVersion`, `licenseKey`, `licenseKeyStatus`, `enabled`, `settings`, `installDate`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Reservation','1.0',NULL,NULL,'unknown',1,NULL,'2015-12-19 01:33:27','2015-12-19 01:33:27','2015-12-28 00:47:47','d6d20b8a-5438-4df9-8ce0-b6f0b9cebb71'),
	(2,'SmartMap','2.3.1','2.3.0',NULL,'unknown',1,NULL,'2015-12-19 01:40:29','2015-12-19 01:40:29','2015-12-28 00:47:47','0b869af0-a5fb-41c6-890a-5deb656eb2d3');

/*!40000 ALTER TABLE `plugins` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table rackspaceaccess
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rackspaceaccess`;

CREATE TABLE `rackspaceaccess` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `connectionKey` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `storageUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cdnUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `rackspaceaccess_connectionKey_unq_idx` (`connectionKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table relations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `relations`;

CREATE TABLE `relations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `sourceId` int(11) NOT NULL,
  `sourceLocale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `targetId` int(11) NOT NULL,
  `sortOrder` smallint(6) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `relations_fieldId_sourceId_sourceLocale_targetId_unq_idx` (`fieldId`,`sourceId`,`sourceLocale`,`targetId`),
  KEY `relations_sourceId_fk` (`sourceId`),
  KEY `relations_sourceLocale_fk` (`sourceLocale`),
  KEY `relations_targetId_fk` (`targetId`),
  CONSTRAINT `relations_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `relations_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `relations_sourceLocale_fk` FOREIGN KEY (`sourceLocale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `relations_targetId_fk` FOREIGN KEY (`targetId`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table routes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `routes`;

CREATE TABLE `routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `urlParts` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `urlPattern` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` tinyint(4) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `routes_urlPattern_unq_idx` (`urlPattern`),
  KEY `routes_locale_idx` (`locale`),
  CONSTRAINT `routes_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table searchindex
# ------------------------------------------------------------

DROP TABLE IF EXISTS `searchindex`;

CREATE TABLE `searchindex` (
  `elementId` int(11) NOT NULL,
  `attribute` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `fieldId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`elementId`,`attribute`,`fieldId`,`locale`),
  FULLTEXT KEY `searchindex_keywords_idx` (`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `searchindex` WRITE;
/*!40000 ALTER TABLE `searchindex` DISABLE KEYS */;

INSERT INTO `searchindex` (`elementId`, `attribute`, `fieldId`, `locale`, `keywords`)
VALUES
	(1,'username',0,'en_us',' markcharter '),
	(1,'firstname',0,'en_us',' mark '),
	(1,'lastname',0,'en_us',' charter '),
	(1,'fullname',0,'en_us',' mark charter '),
	(1,'email',0,'en_us',' damonjentree gmail com '),
	(1,'slug',0,'en_us',''),
	(2,'slug',0,'en_us',' homepage '),
	(2,'title',0,'en_us',' mark charter realty '),
	(2,'field',1,'en_us',' s '),
	(2,'field',49,'en_us',' lorem ipsum dolor sit amet consectetur adipiscing elit donec vehicula eget diam ac sagittis maecenas efficitur aliquet ipsum id fringilla quam aliquam sit amet quisque eu eros nec lectus fermentum aliquet curabitur magna est vulputate ac quam in malesuada eleifend felis in eget suscipit libero cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus nulla facilisi etiam tempor facilisis nisi at tempus morbi felis felis eleifend ac nulla ut dignissim sagittis nisl aenean tempus finibus dignissim nulla tempus fringilla ante sed tempus arcu placerat sit amet class aptent taciti sociosqu ad litora torquent per conubia nostra per inceptos himenaeos '),
	(3,'field',1,'en_us',' craft is the cms that s powering local charterhouseiowa dev it s beautiful powerful flexible and easy to use and it s made by pixel tonic we can t wait to dive in and see what it s capable of this is even more captivating content which you couldn t see on the news index page because it was entered after a page break and the news index template only likes to show the content on the first page craft a nice alternative to word if you re making a website '),
	(3,'field',2,'en_us',''),
	(3,'slug',0,'en_us',''),
	(3,'title',0,'en_us',' we just installed craft '),
	(4,'slug',0,'en_us',' properties '),
	(4,'title',0,'en_us',' properties '),
	(5,'slug',0,'en_us',' partners '),
	(5,'title',0,'en_us',' partners '),
	(6,'slug',0,'en_us',' team '),
	(6,'title',0,'en_us',' team '),
	(7,'slug',0,'en_us',' reviews '),
	(7,'title',0,'en_us',' testimonials '),
	(8,'slug',0,'en_us',' reservation success '),
	(8,'title',0,'en_us',' reservation success '),
	(9,'slug',0,'en_us',' truck '),
	(9,'title',0,'en_us',' truck '),
	(4,'field',1,'en_us',' lorem ipsum dolor sit amet consectetur adipiscing elit duis elit massa ornare et purus a semper sagittis diam in condimentum ipsum eget luctus feugiat est nisi ullamcorper diam elementum placerat sapien neque nec ex duis pulvinar metus mauris non commodo mauris cursus vel phasellus a ligula in lorem tincidunt semper phasellus sed nisl quis eros malesuada egestas eu ultrices massa cras commodo velit in arcu vehicula et egestas magna ullamcorper donec sagittis viverra mi vitae mattis nulla semper sapien ut consequat dignissim nam augue sem luctus eu dictum et feugiat eget turpis nullam sit amet maximus ante pellentesque ut risus semper fermentum erat id auctor leo etiam cursus facilisis justo quis interdum enim bibendum in pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas quisque feugiat massa quis enim gravida vel auctor nisi efficitur '),
	(2,'field',3,'en_us',''),
	(4,'field',3,'en_us',''),
	(6,'field',1,'en_us',' it s true this site doesn t have a whole lot of content yet but don t worry our web developers have just installed the cms and they re setting things up for the content editors this very moment soon local charterhouseiowa dev will be an oasis of fresh perspectives sharp analyses and astute opinions that will keep you coming back again and again '),
	(6,'field',3,'en_us',''),
	(7,'field',1,'en_us',' it s true this site doesn t have a whole lot of content yet but don t worry our web developers have just installed the cms and they re setting things up for the content editors this very moment soon local charterhouseiowa dev will be an oasis of fresh perspectives sharp analyses and astute opinions that will keep you coming back again and again '),
	(7,'field',3,'en_us',''),
	(5,'field',1,'en_us',''),
	(5,'field',49,'en_us',' maecenas accumsan metus non turpis mattis laoreet donec luctus tincidunt sem vel feugiat sapien ornare ac mauris in purus dui nullam commodo ut elit quis blandit quisque tristique ullamcorper nunc et elementum odio vestibulum vel proin sit amet sollicitudin ligula sit amet mollis libero vestibulum vulputate nec nisl et aliquam suspendisse finibus nec nulla sit amet feugiat '),
	(5,'field',3,'en_us',''),
	(13,'field',1,'en_us',' one of mark charter s greatest characteristics is his giving nature he always goes above and beyond for his clients to make sure that their experience in buying or selling a home is first rate while not just an experienced real estate agent mark is also extremely knowledgeable about the investment real estate market as well he is a career agent that absolutely without a doubt has your best interests in mind '),
	(13,'field',3,'en_us',''),
	(13,'slug',0,'en_us',' one of mark charters greatest characteristics is his giving nature '),
	(13,'title',0,'en_us',' one of mark charter s greatest characteristics is his giving nature '),
	(13,'field',15,'en_us',' adam carroll '),
	(14,'field',1,'en_us',' before finding mark and amy i had had a lot of bad experiences with realtors from the minute i contacted them about possibly being my realtor i was in constant communication with them i was never left wondering what was going on i was paired with amy and she is fantastic she listened to not only what i was looking for but what i was going to use spaces for and looked for those things in the places i wanted to view she helped me to pro and con the spaces i was looking on based on what she had heard me say about other spaces she was in constant contact with me about setting up showings and if there was a delay in getting an answer from someone was letting me know what the hold up was i have never met a more caring team to work with and when they say you re part of the family now you are as a first time home buyer i had lots of questions both for amy and for the seller and i got every single one answered i never once felt like there was another goal that the team was working for other than to make sure i was happy with what i was getting while the people are top notch it s also amazing to me that you can use the moving truck for free and they had my new home cleaned before i moved in it saved me so much time and hassle not to mention cost i was so lucky to find them '),
	(14,'field',15,'en_us',' bethany wilcke '),
	(14,'field',3,'en_us',''),
	(14,'slug',0,'en_us',' i was so lucky to find them '),
	(14,'title',0,'en_us',' i was so lucky to find them '),
	(15,'field',1,'en_us',' mark was phenomenal to work with during the sale of our home his no nonsense tell it how it is attitude is the only reason we got our home sold we had a unique situation with our home based on the size garage and price mark was the only person that told us what we needed to hear instead of just fluffing us with what we wanted to hear mark is a great communicator and we always knew what was expected of us through the entire process thank you mark '),
	(15,'field',15,'en_us',' jeremy orr '),
	(15,'field',3,'en_us',''),
	(15,'slug',0,'en_us',' mark was phenomenal to work with '),
	(15,'title',0,'en_us',' mark was phenomenal to work with '),
	(38,'field',49,'en_us',' meet mark and his family mark started his real estate career in 2005 and has been doing it full time since then over time mark realized that the majority of real estate agents are the same they say the same things they market the same way and they treat clients the same mark did not like this so he set about creating his own unique way of practicing real estate in 2014 mark debuted a three tier pricing plan for listing clients giving them the choice on how they want to list their home this is another example of how mark never accepts the status quo in real estate and is always looking to make things better for his clients one of mark charter s greatest characteristics is his giving nature adam carroll mark s overall goal on every deal is simple he wants to provide such great service that after the deal is done that client will happily provide a positive review of the transaction this goal is what makes mark one of the most recommended agents in central iowa when you add it all up when you take into account what mark offers clients in terms of services marketing and care mark truly feels he is the best option available in the market today and he hopes you will experience this for yourself very soon '),
	(9,'field',1,'en_us',' lorem ipsum dolor sit amet consectetur adipiscing elit curabitur mattis sollicitudin felis consectetur eleifend mi ornare sed vivamus cursus tincidunt ultrices nam id tincidunt purus morbi lectus eros molestie non dolor ut tincidunt fermentum odio vivamus consectetur molestie lorem ac varius '),
	(8,'field',1,'en_us',' we got ya covered and you got my truck '),
	(8,'field',3,'en_us',''),
	(17,'field',1,'en_us',' stunning ankeny home located on otter creek golf course this 4 bed 4 5 bath former show home is loaded with features and amenities from top to bottom try building this home today for less than a million dollars it all starts with location you will love cul de sac living close to i35 making a commute a breeze step through the front door and you will be wowed by the view there is not one but two ponds directly behind the home the outside is just as gorgeous as the inside full landscaping flagstone entry exterior lighting huge patio and a wraparound deck with outdoor kitchen truly put this home among ankeny s best inside the quality is top notch beautiful knotty alder trim acacia hardwood viking appliances and a beautiful stone fireplace are impossible to miss the master is an oasis with beautiful materials need an amazing basement sunken family room barn wood floors huge wet bar hidden room and golf cart garage await too much to list email for all features '),
	(17,'field',11,'en_us',' 504907 '),
	(17,'field',6,'en_us',' 1210 ne 42nd st ankeny ia 50021 united states 41 76575850 93 58654680 6403 57288807 '),
	(17,'field',12,'en_us',' singlefamily '),
	(17,'field',14,'en_us',' 2 426 '),
	(17,'field',9,'en_us',' 5 '),
	(17,'field',8,'en_us',' 4 5 '),
	(17,'field',10,'en_us',' 0 '),
	(17,'field',13,'en_us',' 0 '),
	(17,'field',3,'en_us',''),
	(17,'slug',0,'en_us',' amazing golf course home with too many amenities to list '),
	(17,'title',0,'en_us',' amazing golf course home with too many amenities to list '),
	(17,'field',16,'en_us',' http www realtor com realestateandhomes search mlslid=504907 '),
	(17,'field',17,'en_us',' 3 car garage '),
	(17,'field',18,'en_us',' $849 900 '),
	(17,'field',19,'en_us',' yzryxkgqd2c '),
	(18,'field',1,'en_us',' call your builder and call off the meeting this beats new construction and is the house you have been waiting on to hit the market this stunning grimes acreage sits on 2 3 acres and is just a mile north of dcg high school awesome location pull into the long driveway and enjoy the massive front yard with trees stunning curb appeal this 5 bedroom could be 6 home has a floor plan you have not seen before the attention to detail and woodwork is stunning built ins and window seats with storage are found everywhere the master bedroom is on the main level and offers a gorgeous bath with carrera marble and a vey large closet the main floor laundry is also huge you will find a second full laundry room in the finished basement along with a second kitchen one of the beds is separated from the rest of the house and is perfect for an office or teen room too many amenities to list them all 5 garage stallsgeothermalhigh end kitchencovered deckso much more a rare find '),
	(18,'field',11,'en_us',' 506142 '),
	(18,'field',16,'en_us',' http www realtor com realestateandhomes search mlslid=506142 '),
	(18,'field',18,'en_us',' $719 000 '),
	(18,'field',14,'en_us',' 2 700 '),
	(18,'field',6,'en_us',' 7965 nw 142nd st grimes ia 50111 united states 41 70576500 93 81369100 6415 45239364 '),
	(18,'field',9,'en_us',' 5 '),
	(18,'field',8,'en_us',' 4 '),
	(18,'field',12,'en_us',' singlefamily '),
	(18,'field',17,'en_us',' 3 car attached garage 2 car detached garage '),
	(18,'field',19,'en_us',''),
	(18,'field',10,'en_us',' 0 '),
	(18,'field',13,'en_us',' 0 '),
	(18,'field',3,'en_us',''),
	(18,'slug',0,'en_us',' absolutely amazing modern rural meets urban set up just outside of grimes '),
	(18,'title',0,'en_us',' absolutely amazing modern rural meets urban set up just outside of grimes a must see at $719 000 '),
	(18,'field',20,'en_us',' 1hw5ay9jy7y '),
	(18,'field',21,'en_us',' 1990 '),
	(19,'slug',0,'en_us',''),
	(19,'field',6,'en_us',' 3602 ne otter view circle ankeny ia 50021 united states 41 76183000 93 57202570 6402 83642357 '),
	(19,'field',22,'en_us',' remax real estate concepts '),
	(19,'field',5,'en_us',' mark charterhouseiowa com '),
	(20,'slug',0,'en_us',''),
	(19,'field',23,'en_us',' 515 864 6444 '),
	(21,'slug',0,'en_us',' contact '),
	(21,'title',0,'en_us',' contact mark '),
	(21,'field',1,'en_us',''),
	(21,'field',3,'en_us',''),
	(22,'slug',0,'en_us',' ways to sell '),
	(22,'title',0,'en_us',' 3 ways to list your home '),
	(23,'field',28,'en_us',' the perfect plan to keep it simple this seller does not require much and likes to do things themselves just list with a pro and go '),
	(23,'slug',0,'en_us',' pro and go '),
	(23,'title',0,'en_us',' pro and go '),
	(25,'field',32,'en_us',' the perfect plan to keep it simple '),
	(25,'slug',0,'en_us',''),
	(26,'field',34,'en_us',' 5 '),
	(26,'slug',0,'en_us',''),
	(27,'field',33,'en_us',' 3 month listingfull mls exposure '),
	(27,'slug',0,'en_us',''),
	(28,'field',28,'en_us',' this popular standard plan packs a punch great marketing and perks all at common commission rate in the market great service plus more '),
	(28,'slug',0,'en_us',' pro plus '),
	(28,'title',0,'en_us',' pro plus '),
	(29,'field',32,'en_us',' this popular standard plan packs a punch '),
	(29,'slug',0,'en_us',''),
	(30,'field',34,'en_us',' 6 '),
	(30,'slug',0,'en_us',''),
	(31,'field',33,'en_us',' 3 month listingfull mls exposure '),
	(31,'slug',0,'en_us',''),
	(32,'field',28,'en_us',' for the seller who wants the very best maximum marketing $$$ s maximum convince all in one the elite is unrivaled in the marketplace '),
	(32,'slug',0,'en_us',' pro elite '),
	(32,'title',0,'en_us',' pro elite '),
	(33,'field',32,'en_us',' for the seller who wants the very best '),
	(33,'slug',0,'en_us',''),
	(34,'field',33,'en_us',' 3 month listingfull mls exposure '),
	(34,'slug',0,'en_us',''),
	(35,'field',34,'en_us',' 7 '),
	(35,'slug',0,'en_us',''),
	(36,'slug',0,'en_us',' buyers sellers '),
	(36,'title',0,'en_us',' buyers sellers '),
	(36,'field',1,'en_us',''),
	(36,'field',3,'en_us',''),
	(60,'field',51,'en_us',' lorem ipsum dolor sit amet consectetur adipiscing elit donec vehicula eget diam ac sagittis maecenas efficitur aliquet ipsum id fringilla quam aliquam sit amet quisque eu eros nec lectus fermentum aliquet curabitur magna est vulputate ac quam in malesuada eleifend felis in eget suscipit libero cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus nulla facilisi etiam tempor facilisis nisi at tempus morbi felis felis eleifend ac nulla ut dignissim sagittis nisl aenean tempus finibus dignissim nulla tempus fringilla ante sed tempus arcu placerat sit amet class aptent taciti sociosqu ad litora torquent per conubia nostra per inceptos himenaeos '),
	(60,'slug',0,'en_us',''),
	(61,'field',49,'en_us',''),
	(61,'field',3,'en_us',''),
	(61,'field',48,'en_us',''),
	(61,'field',46,'en_us',' http www wealthwithmortgage com '),
	(61,'field',47,'en_us',' 515 257 6729 '),
	(61,'slug',0,'en_us',' mortgage the tyler osby team '),
	(61,'title',0,'en_us',' mortgage the tyler osby team '),
	(62,'field',51,'en_us',''),
	(61,'field',1,'en_us',' tyler osby is where mark turns when his clients need a mortgage or a refinance tyler is knowledgeable accountable and trustworthy he gets the job done every time '),
	(62,'slug',0,'en_us',''),
	(23,'field',37,'en_us',''),
	(23,'field',35,'en_us',' 5 '),
	(23,'field',36,'en_us',' 3 month mistingfull mls exposureprofessional photossocial media marketingsign and lockbox '),
	(28,'field',37,'en_us',''),
	(28,'field',35,'en_us',' 6 '),
	(28,'field',36,'en_us',' 3 month mistingfull mls exposureprofessional photossocial media marketingsign and lockboxfacebook targeted marketing3d matterport virtual touraccess to 15ft truckfree pest inspection$1 000 offer guarantee '),
	(32,'field',37,'en_us',''),
	(32,'field',35,'en_us',' 7 '),
	(32,'field',36,'en_us',' 3 month mistingfull mls exposureprofessional photossocial media marketingsign and lockboxfacebook targeted marketing3d matterport virtual touraccess to 15ft truckfree pest inspection$1 000 offer guaranteefree storagefull move package '),
	(22,'field',1,'en_us',''),
	(22,'field',3,'en_us',''),
	(22,'field',38,'en_us',''),
	(37,'slug',0,'en_us',''),
	(37,'field',39,'en_us',' mark charter is a licensed agent with re max real estate concepts in the state of iowa b if you have received this information and are already listed with an agent please disregard this as it is not meant to intrude on and existing seller agent relationship full moves are available to clients that are staying within central iowa '),
	(38,'slug',0,'en_us',' about '),
	(38,'title',0,'en_us',' about mark charter '),
	(38,'field',1,'en_us',' mark started his real estate career in 2005 and has been doing it full time since then over time mark realized that the majority of real estate agents are the same they say the same things they market the same way and they treat clients the same mark did not like this so he set about creating his own unique way of practicing real estate in 2014 mark debuted a three tier pricing plan for listing clients giving them the choice on how they want to list their home this is another example of how mark never accepts the status quo in real estate and is always looking to make things better for his clients mark s overall goal on every deal is simple he wants to provide such great service that after the deal is done that client will happily provide a positive review of the transaction this goal is what makes mark one of the most recommended agents in central iowa when you add it all up when you take into account what mark offers clients in terms of services marketing and care mark truly feels he is the best option available in the market today and he hopes you will experience this for yourself very soon '),
	(38,'field',3,'en_us',''),
	(39,'field',40,'en_us',''),
	(39,'field',41,'en_us',' owner realtor '),
	(39,'field',43,'en_us',' mark charterhouseiowa com '),
	(39,'field',42,'en_us',' 515 864 6444 '),
	(39,'slug',0,'en_us',' mark charter '),
	(39,'title',0,'en_us',' mark charter '),
	(39,'field',44,'en_us',' mark charter is iowa s most recommended agent on sites like trulia com and angieslist com and is one of the top selling agents in central iowa mark believes in constantly looking for ways to put his clients first and accomplishes this through things like short listing contracts flexible commission plans and great perks that benefit his clients mark is married to katie and has a son named seton and a daughter named hollis '),
	(40,'field',41,'en_us',' buyer specialist '),
	(40,'field',43,'en_us',' amy charterhouseiowa com '),
	(40,'field',42,'en_us',' 515 822 0171 '),
	(40,'field',44,'en_us',' amy meyer is a fantastic addition to the mark charter team talented dedicated and hard working amy s role is to help buyers find the perfect home they are looking for '),
	(40,'slug',0,'en_us',' amy meyer '),
	(40,'title',0,'en_us',' amy meyer '),
	(41,'field',41,'en_us',' business manager client coordinator '),
	(41,'field',43,'en_us',' nic charterhouseiowa com '),
	(41,'field',42,'en_us',' 515 371 1973 '),
	(41,'field',44,'en_us',' nic s role is making sure the client is happy he helps buyers and seller with all the details of a move things like lining up use of the mark charter real estate moving truck scheduling cleaners or anything else that you might need he is a vital part of the team and is here to help you '),
	(41,'slug',0,'en_us',' nic meyer '),
	(41,'title',0,'en_us',' nic meyer '),
	(42,'field',41,'en_us',' photographer '),
	(42,'field',43,'en_us',''),
	(42,'field',42,'en_us',''),
	(42,'field',44,'en_us',' jake boyd is central iowa s premier real estate photographer and owns jake boyd photo jordan and jason are also valuable members of the team as well mark trusts jake boyd photo to shoot all of his listing photos '),
	(42,'slug',0,'en_us',' jake boyd photo '),
	(42,'title',0,'en_us',' jake boyd photo '),
	(66,'slug',0,'en_us',''),
	(66,'field',51,'en_us',' maecenas accumsan metus non turpis mattis laoreet donec luctus tincidunt sem vel feugiat sapien ornare ac mauris in purus dui nullam commodo ut elit quis blandit quisque tristique ullamcorper nunc et elementum odio vestibulum vel proin sit amet sollicitudin ligula sit amet mollis libero vestibulum vulputate nec nisl et aliquam suspendisse finibus nec nulla sit amet feugiat '),
	(65,'slug',0,'en_us',''),
	(18,'field',49,'en_us',' maecenas accumsan metus non turpis mattis laoreet donec luctus tincidunt sem vel feugiat sapien ornare ac mauris in purus dui nullam commodo ut elit quis blandit quisque tristique ullamcorper nunc et elementum odio vestibulum vel proin sit amet sollicitudin ligula sit amet mollis libero vestibulum vulputate nec nisl et aliquam suspendisse finibus nec nulla sit amet feugiat '),
	(17,'field',21,'en_us',''),
	(17,'field',20,'en_us',''),
	(65,'field',51,'en_us',' maecenas accumsan metus non turpis mattis laoreet donec luctus tincidunt sem vel feugiat sapien ornare ac mauris in purus dui nullam commodo ut elit quis blandit quisque tristique ullamcorper nunc et elementum odio vestibulum vel proin sit amet sollicitudin ligula sit amet mollis libero vestibulum vulputate nec nisl et aliquam suspendisse finibus nec nulla sit amet feugiat '),
	(63,'slug',0,'en_us',''),
	(17,'field',49,'en_us',' maecenas accumsan metus non turpis mattis laoreet donec luctus tincidunt sem vel feugiat sapien ornare ac mauris in purus dui nullam commodo ut elit quis blandit quisque tristique ullamcorper nunc et elementum odio vestibulum vel proin sit amet sollicitudin ligula sit amet mollis libero vestibulum vulputate nec nisl et aliquam suspendisse finibus nec nulla sit amet feugiat '),
	(63,'field',51,'en_us',' maecenas accumsan metus non turpis mattis laoreet donec luctus tincidunt sem vel feugiat sapien ornare ac mauris in purus dui nullam commodo ut elit quis blandit quisque tristique ullamcorper nunc et elementum odio vestibulum vel proin sit amet sollicitudin ligula sit amet mollis libero vestibulum vulputate nec nisl et aliquam suspendisse finibus nec nulla sit amet feugiat '),
	(9,'field',3,'en_us',''),
	(55,'field',51,'en_us',' mark started his real estate career in 2005 and has been doing it full time since then over time mark realized that the majority of real estate agents are the same they say the same things they market the same way and they treat clients the same mark did not like this so he set about creating his own unique way of practicing real estate in 2014 mark debuted a three tier pricing plan for listing clients giving them the choice on how they want to list their home this is another example of how mark never accepts the status quo in real estate and is always looking to make things better for his clients '),
	(58,'field',51,'en_us',' mark s overall goal on every deal is simple he wants to provide such great service that after the deal is done that client will happily provide a positive review of the transaction this goal is what makes mark one of the most recommended agents in central iowa when you add it all up when you take into account what mark offers clients in terms of services marketing and care mark truly feels he is the best option available in the market today and he hopes you will experience this for yourself very soon '),
	(55,'slug',0,'en_us',''),
	(56,'field',50,'en_us',' meet mark and his family '),
	(56,'slug',0,'en_us',''),
	(57,'field',52,'en_us',' one of mark charter s greatest characteristics is his giving nature '),
	(57,'field',53,'en_us',' adam carroll '),
	(57,'slug',0,'en_us',''),
	(58,'slug',0,'en_us',''),
	(36,'field',49,'en_us',' buying sellingso you are are thinking about buying or selling that is great here are a few things you need to know buyers buying a home is a big purchase one of the biggest you will ever make why in the world would anyone want to make that purchase without the help of a professional real estate agent beats me but many try the most important thing to remember is that when you are buying a home you do not pay your agent the seller of the home pays the entire commission and that commission is split between the agent selling the home and the agent representing the buyer there is no reason to not have your own agent representing you and working hard on your behalf i have talked to many buyers who have said something like when we bought we just used the agent who had the house listed if you are thinking about doing this use extreme caution this should only be done if you have complete trust in the agent if you just met them this is a terrible idea this situation is called dual agency and it happens when one agent represents both parties that agent is trying to get the most money for the seller and also get you the best price how can they do this it is tricky and again should only be done if you have total faith in that agent the way to avoid this is to hire me remember i can show you all homes not just the ones that i list so when you are my buyer what do you get here is a summary of what my buyers get when they work with me an agent with 24 7 availabilityan agent with a a bbb ratingan agent with more recommendations than any other agent in central iowa trulia com free moving truck based on availability first come first served access to my interior designer if you need help or suggestion on how to furnish your new homelender recommendations i can get you to the right lender who will get you closed on time and with no surprisesan experienced agent i have been selling real estate full time since 2005auto prospecting so you never miss a new listing to the marketweekly check in updates to make sure you are happyand much more when you work with me you are working with a one of a kind agent who simply offers my clients more than other agents please read through the client reviews on this site to get a good feel of how i treat my clients you will never regret hiring me as your agent sellers when the time comes to put your home on the market hiring a real estate professional is a crucial step in the right direction i realize that there are many home selling options but the tried and true method of hiring an agent is still the best option by far it is proven to be the safest way to sell as well as being the way to generate the highest selling price but just listing with an agent is not enough you have to list with an agent who knows how to market and embraces today s trends like social media to reach the masses you want your home exposed everywhere to reach every possible buyer but you also want to work with an agent who will be enjoyable to work with and one who has great services and benefits to offer you that agent is me i would love to talk to you about listing your home but for now here is a brief summary of what i offer to sellers an agent with 24 7 availabilityan agent with an a bbb ratingan agent with more recommendations than any other agent in central iowa trulia com a professional photographer will take the photos of your homea professional videographer will do a video of your home certain price ranges only professional staging assistance my interior designer will make sure your home is ready to gofree moving truck based on availability first come first served professional moving services at 7% plan while staying in central iowa weekly check in updates to make sure you are happysocial media marketingand much more no other agent offers their clients more you will love your selling experience i promise i realize one size does not fit all when it comes to selling your home i want to talk to you about your specific situation and see if i can help '),
	(59,'field',51,'en_us',' buying sellingso you are are thinking about buying or selling that is great here are a few things you need to know buyers buying a home is a big purchase one of the biggest you will ever make why in the world would anyone want to make that purchase without the help of a professional real estate agent beats me but many try the most important thing to remember is that when you are buying a home you do not pay your agent the seller of the home pays the entire commission and that commission is split between the agent selling the home and the agent representing the buyer there is no reason to not have your own agent representing you and working hard on your behalf i have talked to many buyers who have said something like when we bought we just used the agent who had the house listed if you are thinking about doing this use extreme caution this should only be done if you have complete trust in the agent if you just met them this is a terrible idea this situation is called dual agency and it happens when one agent represents both parties that agent is trying to get the most money for the seller and also get you the best price how can they do this it is tricky and again should only be done if you have total faith in that agent the way to avoid this is to hire me remember i can show you all homes not just the ones that i list so when you are my buyer what do you get here is a summary of what my buyers get when they work with me an agent with 24 7 availabilityan agent with a a bbb ratingan agent with more recommendations than any other agent in central iowa trulia com free moving truck based on availability first come first served access to my interior designer if you need help or suggestion on how to furnish your new homelender recommendations i can get you to the right lender who will get you closed on time and with no surprisesan experienced agent i have been selling real estate full time since 2005auto prospecting so you never miss a new listing to the marketweekly check in updates to make sure you are happyand much more when you work with me you are working with a one of a kind agent who simply offers my clients more than other agents please read through the client reviews on this site to get a good feel of how i treat my clients you will never regret hiring me as your agent sellers when the time comes to put your home on the market hiring a real estate professional is a crucial step in the right direction i realize that there are many home selling options but the tried and true method of hiring an agent is still the best option by far it is proven to be the safest way to sell as well as being the way to generate the highest selling price but just listing with an agent is not enough you have to list with an agent who knows how to market and embraces today s trends like social media to reach the masses you want your home exposed everywhere to reach every possible buyer but you also want to work with an agent who will be enjoyable to work with and one who has great services and benefits to offer you that agent is me i would love to talk to you about listing your home but for now here is a brief summary of what i offer to sellers an agent with 24 7 availabilityan agent with an a bbb ratingan agent with more recommendations than any other agent in central iowa trulia com a professional photographer will take the photos of your homea professional videographer will do a video of your home certain price ranges only professional staging assistance my interior designer will make sure your home is ready to gofree moving truck based on availability first come first served professional moving services at 7% plan while staying in central iowa weekly check in updates to make sure you are happysocial media marketingand much more no other agent offers their clients more you will love your selling experience i promise i realize one size does not fit all when it comes to selling your home i want to talk to you about your specific situation and see if i can help '),
	(59,'slug',0,'en_us','');

/*!40000 ALTER TABLE `searchindex` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sections
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sections`;

CREATE TABLE `sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('single','channel','structure') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'channel',
  `hasUrls` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `template` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enableVersioning` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sections_name_unq_idx` (`name`),
  UNIQUE KEY `sections_handle_unq_idx` (`handle`),
  KEY `sections_structureId_fk` (`structureId`),
  CONSTRAINT `sections_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `sections` WRITE;
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;

INSERT INTO `sections` (`id`, `structureId`, `name`, `handle`, `type`, `hasUrls`, `template`, `enableVersioning`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,NULL,'Homepage','homepage','single',1,'index',1,'2015-12-19 00:45:35','2015-12-19 00:45:35','7f150a74-5770-4c0f-8731-e7d31b45e9cc'),
	(2,NULL,'News','news','channel',1,'news/_entry',1,'2015-12-19 00:45:35','2015-12-19 00:45:35','6edd160b-e0ce-42d1-94eb-371aa2f4cd37'),
	(3,1,'Truck Reservations','truckReservations','structure',0,NULL,1,'2015-12-19 00:51:17','2015-12-19 00:51:17','5bf0a186-c6cf-4b6f-8501-0ffdaa705d39'),
	(4,2,'Property','property','structure',1,'properties/_entry',1,'2015-12-19 00:51:40','2015-12-19 02:53:21','48b6a8e5-aaa5-4187-ace7-3fb0ca5d0c0d'),
	(5,NULL,'Properties','properties','single',1,'properties',1,'2015-12-19 00:52:08','2015-12-19 01:01:21','3f73d159-1185-481c-af11-c0043929efe4'),
	(6,NULL,'Partners','partners','single',1,'partners',1,'2015-12-19 00:53:06','2015-12-19 00:53:06','352fab42-9fbe-4676-9b77-5c89c463a039'),
	(7,NULL,'Team','team','single',1,'team',1,'2015-12-19 00:54:13','2015-12-19 00:54:13','bb99b444-6c6d-4b25-9a0f-7ee60a948adb'),
	(8,NULL,'Testimonials','testimonials','single',1,'testimonials',1,'2015-12-19 00:54:52','2015-12-19 02:26:44','b40a3f83-5a09-412d-97a0-c96f6881f46e'),
	(9,NULL,'Reservation Success','reservationSuccess','single',1,'truck/_success',1,'2015-12-19 00:55:30','2015-12-19 00:55:30','fb32ec3b-f589-48a2-a279-ee874fa999d8'),
	(10,NULL,'Truck','truck','single',1,'truck',1,'2015-12-19 01:05:51','2015-12-19 01:05:51','5fc2f0d1-a580-405e-a76a-dda23d23d828'),
	(11,4,'Testimonial','testimonial','structure',0,NULL,1,'2015-12-19 02:24:29','2015-12-19 02:36:00','bfebb88f-10be-4277-b39d-f2495b5d001a'),
	(12,NULL,'Contact','contact','single',1,'contact',1,'2015-12-20 04:23:50','2015-12-20 04:23:50','cabab400-43b3-4425-8c57-116fd274be3a'),
	(13,NULL,'Ways To Sell','waysToSell','single',1,'ways-to-sell',1,'2015-12-20 04:39:21','2015-12-20 04:39:21','bc0c2ff5-310a-4734-bb96-53de679be8ed'),
	(14,NULL,'Buyers & Sellers','buyersSellers','single',1,'buyers-sellers',1,'2015-12-20 05:00:50','2015-12-20 05:00:50','dda9ff39-74b4-4509-a0d2-70f1556a6b6e'),
	(15,NULL,'About','about','single',1,'about',1,'2015-12-20 20:03:09','2015-12-20 20:03:09','e37c152e-1162-4482-b2c6-c7492ccc32ef'),
	(16,8,'Partner','partner','structure',0,NULL,1,'2015-12-28 01:06:31','2015-12-28 01:06:31','86813916-1a64-49e5-87fa-a6ee9eda54f5');

/*!40000 ALTER TABLE `sections` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sections_i18n
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sections_i18n`;

CREATE TABLE `sections_i18n` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sectionId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `enabledByDefault` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `urlFormat` text COLLATE utf8_unicode_ci,
  `nestedUrlFormat` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sections_i18n_sectionId_locale_unq_idx` (`sectionId`,`locale`),
  KEY `sections_i18n_locale_fk` (`locale`),
  CONSTRAINT `sections_i18n_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sections_i18n_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `sections_i18n` WRITE;
/*!40000 ALTER TABLE `sections_i18n` DISABLE KEYS */;

INSERT INTO `sections_i18n` (`id`, `sectionId`, `locale`, `enabledByDefault`, `urlFormat`, `nestedUrlFormat`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'en_us',1,'__home__',NULL,'2015-12-19 00:45:35','2015-12-19 00:45:35','5167aa15-b94f-4dca-8b06-702da5b0b454'),
	(2,2,'en_us',1,'news/{postDate.year}/{slug}',NULL,'2015-12-19 00:45:35','2015-12-19 00:45:35','037441d2-8558-4051-945e-9500b09cea02'),
	(3,3,'en_us',0,NULL,NULL,'2015-12-19 00:51:17','2015-12-19 00:51:17','ac2cf4a2-48c5-4fdb-8716-68d314ead2ea'),
	(4,4,'en_us',0,'properties/{slug}','{parent.uri}/{slug}','2015-12-19 00:51:40','2015-12-19 02:53:21','de995385-b829-4f42-87c5-b472b7f7466c'),
	(5,5,'en_us',0,'properties',NULL,'2015-12-19 00:52:08','2015-12-19 00:52:08','24fc049d-1d8c-4b55-8d2a-14fbb4208871'),
	(6,6,'en_us',0,'partners',NULL,'2015-12-19 00:53:06','2015-12-19 00:53:06','a3615636-4c64-4ca2-9ac0-0c6c687714c9'),
	(7,7,'en_us',0,'team',NULL,'2015-12-19 00:54:13','2015-12-19 00:54:13','a724981b-f928-4a06-b355-01e90002da7e'),
	(8,8,'en_us',0,'testimonials',NULL,'2015-12-19 00:54:52','2015-12-19 02:26:44','786fbb36-57b3-4bbf-9bb7-59fc58c2d417'),
	(9,9,'en_us',0,'truck/reserved',NULL,'2015-12-19 00:55:30','2015-12-19 00:55:30','460cf05e-dd3b-4d63-b70e-0b2a14744903'),
	(10,10,'en_us',0,'truck',NULL,'2015-12-19 01:05:51','2015-12-19 01:05:51','58fc884a-13fb-453a-84de-614f923fbece'),
	(11,11,'en_us',0,NULL,NULL,'2015-12-19 02:24:29','2015-12-19 02:24:29','4170f8af-2fcf-4b29-a339-b7aefecd719b'),
	(12,12,'en_us',0,'contact',NULL,'2015-12-20 04:23:50','2015-12-20 04:23:50','f43359ce-e556-45ef-93b2-deabc70a7035'),
	(13,13,'en_us',0,'ways-to-sell',NULL,'2015-12-20 04:39:21','2015-12-20 04:39:21','e063f83f-964b-4089-95e5-71df720741ba'),
	(14,14,'en_us',0,'buyers-and-sellers',NULL,'2015-12-20 05:00:50','2015-12-20 05:00:50','dcc3326e-1c5e-4480-87c7-eceacc88e21c'),
	(15,15,'en_us',0,'about',NULL,'2015-12-20 20:03:09','2015-12-20 20:03:09','2ec729bc-214a-4f5e-b250-0262935c917e'),
	(16,16,'en_us',0,NULL,NULL,'2015-12-28 01:06:31','2015-12-28 01:06:31','07c30822-e2fe-4acc-9a47-ab0e1842dd0f');

/*!40000 ALTER TABLE `sections_i18n` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sessions`;

CREATE TABLE `sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `token` char(100) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `sessions_uid_idx` (`uid`),
  KEY `sessions_token_idx` (`token`),
  KEY `sessions_dateUpdated_idx` (`dateUpdated`),
  KEY `sessions_userId_fk` (`userId`),
  CONSTRAINT `sessions_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;

INSERT INTO `sessions` (`id`, `userId`, `token`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'240dd64d8d8d2eb71109bb89c0a0ec36858a385bczozMjoiRW8wQTg3NjVGOVk3aFVqSVNPVFBtVlY3ZjFHWk9yM3oiOw==','2015-12-19 00:45:35','2015-12-19 00:45:35','5e9fd262-0735-4ca6-8269-fbd8e87478c2'),
	(2,1,'e6aeae021186724a1e8b9e0018504314fbecdc45czozMjoifmdMSzdQRjFxRjZhTEtBZXJBUHBDNFF3emszWUVmcHgiOw==','2015-12-19 14:57:18','2015-12-19 14:57:18','2ce114c4-780a-433d-862c-bcadf36e0832'),
	(3,1,'9ede0e28d74cdb95d6e9b532746a6f00403ee066czozMjoiTzYxQmVHWDhpN0xDNll6bTd3R05ZdTR4VndBOGNkWGgiOw==','2015-12-19 20:57:36','2015-12-19 20:57:36','15e20660-843e-4ee5-b789-976d69ece407'),
	(4,1,'4b52ac09e75b19584fd7075e862f1e802e918083czozMjoiQ3RuWUZxVTZ0UUNpMn5JVWRtdGl0MVJTR2w5R2Z1MWIiOw==','2015-12-20 04:07:32','2015-12-20 04:07:32','47138b1f-5a3e-4dfd-8eee-30dfd2807927'),
	(5,1,'fc1508ad9ce2a2d039d3e3f6f6af16db0d41e4cbczozMjoiTWlVcU53dGMzaXJHcDlzbWd6TFp1RHJMc1liOThFU0kiOw==','2015-12-20 14:32:28','2015-12-20 14:32:28','fedb406d-b643-40d3-90f2-40454cace395'),
	(6,1,'14b66e71923b33c3cfdc25dfd818dd76366f5bccczozMjoia2tvV3FqSlJkU3dVY1c2OVVidGlTX3lTNE1jdUxDOVYiOw==','2015-12-20 20:02:53','2015-12-20 20:02:53','f787a71b-6ea5-4aa9-b31f-9f2603b38450'),
	(7,1,'f264355fa37e027d81d569d8dcab95264ed00e8fczozMjoicW1XNjhmMkpJQnlxelM4Z0d1UDdST1p3ZlF4dVEwWDMiOw==','2015-12-21 02:18:24','2015-12-21 02:18:24','cf5ad6e2-d335-40e3-a7a8-f1e5074091d8'),
	(8,1,'0ae8ce425bdfd57e56b2d7757fcfda110cd8323dczozMjoiTWl3OXVHOVlyN01hYUVYSnZGRWk0b0p5dGFzakVZcGsiOw==','2015-12-21 20:40:22','2015-12-21 20:40:22','e7bf794f-e2de-4d10-b094-4d5a90a60a11'),
	(9,1,'1e21aba3063660611561070e95892eb8440bc04eczozMjoiNlV5U2d2aXB6U2Z4X35iWThFc0NNU0dEMnF3RFh1bkEiOw==','2015-12-22 22:55:48','2015-12-22 22:55:48','e6c4ed06-8583-4e48-9499-6702d2674fec'),
	(10,1,'348cbaa057055b4bce2ca914b7ed5e61029b2ae6czozMjoiWHBnVzdwaWlpVzNPQkxiRmN0a29VSkkzQjYzeFM3bzIiOw==','2015-12-28 00:47:45','2015-12-28 00:47:45','5acf5b88-7d2b-41b2-a6df-9cb1f84d1bcc');

/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table shunnedmessages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shunnedmessages`;

CREATE TABLE `shunnedmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `shunnedmessages_userId_message_unq_idx` (`userId`,`message`),
  CONSTRAINT `shunnedmessages_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table smartmap_addresses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `smartmap_addresses`;

CREATE TABLE `smartmap_addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `street1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` decimal(12,8) DEFAULT NULL,
  `lng` decimal(12,8) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `smartmap_addresses_elementId_fk` (`elementId`),
  KEY `smartmap_addresses_fieldId_fk` (`fieldId`),
  CONSTRAINT `smartmap_addresses_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `smartmap_addresses_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `smartmap_addresses` WRITE;
/*!40000 ALTER TABLE `smartmap_addresses` DISABLE KEYS */;

INSERT INTO `smartmap_addresses` (`id`, `elementId`, `fieldId`, `street1`, `street2`, `city`, `state`, `zip`, `country`, `lat`, `lng`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(2,17,6,'1210 NE 42nd St',NULL,'Ankeny','IA','50021','United States',41.76575850,-93.58654680,'2015-12-19 02:52:17','2015-12-28 01:29:55','6f1eeb0a-4d6c-4d3d-99ea-c5b83ac122cc'),
	(3,18,6,'7965 NW 142nd St',NULL,'Grimes','IA','50111','United States',41.70576500,-93.81369100,'2015-12-19 15:30:35','2015-12-28 01:30:06','cd688567-ad6d-43e4-99a2-d52a77dceb4d'),
	(4,19,6,'3602 NE Otter View Circle',NULL,'Ankeny','IA','50021','United States',41.76183000,-93.57202570,'2015-12-19 21:07:35','2015-12-20 04:19:06','c960dcfa-c56c-4401-b1a9-44200347a755');

/*!40000 ALTER TABLE `smartmap_addresses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table structureelements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `structureelements`;

CREATE TABLE `structureelements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `elementId` int(11) DEFAULT NULL,
  `root` int(11) unsigned DEFAULT NULL,
  `lft` int(11) unsigned NOT NULL,
  `rgt` int(11) unsigned NOT NULL,
  `level` smallint(6) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `structureelements_structureId_elementId_unq_idx` (`structureId`,`elementId`),
  KEY `structureelements_root_idx` (`root`),
  KEY `structureelements_lft_idx` (`lft`),
  KEY `structureelements_rgt_idx` (`rgt`),
  KEY `structureelements_level_idx` (`level`),
  KEY `structureelements_elementId_fk` (`elementId`),
  CONSTRAINT `structureelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `structureelements_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `structureelements` WRITE;
/*!40000 ALTER TABLE `structureelements` DISABLE KEYS */;

INSERT INTO `structureelements` (`id`, `structureId`, `elementId`, `root`, `lft`, `rgt`, `level`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,NULL,1,1,2,0,'2015-12-19 01:33:52','2015-12-19 01:33:52','5b432d0f-162c-4f13-8728-1896c92e3714'),
	(4,3,NULL,4,1,2,0,'2015-12-19 01:53:53','2015-12-19 01:53:53','de40f022-8974-442d-abe4-9bc24b66a676'),
	(6,4,NULL,6,1,8,0,'2015-12-19 02:36:00','2015-12-19 02:36:00','0754d762-e0dd-409b-872c-e73d7e7857d9'),
	(7,4,13,6,2,3,1,'2015-12-19 02:36:00','2015-12-19 02:36:00','0ac13231-e41c-4a49-8b84-f993b3815c22'),
	(8,4,14,6,4,5,1,'2015-12-19 02:36:00','2015-12-19 02:36:00','e82d24a8-8774-4b3a-bf3f-83b4e08d34fd'),
	(9,4,15,6,6,7,1,'2015-12-19 02:36:00','2015-12-19 02:36:00','ae541ca4-16b9-4499-8286-98b83a2e4b81'),
	(11,2,NULL,11,1,6,0,'2015-12-19 02:52:17','2015-12-19 02:52:17','4924e465-d94e-4560-ac61-753117cd0e85'),
	(12,2,17,11,2,3,1,'2015-12-19 02:52:17','2015-12-19 02:52:17','6d772b2f-b7a5-4ff6-bb3c-0df367546fc2'),
	(13,2,18,11,4,5,1,'2015-12-19 15:30:35','2015-12-19 15:30:35','a3bb2aae-1e30-40db-b6ef-0d71ac23f6fb'),
	(14,5,NULL,14,1,8,0,'2015-12-20 04:45:14','2015-12-20 04:45:14','f0bdd15b-5a53-435a-963f-f9ddb0580f88'),
	(15,5,23,14,2,3,1,'2015-12-20 04:45:14','2015-12-20 04:45:14','013b0960-eeb4-4f71-88b7-04dff7dbfefe'),
	(16,5,28,14,4,5,1,'2015-12-20 04:53:50','2015-12-20 04:53:50','2ab87442-c62f-40c4-9460-6316b73a8b0f'),
	(17,5,32,14,6,7,1,'2015-12-20 04:55:11','2015-12-20 04:55:11','ff06efb0-a85c-4540-a7bb-9595cda8cfb7'),
	(18,6,NULL,18,1,10,0,'2015-12-20 20:27:05','2015-12-20 20:27:05','9012d51b-1b54-4fc6-9644-b43d9e25d858'),
	(19,6,39,18,2,3,1,'2015-12-20 20:27:05','2015-12-20 20:27:05','917e7543-5674-4783-b396-dac0721d6647'),
	(20,6,40,18,4,5,1,'2015-12-20 20:30:17','2015-12-20 20:30:17','3d78a952-1727-44a8-a9d5-4a9dc83bcd63'),
	(21,6,41,18,6,7,1,'2015-12-20 20:31:04','2015-12-20 20:31:04','846a7fd7-e447-44c9-b968-db08e4ae4e4a'),
	(22,6,42,18,8,9,1,'2015-12-20 20:31:22','2015-12-20 20:31:22','79df1090-1c00-4e2a-b5c9-3b36b5fc552e'),
	(23,7,NULL,23,1,2,0,'2015-12-20 20:45:03','2015-12-20 20:45:03','c1f7188e-ecaf-4b9b-89b7-e47911d46b9f'),
	(36,8,NULL,36,1,4,0,'2015-12-28 01:08:39','2015-12-28 01:08:39','2f1f6687-8062-4214-92bf-bb15cb63bfc4'),
	(37,8,61,36,2,3,1,'2015-12-28 01:08:39','2015-12-28 01:08:39','607e84c4-572a-43fc-8a08-e75a942a6deb');

/*!40000 ALTER TABLE `structureelements` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table structures
# ------------------------------------------------------------

DROP TABLE IF EXISTS `structures`;

CREATE TABLE `structures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `maxLevels` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `structures` WRITE;
/*!40000 ALTER TABLE `structures` DISABLE KEYS */;

INSERT INTO `structures` (`id`, `maxLevels`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,NULL,'2015-12-19 00:51:17','2015-12-19 00:51:17','c564974b-cb8f-41b8-9960-a93b3d4b9b12'),
	(2,NULL,'2015-12-19 00:51:40','2015-12-19 02:53:21','a56f9635-5114-4341-bea1-c54496d94d94'),
	(3,NULL,'2015-12-19 01:53:33','2015-12-19 01:53:33','f525db9b-ebed-4fae-898d-6070aaadf7c2'),
	(4,1,'2015-12-19 02:36:00','2015-12-19 02:36:00','b56ecee2-c0fa-4106-90c2-b02f06eba389'),
	(5,NULL,'2015-12-20 04:40:58','2015-12-20 14:40:47','3b1c9aab-dbbc-4955-87a7-64ecabaf0e90'),
	(6,NULL,'2015-12-20 20:23:29','2015-12-20 20:39:56','77b3fc24-125d-40cf-894a-5fe54367b211'),
	(7,NULL,'2015-12-20 20:44:47','2015-12-20 20:56:30','a57ba134-73fc-4161-9825-d23b3b5e34ca'),
	(8,NULL,'2015-12-28 01:06:31','2015-12-28 01:06:31','43826c7c-8952-4b63-bd5c-23a1821caeac');

/*!40000 ALTER TABLE `structures` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table systemsettings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `systemsettings`;

CREATE TABLE `systemsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `systemsettings_category_unq_idx` (`category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `systemsettings` WRITE;
/*!40000 ALTER TABLE `systemsettings` DISABLE KEYS */;

INSERT INTO `systemsettings` (`id`, `category`, `settings`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'email','{\"protocol\":\"php\",\"emailAddress\":\"damonjentree@gmail.com\",\"senderName\":\"Mark Charter Realty\"}','2015-12-19 00:45:35','2015-12-19 00:45:35','2c381dec-f2e4-4a2a-88ae-ed4613297ee8');

/*!40000 ALTER TABLE `systemsettings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table taggroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `taggroups`;

CREATE TABLE `taggroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fieldLayoutId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `taggroups_name_unq_idx` (`name`),
  UNIQUE KEY `taggroups_handle_unq_idx` (`handle`),
  KEY `taggroups_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `taggroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `taggroups` WRITE;
/*!40000 ALTER TABLE `taggroups` DISABLE KEYS */;

INSERT INTO `taggroups` (`id`, `name`, `handle`, `fieldLayoutId`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Default','default',1,'2015-12-19 00:45:35','2015-12-19 00:45:35','bd2467df-480e-44b2-bad0-71cd5c8d7ff4');

/*!40000 ALTER TABLE `taggroups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tags`;

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `tags_groupId_fk` (`groupId`),
  CONSTRAINT `tags_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `taggroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tags_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table tasks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tasks`;

CREATE TABLE `tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `root` int(11) unsigned DEFAULT NULL,
  `lft` int(11) unsigned NOT NULL,
  `rgt` int(11) unsigned NOT NULL,
  `level` smallint(6) unsigned NOT NULL,
  `currentStep` int(11) unsigned DEFAULT NULL,
  `totalSteps` int(11) unsigned DEFAULT NULL,
  `status` enum('pending','error','running') COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `tasks_root_idx` (`root`),
  KEY `tasks_lft_idx` (`lft`),
  KEY `tasks_rgt_idx` (`rgt`),
  KEY `tasks_level_idx` (`level`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table templatecachecriteria
# ------------------------------------------------------------

DROP TABLE IF EXISTS `templatecachecriteria`;

CREATE TABLE `templatecachecriteria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cacheId` int(11) NOT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `criteria` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `templatecachecriteria_cacheId_fk` (`cacheId`),
  KEY `templatecachecriteria_type_idx` (`type`),
  CONSTRAINT `templatecachecriteria_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `templatecaches` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table templatecacheelements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `templatecacheelements`;

CREATE TABLE `templatecacheelements` (
  `cacheId` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  KEY `templatecacheelements_cacheId_fk` (`cacheId`),
  KEY `templatecacheelements_elementId_fk` (`elementId`),
  CONSTRAINT `templatecacheelements_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `templatecaches` (`id`) ON DELETE CASCADE,
  CONSTRAINT `templatecacheelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table templatecaches
# ------------------------------------------------------------

DROP TABLE IF EXISTS `templatecaches`;

CREATE TABLE `templatecaches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cacheKey` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `body` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `templatecaches_expiryDate_cacheKey_locale_path_idx` (`expiryDate`,`cacheKey`,`locale`,`path`),
  KEY `templatecaches_locale_fk` (`locale`),
  CONSTRAINT `templatecaches_locale_fk` FOREIGN KEY (`locale`) REFERENCES `locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tokens`;

CREATE TABLE `tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `route` text COLLATE utf8_unicode_ci,
  `usageLimit` tinyint(3) unsigned DEFAULT NULL,
  `usageCount` tinyint(3) unsigned DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tokens_token_unq_idx` (`token`),
  KEY `tokens_expiryDate_idx` (`expiryDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table usergroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `usergroups`;

CREATE TABLE `usergroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table usergroups_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `usergroups_users`;

CREATE TABLE `usergroups_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `usergroups_users_groupId_userId_unq_idx` (`groupId`,`userId`),
  KEY `usergroups_users_userId_fk` (`userId`),
  CONSTRAINT `usergroups_users_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `usergroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `usergroups_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table userpermissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `userpermissions`;

CREATE TABLE `userpermissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userpermissions_name_unq_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table userpermissions_usergroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `userpermissions_usergroups`;

CREATE TABLE `userpermissions_usergroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissionId` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userpermissions_usergroups_permissionId_groupId_unq_idx` (`permissionId`,`groupId`),
  KEY `userpermissions_usergroups_groupId_fk` (`groupId`),
  CONSTRAINT `userpermissions_usergroups_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `usergroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `userpermissions_usergroups_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `userpermissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table userpermissions_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `userpermissions_users`;

CREATE TABLE `userpermissions_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissionId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userpermissions_users_permissionId_userId_unq_idx` (`permissionId`,`userId`),
  KEY `userpermissions_users_userId_fk` (`userId`),
  CONSTRAINT `userpermissions_users_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `userpermissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `userpermissions_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `preferredLocale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weekStartDay` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `client` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `suspended` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pending` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `archived` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastLoginDate` datetime DEFAULT NULL,
  `lastLoginAttemptIPAddress` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invalidLoginWindowStart` datetime DEFAULT NULL,
  `invalidLoginCount` tinyint(4) unsigned DEFAULT NULL,
  `lastInvalidLoginDate` datetime DEFAULT NULL,
  `lockoutDate` datetime DEFAULT NULL,
  `verificationCode` char(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verificationCodeIssuedDate` datetime DEFAULT NULL,
  `unverifiedEmail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passwordResetRequired` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastPasswordChangeDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unq_idx` (`username`),
  UNIQUE KEY `users_email_unq_idx` (`email`),
  KEY `users_verificationCode_idx` (`verificationCode`),
  KEY `users_uid_idx` (`uid`),
  KEY `users_preferredLocale_fk` (`preferredLocale`),
  CONSTRAINT `users_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `users_preferredLocale_fk` FOREIGN KEY (`preferredLocale`) REFERENCES `locales` (`locale`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `username`, `photo`, `firstName`, `lastName`, `email`, `password`, `preferredLocale`, `weekStartDay`, `admin`, `client`, `locked`, `suspended`, `pending`, `archived`, `lastLoginDate`, `lastLoginAttemptIPAddress`, `invalidLoginWindowStart`, `invalidLoginCount`, `lastInvalidLoginDate`, `lockoutDate`, `verificationCode`, `verificationCodeIssuedDate`, `unverifiedEmail`, `passwordResetRequired`, `lastPasswordChangeDate`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'markcharter',NULL,'Mark','Charter','damonjentree@gmail.com','$2y$13$GJJzL9T7Tf0TK0x.EolIG.ovL6ewvOhqUU1l7gYgamPgHhRpUmNjK',NULL,0,1,0,0,0,0,0,'2015-12-28 00:47:45','127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2015-12-19 00:45:33','2015-12-19 00:45:33','2015-12-28 00:48:24','81f6b86e-53ef-42cd-9e94-70ae234fbf0f');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table widgets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `widgets`;

CREATE TABLE `widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` tinyint(4) DEFAULT NULL,
  `colspan` tinyint(4) unsigned DEFAULT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `widgets_userId_fk` (`userId`),
  CONSTRAINT `widgets_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `widgets` WRITE;
/*!40000 ALTER TABLE `widgets` DISABLE KEYS */;

INSERT INTO `widgets` (`id`, `userId`, `type`, `sortOrder`, `colspan`, `settings`, `enabled`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'RecentEntries',1,NULL,NULL,1,'2015-12-19 00:45:42','2015-12-19 00:45:42','e72ff14a-a709-4d8a-a753-fb739baad71f'),
	(2,1,'GetHelp',2,NULL,NULL,1,'2015-12-19 00:45:42','2015-12-19 00:45:42','8324332c-891f-4293-bba3-23d4860e9656'),
	(3,1,'Updates',3,NULL,NULL,1,'2015-12-19 00:45:42','2015-12-19 00:45:42','e6b92389-4a92-4499-b318-f60b003387bf'),
	(4,1,'Feed',4,NULL,'{\"url\":\"https:\\/\\/craftcms.com\\/news.rss\",\"title\":\"Craft News\"}',1,'2015-12-19 00:45:42','2015-12-19 00:45:42','10dd0609-25ef-4d6c-a7f3-b0d882d962ee');

/*!40000 ALTER TABLE `widgets` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
